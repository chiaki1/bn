const execa = require('execa');
const chalk = require('chalk');

const packages = [
  '@vjcspy/chitility',
  '@vjcspy/apollo',

  '@vjcspy/apollo-bed-kingdom',
  // '@vjcspy/apollo-sale-off',
  '@vjcspy/web-ui-extension',
  '@vjcspy/r',
  '@vjcspy/web-domain',
  '@vjcspy/web-store',
  '@vjcspy/web-url-rewrite',

  '@vjcspy/web-image',
  '@vjcspy/web-r',
  '@vjcspy/web-i18n',
  '@vjcspy/web-apollo',

  // '@vjcspy/web-toastify',

  '@vjcspy/web-mui',
];

const build = async () => {
  for (let i = 0; i < packages.length; i++) {
    console.info(`>> start build module ${chalk.yellow(packages[i])}`);
    const { stderr, stdout } = await execa('yarn', [
      'workspace',
      packages[i],
      'build',
    ]);

    if (stderr) {
      console.log(stderr);
    }

    if (stdout) {
      console.log(stdout);
    }
  }
};

build();
