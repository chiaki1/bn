import { AsyncPersistent } from '@util/async-persistent';

export const AccountPersistent = new AsyncPersistent(
  undefined,
  'chiaki_account'
);
