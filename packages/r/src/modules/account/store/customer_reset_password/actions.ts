import { generateAction } from '@util/createAction';

const PREFIX = 'RESET_PASSWORD';

const requestResetPassword = generateAction<{}, { status: boolean }>(
  'REQUEST_PASSWORD_RESET',
  PREFIX
);
export const requestPasswordResetAction = requestResetPassword.ACTION;
export const requestPasswordResetAfterAction = requestResetPassword.AFTER;
export const requestPasswordResetFailAction = requestResetPassword.ERROR;

const resetPassword = generateAction<{}, { status: boolean }>(
  'RESET_PASSWORD',
  PREFIX
);
export const resetPasswordAction = resetPassword.ACTION;
export const resetPasswordAfterAction = resetPassword.AFTER;
export const resetPasswordFailAction = resetPassword.ERROR;
