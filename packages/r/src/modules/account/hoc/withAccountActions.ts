import { useAccountActions } from '@modules/account/hook/useAccountActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withAccountActions = createUiHOC(() => {
  return useAccountActions();
}, 'withAccountActions');
