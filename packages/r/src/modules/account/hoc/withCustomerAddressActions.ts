import { useCustomerAddActions } from '@modules/account/hook/useCustomerAddActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withCustomerAddressActions = createUiHOC(
  () => useCustomerAddActions(),
  'withCustomerAddressActions'
);
