import { useCustomerInfoActions } from '@modules/account/hook/useCustomerInfoActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withUpdateCustomerInfoActions = createUiHOC(
  () => useCustomerInfoActions(),
  'withUpdateCustomerInfoActions'
);
