import { selectCustomer } from '@modules/account/store/account.selector';
import { selectIsUpdatingCustomerInfo } from '@modules/account/store/customer-info/customer-info.selector';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useSelector } from 'react-redux';

export const withCustomer = createUiHOC(() => {
  const customer = useSelector(selectCustomer);
  const isUpdatingCustomerInfo = useSelector(selectIsUpdatingCustomerInfo);
  return {
    state: {
      customer,
      isUpdatingCustomerInfo,
    },
  };
}, 'withCustomer');
