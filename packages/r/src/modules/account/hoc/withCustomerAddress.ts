import { useCustomerAddress } from '@modules/account/hook/useCustomerAddress';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withCustomerAddress = createUiHOC(
  () => useCustomerAddress(),
  'withCustomerAddress'
);
