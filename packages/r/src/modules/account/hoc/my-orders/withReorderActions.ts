import { useReorderActions } from '@modules/account/hook/my-orders/useReorderActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withReorderActions = createUiHOC(() => {
  return useReorderActions();
}, 'withReorderActions');
