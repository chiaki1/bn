import { selectCustomerAddress } from '@modules/account/store/account.selector';
import { useSelector } from 'react-redux';

import {
  selectDefaultShippingAddressId,
  selectDeletingAddressId,
} from '../store/customer-address/selector';

export const useCustomerAddress = () => {
  const customerAddress = useSelector(selectCustomerAddress);
  const deletingAddressId = useSelector(selectDeletingAddressId);
  const defaultAddressId = useSelector(selectDefaultShippingAddressId);

  return {
    state: {
      customerAddress,
      deletingAddressId,
      defaultAddressId,
    },
  };
};
