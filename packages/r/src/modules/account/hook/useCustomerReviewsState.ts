import { selectCustomerReviews } from '@modules/account/store/account.selector';
import { useSelector } from 'react-redux';

export const useCustomerReviews = () => {
  const reviews = useSelector(selectCustomerReviews);

  return {
    state: {
      reviews,
    },
  };
};
