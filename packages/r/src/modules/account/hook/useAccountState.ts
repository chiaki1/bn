import { selectAccount } from '@modules/account/store/account.selector';
import { useSelector } from 'react-redux';

export const useAccountState = () => {
  const accountState = useSelector(selectAccount);

  return {
    state: {
      accountState,
    },
  };
};
