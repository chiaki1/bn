import { selectRewardPoints } from '@modules/account/store/account.selector';
import { useSelector } from 'react-redux';

export const useRewardPoints = () => {
  const reward_points = useSelector(selectRewardPoints);

  return {
    state: {
      reward_points,
    },
  };
};
