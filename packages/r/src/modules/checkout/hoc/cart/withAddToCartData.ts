import { useAddToCartData } from '@modules/checkout/hook/cart/useAddToCartData';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withAddToCartData = createUiHOC(
  () => useAddToCartData(),
  'withAddToCartData'
);
