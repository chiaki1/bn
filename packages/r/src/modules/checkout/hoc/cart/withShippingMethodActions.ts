import { useShippingMethodActions } from '@modules/checkout/hook/cart/useShippingMethodActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withShippingMethodActions = createUiHOC(
  () => useShippingMethodActions(),
  'withShippingMethodActions'
);
