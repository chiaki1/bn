import { useCheckoutCartData } from '@modules/checkout/hook/cart/useCheckoutCartData';
import { selectIsResolvedCart } from '@modules/checkout/store/cart/cart.selector';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useSelector } from 'react-redux';

export const withCheckoutCartData = createUiHOC(() => {
  const isResolvedCart = useSelector(selectIsResolvedCart);
  const { state, actions } = useCheckoutCartData();
  return {
    state: {
      isResolvedCart,
      ...state,
    },
    actions: {
      ...actions,
    },
  };
}, 'withCheckoutCartData');
