import { useIsResolvedCart } from '@modules/checkout/hook/cart/useIsResolvedCart';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withIsResolvedCart = createUiHOC(
  () => useIsResolvedCart(),
  'withIsResolvedCart'
);
