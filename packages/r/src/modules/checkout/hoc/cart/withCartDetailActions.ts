import { useCartDetailActions } from '@modules/checkout/hook/cart/useCartDetailActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withCartDetailActions = createUiHOC(() => {
  return useCartDetailActions();
}, 'withCartDetailActions');
