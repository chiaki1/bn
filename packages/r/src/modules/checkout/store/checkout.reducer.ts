import { cartReducer } from '@modules/checkout/store/cart/cart.reducer';
import { combineReducers } from '@reduxjs/toolkit';

export const checkoutReducer = combineReducers({
  cart: cartReducer,
});
