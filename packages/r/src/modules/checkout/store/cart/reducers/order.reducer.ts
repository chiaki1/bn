import {
  placeOrder,
  placeOrderAfter,
  placeOrderError,
} from '@modules/checkout/store/cart/actions/order.actions';
import { CartState } from '@modules/checkout/store/cart/cart.state';
import { createBuilderCallback } from '@util/createBuilderCallback';

export const orderReducer = createBuilderCallback<CartState>((builder) => {
  builder
    .addCase(placeOrder, (state) => {
      state.isPlacingOrder = true;
    })
    .addCase(placeOrderAfter, (state, action) => {
      state.isPlacingOrder = false;
      state.completeOrderNumber = action.payload.order['order_number'];
    })
    .addCase(placeOrderError, (state) => {
      state.isPlacingOrder = false;
    });
});
