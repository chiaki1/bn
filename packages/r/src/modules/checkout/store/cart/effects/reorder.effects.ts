import { graphqlFetchForCustomer } from '@modules/account/util/graphqlFetchForCustomer';
import { appRunTimeError } from '@modules/app/store/app.actions';
import {
  reorderCartAction,
  reorderCartAfterAction,
  reorderCartErrorAction,
} from '@modules/checkout/store/cart/actions/reorder.actions';
import CartDetail from '@modules/graphql/schema/CartDetail';
import { createEffect } from '@util/createEffect';
import { ofType } from '@util/ofType';
import { RuntimeError } from '@vjcspy/chitility/build/util/general';
import { of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { catchError, map, switchMap } from 'rxjs/operators';

const whenReorderCart$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(reorderCartAction),
    switchMap((action) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `mutation reorderItems($orderNumber: String!) {
    reorderItems(orderNumber: $orderNumber) {
       cart{
        ${CartDetail.r('query')}
        }
    }
}
`,
          variables: {
            orderNumber: action.payload.orderNumber,
          },
        })
      ).pipe(
        map((data) => {
          if (data && data?.reorderItems?.cart) {
            return reorderCartAfterAction({
              cart: data?.reorderItems?.cart,
            });
          } else {
            return appRunTimeError({
              error: new RuntimeError('Wrong data format when get cart detail'),
            });
          }
        }),
        catchError((error: any) => {
          return of(
            reorderCartErrorAction({
              error,
            })
          );
        })
      )
    )
  )
);

export const CHECKOUT_REORDER_EFFECTS = [whenReorderCart$];
