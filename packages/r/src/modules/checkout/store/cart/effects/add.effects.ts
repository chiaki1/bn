import { addProductsToCartAfter } from '@modules/checkout/store/cart/actions/add.actions';
import { getCartDetail } from '@modules/checkout/store/cart/actions/init.actions';
import { createEffect } from '@util/createEffect';
import { ofType } from '@util/ofType';
import { map } from 'rxjs/operators';

const beforeGetCartDetail$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCartAfter),
    map((action) =>
      getCartDetail({
        cartId: action.payload.cartId,
      })
    )
  )
);

export const CHECKOUT_CART_ADD_EFFECTS = [beforeGetCartDetail$];
