import { AsyncPersistent } from '@util/async-persistent';

export const CheckoutPersistent = new AsyncPersistent(
  undefined,
  'chiaki_checkout'
);
