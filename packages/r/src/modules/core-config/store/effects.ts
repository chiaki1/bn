import { createEffect } from '@util/createEffect';
import { ofType } from '@util/ofType';
import { proxyFetch } from '@util/proxy-fetch';
import { of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
  getCheckoutConfig,
  getCheckoutConfigAfter,
  getCheckoutConfigError,
} from './actions';

const getCheckoutConfig$ = createEffect((action$) =>
  action$.pipe(
    ofType(getCheckoutConfig),
    switchMap(() =>
      fromPromise(
        proxyFetch({
          type: 'get-checkout-config',
        })
      ).pipe(
        map((res: any) => {
          return getCheckoutConfigAfter({ checkout: res });
        }),
        catchError((error) => of(getCheckoutConfigError({ error })))
      )
    )
  )
);

export const R_CONFIG_EFFECTS = [getCheckoutConfig$];
