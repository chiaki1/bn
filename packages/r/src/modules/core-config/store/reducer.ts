import { createReducer } from '@reduxjs/toolkit';

import { getCheckoutConfigAfter } from './actions';
import { CoreConfigFactory } from './state';

export const configReducer = createReducer(CoreConfigFactory(), (builder) => {
  builder.addCase(getCheckoutConfigAfter, (state, action) => {
    state.checkout = action.payload.checkout;
  });
});
