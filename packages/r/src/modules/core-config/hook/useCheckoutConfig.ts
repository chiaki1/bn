import { useSelector } from 'react-redux';

export const useCheckoutConfig = () => {
  const checkoutConfig = useSelector((state: any) => state.coreConfig.checkout);

  return { state: { checkoutConfig } };
};
