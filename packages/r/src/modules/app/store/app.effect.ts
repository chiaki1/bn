import {
  appCheckReferUrl,
  appHasReferUrl,
  increaseCount,
} from '@modules/app/store/app.actions';
import { createEffect } from '@util/createEffect';
import { ofType } from '@util/ofType';
import { EMPTY } from 'rxjs';
import { map, mapTo, tap, withLatestFrom } from 'rxjs/operators';

const whenIncrease$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(increaseCount),
    withLatestFrom(state$),
    tap(([action, state]) =>
      console.log('Run effect: ' + action.type, state.app)
    ),
    mapTo({ type: 'AFTER_INDECREASE_COUNT' })
  )
);

const whenCheckUrlRefer$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(appCheckReferUrl),
    withLatestFrom(state$, (v1, v2) => [v1, v2.app.referUrl]),
    map((data) => {
      if (data[1]) {
        return appHasReferUrl({
          url: data[1],
        });
      }
      return EMPTY;
    })
  )
);

export const APP_EFFECTS = [whenCheckUrlRefer$];
