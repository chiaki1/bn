import { appShowErrorMessages } from '@modules/app/store/app.actions';
import { PayloadActionCreator } from '@reduxjs/toolkit';
import { createEffect } from '@util/createEffect';
import { ofType } from '@util/ofType';
import { EMPTY } from 'rxjs';
import { map } from 'rxjs/operators';

export const createEffectHandleErrorMessage = (
  ...allowedTypes: Array<string | PayloadActionCreator<any>>
) => {
  return createEffect((action$) =>
    action$.pipe(
      ofType(...allowedTypes),
      map((action: any) => {
        if (
          action['payload'] &&
          action.payload['error'] &&
          typeof action.payload.error['message'] === 'string'
        ) {
          return appShowErrorMessages({
            messages: action.payload.error['message'],
          });
        } else {
          return EMPTY;
        }
      })
    )
  );
};
