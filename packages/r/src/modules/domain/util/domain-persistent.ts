import { AsyncPersistent } from '@util/async-persistent';

export const DomainPersistent = new AsyncPersistent(undefined, 'chiaki_domain');
