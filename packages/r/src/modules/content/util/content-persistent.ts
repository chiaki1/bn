import { AsyncPersistent } from '@util/async-persistent';

export const ContentPersistent = new AsyncPersistent(
  undefined,
  'chiaki_content'
);
