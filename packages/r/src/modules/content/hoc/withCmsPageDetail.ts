import { selectCmsPageDetail } from '@modules/content/store/selectors';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useSelector } from 'react-redux';

export const withCmsPageDetail = createUiHOC(() => {
  const selectDetail = useSelector(selectCmsPageDetail);
  return { state: { selectDetail } };
}, 'withCmsPageDetail');
