import { useContentAddressActions } from '@modules/content/hook/useContentAddressActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withContentAddressActions = createUiHOC(
  () => useContentAddressActions(),
  'withContentAddressActions'
);
