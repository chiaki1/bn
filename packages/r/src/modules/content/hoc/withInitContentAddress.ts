import { getContentAddressDataAction } from '@modules/content/store/address/actions';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

export const withInitContentAddress = createUiHOC((props) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getContentAddressDataAction());
  });

  return {};
}, 'withInitContentAddress');
