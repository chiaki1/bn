import { useUiConfigData } from '@modules/content/hook/useUiConfigData';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withUiConfigData = createUiHOC(
  () => useUiConfigData(),
  'withUiConfigData'
);
