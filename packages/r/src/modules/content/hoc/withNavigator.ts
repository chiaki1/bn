import { useNavigator } from '@modules/content/hook/useNavigator';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withNavigator = createUiHOC(() => useNavigator(), 'withNavigator');
