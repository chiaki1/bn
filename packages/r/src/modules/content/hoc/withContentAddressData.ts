import { useContentAddressData } from '@modules/content/hook/useContentAddressData';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withContentAddressData = createUiHOC(
  () => useContentAddressData(),
  'withContentAddressData'
);
