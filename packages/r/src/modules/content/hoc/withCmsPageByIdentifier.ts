import { useCmsPageByIdentifier } from '@modules/content/hook/useCmsPageByIdentifier';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withCmsPageByIdentifier = createUiHOC((props) => {
  return useCmsPageByIdentifier();
}, 'withCmsPageByIdentifier');
