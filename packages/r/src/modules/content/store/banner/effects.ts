import { createEffect } from '@util/createEffect';
import { graphqlFetch } from '@util/graphql-fetch';
import { ofType } from '@util/ofType';
import { of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
  getBestSeller,
  getBestSellerAfter,
  getBestSellerError,
} from './actions';

const whenGetBestSeller$ = createEffect((action$) =>
  action$.pipe(
    ofType(getBestSeller),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `query bestSellerProduct{
              bestSellerProduct(
                pageSize: 2
                currentPage: 1
              ) {
                items{
                  url_key
                  image{
                    url
                    label
                  }
                }
              }
            }`,
        })
      ).pipe(
        map((data) => {
          return getBestSellerAfter({ data });
        }),
        catchError((error) => of(getBestSellerError({ error })))
      )
    )
  )
);

export const R_CONTENT_BANNER_EFFECTS = [whenGetBestSeller$];
