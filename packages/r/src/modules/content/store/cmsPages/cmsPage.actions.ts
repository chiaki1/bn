import { generateAction } from '@util/createAction';

const PREFIX = 'CONTENT_CMS_PAGES';
const getCmsPages = generateAction<{}, { cmsPages: any[] }>(
  'GET_CMS_PAGES',
  PREFIX
);

export const getCmsPageAction = getCmsPages.ACTION;
export const getCmsPageAfterAction = getCmsPages.AFTER;
export const getCmsPageErrorAction = getCmsPages.ERROR;
