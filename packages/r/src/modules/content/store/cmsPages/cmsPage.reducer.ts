import { createBuilderCallback } from '@util/createBuilderCallback';

import { ContentState } from '../state';
import { getCmsPageAfterAction } from './cmsPage.actions';

export const cmsPageReducerCallback = createBuilderCallback<ContentState>(
  (builder) => {
    builder.addCase(getCmsPageAfterAction, (state, action) => {
      state.cmsPages = action.payload.cmsPages;
    });
  }
);
