import { ContentState } from '@modules/content/store/state';
import { createSelector } from '@reduxjs/toolkit';
import memoize from 'lodash/memoize';

export const selectBanners = (state: { content: ContentState }) =>
  state.content.banners;

export const selectHomeBrand = (state: { content: ContentState }) =>
  state.content.homeBrand;

export const selectBrandCampaign = (state: { content: ContentState }) =>
  state.content.brandCampaign;

export const selectBestSeller = (state: { content: ContentState }) =>
  state.content.bestSeller;

export const selectCmsPages = (state: { content: ContentState }) =>
  state.content.cmsPages;

export const selectCmsPageDetail = createSelector(
  (state: { content: ContentState }) => state.content?.cmsPages,
  (cmsPages: any[]) =>
    memoize((identifier: string) =>
      cmsPages.find((p) => p.identifier === identifier)
    )
);
