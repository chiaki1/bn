import { getContentAddressDataAfterAction } from '@modules/content/store/address/actions';
import { ContentState } from '@modules/content/store/state';
import { createBuilderCallback } from '@util/createBuilderCallback';

export const addressBuilderCallback = createBuilderCallback<ContentState>(
  (builder) => {
    builder.addCase(getContentAddressDataAfterAction, (state) => {
      state.isLoadedAddressData = true;
    });
  }
);
