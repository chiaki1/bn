import { AsyncPersistent } from '@util/async-persistent';

export const CatalogPersistent = new AsyncPersistent(
  undefined,
  'chiaki_catalog'
);
