import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve('R_CATALOG', {
  DEFAULT_CATEGORY_PAGE_SIZE: 10,
  PRODUCTS_PAGE_SIZE: 20,
});
