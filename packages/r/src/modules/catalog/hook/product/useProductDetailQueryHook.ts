import { useProductDetailByUrlKeyQuery } from '@vjcspy/apollo';

export const useProductDetailQueryHook = (options: any) => {
  return useProductDetailByUrlKeyQuery(options);
};
