import { selectProductOptions } from '@modules/catalog/store/product/product.selectors';
import { useSelector } from 'react-redux';

export const useProductCustomizableOptionsData = () => {
  const customizableOptions = useSelector(selectProductOptions);
  return { state: { customizableOptions } };
};
