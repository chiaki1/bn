import { gotProductData } from '@modules/catalog/store/product/product.actions';
import { useUrlRewriteContext } from '@modules/router/context/url-rewrite';
import { ProductDetailByUrlKeyQueryHookResult } from '@vjcspy/apollo';
import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { getUrlKey } from '@vjcspy/chitility/build/util/getUrlKey';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';

export const useProductData = (
  queryHook: (options: any) => ProductDetailByUrlKeyQueryHookResult
) => {
  const urlRewriteContextValue = useUrlRewriteContext();
  const urlKey = useMemo(() => {
    if (urlRewriteContextValue.urlRewriteData?.pathname) {
      return getUrlKey(urlRewriteContextValue.urlRewriteData?.pathname);
    }
    return null;
  }, [urlRewriteContextValue.urlRewriteData?.pathname]);

  if (!urlKey) {
    logger.error('could not resolve urlKey for product detail page');
  }

  const productQuery = queryHook({
    variables: {
      urlKey,
    },
    fetchPolicy: FetchPolicyResolve.CACHE_AND_NETWORK,
    nextFetchPolicy: 'cache-first',
  });

  const dispatch = useDispatch();

  useEffect(() => {
    if (
      Array.isArray(productQuery.data?.products?.items) &&
      productQuery.data!.products!.items!.length > 0
    ) {
      dispatch(
        gotProductData({
          product: productQuery!.data!.products!.items[0],
        })
      );
    }

    return () => {
      dispatch(
        gotProductData({
          product: undefined,
        })
      );
    };
  }, [productQuery.data]);

  useEffect(() => {
    if (productQuery.error) {
      logger.error('get product detail data error');
    }
  }, [productQuery.error]);

  return {
    product: productQuery.data?.products?.items![0],
  };
};
