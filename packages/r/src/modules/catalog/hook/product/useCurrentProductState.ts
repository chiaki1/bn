import { selectProduct } from '@modules/catalog/store/product/product.selectors';
import { selectProductInfo } from '@modules/catalog/store/product-info/product-info.selectors';
import { useSelector } from 'react-redux';

export const useCurrentProductState = () => {
  const product = useSelector(selectProduct);
  const productInfo = useSelector(selectProductInfo)(product);

  return {
    state: {
      product,
      productInfo,
    },
  };
};
