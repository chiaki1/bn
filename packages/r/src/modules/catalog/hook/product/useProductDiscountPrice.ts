import { productPriceHasDiscount } from '@modules/catalog/util/productPriceHasDiscount';
import { ProductInterface } from '@vjcspy/apollo';
import _ from 'lodash';
import { useMemo } from 'react';

export const useProductDiscountPrice = (props: {
  product: ProductInterface;
}) => {
  const { isShowDiscount, discountPrice } = useMemo(() => {
    let tierPrice: any = null;

    if (_.size(props?.product?.price_tiers) > 0) {
      tierPrice = _.first(props?.product?.price_tiers);
    }

    const hasDiscount = productPriceHasDiscount(props?.product?.price_range);

    let discountPrice = null;
    if (hasDiscount) {
      if (
        !isNaN(tierPrice?.final_price?.value) &&
        props.product?.price_range?.minimum_price?.final_price?.value &&
        tierPrice?.final_price?.value <
          props.product?.price_range?.minimum_price?.final_price?.value
      ) {
        discountPrice = tierPrice;
      } else {
        discountPrice = props.product?.price_range?.minimum_price;
      }
    }

    return {
      isShowDiscount: hasDiscount,
      discountPrice,
    };
  }, [props.product?.price_range]);

  return { isShowDiscount, discountPrice };
};
