import {
  ProductAttributeFilterInput,
  ProductAttributeSortInput,
  useGetCatalogProductsQuery,
} from '@vjcspy/apollo';
import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';

export const useProductsQuery = (
  search: string,
  filter: ProductAttributeFilterInput,
  currentPage = 1,
  pageSize = 20,
  sort?: ProductAttributeSortInput
) => {
  return useGetCatalogProductsQuery({
    variables: {
      search,
      filter,
      sort,
      currentPage,
      pageSize,
    },
    fetchPolicy: FetchPolicyResolve.CACHE_AND_NETWORK,
  });
};
