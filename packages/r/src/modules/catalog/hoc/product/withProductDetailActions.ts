import { useProductDetailActions } from '@modules/catalog/hook/product/useProductDetailActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withProductDetailActions = createUiHOC(
  () => useProductDetailActions(),
  'withProductDetailActions'
);
