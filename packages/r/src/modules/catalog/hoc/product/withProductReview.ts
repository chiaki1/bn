import { useProductReviews } from '@modules/catalog/hook/product/useProductReviews';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withProductReview = createUiHOC(() => {
  return useProductReviews();
}, 'withProductReview');
