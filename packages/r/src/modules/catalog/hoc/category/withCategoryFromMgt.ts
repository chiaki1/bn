import { useCategoryFromMgt } from '@modules/catalog/hook/category/useCategoryFromMgt';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withCategoryFromMgt = createUiHOC(
  (props) => useCategoryFromMgt(props),
  'withCategoryFromMgt'
);
