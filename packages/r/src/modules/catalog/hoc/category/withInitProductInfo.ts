import { initProductInfo } from '@modules/catalog/store/product-info/product-info.actions';
import { selectProductInfo } from '@modules/catalog/store/product-info/product-info.selectors';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

export const withInitProductInfo = createUiHOC((props: any) => {
  const productInfo = useSelector(selectProductInfo)(props?.product?.id);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!productInfo && props?.product?.id) {
      dispatch(initProductInfo({ product: props.product }));
    }
  }, [productInfo, props?.product?.id]);

  return {};
}, 'withInitProductInfo');
