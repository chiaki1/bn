import { useCategoryList } from '@modules/catalog/hook/category/useCategoryList';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withCategoryList = createUiHOC(
  (props) => useCategoryList(props),
  'withCategoryList'
);
