import { selectRangePriceFilter } from '@modules/catalog/store/products/products.selectors';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useSelector } from 'react-redux';

export const withRangPriceFilterData = createUiHOC((props) => {
  const rangePriceFilters = useSelector(selectRangePriceFilter);
  return {
    rangePriceFilters,
  };
}, 'withRangPriceFilterData');
