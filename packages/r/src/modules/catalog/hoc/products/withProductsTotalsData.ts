import { selectProductTotals } from '@modules/catalog/store/products/products.selectors';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useSelector } from 'react-redux';

export const withProductsTotalsData = createUiHOC(() => {
  const productTotals = useSelector(selectProductTotals);

  return { productTotals };
}, 'withProductsTotalsData');
