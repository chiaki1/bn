import { useProductsState } from '@modules/catalog/hook/products/useProductsState';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withProductsState = createUiHOC(() => {
  return useProductsState();
}, 'withProductsState');
