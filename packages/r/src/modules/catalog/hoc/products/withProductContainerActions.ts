import { useProductContainerActions } from '@modules/catalog/hook/products/useProductContainerActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withProductContainerActions = createUiHOC(
  () => useProductContainerActions(),
  'withProductContainerActions'
);
