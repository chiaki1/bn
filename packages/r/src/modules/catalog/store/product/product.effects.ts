import {
  createProductRatingAction,
  createProductRatingAfter,
  createProductRatingError,
} from '@modules/catalog/store/product/product.actions';
import { createEffect } from '@util/createEffect';
import { graphqlFetch } from '@util/graphql-fetch';
import { ofType } from '@util/ofType';
import { of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import {
  catchError,
  filter,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';

const createProductRating$ = createEffect((action$, state$: any) =>
  action$.pipe(
    ofType(createProductRatingAction),
    withLatestFrom(state$, (v1, v2: any) => [v1, v2.product.entity]),
    filter(([ratingData, product]) => product && product['sku']),
    switchMap(([ratingData, product]) => {
      console.log(ratingData);
      return fromPromise(
        graphqlFetch({
          query: `
        mutation createProductReview($nickname: String!,$summary : String!, $text: String!,$sku:String!, $ratingId: String!, $ratingValue: String!){
    createProductReview(input: {
        nickname: $nickname,
        ratings:{
            id: $ratingId,
            value_id: $ratingValue
        }
        sku: $sku,
        summary: $summary,
        text: $text
    }){
        __typename
    }
}
        `,
          variables: {
            nickname: ratingData.payload['nickname'],
            summary: ratingData.payload['summary'],
            text: ratingData.payload['text'],
            sku: product['sku'],
            ratingId: ratingData.payload['ratingInfo']['id'],
            ratingValue: ratingData.payload['ratingInfo']['value'],
          },
        })
      ).pipe(
        map((res) => {
          console.log(res);
          return createProductRatingAfter({});
        }),
        catchError((error) => of(createProductRatingError({ error })))
      );
    })
  )
);

export const R_CATALOG_EFFECTS = [createProductRating$];
