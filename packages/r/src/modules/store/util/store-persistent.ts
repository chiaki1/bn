import { AsyncPersistent } from '@util/async-persistent';

export const StorePersistent = new AsyncPersistent(undefined, 'chiaki_store');
