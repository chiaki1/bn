import { R_DEFAULT_VALUE } from '@values/R_DEFAULT_VALUE';
import { RuntimeError } from '@vjcspy/chitility/build/util/general';
import { proxyRequest } from '@vjcspy/chitility/build/util/proxy-request';
import { Registry } from '@vjcspy/chitility/build/util/registry';

export const proxyFetch = async (action: {
  type: string;
  payload?: any;
}): Promise<any> => {
  const proxyEndpoint = Registry.getInstance().registry(
    R_DEFAULT_VALUE.PROXY_DEFAULT_URL_KEY
  );
  if (typeof proxyEndpoint !== 'string') {
    throw new RuntimeError('Please define PROXY_DEFAULT_END_POINT in registry');
  }

  return proxyRequest(proxyEndpoint, action);
};
