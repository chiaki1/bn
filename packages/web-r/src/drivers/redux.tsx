import { configureStore } from '@reduxjs/toolkit';
import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { NextPage } from 'next';
import React, { useMemo } from 'react';
import { Provider as ReduxProvider } from 'react-redux';

import { storeManager } from '../store';

export const withRedux = (
  Page: NextPage,
  webUiAdapterOptions?: WebUiAdapterOptions
) => {
  // Create a store with the root reducer function being the one exposed by the manager.
  const store = configureStore({
    reducer: storeManager.reduce,
    middleware: storeManager.middleware,
    devTools:
      process?.env?.NODE_ENV == 'production' ||
      process?.env?.NEXT_PUBLIC_NODE_ENV == 'production'
        ? false
        : {
            maxAge: 50,
            trace: true,
            traceLimit: 10,
          },
  });
  storeManager.runEpic();

  // Optional: Put the reducer manager on the store so it is easily accessible
  // @ts-ignore
  store.storeManager = storeManager;

  const WithRedux: NextPage = React.memo((props) => {
    logger.render('WithRedux');
    const storeContextValue = useMemo(() => store, []);
    return (
      <ReduxProvider store={storeContextValue}>
        <Page {...props} />
      </ReduxProvider>
    );
  });

  wrapSSRFn(Page, WithRedux, undefined, undefined, webUiAdapterOptions?.ssr);

  const displayName = Page.displayName || Page.name || 'PageComponent';
  WithRedux.displayName = `withRedux(${displayName})`;

  return WithRedux;
};
