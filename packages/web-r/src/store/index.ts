import { createReducer } from '@reduxjs/toolkit';
import { createStoreManager } from '@vjcspy/r/build/util/createStoreManager';

export const storeManager = createStoreManager(
  {
    empty: createReducer({}, () => {}),
  },
  []
);
export const middleware = () => [];
