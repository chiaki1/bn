import { ExtensionConfig, UiHOC } from '../types';
import { HOCManager } from './hoc';
import { ExtensionManager } from './extensions';

export class UiManager {
  static config(uiData: {
    extensionConfigs: ExtensionConfig[];
    uiHOCs?: UiHOC[];
  }) {
    if(uiData?.uiHOCs){
      HOCManager.getInstance().configHOCs(uiData.uiHOCs);
    }
    ExtensionManager.getInstance().config(uiData.extensionConfigs);
  }
}
