import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve('WEB_STORE', {
  INCLUDE_STORE_CODE_IN_URL: false,
  SLUG_KEY: 'slug',
});
