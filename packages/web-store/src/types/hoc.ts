export interface WithRouterProps {
  actions: {
    go: any;
    back: any;
    saveReferUrl: (url?: string) => void;
    clearReferUrl: () => void;
    checkReferUrl: () => void;
  };
}
