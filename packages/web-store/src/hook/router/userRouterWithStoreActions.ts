import {
  appCheckReferUrl,
  appSaveReferUrl,
} from '@vjcspy/r/build/modules/app/store/app.actions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { useStoreContext } from '@vjcspy/r/build/modules/store/context/store';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import WEB_STORE from '../../values/extendable/WEB_STORE';

export const useRouterWithStoreActions = (
  isIncludeStoreCode = WEB_STORE.r('INCLUDE_STORE_CODE_IN_URL')
) => {
  const dispatch = useDispatch();

  const storeContextValue = useStoreContext();

  const go = useCallback(
    (url: URL | string) => {
      if (isIncludeStoreCode) {
        RouterSingleton.push(
          `${storeContextValue.storeData?.store['code']}/${url}`.replace(
            '//',
            '/'
          )
        );
      } else {
        RouterSingleton.push(url);
      }
    },
    [storeContextValue.storeData?.storeId]
  );

  const back = useCallback(() => {
    RouterSingleton.back();
  }, []);

  const saveReferUrl = useCallback(
    (url: { pathname: string; search: string }) => {
      if (!url) {
        dispatch(
          appSaveReferUrl({
            url: {
              pathname: RouterSingleton.pathname,
              search: '',
            },
          })
        );
      } else {
        dispatch(
          appSaveReferUrl({
            url,
          })
        );
      }
    },
    []
  );

  const clearReferUrl = useCallback(() => {
    dispatch(
      appSaveReferUrl({
        url: undefined,
      })
    );
  }, []);

  const checkReferUrl = useCallback(() => {
    dispatch(appCheckReferUrl());
  }, []);

  return {
    actions: {
      go,
      back,
      saveReferUrl,
      clearReferUrl,
      checkReferUrl,
    },
  };
};
