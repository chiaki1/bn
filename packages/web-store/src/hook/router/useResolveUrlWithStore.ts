import { useDomainContext } from '@vjcspy/r/build/modules/domain/context/domain';
import { DomainManager } from '@vjcspy/web-domain/build/util/domain-manager';
import { useRouter } from 'next/router';
import { useMemo } from 'react';

/**
 * Lấy ra đúng pathname kể cả nó đã bao gồm store code
 */
export const useResolveUrlWithStore = () => {
  const domainContextValue = useDomainContext();
  const router = useRouter();

  const urlKey = useMemo(() => {
    if (router.query && Array.isArray(router.query['slug'])) {
      return router.query['slug'].join('/');
    }
    if (Object.keys(router.query).length === 0) {
      //home page
      return '';
    }

    return '';
  }, [router.query]);

  // Lấy ra store hiện tại dựa vào url
  const resolvedUrlData = useMemo(() => {
    if (typeof urlKey !== 'string') {
      throw new Error('Could not resolve url');
    }

    return DomainManager.getInstance().resolveUrl(
      urlKey,
      domainContextValue.domainData.defaultStore,
      domainContextValue.domainData.stores
    );
  }, [urlKey]);

  return {
    pathname: resolvedUrlData?.pathname,
    urlHasStoreCode: resolvedUrlData?.urlHasStoreCode,
    currentStore: resolvedUrlData?.currentStore,
  };
};
