import { map } from 'rxjs/operators';
import { generateCustomerTokenSuccessAction } from '@vjcspy/r/build/modules/account/store/account.actions';
import {
  appCheckReferUrl,
  appHasReferUrl,
  appSaveReferUrl,
} from '@vjcspy/r/build/modules/app/store/app.actions';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';

const generateCustomerSuccess$ = createEffect((action$) =>
  action$.pipe(
    ofType(generateCustomerTokenSuccessAction),
    map(() => {
      return appCheckReferUrl();
    })
  )
);

const whenHasReferUrl$ = createEffect((action$) =>
  action$.pipe(
    ofType(appHasReferUrl),
    map(() => {
      return appSaveReferUrl({
        url: undefined,
      });
    })
  )
);

export const WEB_ROUTER_EFFECTS = [whenHasReferUrl$, generateCustomerSuccess$];
