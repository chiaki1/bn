import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useRouterWithStoreActions } from '../../hook/router/userRouterWithStoreActions';

export const withRouterWithStoreActions = createUiHOC(() => {
  return useRouterWithStoreActions();
}, 'withRouterWithStoreActions');

export const withRouter = createUiHOC(() => {
  return useRouterWithStoreActions();
}, 'withRouter');
