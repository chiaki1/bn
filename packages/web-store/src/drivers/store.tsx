import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { StoreContextProvider } from '@vjcspy/r/build/modules/store/context/store';
import { StoreConstant } from '@vjcspy/r/build/modules/store/util/constant';
import { StorePersistent } from '@vjcspy/r/build/modules/store/util/store-persistent';
import { NextPage } from 'next';
import React, { useEffect, useState } from 'react';

import { useAddStoreCodeToUrl } from '../hook/router/useAddStoreCodeToUrl';
import { useResolveUrlWithStore } from '../hook/router/useResolveUrlWithStore';
import WEB_STORE from '../values/extendable/WEB_STORE';

export const withStore = (
  PageComponent: any,
  webUiAdapterOptions?: WebUiAdapterOptions
): NextPage<any> => {
  const WithStore: NextPage<any> = React.memo((props) => {
    logger.render('WithStore');
    const resolveUrlWithStore = useResolveUrlWithStore();

    const [storeData, setStoreData] = useState({
      storeId: resolveUrlWithStore?.currentStore?.id,
      store: resolveUrlWithStore?.currentStore,
    });

    useAddStoreCodeToUrl(
      resolveUrlWithStore,
      WEB_STORE.r('INCLUDE_STORE_CODE_IN_URL')
    );

    if (!resolveUrlWithStore?.currentStore) {
      logger.error('Could not resolve current store');

      return null;
    }

    useEffect(() => {
      if (storeData?.store?.code) {
        StorePersistent.saveItem(
          StoreConstant.STORE_CODE_KEY,
          storeData?.store?.code
        );
      }
    }, [storeData?.store?.code]);

    return (
      <StoreContextProvider value={{ storeData, setStoreData }}>
        <PageComponent {...props} />
      </StoreContextProvider>
    );
  });

  wrapSSRFn(
    PageComponent,
    WithStore,
    undefined,
    undefined,
    webUiAdapterOptions?.ssr
  );

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithStore.displayName = `withStore(${displayName})`;

  return WithStore;
};
