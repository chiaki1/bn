import { ExtensionConfig } from '@vjcspy/web-ui-extension';

import Image from './Image';
import PlaceholderImage from './PlaceholderImage';

export const WEB_IMAGE_EXTENSION_CONFIGS: ExtensionConfig[] = [
  {
    uiId: 'IMAGE',
    uiTags: ['IMAGE'],
    component: Image,
    priority: 1,
  },
  {
    uiId: 'IMAGE_PLACEHOLDER_DEFAULT',
    uiTags: ['IMAGE_PLACEHOLDER_DEFAULT'],
    component: PlaceholderImage,
    priority: 1,
  },
];
