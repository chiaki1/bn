import React from 'react';
import LazyLoad from 'react-lazyload';

import { useImage } from '../hook/image/useImage';

const Image: React.FC<{
  width: number;
  height?: number;
  ratio?: number;
  src: any;
  alt?: string;
  overflow?: boolean;
  lazy?: boolean;
}> = (props) => {
  const { width, height, ratio, src, alt = '', overflow = false } = props;
  const { resourceWidth, resourceHeight } = useImage({
    width,
    height,
    ratio,
  });

  return (
    <>
      {props?.lazy === true ? (
        <LazyLoad
          height={resourceHeight}
          overflow={overflow}
          // placeholder={
          //   <Skeleton
          //     variant="rectangular"
          //     width={resourceWidth}
          //     height={resourceHeight}
          //   />
          // }
        >
          <img
            alt={alt}
            height={resourceHeight}
            src={src}
            width={resourceWidth}
          />
        </LazyLoad>
      ) : (
        <img
          alt={alt}
          height={resourceHeight}
          src={src}
          width={resourceWidth}
        />
      )}
    </>
  );
};

export default Image;
