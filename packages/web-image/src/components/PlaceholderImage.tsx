import React from 'react';
import { transparentPlaceholder } from '../values/transparentPlaceholder';

const PlaceholderImage: React.FC<{
  width?: number;
  height?: number;
}> = (props) => {
  return (
    <img
      alt="placeholder"
      width={props.width}
      height={props.height}
      src={transparentPlaceholder}
    />
  );
};

export default PlaceholderImage;
