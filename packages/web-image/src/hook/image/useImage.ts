import { useCallback, useMemo, useState } from 'react';

export const useImage = (props: {
  width: number;
  height?: number;
  ratio?: number;
}) => {
  const { width, height, ratio } = props;

  // Use the unconstrained / default entry in widths.
  const resourceWidth = useMemo(() => {
    if (width) {
      return width;
    } else if (height && ratio) {
      return height * ratio;
    } else {
      return undefined;
    }
  }, [width, height]);

  const resourceHeight = useMemo(() => {
    if (height) {
      return height;
    } else if (resourceWidth && ratio) {
      return resourceWidth / ratio;
    } else {
      return undefined;
    }
  }, [height, ratio, resourceWidth]);

  return {
    resourceWidth,
    resourceHeight,
  };
};
