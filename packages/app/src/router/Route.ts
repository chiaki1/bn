import { getUiExtension } from '@vjcspy/ui-extension';
import { ComponentType } from 'react';

export interface RouterConfig {
  path: string;
  component: () => ComponentType<any>;
}

export class AppRoute {
  static SPLASH = 'splash';
  static HOME = 'home';
  static BRAND = 'brand';
  static BRAND_DETAIL = 'brand-details';
  static PRODUCTS_LIST = 'products-list';
  static HOME_NEWS_LIST = 'new-list';
  static CATEGORY_LIST = 'category-list';
  static PRODUCT_PAGE = 'product-detail';
  static LOGIN_PAGE = 'login';
  static ACCOUNT_INVITE_PAGE = 'account-invite';
  static CART_PAGE = 'cart';
  static CHECKOUT_PAGE = 'checkout';
  static PRODUCTS = 'products';
  static WISH_LIST_PAGE = 'wishlist';
  static CART = 'cart';
  static CHECKOUT = 'checkout';
  static CHECKOUT_COMPLETE = 'checkout-complete';
  static ACCOUNT = 'account';
  static MY_ORDERS = 'my-orders';
  static ORDER_DETAIL = 'order-detail';
  static RESET_PASSWORD = 'reset-password';
  static COUPON_PAGE = 'promotion';
  static ADDRESS_LIST = 'address_list';
  static RESET_PASSWORD_DEFAULT = 'reset-password-default';
  static CUSTOMER_INFORMATION = 'customer-information';
  static EDIT_CUSTOMER_INFORMATION = 'edit-customer-information';
  static VOUCHER_PAGE = 'voucher';
  static REWARD_POINTS = 'reward-points';
  static ACCOUNT_INFORMATION_POLICY = 'account-information-policy';
  static POLICY_RULES = 'policy-rules';
  static INFORMATION_SECURITY_POLICY = 'information-security-policy';
  static ABOUT = 'about';
  static ORDER_HISTORY_PAGE = 'order-history';
  static ACCOUNT_UPDATE_INFO = 'account-update';
  static POINTS_PAGE = 'points-page';
  static MY_SIZE_PAGE = 'my-size';
  static MY_SIZE_INPUT_PAGE = 'my-input-size';
  static ACCOUNT_BRAND_FLOW_PAGE = 'account-brand-flow';
  static CHAT_MESSAGE_PAGE = 'chat';
  static NOTIFICATION_PAGE = 'notification';
  static QUESTION_PAGE = 'question';
  static RETURN_ORDER = 'return-order';
  static CMS_DETAIL_PAGE = 'cms-details/:identifier';
  static ADDRESS_LIST_PAGE = 'address-list-page';
  static ACCOUNT_INVITE = 'account-invite';
  static SIZE_GUIDE = 'size-guild';
  static SHOPPING = 'shopping';
  static PAY = 'pay';
  static TRANSPORT = 'transport';
  static EXCHANGE_AND_REFUND = 'exchange_and_refund';
  static OTHER = 'other';

  private static _routes: RouterConfig[] = [
    {
      path: `/${AppRoute.SPLASH}`,
      component: () => getUiExtension('SPLASH'),
    },
    {
      path: `/${AppRoute.HOME}`,
      component: () => getUiExtension('HOME_PAGE'),
    },
    {
      path: `/${AppRoute.BRAND}`,
      component: () => getUiExtension('BRAND_PAGE'),
    },
    {
      path: `/${AppRoute.BRAND_DETAIL}`,
      component: () => getUiExtension('BRAND_DETAIL_PAGE'),
    },
    {
      path: `/${AppRoute.PRODUCTS_LIST}`,
      component: () => getUiExtension('PRODUCTS_LIST_PAGE'),
    },
    {
      path: `/${AppRoute.LOGIN_PAGE}`,
      component: () => getUiExtension('LOGIN_PAGE'),
    },
    {
      path: `/${AppRoute.ACCOUNT_INVITE_PAGE}`,
      component: () => getUiExtension('ACCOUNT_INVITE_PAGE'),
    },
    {
      path: `/${AppRoute.CART_PAGE}`,
      component: () => getUiExtension('CART_PAGE'),
    },
    {
      path: `/${AppRoute.CHECKOUT_PAGE}`,
      component: () => getUiExtension('CHECKOUT_PAGE'),
    },
    {
      path: `/${AppRoute.CATEGORY_LIST}`,
      component: () => getUiExtension('CATEGORY_LIST_PAGE'),
    },
    {
      path: `/${AppRoute.PRODUCTS}`,
      component: () => getUiExtension('PRODUCTS_PAGE'),
    },
    {
      path: `/${AppRoute.CART}`,
      component: () => getUiExtension('CART_PAGE'),
    },
    {
      path: `/${AppRoute.CHECKOUT}`,
      component: () => getUiExtension('CHECKOUT_PAGE'),
    },
    {
      path: `/${AppRoute.CHECKOUT_COMPLETE}`,
      component: () => getUiExtension('CHECKOUT_COMPLETE_PAGE'),
    },
    {
      path: `/${AppRoute.VOUCHER_PAGE}`,
      component: () => getUiExtension('VOUCHER_PAGE'),
    },
    {
      path: `/${AppRoute.REWARD_POINTS}`,
      component: () => getUiExtension('REWARD_POINTS'),
    },
    {
      path: `/${AppRoute.ORDER_HISTORY_PAGE}`,
      component: () => getUiExtension('ORDER_HISTORY_PAGE'),
    },
    {
      path: `/${AppRoute.POINTS_PAGE}`,
      component: () => getUiExtension('POINTS_PAGE'),
    },
    {
      path: `/${AppRoute.WISH_LIST_PAGE}`,
      component: () => getUiExtension('WISHLIST_PAGE'),
    },
    {
      path: `/${AppRoute.MY_SIZE_PAGE}`,
      component: () => getUiExtension('MY_SIZE_PAGE'),
    },
    {
      path: `/${AppRoute.MY_SIZE_INPUT_PAGE}`,
      component: () => getUiExtension('MY_SIZE_INPUT_PAGE'),
    },
    {
      path: `/${AppRoute.RETURN_ORDER}/:orderId`,
      component: () => getUiExtension('RETURN_ORDER_PAGE'),
    },
    {
      path: `/${AppRoute.CHAT_MESSAGE_PAGE}`,
      component: () => getUiExtension('CHAT_MESSAGE_PAGE'),
    },
    {
      path: `/${AppRoute.NOTIFICATION_PAGE}`,
      component: () => getUiExtension('NOTIFICATION_PAGE'),
    },
    {
      path: `/${AppRoute.QUESTION_PAGE}`,
      component: () => getUiExtension('QUESTION_PAGE'),
    },

    // accountaccount
    {
      path: `/${AppRoute.ACCOUNT}`,
      component: () => getUiExtension('ACCOUNT_PAGE'),
    },
    {
      path: `/${AppRoute.ACCOUNT_UPDATE_INFO}`,
      component: () => getUiExtension('ACCOUNT_UPDATE_INFO'),
    },
    {
      path: `/${AppRoute.MY_ORDERS}`,
      component: () => getUiExtension('MY_ORDERS_PAGE'),
    },
    {
      path: `/${AppRoute.ORDER_DETAIL}/:orderId`,
      component: () => getUiExtension('ORDER_DETAILS_PAGE'),
    },
    {
      path: `/${AppRoute.ADDRESS_LIST}`,
      component: () => getUiExtension('ADDRESS_LIST'),
    },
    //------------------
    // {
    //   path: `/${AppRoute.ACCOUNT_INFORMATION_POLICY}`,
    //   component: () => getUiExtension('ACCOUNT_INFORMATION_POLICY_PAGE'),
    // },
    {
      path: `/${AppRoute.POLICY_RULES}`,
      component: () => getUiExtension('POLICY_RULE_PAGE'),
    },
    {
      path: `/${AppRoute.INFORMATION_SECURITY_POLICY}`,
      component: () => getUiExtension('INFORMATION_SECURITY_POLICY_PAGE'),
    },
    {
      path: `/${AppRoute.ABOUT}`,
      component: () => getUiExtension('ABOUT_PAGE'),
    },
    {
      path: `/${AppRoute.ACCOUNT_BRAND_FLOW_PAGE}`,
      component: () => getUiExtension('ACCOUNT_BRAND_FLOW_PAGE'),
    },
    {
      path: `/${AppRoute.CMS_DETAIL_PAGE}`,
      component: () => getUiExtension('CMS_DETAIL_PAGES'),
    },
    {
      path: `/${AppRoute.ADDRESS_LIST_PAGE}`,
      component: () => getUiExtension('ADDRESS_LIST_PAGE'),
    },
    {
      path: `/${AppRoute.ACCOUNT_INVITE}`,
      component: () => getUiExtension('ACCOUNT_INVITE_PAGE'),
    },
    {
      path: `/${AppRoute.SIZE_GUIDE}`,
      component: () => getUiExtension('SIZE_GUIDE'),
    },
    {
      path: `/${AppRoute.SHOPPING}`,
      component: () => getUiExtension('SHOPPING'),
    },
    {
      path: `/${AppRoute.PAY}`,
      component: () => getUiExtension('PAY'),
    },
    {
      path: `/${AppRoute.TRANSPORT}`,
      component: () => getUiExtension('TRANSPORT'),
    },
    {
      path: `/${AppRoute.EXCHANGE_AND_REFUND}`,
      component: () => getUiExtension('EXCHANGE_AND_REFUND'),
    },
    {
      path: `/${AppRoute.OTHER}`,
      component: () => getUiExtension('OTHER'),
    },
  ];

  static get routes() {
    return AppRoute._routes;
  }

  static configRouter(routes: RouterConfig[]) {
    routes.forEach((r) => {
      AppRoute._routes = AppRoute._routes.filter((_r) => _r.path !== r.path);
      AppRoute._routes.push(r);
    });
  }
}
