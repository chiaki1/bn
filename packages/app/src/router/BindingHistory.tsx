import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import qs from 'query-string';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';

/*
 * Inject router global
 * */
function injectRouter(router: any) {
  RouterSingleton.registerRouter({
    push: async (url) => {
      router.push(url, undefined, { shallow: true });
    },
    getPathname: () => {
      return typeof window !== 'undefined' ? window.location.pathname : '';
    },
    getQuery: () => {
      return qs.parse(window.location?.search);
    },
    back: () => {
      router.goBack();
    },
    forward: () => {
      router.goForward();
    },
    replace: async (url) => {
      router.replace(url);
    },
    reload: () => {
      typeof window !== 'undefined' && window.location.reload();
    },
  });
}

const BindingHistory: React.FC<{}> = () => {
  const history = useHistory();
  useEffect(() => {
    injectRouter(history);
  }, [history]);
  return <></>;
};

export default BindingHistory;
