import { IonReactRouter } from '@ionic/react-router';
import { withInitAccountState } from '@vjcspy/r/build/modules/account/hoc/withInitAccountState';
import { withInitContentAddress } from '@vjcspy/r/build/modules/content/hoc/withInitContentAddress';
import { combineHOC, getUiExtension } from '@vjcspy/ui-extension';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import ErrorBoundary from '../extensions/soa/components/error/ErrorBoundary';
import BindingHistory from './BindingHistory';
import { AppRoute } from './Route';

const AppRouter: React.FC<any> = combineHOC(
  withInitAccountState,
  withInitContentAddress
)(() => {
  return (
    <>
      <ErrorBoundary>
        <IonReactRouter>
          <Switch>
            {AppRoute.routes.map((r) => (
              <Route
                exact
                path={r.path}
                component={r.component()}
                key={r.path}
              />
            ))}
            <Route exact path="/">
              <Redirect to={`/${AppRoute.SPLASH}`} />
            </Route>
            <Route
              path="/html-suffix"
              component={getUiExtension('HTML_SUFFIX')}
            />
            <Route path="*" component={getUiExtension('URL_REWRITE_PAGE')} />
          </Switch>
          <BindingHistory />
        </IonReactRouter>
      </ErrorBoundary>
    </>
  );
});

export default AppRouter;
