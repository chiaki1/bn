import { ExtensionConfig } from '@vjcspy/ui-extension';

import AppRouter from './App.router';

export const APP_ROUTER_CFG: ExtensionConfig[] = [
  {
    uiId: 'APP_ROUTER',
    component: AppRouter,
    priority: 100,
  },
];
