import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve('REGISTRY', {
  PROXY_DEFAULT_URL_KEY: 'https://k-api.ggg.systems',
  GRAPHQL_DEFAULT_URL_KEY: 'https://k-api.ggg.systems/proxy/soa-prod/graphql',
  PCMS_DEFAULT_URL_KEY: 'https://soa-pcms-prod.ggg.systems',
  CLIENT_ID_KEY: 'YXBwbGljYXRpb25faWQ6YXBwbGljYXRpb25fc2VjcmV0',
  PROXY_APP_NAME: 'soa-prod',

  DEFAULT_WEBSITE_IDS: [1],

  MGT_CE: false,
});
