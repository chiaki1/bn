import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve(
  'COMMON',
  {
    BRAND_NAME: 'SaleOff Asia',
    CONFIG_PREFIX: '',
    APP_VERSION: '1.13.0',
  },
  true
);
