import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve('ACCOUNT', {
  DEFAULT_USERNAME: 'roni_cost@example.com',
  DEFAULT_PASSWORD: 'roni_cost3@example.com',
  MY_ORDER_STATUS: [
    { title: 'Tất cả', value: 'all' },
    { title: 'Chờ xác nhận', value: 'wait_for_confirmation' },
    { title: 'Đang xử lý', value: 'processing' },
    { title: 'Đang giao hàng', value: 'delivery' },
    { title: 'Đã giao hàng', value: 'delivered' },
    { title: 'Đã hủy', value: 'canceled' },
    { title: 'Trả hàng', value: 'closed' },
  ],
  TELEPHONE_FIELD: 'retail_telephone',
});
