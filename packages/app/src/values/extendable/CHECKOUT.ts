import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve('CHECKOUT', {
  GT_FREE_SHIP: 1000000,
});
