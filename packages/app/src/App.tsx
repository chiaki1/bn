/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
/* Theme variables */
import './theme/variables.css';
/* Swiper https://ionicframework.com/docs/react/slides */
import 'swiper/swiper.min.css';
import 'swiper/modules/autoplay/autoplay.min.css';
import 'swiper/modules/keyboard/keyboard.min.css';
import 'swiper/modules/pagination/pagination.min.css';
import 'swiper/modules/scrollbar/scrollbar.min.css';
import 'swiper/modules/zoom/zoom.min.css';
import '@ionic/react/css/ionic-swiper.css';
import 'react-toastify/dist/ReactToastify.min.css';
import 'animate.css';

import { SplashScreen } from '@capacitor/splash-screen';
import { IonApp, setupIonicReact } from '@ionic/react';
import { UiExtension } from '@vjcspy/ui-extension';
import { useEffect } from 'react';
import { ToastContainer } from 'react-toastify';

import { bootstrap } from './bootstrap';
import ForceUpdate from './extensions/soa/components/update/ForceUpdate';
import { withAppAdapter } from './modules/core/util/adapter';
import { useLanguageHandler } from './modules/ui/hook/useLanguageHandler';
import COMMON from './values/extendable/COMMON';

const config = async () => {
  try {
    await SplashScreen.hide();
  } catch (e) {}
};
config();
setupIonicReact();
bootstrap();

if (
  typeof navigator !== 'undefined' &&
  // @ts-ignore
  typeof navigator?.splashscreen?.hide === 'function'
) {
  // @ts-ignore
  navigator.splashscreen.hide();
}

const App: React.FC = () => {
  useLanguageHandler();
  useEffect(() => {
    console.log(`version: ${COMMON.r('APP_VERSION')}`);
  }, []);
  return (
    <IonApp>
      <ForceUpdate />
      <UiExtension uiId="APP_ROUTER" />
      <ToastContainer />
    </IonApp>
  );
};

export default withAppAdapter(App);
