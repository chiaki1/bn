import { Registry } from '@vjcspy/chitility';
import { BrowserPersistence } from '@vjcspy/chitility/build/util/browser-persistence';
import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';
import { boostrapR } from '@vjcspy/r';
import { R_DEFAULT_VALUE } from '@vjcspy/r/build/values/R_DEFAULT_VALUE';
import { UiManager } from '@vjcspy/ui-extension';
import forEach from 'lodash/forEach';

import { bootSOA } from './extensions/soa/boot';
import CartDetail from './extensions/soa/schema/CartDetail';
import { bootAccount } from './modules/account/boot';
import { bootCatalog } from './modules/catalog/boot';
import { bootCheckout } from './modules/checkout/boot';
import { bootCore } from './modules/core/boot';
import { bootUiModule } from './modules/ui/boot';
import { APP_ROUTER_CFG } from './router';
import REGISTRY from './values/extendable/REGISTRY';

UiManager.config({
  extensionConfigs: [...APP_ROUTER_CFG],
});

const browserPersistence = new BrowserPersistence();
Registry.getInstance().register(
  R_DEFAULT_VALUE.ASYNC_STORAGE_INSTANCE_KEY,
  () => browserPersistence
);

export function bootstrap() {
  boostrapR();

  bootCore();
  bootUiModule();

  bootAccount();
  bootCatalog();
  bootCheckout();

  // extension module
  bootSOA();

  // INIT CONFIG
  forEach(REGISTRY.getData(), (v, k) => Registry.getInstance().register(k, v));

  DataValueExtension.add(
    'soa',
    'CART_DETAIL_SCHEMA',
    {
      query: CartDetail,
    },
    1
  );
}
