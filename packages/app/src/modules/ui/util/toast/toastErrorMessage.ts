import { toast } from 'react-toastify';
import { ToastContent, ToastOptions } from 'react-toastify/dist/types';

export const toastErrorMessage = (
  message: ToastContent,
  options?: ToastOptions
) => {
  const _to = Object.assign(
    {
      position: toast.POSITION.TOP_CENTER,
      autoClose: 2000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
      type: 'error',
      // type: 'success',
    },
    options
  );
  toast.error(message, _to);
};
