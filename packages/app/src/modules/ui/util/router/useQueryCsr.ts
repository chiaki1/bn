import qs from 'query-string';
import { useMemo } from 'react';

export const useQueryCsr = () => {
  const query = useMemo(() => {
    if (typeof window !== 'undefined') {
      return qs.parse(window.location?.search);
    }
  }, []);

  return query;
};
