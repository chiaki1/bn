import { createBrowserHistory } from 'history';

export const history: any = createBrowserHistory();

export class HistoryInstance {
  static history: any;
}
