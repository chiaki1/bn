import { CatalogCategoryListingFilter } from '@vjcspy/apollo';
import _ from 'lodash';

export const filtersToUrl = (filters: CatalogCategoryListingFilter[]) => {
  const query: any = {};
  if (_.isArray(filters)) {
    _.forEach(filters, (value: any) => {
      if (!value?.type) {
        if (typeof value.data.eq === 'string') {
          query[value.code] = encodeURI(value.data.eq);
        } else if (typeof value.data.in === 'object') {
          // @ts-ignore
          query[value.code] = encodeURI(value.data.in.join(','));
        }
      }
    });
  }

  // pathname
  let pathname = '';

  if (typeof window !== 'undefined') {
    pathname = window.location.pathname;
  }

  return { pathname, query };
};
