import React, { useContext } from 'react';

export interface UiContextValue {
  readonly themeName: string;
  readonly uiConfig?: any;
}

const UiContext = React.createContext<UiContextValue>({
  themeName: 'default',
  uiConfig: {},
});

export const useUiContext = () => useContext(UiContext);

export const UiContextProvider: React.FC<{ value: UiContextValue }> = (
  props
) => {
  return (
    <UiContext.Provider value={props.value}>
      {
        // @ts-ignore
        props.children
      }
    </UiContext.Provider>
  );
};
