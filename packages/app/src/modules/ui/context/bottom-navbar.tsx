import React, { useCallback, useContext, useMemo, useState } from 'react';

export interface BottomNavBarContextValue {
  index: string;
  setIndex: (index: string) => void;
  forceHide: boolean;
}

const BottomNavBarContext = React.createContext<BottomNavBarContextValue>({
  forceHide: false,
  index: 'home',
  setIndex: () => {},
});
export const useBottomNavBarContext = () => useContext(BottomNavBarContext);

export const BottomNavBarContextProvider: React.FC<{}> = (props) => {
  const [contextState, setContextState] = useState<BottomNavBarContextValue>({
    index: 'home',
    forceHide: false,
    setIndex: () => {},
  });
  const setIndex = useCallback(
    (index: string) => {
      setContextState({ ...contextState, index });
    },
    [contextState]
  );

  const contextValue = useMemo(
    () => ({ ...contextState, setIndex }),
    [contextState]
  );

  return (
    <BottomNavBarContext.Provider value={contextValue}>
      {
        // @ts-ignore
        props.children
      }
    </BottomNavBarContext.Provider>
  );
};
