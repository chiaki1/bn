import { UiContextProvider } from '@vjcspy/chitility/build/context/ui';
import {
  UrlRewriteContextProvider,
  UrlRewriteContextValue,
} from '@vjcspy/r/build/modules/router/context/url-rewrite';
import React, { useMemo, useState } from 'react';

import { BottomNavBarContextProvider } from '../context';
import { useWebAppUiConfig } from '../hook/useWebAppUiConfig';

export const withUi = (Component: React.FC) => {
  const WithUi = React.memo((props) => {
    const {
      state: { webAppUiConfig },
    } = useWebAppUiConfig();

    const [uiContextValue, setUiContextValue] = useState({
      themeName: 'default',
      isBottomShowNavBar: true,
    });

    const value = useMemo(() => {
      const setShowNavBar = (show: boolean) => {
        if (uiContextValue.isBottomShowNavBar !== show) {
          setUiContextValue({ ...uiContextValue, isBottomShowNavBar: show });
        }
      };
      return {
        ...uiContextValue,
        uiConfig: webAppUiConfig,
        setShowNavBar,
      };
    }, [uiContextValue, webAppUiConfig]);

    const [urlRewriteData, setUrlRewriteData] = useState<any>({
      isResolved: false,
    });

    const urlRewriteContextValue: UrlRewriteContextValue = useMemo(() => {
      return {
        urlRewriteData,
        setUrlRewriteData,
      };
    }, [urlRewriteData]);

    return (
      <UiContextProvider value={value}>
        <UrlRewriteContextProvider value={urlRewriteContextValue}>
          {/*@ts-ignore*/}
          <BottomNavBarContextProvider>
            <Component {...props} />
          </BottomNavBarContextProvider>
        </UrlRewriteContextProvider>
      </UiContextProvider>
    );
  });
  const displayName =
    Component.displayName || Component.name || 'PageComponent';
  WithUi.displayName = `withUi(${displayName})`;
  return WithUi;
};
