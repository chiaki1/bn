import animateScrollTo from 'animated-scroll-to';
import { useCallback, useEffect, useState } from 'react';

export const useIonContentScrollElem = () => {
  const [ionContentScrollElem, setIonContentScrollElem] = useState<any>();

  useEffect(() => {
    const ionContentElem = document.querySelector('ion-content');
    // @ts-ignore
    if (typeof ionContentElem?.getScrollElement === 'function') {
      // @ts-ignore
      const retrieveElem = async () => {
        // @ts-ignore
        const _elem = await ionContentElem?.getScrollElement();
        setIonContentScrollElem(_elem);
      };

      retrieveElem();
    }
  }, []);

  const scrollTo = useCallback(
    (offset: any) => {
      if (ionContentScrollElem && (offset || offset === 0)) {
        animateScrollTo(offset, {
          elementToScroll: ionContentScrollElem,
        }).then();
      }
    },
    [ionContentScrollElem]
  );

  const scrollToElem = useCallback(
    (elem: HTMLElement) => {
      if (ionContentScrollElem && elem) {
        animateScrollTo(elem, {
          elementToScroll: ionContentScrollElem,
        }).then();
      }
    },
    [ionContentScrollElem]
  );

  return {
    ionContentScrollElem,
    scrollTo,
    scrollToElem,
  };
};
