import throttle from 'lodash/throttle';
import { useCallback, useEffect, useState } from 'react';

import { useIonContentScrollElem } from './useIonContentScrollElem';

export const useIsScrollBottom = (element?: any) => {
  const [isBottom, setIsBottom] = useState(false);
  const { ionContentScrollElem } = useIonContentScrollElem();
  const listener = useCallback(
    throttle(() => {
      if (typeof ionContentScrollElem === 'undefined') {
        return;
      }

      const currentScrollTop =
        ionContentScrollElem.offsetParent.getBoundingClientRect().height +
        ionContentScrollElem.scrollTop;
      const scrollHeight = ionContentScrollElem?.scrollHeight;

      if (currentScrollTop + 50 >= scrollHeight) {
        setIsBottom(true);
      } else {
        setIsBottom(false);
      }
    }, 100),
    [ionContentScrollElem]
  );

  useEffect(() => {
    if (typeof ionContentScrollElem === 'undefined') {
      return;
    }
    ionContentScrollElem.addEventListener('scroll', listener, {
      passive: false,
    });

    // return a callback, which is called on unmount
    return () => {
      ionContentScrollElem.removeEventListener('scroll', listener);
    };
  }, [ionContentScrollElem, listener]);

  return {
    isBottom,
  };
};
