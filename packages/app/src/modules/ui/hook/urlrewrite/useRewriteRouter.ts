import { useResolveChiakiPageLazyQuery } from '@vjcspy/apollo';
import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useDomainContext } from '@vjcspy/r/build/modules/domain/context/domain';
import { useUrlRewriteContext } from '@vjcspy/r/build/modules/router/context/url-rewrite';
import { resolveChiakiPageResolver } from '@vjcspy/web-url-rewrite/build/util/resolveChiakiPageResolver';
import _ from 'lodash';
import { useCallback, useEffect } from 'react';
import { useLocation } from 'react-router';

export const useRewriteRouter = () => {
  const location = useLocation();

  const domainContextValue = useDomainContext();
  const { urlRewriteData, setUrlRewriteData } = useUrlRewriteContext();

  const [resolveUrlQuery, resolveUrlRes] = useResolveChiakiPageLazyQuery({
    fetchPolicy: FetchPolicyResolve.withLifetime(
      'useResolveChiakiPageLazyQuery'
    ),
  });

  const doQueryDbRef = useCallback(
    _.debounce((urlKey) => {
      // Khi thực hiện chuyển page trên client cũng cần phải resolve static trước
      resolveUrlQuery({
        variables: {
          urlKey: _.isEmpty(urlKey) ? 'index' : urlKey,
          userId: domainContextValue.domainData.shopOwnerId,
        },
      });
    }, 50),
    []
  );

  useEffect(() => {
    if (location.pathname !== urlRewriteData?.pathname) {
      console.log('=> do query url rewrite');
      doQueryDbRef(location.pathname);
    }

    return () => {
      console.log('UNMOUNTED URL_REWRITE_PAGE');
    };
  }, [location.pathname]);

  useEffect(() => {
    if (resolveUrlRes.data && typeof setUrlRewriteData === 'function') {
      setUrlRewriteData(resolveChiakiPageResolver(resolveUrlRes));
    }
  }, [resolveUrlRes.data]);

  useEffect(() => {
    return () => {
      if (setUrlRewriteData) setUrlRewriteData({ isResolved: false });
    };
  }, [setUrlRewriteData]);

  return { urlRewriteData };
};
