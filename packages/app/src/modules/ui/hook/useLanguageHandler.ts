import { useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

export const useLanguageHandler = () => {
  const { i18n } = useTranslation();

  const changeLanguageHandler = useCallback(
    (lang) => {
      i18n
        .changeLanguage(lang)
        .then(() => {})
        .catch(() => {});
    },
    [i18n]
  );

  useEffect(() => {
    changeLanguageHandler('vi');
  }, []);

  return {
    i18n,
    changeLanguageHandler,
  };
};
