import { useGetChiakiConfigLazyQuery } from '@vjcspy/apollo';
import _ from 'lodash';
import { useEffect, useState } from 'react';

import COMMON from '../../../values/extendable/COMMON';

export const useWebAppUiConfig = () => {
  const [webAppUiConfig, setUiConfig] = useState<any>();
  const [configRequest, configRes] = useGetChiakiConfigLazyQuery({
    variables: {
      storeId: 'default',
      userId: 'default',
      key: COMMON.r('CONFIG_PREFIX') + 'WEB_APP_UI_CONFIG',
    },
  });

  useEffect(() => {
    configRequest();
  }, []);

  useEffect(() => {
    if (configRes?.data?.chiakiConfig) {
      if (
        Array.isArray(configRes?.data?.chiakiConfig) &&
        configRes?.data?.chiakiConfig.length === 1
      ) {
        const _config: any = _.first(configRes?.data?.chiakiConfig);
        try {
          setUiConfig(JSON.parse(_config.value));
        } catch (e) {
          console.warn('could not parse web app ui config');
        }
      }
    }
  }, [configRes?.data]);

  return { state: { webAppUiConfig } };
};
