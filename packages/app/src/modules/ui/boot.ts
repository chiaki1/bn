import { UiManager } from '@vjcspy/ui-extension';
import { WEB_IMAGE_EXTENSION_CONFIGS } from '@vjcspy/web-image';

import { UI_EXT_CFG } from './components';
import { PAGE_CPT_CFG } from './pages';

UiManager.config({
  extensionConfigs: [
    ...WEB_IMAGE_EXTENSION_CONFIGS,
    ...UI_EXT_CFG,
    ...PAGE_CPT_CFG,
  ],
});

export function bootUiModule() {}
