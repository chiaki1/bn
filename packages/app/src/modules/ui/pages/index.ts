import { ExtensionConfig } from '@vjcspy/ui-extension';

import UrlRewrite from './UrlRewrite';

export const PAGE_CPT_CFG: ExtensionConfig[] = [
  {
    uiId: 'URL_REWRITE_PAGE',
    component: UrlRewrite,
    hoc: [],
    priority: () => 100,
  },
];
