import { UiExtension } from '@vjcspy/ui-extension';
import { useMemo } from 'react';
import * as React from 'react';

import { useRewriteRouter } from '../hook/urlrewrite/useRewriteRouter';

const UrlRewrite: React.FC = React.memo(() => {
  const rewriteRouter = useRewriteRouter();

  const CONTENT = useMemo(() => {
    if (rewriteRouter.urlRewriteData?.isResolved) {
      switch (rewriteRouter.urlRewriteData.type) {
        case 'CATEGORY':
          return <UiExtension uiId="PRODUCTS_PAGE" />;
        case 'PRODUCT':
          return <UiExtension uiId="PRODUCT_PAGE" />;
        default:
          return <UiExtension uiId="STATIC_404" />;
      }
    } else {
      return <UiExtension uiId="DEFAULT_LOADING" loading={true} />;
    }
  }, [rewriteRouter.urlRewriteData]);

  return <>{CONTENT}</>;
});

export default UrlRewrite;
