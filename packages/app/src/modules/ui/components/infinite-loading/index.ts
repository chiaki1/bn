import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const INFINITE_LOADING_CPT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ION_INFINITE_LOADING',
    component: { lazy: React.lazy(() => import('./IonInfiniteLoading')) },
  },
];
