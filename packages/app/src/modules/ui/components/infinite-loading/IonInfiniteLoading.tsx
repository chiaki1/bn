import { IonInfiniteScroll, IonInfiniteScrollContent } from '@ionic/react';
import React, { useCallback, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

const IonInfiniteLoading: React.FC<{
  handleLoadMorePage: () => void;
  isDone: boolean;
  isLoading: boolean;
}> = (props) => {
  const { t } = useTranslation('common');
  const infiniteScrollRef = useRef<any>(null);

  // useEffect(() => {
  //   if (!infiniteScrollRef.current) {
  //     return;
  //   }
  //   if (props.pageFilterInfo.currentPage == 1) {
  //     infiniteScrollRef.current.disabled = false;
  //   }
  //
  //   if (props.isDone) {
  //     infiniteScrollRef.current.disabled = true;
  //     infiniteScrollRef.current.complete();
  //   }
  // }, [props?.pageFilterInfo?.currentPage, props.isDone]);
  //
  // useEffect(() => {
  //   if (!props.isLoading && infiniteScrollRef.current) {
  //     infiniteScrollRef.current.complete();
  //   }
  // }, [props.isLoading]);

  // useEffect(() => {
  //   if (!infiniteScrollRef.current) {
  //     return;
  //   }
  //   infiniteScrollRef.current.disabled = props.disabled;
  // }, [props.disabled]);

  useEffect(() => {
    if (!infiniteScrollRef.current) {
      return;
    }
    if (!props.isLoading) {
      infiniteScrollRef.current.complete();
    }
  }, [props.isLoading]);

  useEffect(() => {
    if (!infiniteScrollRef.current) {
      return;
    }
    if (props.isDone) {
      infiniteScrollRef.current.disabled = true;
      infiniteScrollRef.current.complete();
    } else {
      infiniteScrollRef.current.disabled = false;
    }
  }, [props.isDone]);

  const handleLoadMorePage = useCallback(
    (event: any) => {
      if (typeof props.handleLoadMorePage === 'function') {
        props.handleLoadMorePage();
      }
    },
    [props.handleLoadMorePage]
  );

  // handle load more page
  useEffect(() => {
    if (infiniteScrollRef.current) {
      infiniteScrollRef.current!.addEventListener(
        'ionInfinite',
        handleLoadMorePage
      );

      return () => {
        if (infiniteScrollRef.current) {
          infiniteScrollRef.current!.removeEventListener(
            'ionInfinite',
            handleLoadMorePage
          );
        }
      };
    }
  }, [handleLoadMorePage]);

  return (
    <IonInfiniteScroll
      ref={infiniteScrollRef}
      threshold="100px"
      id="infinite-scroll"
    >
      <IonInfiniteScrollContent
        loading-spinner="bubbles"
        loading-text={t('loading_more_data')}
      />
    </IonInfiniteScroll>
  );
};

export default IonInfiniteLoading;
