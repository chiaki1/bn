import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import React, { useEffect } from 'react';
import { useLocation } from 'react-router';

const HtmlSuffix = React.memo(() => {
  const location = useLocation();
  useEffect(() => {
    const rex = new RegExp('(/html-suffix)(/.*)');
    const re = rex.exec(location.pathname);
    if (re && re.length === 3 && re[2].indexOf('html') < 0) {
      RouterSingleton.push(re[2] + '.html');
    } else {
    }
  }, [location.pathname]);
  return <></>;
});

HtmlSuffix.displayName = 'HtmlSuffix';
export default HtmlSuffix;
