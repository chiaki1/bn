import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const HTML_SUFFIX_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'HTML_SUFFIX',
    component: { lazy: React.lazy(() => import('./HtmlSuffix')) },
    priority: 1,
  },
];
