import { ExtensionConfig } from '@vjcspy/ui-extension';

import { INFINITE_LOADING_CPT_CFG } from './infinite-loading';
import { LOADING_CPT_CFG } from './loading';
import { HTML_SUFFIX_EXT_CFG } from './redirect-html-suffix';
import { UI_STATIC_CFG } from './static';

export const UI_EXT_CFG: ExtensionConfig[] = [
  ...LOADING_CPT_CFG,
  ...INFINITE_LOADING_CPT_CFG,
  ...UI_STATIC_CFG,
  ...HTML_SUFFIX_EXT_CFG,
];
