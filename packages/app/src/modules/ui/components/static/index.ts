import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const UI_STATIC_CFG: ExtensionConfig[] = [
  {
    uiId: 'STATIC_404',
    component: { lazy: React.lazy(() => import('./404')) },
    priority: 1,
  },
];
