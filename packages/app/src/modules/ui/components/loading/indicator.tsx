import { IonLoading } from '@ionic/react';
import React from 'react';
import { useTranslation } from 'react-i18next';

import classes from './indicator.module.scss';

const LoadingIndicator = (props: any) => {
  return (
    <div className={props.global ? classes.root : classes.container}>
      {/*<div className={classes.indicator_inner}>*/}
      {/*  <span className={classes.indicator}>*/}
      {/*    <LoaderIcon size={64} />*/}
      {/*  </span>*/}
      {/*  <div className={classes.message}>*/}
      {/*    <span className="indicator-message">*/}
      {/*      {props.defaultMessage ? t('loading_data') : props.children}*/}
      {/*    </span>*/}
      {/*  </div>*/}
      {/*</div>*/}
      <IonLoading
        cssClass={
          props.global ? classes.ionLoadingGlobal : classes.ionLoadingContainer
        }
        isOpen={true}
        onDidDismiss={() => () => {}}
        spinner="bubbles" //"bubbles" | "circles" | "circular" | "crescent" | "dots" | "lines" | "lines-small"
        showBackdrop={true}
        duration={5000}
      />
    </div>
  );
};

export default LoadingIndicator;
