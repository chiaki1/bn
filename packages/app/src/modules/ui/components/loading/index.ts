import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const LOADING_CPT_CFG: ExtensionConfig[] = [
  {
    uiId: 'LOADING_INDICATOR',
    component: { lazy: React.lazy(() => import('./indicator')) },
    priority: 1,
  },
  {
    uiId: 'DEFAULT_LOADING',
    component: { lazy: React.lazy(() => import('./DefaultLoading')) },
    priority: () => 100,
  },
];
