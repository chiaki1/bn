import { IonLoading } from '@ionic/react';
import React from 'react';

const DefaultLoading: React.FC<{
  loading: boolean;
  mess?: string;
  global?: boolean;
}> = (props) => {
  return (
    <>
      {props.loading && (
        <IonLoading
          cssClass="my-custom-class"
          isOpen={true}
          spinner="bubbles" //"bubbles" | "circles" | "circular" | "crescent" | "dots" | "lines" | "lines-small"
          onDidDismiss={() => {}}
          message={props.mess ?? ''}
        />
      )}
    </>
  );
};

export default DefaultLoading;
