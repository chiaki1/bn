/**
 * Do app sẽ fix store hiển thị nên sẽ tự implement function resolve store
 * @param props
 */
import { useDomainContext } from '@vjcspy/r/build/modules/domain/context/domain';
import { StorePersistent } from '@vjcspy/r/build/modules/store/util/store-persistent';
import _ from 'lodash';
import { useCallback, useEffect, useState } from 'react';

export const useResolveStoreData = (props: {
  storeId: any;
  websiteId: any;
}) => {
  const [storeData, setStoreData] = useState<any>();
  const domainContextValue = useDomainContext();

  const _resolveStoreData = useCallback(async () => {
    const storeDataCached = await StorePersistent.getItem(
      'CHIAKI.KEY.STORE_DATA'
    );
    if (storeDataCached) {
      setStoreData(storeDataCached);
      return;
    }

    const websites = domainContextValue.domainData.websites;

    if (Array.isArray(websites)) {
      _.forEach(websites, (website: any) => {
        if (_.isArray(website['groups'])) {
          _.forEach(website.groups, (group: any) => {
            if (_.isArray(group['stores'])) {
              const store = _.find(
                group.stores,
                (s: any) => s['id'] == props.storeId
              );

              if (store) {
                StorePersistent.saveItem('CHIAKI.KEY.STORE_DATA', store);
                setStoreData(store);
              }
            }
          });
        }
      });
    }
  }, [domainContextValue?.domainData?.websites]);

  useEffect(() => {
    if (
      Array.isArray(domainContextValue?.domainData?.websites) &&
      domainContextValue.domainData.websites.length > 0
    ) {
      _resolveStoreData();
    }
  }, [domainContextValue?.domainData?.websites]);

  return {
    storeData,
  };
};
