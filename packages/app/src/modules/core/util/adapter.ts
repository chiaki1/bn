import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { withAdapter } from '@vjcspy/chitility/build/util/withAdapters';
import { withApollo } from '@vjcspy/web-apollo';
import { withDomain } from '@vjcspy/web-domain/build/drivers/domain';
import { withI18n } from '@vjcspy/web-i18n';
import { withRedux } from '@vjcspy/web-r';

import { withWebAccount } from '../../account/drivers/with-account';
import { withWebCatalog } from '../../catalog/drivers/with-catalog';
import { withWebCheckout } from '../../checkout/drivers/with-checkout';
import { withUi } from '../../ui/driver/ui';
import { withStore } from '../driver/store';

const ADAPTERS: any[] = [
  withDomain,
  withStore,
  withRedux,
  withI18n,
  withApollo,

  withUi,

  withWebAccount,
  withWebCatalog,
  withWebCheckout,
];

export const withAppAdapter = (
  PageComponent: any,
  webUiAdapterOptions?: WebUiAdapterOptions
): any => {
  webUiAdapterOptions = { ssr: false, ...webUiAdapterOptions };
  return withAdapter(PageComponent, webUiAdapterOptions, ADAPTERS);
};
