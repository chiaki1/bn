import { useDomainContext } from '@vjcspy/r/build/modules/domain/context/domain';
import { StoreContextProvider } from '@vjcspy/r/build/modules/store/context/store';
import { StoreContextValue } from '@vjcspy/r/build/modules/store/types';
import { StoreConstant } from '@vjcspy/r/build/modules/store/util/constant';
import { StorePersistent } from '@vjcspy/r/build/modules/store/util/store-persistent';
import React, { useEffect, useMemo } from 'react';

import { useResolveStoreData } from '../hook/domain/useResolveStoreData';

export const withStore = (Component: React.FC) => {
  const WithInitConfig = React.memo((props) => {
    const domainContextValue = useDomainContext();
    const { storeData } = useResolveStoreData({
      storeId: domainContextValue?.domainData?.defaultStore?.id,
      websiteId: domainContextValue?.domainData?.defaultStore?.website_id,
    });
    useEffect(() => {
      if (storeData?.code) {
        StorePersistent.saveItem(StoreConstant.STORE_CODE_KEY, storeData.code);
      }
    }, [storeData?.code]);
    const storeContextValue: StoreContextValue = useMemo(
      () => ({
        storeData: {
          storeId: domainContextValue.domainData.defaultStore.id,
          store: storeData,
        },
      }),
      [storeData]
    );
    return (
      <>
        {storeData && (
          <StoreContextProvider value={storeContextValue}>
            <Component {...props} />
          </StoreContextProvider>
        )}
      </>
    );
  });
  const displayName =
    Component.displayName || Component.name || 'PageComponent';
  WithInitConfig.displayName = `withStore(${displayName})`;
  return WithInitConfig;
};
