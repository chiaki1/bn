import { logger } from '@vjcspy/chitility/build/util/logger';
import { ACCOUNT_EFFECTS } from '@vjcspy/r/build/modules/account/store/account.effects';
import { accountReducer } from '@vjcspy/r/build/modules/account/store/account.reducer';
import { R_CONTENT_EFFECTS } from '@vjcspy/r/build/modules/content/store/effects';
import { contentReducer } from '@vjcspy/r/build/modules/content/store/reducer';
import { storeManager } from '@vjcspy/web-r';
import React from 'react';

let _initAccount = false;
storeManager.mergeReducers({
  account: accountReducer,
  content: contentReducer,
});
function initAccount() {
  if (_initAccount) {
    return;
  }
  storeManager.addEpics('web-account', [
    ...ACCOUNT_EFFECTS,
    ...R_CONTENT_EFFECTS,
  ]);

  _initAccount = true;
}

initAccount();

export const withWebAccount = (PageComponent: any): any => {
  const WithWebAccount = React.memo((props: any) => {
    logger.render('WithWebAccount');

    return (
      <>
        <PageComponent {...props} />
      </>
    );
  });

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithWebAccount.displayName = `withWebAccount(${displayName})`;

  return WithWebAccount;
};
