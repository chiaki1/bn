import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { R_CATALOG_EFFECTS } from '@vjcspy/r/build/modules/catalog/store/product/product.effects';
import { productReducer } from '@vjcspy/r/build/modules/catalog/store/product/product.reducer';
import { productInfoReducer } from '@vjcspy/r/build/modules/catalog/store/product-info/product-info.reducer';
import { productsReducer } from '@vjcspy/r/build/modules/catalog/store/products/products.reducer';
import { storeManager } from '@vjcspy/web-r';
import React from 'react';

import { WEB_APP_CATALOG_EFFECTS } from '../store/category/products.effects';

let _initCatalog = false;

storeManager.mergeReducers({
  products: productsReducer,
  product: productReducer,
  productInfo: productInfoReducer,
});

function initCatalog() {
  if (_initCatalog) {
    return;
  }

  storeManager.addEpics('web-catalog', [
    ...R_CATALOG_EFFECTS,
    ...WEB_APP_CATALOG_EFFECTS,
  ]);

  _initCatalog = true;
}

initCatalog();

export const withWebCatalog = (
  PageComponent: any,
  webUiAdapterOptions?: WebUiAdapterOptions
): any => {
  const WithWebCatalog = React.memo((props: any) => {
    logger.render('WithWebCatalog');

    return (
      <>
        <PageComponent {...props} />
      </>
    );
  });

  wrapSSRFn(
    PageComponent,
    WithWebCatalog,
    undefined,
    undefined,
    webUiAdapterOptions?.ssr
  );

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithWebCatalog.displayName = `withWebCatalog(${displayName})`;

  return WithWebCatalog;
};
