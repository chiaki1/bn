import { createUiHOC } from '@vjcspy/ui-extension';

import { useProductsContainer } from '../../hook/products/useProductsContainer';

export const withAppProductsContainer = createUiHOC(() => {
  return useProductsContainer();
}, 'withAppProductsContainer');
