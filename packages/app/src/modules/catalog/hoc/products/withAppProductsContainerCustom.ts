import { createUiHOC } from '@vjcspy/ui-extension';

import { useProductsContainerCustom } from '../../hook/products/useProductsContainerCustom';

export const withAppProductsContainerCustom = createUiHOC(() => {
  return useProductsContainerCustom();
}, 'withAppProductsContainerCustom');
