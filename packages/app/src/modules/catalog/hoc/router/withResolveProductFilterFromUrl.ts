import { createUiHOC } from '@vjcspy/ui-extension';

import { useResolveProductFilterFromUrl } from '../../hook/category/useResolveProductFilterFromUrl';

export const withResolveProductFilterFromUrl = createUiHOC(
  () => useResolveProductFilterFromUrl(),
  'withResolveProductFilterFromUrl'
);
