import { CatalogCategoryListingFilter } from '@vjcspy/apollo';
import { productsResolvedFiltersData } from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import { createUiHOC } from '@vjcspy/ui-extension';
import forEach from 'lodash/forEach';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

export const withResolveProductFilterCustom = createUiHOC(() => {
  const dispatch = useDispatch();
  const setFilterToStore = useCallback((filterData: any) => {
    const filters: any[] = [];
    if (typeof filterData === 'object') {
      forEach(filterData, (value, key) => {
        if (typeof value === 'object' && key) {
          if (value?.in) {
            filters.push({
              code: key,
              data: {
                in: value?.in,
              },
              type: 'fix',
            });
          } else if (value?.eq) {
            filters.push({
              code: key,
              data: {
                eq: value?.eq,
              },
              type: 'fix',
            });
          } else {
            return true;
          }
        }
      });
    }
    dispatch(productsResolvedFiltersData({ filters }));
  }, []);

  return {
    actions: {
      setFilterToStore,
    },
  };
}, 'withResolveProductFilterCustom');
