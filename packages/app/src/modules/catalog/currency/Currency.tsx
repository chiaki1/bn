import { withPriceFormat } from '@vjcspy/r/build/modules/catalog/hoc/product/withPriceFormat';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback } from 'react';

const Currency = combineHOC(withPriceFormat)((props) => {
  const formatPrice = useCallback((price?: any) => {
    if ((!price && isNaN(price)) || typeof props.priceFormat !== 'function') {
      return '-';
    }
    return props.priceFormat(price);
  }, []);

  return <>{formatPrice(props.price ?? 0)}</>;
});

export default Currency;
