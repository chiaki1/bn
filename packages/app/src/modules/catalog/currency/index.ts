import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const CURRENCY_CPT: ExtensionConfig[] = [
  {
    uiId: 'CURRENCY',
    uiTags: ['CURRENCY'],
    component: { lazy: React.lazy(() => import('./Currency')) },
    priorityFn: () => 100,
  },
];
