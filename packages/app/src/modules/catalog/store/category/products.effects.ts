import {
  productsAddFilter,
  productsClearFilters,
  productsRemoveFilter,
  productsToggleAggregationItem,
  setPageFilterInfo,
} from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import { ProductsState } from '@vjcspy/r/build/modules/catalog/store/products/products.state';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import qs from 'query-string';
import { map, withLatestFrom } from 'rxjs/operators';

import { filtersToUrl } from '../../../ui/util/router/filtersToUrl';

const whenUpdateFilter$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(
      productsRemoveFilter,
      productsAddFilter,
      productsClearFilters,
      productsToggleAggregationItem
    ),
    withLatestFrom(state$, (v1, v2: { products: ProductsState }) => [
      v1,
      v2.products.filters,
    ]),
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    map(([_, filters]) => {
      const urlData = filtersToUrl(filters as any);
      const newUrlSearch = qs.stringify(urlData.query);
      RouterSingleton.push({
        pathname: urlData.pathname,
        search: !!newUrlSearch && newUrlSearch != '' ? `?${newUrlSearch}` : '',
      });

      return setPageFilterInfo({
        pageFilterInfo: {
          currentPage: 1,
        },
      });
    })
  )
);

export const WEB_APP_CATALOG_EFFECTS = [whenUpdateFilter$];
