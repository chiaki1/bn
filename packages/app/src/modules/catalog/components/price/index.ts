import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export * from './Price';
export const CATALOG_PRICE_CPT: ExtensionConfig[] = [
  {
    uiId: 'PRICE',
    component: { lazy: React.lazy(() => import('./Price')) },
    hoc: [],
    priorityFn: () => 100,
  },
];
