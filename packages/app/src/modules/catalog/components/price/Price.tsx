import { IonText } from '@ionic/react';
import { withPriceFormat } from '@vjcspy/r/build/modules/catalog/hoc/product/withPriceFormat';
import { productPriceHasDiscount } from '@vjcspy/r/build/modules/catalog/util/productPriceHasDiscount';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

const Price = combineHOC(withPriceFormat)((props) => {
  const hasDiscount = useMemo(() => {
    return productPriceHasDiscount(props.priceRange);
  }, [props.priceRange]);

  if (!props.priceRange) {
    return null;
  }

  return (
    <>
      <div className="price-wrap">
        {!hasDiscount ? (
          <IonText color="primary" className="special-price">
            <UiExtension
              uiId="CURRENCY"
              price={props.priceRange.minimum_price?.final_price?.value}
            />
          </IonText>
        ) : (
          <>
            {!!props.priceRange.maximum_price?.regular_price?.value && (
              <IonText className="old-price text-xs">
                <UiExtension
                  uiId="CURRENCY"
                  price={props.priceRange.maximum_price?.regular_price?.value}
                />
              </IonText>
            )}
            <IonText color="primary" className="special-price">
              <UiExtension
                uiId="CURRENCY"
                price={props.priceRange.minimum_price?.final_price?.value}
              />
            </IonText>
          </>
        )}
      </div>
    </>
  );
});

export default Price;
