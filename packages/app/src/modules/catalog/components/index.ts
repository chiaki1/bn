import { ExtensionConfig } from '@vjcspy/ui-extension';

import { CURRENCY_CPT } from '../currency';
import { CATALOG_PRICE_CPT } from './price';
import { CATALOG_PRODUCT_EXT_CFG } from './product';

export const CATALOG_EXT_CFG: ExtensionConfig[] = [
  ...CATALOG_PRICE_CPT,
  ...CURRENCY_CPT,
  ...CATALOG_PRODUCT_EXT_CFG,
];
