import { ExtensionConfig } from '@vjcspy/ui-extension';

import { PRODUCT_OPTIONS_EXT_CFG } from './options-type';

export const CATALOG_PRODUCT_EXT_CFG: ExtensionConfig[] = [
  ...PRODUCT_OPTIONS_EXT_CFG,
];
