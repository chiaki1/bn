import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

import { PRODUCT_TYPE_OPTION_CONFIGURABLE_OPTIONS_CPT } from './configurable-options';

export const PRODUCT_OPTIONS_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'PRODUCT_TYPE_OPTIONS_CONFIGURABLE',
    component: { lazy: React.lazy(() => import('./ConfigurableOptions')) },
  },
  {
    uiId: 'PRODUCT_TYPE_OPTIONS_SIMPLE',
    component: { lazy: React.lazy(() => import('./Simple')) },
  },
  ...PRODUCT_TYPE_OPTION_CONFIGURABLE_OPTIONS_CPT,
];
