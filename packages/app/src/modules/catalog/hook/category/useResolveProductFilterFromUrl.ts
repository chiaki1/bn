import { CatalogCategoryListingFilter } from '@vjcspy/apollo';
import { useResolveProductsFilters } from '@vjcspy/r/build/modules/catalog/hook/products/useResolveProductsFilters';
import { productsResolvedFiltersData } from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import { queryToFilters } from '@vjcspy/r/build/modules/catalog/util/queryToFilters';
import { useUrlRewriteContext } from '@vjcspy/r/build/modules/router/context/url-rewrite';
import _ from 'lodash';
import qs from 'query-string';
import { useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';

/**
 * Init filters data from url
 * @returns {{filters: CatalogCategoryListingFilter[]}}
 */
export const useResolveProductFilterFromUrl = () => {
  const urlRewriteContextValue = useUrlRewriteContext();
  const filterFromState = useResolveProductsFilters();
  const dispatch = useDispatch();

  /**
   * Chỗ này bắt buộc phải sử dụng memo để lấy filters data synchronized
   * Filters sau đó được sử dụng để query get products
   *
   * @type {CatalogCategoryListingFilter[]}
   */
  const filters: CatalogCategoryListingFilter[] = useMemo(() => {
    const init: CatalogCategoryListingFilter[] = [];
    // resolve category filter
    if (urlRewriteContextValue.urlRewriteData.type === 'CATEGORY') {
      init.push({
        code: 'category_id',
        data: {
          eq: urlRewriteContextValue.urlRewriteData.id,
        },
      });
    }

    if (typeof window !== 'undefined') {
      // resolve attribute filters
      const query = qs.parse(window.location?.search);
      init.push(...queryToFilters(query));
    }

    return init;
  }, [location.search, urlRewriteContextValue]);

  useEffect(() => {
    if (!_.isEqual(filterFromState.filters, filters)) {
      dispatch(productsResolvedFiltersData({ filters }));
    }
  }, [filters, filterFromState]);

  return {
    filters,
  };
};
