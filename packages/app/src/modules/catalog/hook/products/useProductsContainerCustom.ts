import {
  SortEnum,
  useGetSoaCategoryListingDataLazyQuery,
} from '@vjcspy/apollo-sale-off';
import { useResolveProductsFilters } from '@vjcspy/r/build/modules/catalog/hook/products/useResolveProductsFilters';
import {
  productsGotData,
  setPageFilterInfo,
} from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import {
  selectAggregations,
  selectPageFilterInfo,
  selectProducts,
  selectSearchString,
} from '@vjcspy/r/build/modules/catalog/store/products/products.selectors';
import _ from 'lodash';
import omit from 'lodash/omit';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

export const useProductsContainerCustom = () => {
  const { filters } = useResolveProductsFilters();
  const dispatch = useDispatch();
  const products = useSelector(selectProducts);
  const aggregations = useSelector(selectAggregations);
  const searchString = useSelector(selectSearchString);
  const pageFilterInfo = useSelector(selectPageFilterInfo);

  const [totalPage, setTotalPage] = useState(-1);
  const [totalCount, setTotalCount] = useState(-1);
  const [categoryInfo, setCategoryInfo] = useState<any>();
  const [rangePriceFilter, setRangePriceFilter] = useState<any>({
    min: 0,
    max: 0,
  });
  const [getCategoryListingData, getCategoryListingDataResponse] =
    useGetSoaCategoryListingDataLazyQuery({
      fetchPolicy: 'cache-and-network', // Used for first execution
      // nextFetchPolicy: 'cache-first',
    });

  const aggregationsWithoutCategory = useMemo(() => {
    return _.filter(
      aggregations,
      (aggregation) => aggregation['attribute_code'] !== 'category_id'
    );
  }, [aggregations]);

  const debounceRunQueryGetList = useCallback(
    (variables: any) => getCategoryListingData({ variables }),
    []
  );

  const debounceLoadMorePage = useMemo(() => {
    return _.debounce(() => {
      dispatch(
        setPageFilterInfo({
          pageFilterInfo: {
            currentPage: pageFilterInfo.currentPage! + 1,
          },
        })
      );
    }, 100);
  }, [pageFilterInfo]);

  const handleLoadMorePage = useCallback(() => {
    if (pageFilterInfo.currentPage! < totalPage || totalPage < 0) {
      if (!getCategoryListingDataResponse.loading) {
        debounceLoadMorePage();
      }
    }
  }, [pageFilterInfo, totalPage, getCategoryListingDataResponse.loading]);

  // trigger load data
  useEffect(() => {
    if (!_.isEmpty(filters)) {
      let filterx: any[] = [];
      if (filters) {
        filterx = filters.map((it: any) => {
          return { code: it.code, data: it.data };
        });
      }

      debounceRunQueryGetList({
        search: searchString ?? '',
        currentPage: pageFilterInfo.currentPage,
        filters: filterx || filters,
        pageSize: 20,
        sort: !_.isEmpty(searchString)
          ? {
              relevance: SortEnum.Desc,
            }
          : pageFilterInfo.sort,
      });
    }
  }, [filters, searchString, pageFilterInfo]);

  // save to store
  useEffect(() => {
    if (getCategoryListingDataResponse.error) {
      console.error('Could not query getCategoryListingDataResponse');
    }
    if (getCategoryListingDataResponse.data?.catalogCategoryListingData) {
      if (
        _.isNumber(
          getCategoryListingDataResponse.data.catalogCategoryListingData
            .page_info?.total_pages
        )
      ) {
        setTotalPage(
          getCategoryListingDataResponse.data.catalogCategoryListingData
            .page_info!.total_pages!
        );
        setTotalCount(
          getCategoryListingDataResponse.data.catalogCategoryListingData
            .total_count!
        );
      }

      dispatch(
        productsGotData({
          products:
            getCategoryListingDataResponse.data.catalogCategoryListingData
              ?.items!,
          aggregations:
            getCategoryListingDataResponse.data.catalogCategoryListingData
              ?.aggregations!,
          mergeWithExisting: true,
          pageInfo:
            getCategoryListingDataResponse.data.catalogCategoryListingData!
              .page_info!,
          totals:
            getCategoryListingDataResponse.data.catalogCategoryListingData
              ?.total_count || 0,
        })
      );
    }
  }, [
    getCategoryListingDataResponse.data,
    getCategoryListingDataResponse.error,
  ]);

  useEffect(() => {
    if (
      filters &&
      aggregationsWithoutCategory &&
      aggregationsWithoutCategory.length > 0 &&
      filters.filter((fi: any) => fi.code === 'price').length === 0
    ) {
      const aggregationPrice = aggregationsWithoutCategory.filter(
        (agg: any) => agg?.attribute_code === 'price'
      );

      if (
        aggregationPrice[0] &&
        aggregationPrice[0]?.options &&
        aggregationPrice[0]?.options.length > 0
      ) {
        const defaultValue = {
          min: 0,
          max: 0,
        };
        const count = parseInt(aggregationPrice[0]?.options.length) - 1;
        if (aggregationPrice[0].options[0]?.value) {
          defaultValue.min = parseInt(
            aggregationPrice[0]?.options[0]?.value.split('_')[0]
          );
        }
        if (aggregationPrice[0]?.options[count]?.value) {
          defaultValue.max = parseInt(
            aggregationPrice[0]?.options[count]?.value.split('_')[1]
          );
        }
        setRangePriceFilter(defaultValue);
      }
    }
  }, [filters, aggregationsWithoutCategory]);

  const resetFilterInfo = () => {
    dispatch(
      setPageFilterInfo({
        pageFilterInfo: {
          currentPage: 1,
        },
      })
    );
  };
  return {
    state: {
      products,
      aggregations: aggregationsWithoutCategory,
      isSearching: !!searchString,
      isLoading:
        totalPage !== 0 &&
        (pageFilterInfo.currentPage! < totalPage ||
          totalPage == -1 ||
          getCategoryListingDataResponse.loading),
      realLoading: getCategoryListingDataResponse.loading,
      isDone:
        totalPage === 0 ||
        (pageFilterInfo.currentPage === totalPage &&
          pageFilterInfo.currentPage > 0 &&
          !getCategoryListingDataResponse.loading),
      currentPage: pageFilterInfo.currentPage,
      pageFilterInfo,
      searchString,
      totalPage,
      totalCount,
      categoryInfo,
      rangePriceFilters: rangePriceFilter,
    },
    actions: {
      handleLoadMorePage,
      resetFilterInfo,
    },
  };
};
