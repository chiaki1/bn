import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const CATALOG_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CATEGORY_LIST_PAGE',
    component: { lazy: React.lazy(() => import('./CategoryList')) },
  },
];
