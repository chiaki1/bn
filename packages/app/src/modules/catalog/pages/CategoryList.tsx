import { IonContent, IonPage } from '@ionic/react';
import { UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { AppRoute } from '../../../router/Route';
import { useBottomNavBarContext } from '../../ui/context';

const CategoryList = React.memo((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex(AppRoute.CATEGORY_LIST);
  }, []);
  return (
    <>
      <IonPage className={'page-header-no-mrb'}>
        <UiExtension uiId="HEADER" />
        <IonContent>
          <UiExtension uiId="CATEGORY_LIST" />
        </IonContent>
        <UiExtension uiId="BOTTOM_NAV_BAR" />
      </IonPage>
    </>
  );
});

CategoryList.displayName = 'CategoryList';
export default CategoryList;
