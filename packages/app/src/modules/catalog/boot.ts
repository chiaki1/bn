import { UiManager } from '@vjcspy/ui-extension';

import { CATALOG_EXT_CFG } from './components';
import { CATALOG_PAGE_EXT_CFG } from './pages';

UiManager.config({
  extensionConfigs: [...CATALOG_PAGE_EXT_CFG, ...CATALOG_EXT_CFG],
});
export function bootCatalog() {}
