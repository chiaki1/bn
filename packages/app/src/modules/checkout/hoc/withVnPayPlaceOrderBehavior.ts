import { Browser } from '@capacitor/browser';
import { addPlaceOrderHookFn } from '@vjcspy/r/build/modules/checkout/util/cart/addPlaceOrderHookFn';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { proxyFetch } from '@vjcspy/r/build/util/proxy-fetch';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useEffect } from 'react';

import { AppRoute } from '../../../router/Route';
import { toastErrorMessage } from '../../ui/util/toast/toastErrorMessage';

const openCapacitorSite = async (url: string) => {
  await Browser.open({ url });
};

const checkVnPayStatusAndCloseBrowser = async (orderNumber: any) => {
  setTimeout(() => {
    Browser.close();
  }, 500);
};

export const withVnPayPlaceOrderBehavior = createUiHOC(() => {
  useEffect(() => {
    addPlaceOrderHookFn(
      'vnPay',
      async (cart, order) => {
        if (cart?.selected_payment_method?.code === 'vnpay' && order) {
          try {
            const res = await proxyFetch({
              type: 'get-pay-url',
              payload: {
                order_number: order!.order_number!,
                callback_url: 'https://saleoff.asia/vnpay-order-loading',
              },
            });
            if (res.hasOwnProperty('url')) {
              RouterSingleton.push('/vnpay-order-loading');
              await openCapacitorSite(res.url);
            }
          } catch (e) {
            toastErrorMessage('Không thể kết nối VnPay');
          }

          return false;
        }
        return true;
      },
      50
    );
  }, []);
  return {};
}, 'withVnPayPlaceOrderBehavior');
