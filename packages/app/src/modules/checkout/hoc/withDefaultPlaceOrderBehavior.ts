import { addPlaceOrderHookFn } from '@vjcspy/r/build/modules/checkout/util/cart/addPlaceOrderHookFn';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useEffect } from 'react';

import { AppRoute } from '../../../router/Route';

export const withDefaultPlaceOrderBehavior = createUiHOC(() => {
  useEffect(() => {
    addPlaceOrderHookFn(
      'default',
      async () => {
        RouterSingleton.push('/' + AppRoute.CHECKOUT_COMPLETE);
        return false;
      },
      100
    );
  }, []);

  return {};
}, 'withDefaultPlaceOrderBehavior');
