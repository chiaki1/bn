import { Browser } from '@capacitor/browser';
import { selectCompleteOrderNumber } from '@vjcspy/r/build/modules/checkout/store/cart/cart.selector';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { proxyFetch } from '@vjcspy/r/build/util/proxy-fetch';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { AppRoute } from '../../../router/Route';
import { useQueryCsr } from '../../ui/util/router/useQueryCsr';

export const withVnpayCheckingStatus = createUiHOC(() => {
  const [loading, setLoading] = useState(true);
  const orderNumber = useSelector(selectCompleteOrderNumber);
  const query = useQueryCsr();
  console.log(query);

  const getStatus = useCallback(async (order_number: any) => {
    try {
      const res = await proxyFetch({
        type: 'check-pay-success',
        payload: {
          order_number,
        },
      });
      if (res?.status === 'success') {
        Browser.close();
        RouterSingleton.push('/' + AppRoute.CHECKOUT_COMPLETE);

        return;
      } else {
        setTimeout(() => {
          getStatus(order_number);
        }, 5000);
      }
    } catch (e) {}
  }, []);

  useEffect(() => {
    if (typeof orderNumber !== 'undefined') {
      getStatus(orderNumber);
    } else if (query?.vnp_OrderInfo) {
      getStatus(query?.vnp_OrderInfo);
    }
  }, [orderNumber, query?.vnp_OrderInfo]);

  return {
    state: {
      loading,
    },
  };
}, 'withVnpayCheckingStatus');
