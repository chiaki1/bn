import { logger } from '@vjcspy/chitility/build/util/logger';
import { CHECKOUT_EFFECTS } from '@vjcspy/r/build/modules/checkout/store/checkout.effects';
import { checkoutReducer } from '@vjcspy/r/build/modules/checkout/store/checkout.reducer';
import { storeManager } from '@vjcspy/web-r';
import React from 'react';

let _initCheckout = false;
storeManager.mergeReducers({
  checkout: checkoutReducer,
});
function initCheckout() {
  if (_initCheckout) return;

  storeManager.addEpics('web-checkout', [...CHECKOUT_EFFECTS]);

  _initCheckout = true;
}

initCheckout();

export const withWebCheckout = (PageComponent: any): any => {
  const WithWebCheckout = React.memo((props: any) => {
    logger.render('WithWebCheckout');

    return (
      <>
        <PageComponent {...props} />
      </>
    );
  });

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithWebCheckout.displayName = `withWebAccount(${displayName})`;

  return WithWebCheckout;
};
