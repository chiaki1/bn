import { getUiExtension } from '@vjcspy/ui-extension';

import { AppRoute } from '../../router/Route';
import { SoaAppRoute } from './values/Route';

export const soaConfigRouter = () => {
  AppRoute.configRouter([
    {
      path: `/${SoaAppRoute.BRAND_DETAIL}`,
      component: () => getUiExtension('BRAND_DETAIL_PAGE'),
    },
    {
      path: `/${'vnpay-order-loading'}`,
      component: () => getUiExtension('CHECKOUT_VNPAY_LOADING'),
    },
  ]);
};
