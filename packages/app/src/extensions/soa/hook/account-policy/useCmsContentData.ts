import { useGetAccountPolicyDataLazyQuery } from '@vjcspy/apollo-sale-off';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

export const useCmsContentData = (props: any) => {
  const dispatch = useDispatch();
  const [getAccountPolicyDataQuery, getAccountPolicyRes] =
    useGetAccountPolicyDataLazyQuery({
      fetchPolicy: 'cache-and-network',
    });
  useEffect(() => {
    getAccountPolicyDataQuery({
      variables: {
        identifier: props.identifier,
      },
    });
  }, []);

  useEffect(() => {
    if (getAccountPolicyRes?.error) {
      console.warn('Can"t get data brand list : ', getAccountPolicyRes?.error);
    }
  }, [getAccountPolicyRes.error, getAccountPolicyRes.data]);
  return {
    state: {
      accountPolicyData: getAccountPolicyRes.data?.cmsPage || {},
    },
  };
};
