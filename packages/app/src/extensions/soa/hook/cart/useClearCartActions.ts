import { useClearCustomerCartMutation } from '@vjcspy/apollo-sale-off';
import { getCartDetail } from '@vjcspy/r/build/modules/checkout/store/cart/actions/init.actions';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

export const useClearCartActions = () => {
  const dispatch = useDispatch();
  const [cartIds, setCartIds] = useState('');
  const [clearCustomerCartMutation, clearCustomerCartRes] =
    useClearCustomerCartMutation();

  const clearCart = useCallback((cartId: string) => {
    setCartIds(cartId);
    clearCustomerCartMutation({
      variables: {
        cartUid: cartId,
      },
    });
  }, []);

  useEffect(() => {
    if (clearCustomerCartRes?.error) {
      console.warn('Can"t clear Cart all : ');
    }
    if (clearCustomerCartRes?.data?.clearCustomerCart?.status && cartIds) {
      dispatch(
        getCartDetail({
          cartId: cartIds,
        })
      );
    }
  }, [clearCustomerCartRes.error, clearCustomerCartRes.data]);

  return {
    actions: {
      clearCart,
    },
  };
};
