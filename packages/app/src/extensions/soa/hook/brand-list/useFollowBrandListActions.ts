import {
  useSetFollowBrandMutation,
  useUnFollowBrandMutation,
} from '@vjcspy/apollo-sale-off';
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  soaSetFollowBrand,
  soaSetFollowBrandAfter,
  soaSetFollowBrandError,
  soaSetUnFollowBrand,
  soaSetUnFollowBrandAfter,
  soaSetUnFollowBrandError,
} from '../../store/brand/brand.content.actions';

export const useFollowBrandListActions = () => {
  const dispatch = useDispatch();

  const [setFollowBrandMutation, setFollowBrandRes] =
    useSetFollowBrandMutation();

  const [unFollowBrandMutation, unFollowBrandRes] = useUnFollowBrandMutation();

  const setFollowBrand = useCallback(
    async (brandId: any) => {
      if (setFollowBrandRes?.loading === true) {
        return;
      }
      dispatch(
        soaSetFollowBrand({
          brandId,
        })
      );
      try {
        await setFollowBrandMutation({
          variables: {
            brandId,
          },
        });
      } catch (e) {}
    },
    [setFollowBrandRes?.loading]
  );

  useEffect(() => {
    if (setFollowBrandRes.error) {
      dispatch(
        soaSetFollowBrandError({
          error: setFollowBrandRes.error,
        })
      );
    }

    if (setFollowBrandRes.data?.followBrand) {
      dispatch(
        soaSetFollowBrandAfter({
          data: setFollowBrandRes.data.followBrand?.follow_brands,
        })
      );
    }
  }, [setFollowBrandRes.data, setFollowBrandRes.error]);

  const setUnFollowBrand = useCallback(
    async (brandId: any) => {
      if (unFollowBrandRes?.loading === true) {
        return;
      }
      dispatch(
        soaSetUnFollowBrand({
          brandId,
        })
      );
      try {
        await unFollowBrandMutation({
          variables: {
            brandId,
          },
        });
      } catch (e) {}
    },
    [unFollowBrandRes?.loading]
  );

  useEffect(() => {
    if (unFollowBrandRes.error) {
      dispatch(
        soaSetUnFollowBrandError({
          error: unFollowBrandRes.error,
        })
      );
    }

    if (unFollowBrandRes?.data?.unFollowBrand) {
      dispatch(
        soaSetUnFollowBrandAfter({
          data: unFollowBrandRes.data.unFollowBrand?.follow_brands,
        })
      );
    }
  }, [unFollowBrandRes.data, unFollowBrandRes.error]);

  return {
    actions: {
      setFollowBrand,
      setUnFollowBrand,
    },
  };
};
