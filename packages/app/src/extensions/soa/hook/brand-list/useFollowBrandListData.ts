import { useSelector } from 'react-redux';

import { selectFollowBrand } from '../../store/brand/brand.content.selector';

export const useFollowBrandListData = () => {
  const followBrandData = useSelector(selectFollowBrand);

  return {
    state: {
      dataFollowBrand: followBrandData,
    },
  };
};
