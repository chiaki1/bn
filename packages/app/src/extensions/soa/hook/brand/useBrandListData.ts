import { useGetBrandListLazyQuery } from '@vjcspy/apollo-sale-off';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { soaGetDataBrandListAfterAction } from '../../store/brand/brand.content.actions';

export const useBrandListData = () => {
  const dispatch = useDispatch();
  const [getBrandListDataQuery, getBrandListDataRes] = useGetBrandListLazyQuery(
    {
      fetchPolicy: 'cache-and-network',
    }
  );

  useEffect(() => {
    getBrandListDataQuery();
  }, []);
  //
  useEffect(() => {
    if (getBrandListDataRes?.error) {
      console.warn('Can"t get data brand list : ', getBrandListDataRes?.error);
    }
    if (getBrandListDataRes?.data?.brandList) {
      dispatch(
        soaGetDataBrandListAfterAction({
          data: getBrandListDataRes.data?.brandList,
        })
      );
    }
  }, [getBrandListDataRes.error, getBrandListDataRes.data]);

  return {
    state: {
      brandListData: getBrandListDataRes.data,
    },
  };
};
