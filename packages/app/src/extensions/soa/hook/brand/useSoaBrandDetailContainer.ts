import { useGetSoaBrandDetailLazyQuery } from '@vjcspy/apollo-sale-off';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import {
  soaGetDataBrandPage,
  soaGetDataBrandPageAfterAction,
} from '../../store/brand/brand.content.actions';

export const useSoaBrandDetailContainer = () => {
  const dispatch = useDispatch();
  const [getSoaBrandDetailQuery, getSoaBrandDetailRes] =
    useGetSoaBrandDetailLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  const getDataBrandDetail = useCallback((brandId: number) => {
    dispatch(soaGetDataBrandPage());
    getSoaBrandDetailQuery({
      variables: {
        brandId,
      },
    });
  }, []);

  useEffect(() => {
    if (getSoaBrandDetailRes?.error) {
      console.warn('Can"t get data home page : ', getSoaBrandDetailRes?.error);
    }
    if (getSoaBrandDetailRes?.data?.brandDetail) {
      dispatch(
        soaGetDataBrandPageAfterAction({
          data: getSoaBrandDetailRes?.data?.brandDetail,
        })
      );
    }
  }, [getSoaBrandDetailRes.error, getSoaBrandDetailRes.data]);

  return {
    actions: {
      getDataBrandDetail: getDataBrandDetail,
    },
  };
};
