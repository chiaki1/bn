import { useGetBrandListLazyQuery } from '@vjcspy/apollo-sale-off';
import { selectAggregations } from '@vjcspy/r/build/modules/catalog/store/products/products.selectors';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { soaGetDataBrandListAfterAction } from '../../store/brand/brand.content.actions';
import { selectListAllBrands } from '../../store/brand/brand.content.selector';

export const useBrandListDataForSearch = () => {
  const dispatch = useDispatch();
  const brandData = useSelector(selectListAllBrands);
  const aggregations = useSelector(selectAggregations);
  const [brandSearchResult, setBrandSearchResult] = useState<any[]>([]);
  const [getBrandListDataQuery, getBrandListDataRes] = useGetBrandListLazyQuery(
    {
      fetchPolicy: 'cache-and-network',
    }
  );

  useEffect(() => {
    if (!brandData || brandData.length === 0) {
      getBrandListDataQuery();
    }
  }, [brandData]);

  useEffect(() => {
    if (brandData && brandData.length > 0 && aggregations.length > 0) {
      const aggreBrand = aggregations.filter(
        (agg: any) => agg?.attribute_code === 'brand'
      );
      if (
        aggreBrand.length > 0 &&
        aggreBrand[0]?.options &&
        aggreBrand[0]?.options.length > 0
      ) {
        const searchBrand: any[] = [];
        aggreBrand[0]?.options.forEach((agg: any) => {
          brandData.forEach((brand: any) => {
            if (brand?.brand_id == agg?.value) {
              searchBrand.push({ ...brand, count: agg?.count });
            }
          });
        });
        if (searchBrand.length > 0) {
          setBrandSearchResult(searchBrand);
        } else {
          setBrandSearchResult([]);
        }
      }
    } else {
      setBrandSearchResult([]);
    }
  }, [brandData, aggregations]);

  useEffect(() => {
    if (getBrandListDataRes?.error) {
      console.warn('Can"t get data brand list : ', getBrandListDataRes?.error);
    }
    if (getBrandListDataRes?.data?.brandList) {
      dispatch(
        soaGetDataBrandListAfterAction({
          data: getBrandListDataRes.data?.brandList,
        })
      );
    }
  }, [getBrandListDataRes.error, getBrandListDataRes.data]);

  return {
    state: {
      brandListData: getBrandListDataRes.data,
      brandSearchResult: brandSearchResult,
    },
  };
};
