import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useGetSoaCategoryListLazyQuery } from '@vjcspy/apollo-sale-off';
import { useEffect, useState } from 'react';

export const useCategoryListData = () => {
  const [activeCat, setActiveCat] = useState<any>();
  const [queryCategory, categoryRes] = useGetSoaCategoryListLazyQuery({
    fetchPolicy: FetchPolicyResolve.CACHE_AND_NETWORK,
  });

  useEffect(() => {
    queryCategory();
  }, []);

  useEffect(() => {
    if (
      Array.isArray(categoryRes?.data?.categoryTab?.category_ids) &&
      categoryRes!.data!.categoryTab!.category_ids.length > 0 &&
      typeof activeCat === 'undefined'
    ) {
      setActiveCat(categoryRes!.data!.categoryTab!.category_ids[0]);
    }
  }, [categoryRes?.data?.categoryTab, activeCat]);

  return {
    state: {
      categories: categoryRes?.data?.categoryTab?.category_ids,
      activeCat,
    },
    actions: {
      setActiveCat,
    },
  };
};
