import { useGetSaleOfSoHomeCmsDataLazyQuery } from '@vjcspy/apollo-sale-off';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { soaAddDataHomePageAfterAction } from '../../store/home/home.content.actions';

export const useSoaHomePageData = () => {
  const dispatch = useDispatch();
  const [getSaleOfSoHomeCmsDataQuery, getSaleOfSoHomeCmsRes] =
    useGetSaleOfSoHomeCmsDataLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  useEffect(() => {
    getSaleOfSoHomeCmsDataQuery();
  }, []);

  useEffect(() => {
    if (getSaleOfSoHomeCmsRes?.error) {
      console.warn('Can"t get data home page : ', getSaleOfSoHomeCmsRes?.error);
    }
    if (getSaleOfSoHomeCmsRes?.data?.soHomeCmsData) {
      dispatch(
        soaAddDataHomePageAfterAction({
          data: getSaleOfSoHomeCmsRes.data?.soHomeCmsData,
        })
      );
    }
  }, [getSaleOfSoHomeCmsRes.error, getSaleOfSoHomeCmsRes.data]);

  return {
    state: {
      dataHomePage: getSaleOfSoHomeCmsRes.data?.soHomeCmsData || {},
    },
  };
};
