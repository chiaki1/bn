import { useGetCatalogProductsLazyQuery } from '@vjcspy/apollo-sale-off';
import { useEffect, useMemo } from 'react';
// import { useDispatch } from 'react-redux';
// import { getListProductHotSaleDataAfter } from '../../store/home/home.content.actions';

export const useListProductHotSaleData = (props: any) => {
  // const dispatch = useDispatch();

  const [getListProductHotSaleDataQuery, getListProductHotSaleRes] =
    useGetCatalogProductsLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  const dataFilter = useMemo(() => {
    if (props.dataBlockCampaignImageListing?.data_filters) {
      return JSON.parse(
        props.dataBlockCampaignImageListing?.data_filters.toString()
      );
    }
  }, [props.dataBlockCampaignImageListing?.data_filters]);

  useEffect(() => {
    if (dataFilter) {
      getListProductHotSaleDataQuery({
        variables: {
          search: '',
          filter: dataFilter?.filter,
          pageSize: 3,
          currentPage: 1,
        },
      });
    }
  }, [dataFilter]);

  useEffect(() => {
    if (getListProductHotSaleRes?.error) {
      console.warn(
        'Can"t get data home page : ',
        getListProductHotSaleRes?.error
      );
    }
    // if (getListProductHotSaleRes?.data) {
    //   dispatch(
    //     getListProductHotSaleDataAfter({
    //       data: getListProductHotSaleRes.data,
    //     })
    //   );
    // }
  }, [getListProductHotSaleRes.data, getListProductHotSaleRes.error]);

  return {
    state: {
      listProductHotSaleData: getListProductHotSaleRes?.data ?? {},
    },
  };
};
