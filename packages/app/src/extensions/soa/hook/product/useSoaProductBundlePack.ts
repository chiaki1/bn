import { useGetSoaProductBundlePackLazyQuery } from '@vjcspy/apollo-sale-off';
import { selectProduct } from '@vjcspy/r/build/modules/catalog/store/product/product.selectors';
import { useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';

export const useSoaProductBundlePack = () => {
  const [packQuery, packRes] = useGetSoaProductBundlePackLazyQuery({
    fetchPolicy: 'cache-and-network',
  });
  const product = useSelector(selectProduct);

  useEffect(() => {
    if (typeof product?.sku === 'string') {
      packQuery({
        variables: {
          sku: product?.sku,
        },
      });
    }
  }, [product?.sku]);

  const discountType = useMemo(() => {
    if (
      packRes?.data?.amMostviewedBundlePacks?.items &&
      Array.isArray(packRes?.data?.amMostviewedBundlePacks?.items) &&
      packRes?.data?.amMostviewedBundlePacks?.items.length > 0
    ) {
    }

    return undefined;
  }, [packRes?.data?.amMostviewedBundlePacks?.items]);

  return {
    state: {
      bundlePacks: packRes?.data?.amMostviewedBundlePacks?.items,
    },
  };
};
