import { WishlistItemInput } from '@vjcspy/apollo';
import { useProductDiscountPrice } from '@vjcspy/r/build/modules/catalog/hook/product/useProductDiscountPrice';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { useCallback } from 'react';

import { AppRoute } from '../../../../router/Route';
import { useProductRewriteUrl } from './useProductRewriteUrl';

export const useProductListItem = (props: any) => {
  const { urlRewrite } = useProductRewriteUrl(props);

  const goDetail = useCallback(() => {
    if (typeof props?.actions?.go === 'function') {
      props.actions.go('/' + urlRewrite);
    }
  }, [urlRewrite]);

  const toggleWishlist = useCallback(
    (wishlistItem?: WishlistItemInput) => {
      if (props.state?.accountState?.customer) {
        let wishlistId = props?.state?.accountState?.wishlist[0]?.id;
        if (!wishlistId) {
          wishlistId = 0;
        }
        if (props.state?.productInWishlist) {
          if (typeof props.actions.removeWishlistItem === 'function') {
            props.actions.removeWishlistItem(
              wishlistId,
              props?.state.productInWishlist.id
            );
          }
        } else {
          if (
            typeof props.actions.addProductsToWishlist === 'function' &&
            wishlistItem
          ) {
            props.actions.addProductsToWishlist(wishlistId, [wishlistItem]);
          }
        }
      } else {
        // save after go to login
        RouterSingleton.push(`/${AppRoute.ACCOUNT}`);
      }
    },
    [props.state?.productInWishlist?.id, props.state?.accountState?.customer]
  );

  const { isShowDiscount, discountPrice } = useProductDiscountPrice(props);

  return {
    isShowDiscount,
    discountPrice,
    goDetail,
    toggleWishlist,
  };
};
