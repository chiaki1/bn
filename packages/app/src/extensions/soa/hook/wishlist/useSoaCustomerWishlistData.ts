import { useSoaGetWishlistDetailLazyQuery } from '@vjcspy/apollo-sale-off';
import { selectCustomer } from '@vjcspy/r/build/modules/account/store/account.selector';
import {
  getWishListAction,
  getWishListAfterAction,
  getWishListErrorAction,
} from '@vjcspy/r/build/modules/account/store/wishlisht/wishlist.actions';
import {
  selectCustomerWishlist,
  selectIsLoadingWishlist,
} from '@vjcspy/r/build/modules/account/store/wishlisht/wishlist.selector';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

export const useSoaCustomerWishlistData = () => {
  const wishlists = useSelector(selectCustomerWishlist);
  const isLoading = useSelector(selectIsLoadingWishlist);
  const customer = useSelector(selectCustomer);
  const dispatch = useDispatch();
  const [getWishlistQuery, getWishlistRes] = useSoaGetWishlistDetailLazyQuery({
    fetchPolicy: 'network-only',
  });
  useEffect(() => {
    if (!!customer && (!wishlists || wishlists.length === 0)) {
      dispatch(getWishListAction());
      getWishlistQuery({});
    }
  }, [customer]);

  useEffect(() => {
    if (getWishlistRes.error) {
      dispatch(
        getWishListErrorAction({
          error: getWishlistRes.error,
        })
      );
    }

    if (getWishlistRes?.data?.customer?.wishlists) {
      dispatch(
        getWishListAfterAction({
          wishlists: getWishlistRes?.data?.customer?.wishlists,
        })
      );
    }
  }, [getWishlistRes?.data?.customer?.wishlists, getWishlistRes.error]);

  return {
    state: {
      wishlists,
      isLoading,
    },
  };
};
