import { CustomerUpdateInput } from '@vjcspy/apollo';
import { useSoaUpdateCustomerInfoMutation } from '@vjcspy/apollo-sale-off';
import {
  updateCustomerInfoAction,
  updateCustomerInfoAfterAction,
  updateCustomerInfoErrorAction,
} from '@vjcspy/r/build/modules/account/store/customer-info/customer.info.actions';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

export const useSoaCustomerInfoActions = () => {
  const dispatch = useDispatch();
  const [updateCustomerMutation, updateCustomerRes] =
    useSoaUpdateCustomerInfoMutation();

  const updateCustomerInfo = useCallback(async (info: CustomerUpdateInput) => {
    dispatch(
      updateCustomerInfoAction({
        info,
      })
    );
    try {
      await updateCustomerMutation({
        variables: {
          input: info,
        },
      });
    } catch (e) {}
  }, []);

  useEffect(() => {
    if (updateCustomerRes.error) {
      dispatch(
        updateCustomerInfoErrorAction({
          error: updateCustomerRes.error,
        })
      );
    }

    if (updateCustomerRes?.data?.updateCustomerV2?.customer) {
      dispatch(
        updateCustomerInfoAfterAction({
          customer: updateCustomerRes?.data?.updateCustomerV2?.customer,
        })
      );
    }
  }, [
    updateCustomerRes.data?.updateCustomerV2?.customer,
    updateCustomerRes.error,
  ]);

  return {
    actions: { updateCustomerInfo },
  };
};
