import { SearchResultPageInfo } from '@vjcspy/apollo';
import { useGetSoaCustomerOrdersCustomerLazyQuery } from '@vjcspy/apollo-sale-off';
import {
  gotCustomerOrders,
  setOrdersPageFilter,
} from '@vjcspy/r/build/modules/account/store/customer-order/actions';
import {
  selectOrders,
  selectOrdersPageFilter,
} from '@vjcspy/r/build/modules/account/store/customer-order/selector';
import { selectIsUpdatingTotals } from '@vjcspy/r/build/modules/checkout/store/cart/cart.selector';
import debounce from 'lodash/debounce';
import isEmpty from 'lodash/isEmpty';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

export const useSoaCustomerOrdersCustomer = () => {
  const dispatch = useDispatch();
  const pageFilter = useSelector(selectOrdersPageFilter);
  const isUpdatingTotals = useSelector(selectIsUpdatingTotals);
  const orders = useSelector(selectOrders);
  const [pageInfoRes, setPageInfoRes] = useState<SearchResultPageInfo>();

  const [getSoaCustomerOrdersCustomerQuery, getSoaCustomerOrdersCustomerRes] =
    useGetSoaCustomerOrdersCustomerLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  const setFilterStatus = useCallback(
    (status: string) => {
      const _f = { ...pageFilter };
      if (pageFilter.status == status || isEmpty(status)) {
        _f.status = 'all';
      } else {
        _f.status = status;
      }
      _f.currentPage = 1;
      dispatch(
        setOrdersPageFilter({
          pageFilter: _f,
        })
      );
    },
    [pageFilter]
  );

  const debounceRunQueryGetList = useMemo(() => {
    return debounce((variables) => {
      getSoaCustomerOrdersCustomerQuery({ variables });
    }, 250);
  }, []);

  const debounceLoadMorePage = useMemo(() => {
    return debounce(() => {
      const _f = { ...pageFilter };
      _f.currentPage = _f.currentPage + 1;
      dispatch(
        setOrdersPageFilter({
          pageFilter: _f,
        })
      );
    }, 250);
  }, [pageFilter]);

  const handleLoadMorePage = useCallback(() => {
    if (
      !pageInfoRes?.total_pages ||
      pageFilter?.currentPage < pageInfoRes?.total_pages
    ) {
      if (!getSoaCustomerOrdersCustomerRes.loading) {
        debounceLoadMorePage();
      }
    }
  }, [
    pageFilter,
    pageInfoRes,
    getSoaCustomerOrdersCustomerRes.loading,
    debounceLoadMorePage,
  ]);

  useEffect(() => {
    let filters: any;
    if (Array.isArray(pageFilter?.status)) {
      filters = {
        app_status: {
          in: pageFilter.status,
        },
      };
    } else if (pageFilter?.status !== 'all') {
      filters = {
        app_status: {
          eq: pageFilter.status,
        },
      };
    }

    debounceRunQueryGetList({
      pageSize: pageFilter?.status !== 'all' ? 10 : 100,
      currentPage: pageFilter?.currentPage,
      filter: filters,
    });
  }, [pageFilter]);

  useEffect(() => {
    if (getSoaCustomerOrdersCustomerRes.error) {
      console.warn('Could not query getCustomerOrders');
    }
    if (getSoaCustomerOrdersCustomerRes.data) {
      try {
        if (!!getSoaCustomerOrdersCustomerRes.data?.customer?.ordersCustomer) {
          if (
            getSoaCustomerOrdersCustomerRes.data?.customer?.ordersCustomer
              ?.page_info
          ) {
            setPageInfoRes(
              getSoaCustomerOrdersCustomerRes.data.customer.ordersCustomer
                .page_info
            );
          }
          dispatch(
            gotCustomerOrders({
              orders:
                getSoaCustomerOrdersCustomerRes.data.customer.ordersCustomer
                  .items,
              mergeWithExisting: true,
              pageInfo:
                getSoaCustomerOrdersCustomerRes.data.customer.ordersCustomer
                  .page_info!,
            })
          );
        }
      } catch (e) {}
    }
  }, [
    getSoaCustomerOrdersCustomerRes.data,
    getSoaCustomerOrdersCustomerRes.error,
  ]);

  return {
    state: {
      orders,
      pageFilter,
      pageInfoRes,
      isLoading: getSoaCustomerOrdersCustomerRes.loading,
      isUpdatingTotals,
    },
    actions: {
      handleLoadMorePage,
      setFilterStatus,
    },
  };
};
