import { IonContent, IonModal } from '@ionic/react';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { withIsResolvedCart } from '@vjcspy/r/build/modules/checkout/hoc/cart/withIsResolvedCart';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { AppRoute } from '../../../../router/Route';
import iconSprites from '../../assets/images/icon-sprites.svg';

const CartContentContainer: React.FC = combineHOC(
  withCheckoutCartData,
  withIsResolvedCart
)((props) => {
  if (props.state?.cart?.total_quantity === 0) {
    return (
      <>
        <IonContent>
          <div className="cart__items text-center pt-40">
            <div className="container">
              <svg className="fill-text-gray" width="100" height="92">
                <use xlinkHref={iconSprites + '#icon-cart-empty'} />
              </svg>
              <h3 className="mb-20 text-lg-semi">
                Giỏ hàng của bạn đang trống!
              </h3>
              <div
                className="btn-main btn-fs inline-flex items-center ion-justify-content-center btn-pd-xs max-w-160"
                onClick={() => RouterSingleton.push(AppRoute.HOME)}
              >
                Quay lại mua hàng
              </div>
            </div>
          </div>
        </IonContent>
        <UiExtension uiId="BOTTOM_NAV_BAR" />
      </>
    );
  }

  return (
    <>
      <IonContent>
        <UiExtension
          uiId="DEFAULT_LOADING"
          loading={!props.state?.isResolvedCart}
        />
        <UiExtension
          uiId="DEFAULT_LOADING"
          loading={props.state?.isResolvedCart && props.state?.isUpdatingTotals}
        />
        <UiExtension uiId="CART_BRAND_LIST" />

        <UiExtension uiId="CART_PROMO" />
      </IonContent>
      <UiExtension uiId="FOOTER_CART" />
    </>
  );
});

export default CartContentContainer;
