import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo, useState } from 'react';

import iconReward from '../../assets/images/icons/icon-reward.svg';
import iconVoucher from '../../assets/images/icons/icon-voucher.svg';

const CartPromotion: React.FC = combineHOC(
  withCheckoutCartData,
  withAccountState
)((props) => {
  const [showPopupVoucher, setShowPopupVoucher] = useState(false);
  const [showPopupPoints, setShowPopupPoints] = useState(false);

  const handleToggleVoucher = (flag: boolean) => {
    setShowPopupVoucher(flag);
  };
  const handleTogglePoints = (flag: boolean) => {
    setShowPopupPoints(flag);
  };

  const cartDiscount = useMemo(() => {
    if (
      props?.state?.cart?.applied_coupons &&
      props?.state?.cart?.applied_coupons[0]?.code &&
      props.state?.accountState?.customer?.vouchers?.items &&
      props.state?.accountState?.customer?.vouchers?.items?.length > 0
    ) {
      const data = props.state?.accountState?.customer?.vouchers?.items.filter(
        (it: any) =>
          props?.state?.cart?.applied_coupons &&
          it.code === props?.state?.cart?.applied_coupons[0]?.code
      );

      return {
        amount: data[0]?.discount_amount,
        name: data[0]?.name,
      };
    }
    return {
      amount: null,
      name: null,
    };
  }, [props?.state?.cart?.applied_coupons]);

  if (!props.state?.accountState?.customer) {
    return null;
  }
  return (
    <div className="promo__items">
      <div className="container">
        <div className="promo-item promo-voucher">
          <div
            className="promo-btn flex-center btn-arrow-right"
            onClick={() => setShowPopupVoucher(true)}
          >
            <span className="promo-logo">
              <img className="icon" src={iconVoucher} />
            </span>
            <span className="promo-name text-lg">Mã giảm giá</span>
          </div>
          <div className="promo-detail text-gray flex flex-wrap ion-justify-content-between">
            {!cartDiscount?.name && !cartDiscount?.amount && (
              <p className="promo-des">Bạn chưa chọn voucher nào!</p>
            )}
            {cartDiscount?.name && (
              <p className="promo-des">{cartDiscount?.name}</p>
            )}
            {cartDiscount?.amount && (
              <p className="no-mr">
                <UiExtension uiId="CURRENCY" price={cartDiscount?.amount} />
              </p>
            )}
          </div>
        </div>
        <div className="promo-item promo-reward">
          <div
            className="promo-btn flex-center btn-arrow-right"
            onClick={() => setShowPopupPoints(true)}
          >
            <span className="promo-logo">
              <img className="icon" src={iconReward} />
            </span>
            <span className="promo-name text-lg">Dùng reward points</span>
          </div>
          <div className="promo-detail text-gray flex flex-wrap ion-justify-content-between">
            <p className="promo-des">
              Sử dụng{' '}
              {Intl.NumberFormat('en-EN').format(
                props?.state?.cart?.applied_reward_points?.points || 0
              )}{' '}
              reward points
            </p>
          </div>
        </div>
      </div>

      <UiExtension
        uiId="CART_POPUP_VOUCHER"
        close={() => handleToggleVoucher(false)}
        showPopupVoucher={showPopupVoucher}
      />

      <UiExtension
        uiId="CART_POPUP_POINTS"
        close={() => handleTogglePoints(false)}
        showPopupPoints={showPopupPoints}
      />
    </div>
  );
});

export default CartPromotion;
