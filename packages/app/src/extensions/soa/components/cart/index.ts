import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

import { SOA_CART_ITEM_EXT_CFG } from './item';
import { SOA_POPUP_CART_EXT_CFG } from './popup';

export const SOA_CART_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CART_BRAND_LIST',
    component: { lazy: React.lazy(() => import('./CartListBrandItem')) },
  },
  {
    uiId: 'CART_CONTENT_CONTAINER',
    component: { lazy: React.lazy(() => import('./CartContentContainer')) },
  },
  {
    uiId: 'CART_PROMO',
    component: { lazy: React.lazy(() => import('./CartPromotion')) },
  },
  {
    uiId: 'FOOTER_CART',
    component: { lazy: React.lazy(() => import('./FooterCart')) },
  },
  ...SOA_POPUP_CART_EXT_CFG,
  ...SOA_CART_ITEM_EXT_CFG,
];
