import { IonContent, IonModal } from '@ionic/react';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withCartDetailActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCartDetailActions';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

const PopupVoucher: React.FC<{
  close: () => void;
  showPopupVoucher: boolean;
}> = combineHOC(
  withCheckoutCartData,
  withAccountState,
  withCartDetailActions
)((props) => {
  const [couponCode, setCouponCode] = useState('');
  const voucherLists = useMemo(() => {
    let flag: any[] = [];
    if (
      Array.isArray(props.state?.accountState?.customer?.vouchers?.items) &&
      props.state?.accountState?.customer?.vouchers?.items.length
    ) {
      flag = props.state?.accountState?.customer?.vouchers?.items;
    }
    return flag;
  }, [props.state?.accountState?.customer?.vouchers?.items]);

  const handleAddCoupon = useCallback(
    (couponCode: string) => {
      if (typeof props.actions.addCouponCode === 'function') {
        if (props.state?.cart?.id) {
          props.actions.addCouponCode(props.state?.cart?.id, couponCode);
        }
      }
      props.close();
    },
    [props.state?.cart]
  );

  const handleRemoveCoupon = useCallback(() => {
    if (typeof props.actions.removeCouponCode === 'function') {
      if (props.state?.cart?.id) {
        props.actions.removeCouponCode(props.state?.cart?.id);
      }
    }
    props.close();
  }, []);

  useEffect(() => {
    if (props.state.couponCode != couponCode) {
      setCouponCode(props.state.couponCode ?? '');
    }
  }, [props.state.couponCode]);

  return (
    <IonModal isOpen={props?.showPopupVoucher} className="modal-wrapper-full">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => props.close()}
        title="Mã giảm giá"
        showIcon={false}
      />
      <IonContent>
        <UiExtension
          uiId="VOUCHER_LIST"
          voucherLists={voucherLists}
          handleAddCoupon={handleAddCoupon}
          handleRemoveCoupon={handleRemoveCoupon}
          activedCoupon={couponCode}
          voucherSelected={props?.state?.cart?.applied_coupons || null}
        />
      </IonContent>
    </IonModal>
  );
});

export default PopupVoucher;
