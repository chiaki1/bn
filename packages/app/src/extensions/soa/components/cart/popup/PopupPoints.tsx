import { IonModal, IonPage } from '@ionic/react';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const PopupPoints: React.FC<{
  close: () => void;
  showPopupPoints: boolean;
}> = combineHOC(
  withCheckoutCartData,
  withAccountState
)((props) => {
  return (
    <IonModal isOpen={props?.showPopupPoints} className="modal-wrapper-full">
      <IonPage className="page-header-no-mrb">
        <UiExtension uiId="REWARD_POINTS_HEADER" close={props?.close} />
        <UiExtension
          uiId="REWARD_POINTS_OPTION"
          pointUsed={props?.state?.cart?.applied_reward_points?.points}
          pointAll={
            props?.state?.accountState?.customer?.reward_points?.balance?.points
          }
          isUpdatingPoint={props?.state?.isUpdatingPoint}
          close={props?.close}
        />
      </IonPage>
    </IonModal>
  );
});

export default PopupPoints;
