import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_POPUP_CART_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CART_POPUP_VOUCHER',
    component: { lazy: React.lazy(() => import('./PopupVoucher')) },
  },
  {
    uiId: 'CART_POPUP_POINTS',
    component: { lazy: React.lazy(() => import('./PopupPoints')) },
  },
];
