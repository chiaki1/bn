import { IonCol, IonFooter, IonRow, IonToolbar } from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { withCartDetailActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCartDetailActions';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useMemo } from 'react';

import { AppRoute } from '../../../../router/Route';
import { withCartItemsRemove } from '../../hoc/cart/withCartItemsRemove';

const FooterCart: React.FC = combineHOC(
  withCheckoutCartData,
  withCustomer,
  withCartItemsRemove,
  withCartDetailActions
)((props) => {
  const checkDisableCheckout = useMemo(() => {
    if (
      props?.state?.listRemoveStore &&
      props?.state?.cart?.items &&
      props?.state?.cart?.items.length === props?.state?.listRemoveStore.length
    ) {
      return true;
    }
    return false;
  }, [props?.state?.cart?.items && props?.state?.listRemoveStore]);

  const goCheckout = useCallback(() => {
    // props?.actions?.toggleItemCart(null);
    if (
      props?.state?.listRemoveStore &&
      props?.state?.listRemoveStore.length > 0
    ) {
      if (!props.state?.customer) {
        // save after go to login
        RouterSingleton.redirect('/' + AppRoute.ACCOUNT, `/${AppRoute.CART}`);
      } else {
        if (props?.state?.cart?.id) {
          const dataItemsCart: any[] = [];
          props?.state?.listRemoveStore.forEach((item: any) => {
            dataItemsCart.push({ cart_item_id: item, quantity: 0 });
          });

          props?.actions?.updateMultiCartItem({
            cartId: props?.state?.cart?.id,
            dataItemsCart: dataItemsCart,
          });
        }
      }
    } else {
      if (!props.state?.customer) {
        // save after go to login
        RouterSingleton.redirect(
          '/' + AppRoute.ACCOUNT,
          `/${AppRoute.CHECKOUT}`
        );
      } else RouterSingleton.push(`/${AppRoute.CHECKOUT}`);
    }
  }, [props.state?.customer]);

  const totalTemporary = useMemo(() => {
    if (
      props?.state?.listRemoveStore &&
      props?.state?.cart?.items &&
      props?.state?.listRemoveStore.length > 0
    ) {
      let total = props?.state?.cart?.prices?.grand_total?.value || 0;
      const itemRemoved = props?.state?.cart?.items.filter((item: any) =>
        props?.state?.listRemoveStore.includes(item?.id)
      );
      itemRemoved.forEach((it: any) => {
        if (it?.quantity && it?.prices?.price?.value) {
          total = total - it?.quantity * it?.prices?.price?.value;
        }
      });

      if (total > 0) {
        return total;
      } else {
        return 0;
      }
    }
    return props?.state?.cart?.prices?.grand_total?.value;
  }, [props?.state?.cart?.items && props?.state?.listRemoveStore]);

  const quantityTemporary = useMemo(() => {
    if (
      props?.state?.listRemoveStore &&
      props?.state?.cart?.items &&
      props?.state?.listRemoveStore.length > 0
    ) {
      let qty = props?.state?.cart?.items?.length || 0;

      const itemRemoved = props?.state?.cart?.items.filter((item: any) =>
        props?.state?.listRemoveStore.includes(item?.id)
      );
      if (itemRemoved && itemRemoved.length > 0) {
        qty = qty - itemRemoved.length;
      }
      // itemRemoved.forEach((it: any) => {
      //   if (it?.quantity) {
      //     qty = qty - it?.quantity;
      //   }
      // });
      return qty;
    }
    return props?.state?.cart?.items?.length || 0;
  }, [props?.state?.cart?.items && props?.state?.listRemoveStore]);

  return (
    <IonFooter class="footer-cart">
      <IonToolbar className="ion-no-padding">
        <div className="container">
          <p className="text-lg text-center cart-total">
            <span className="price-label">
              Tạm tính ({quantityTemporary || 0} sản phẩm):
            </span>
            <span className="price text-main">
              <UiExtension uiId="CURRENCY" price={totalTemporary || 0} />
            </span>
          </p>
          <IonRow className="mr-row">
            <IonCol size="6">
              <div
                className="btn-reset-black btn-fs flex-center-center btn-pd-xs"
                onClick={() => {
                  RouterSingleton.push(`/${AppRoute.HOME}`);
                }}
              >
                Trang chủ
              </div>
            </IonCol>
            <IonCol size="6">
              <div
                className={clsx(
                  'btn-black btn-fs flex-center-center btn-pd-xs',
                  checkDisableCheckout && 'disabled'
                )}
                onClick={() => {
                  goCheckout();
                }}
              >
                Thanh toán ngay
              </div>
            </IonCol>
          </IonRow>
        </div>
      </IonToolbar>
    </IonFooter>
  );
});

export default FooterCart;
