import { withCartDetailActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCartDetailActions';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withCartListBrandData } from '../../hoc/cart/withCartListBrandData';
import { withClearCartActions } from '../../hoc/cart/withClearCartActions';

const CartListBrandItem: React.FC = combineHOC(
  withCartListBrandData,
  withCheckoutCartData,
  withCartDetailActions,
  withClearCartActions
)((props) => {
  return (
    <div className="cart__items">
      <div className="container">
        <div className="cart-check-item flex-center ion-justify-content-between">
          <label className="label-input text-lg-semi">
            <input
              type="checkbox"
              checked={!props?.state?.unCheckAllItem}
              onChange={() => {
                props?.actions?.unCheckAllBrandAction();
              }}
            />
            <span className="label">
              Tất cả ({props?.state?.cart?.items?.length || 0} sản phẩm)
            </span>
          </label>
          <button
            className="btn-link btn-delete"
            onClick={() => {
              if (props?.state?.cart?.id) {
                props?.actions?.clearCart(props?.state?.cart?.id);
              }
            }}
          >
            <i className="fal fa-trash-alt" />
          </button>
        </div>
        <hr className="un-container" />
        {props?.state?.listItemShow.map((itemCart: any) => (
          <UiExtension
            uiId="CART_BRAND_ITEM"
            itemCart={itemCart}
            cartId={props?.state?.cart?.id}
            key={itemCart?.name}
            toggleItemCart={props?.actions?.toggleItemCart}
            listRemoveItem={props?.state?.listRemoveItem}
            checkedBrand={props?.actions?.checkedBrand}
            unCheckBrandAction={props?.actions?.unCheckBrandAction}
          />
        ))}
      </div>
      <hr />
    </div>
  );
});

export default CartListBrandItem;
