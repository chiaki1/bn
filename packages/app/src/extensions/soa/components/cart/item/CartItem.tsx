import { withCartDetailActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCartDetailActions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback } from 'react';

const CartItem: React.FC = combineHOC(withCartDetailActions)((props) => {
  const removeCartItem = useCallback(
    (cartItemId: number) => {
      const cartId: any = props?.cartId;
      if (typeof props.actions.removeItemFromCart === 'function') {
        props.actions.removeItemFromCart(cartId, cartItemId);
      }
    },
    [props.cartId]
  );

  const updateQty = useCallback(
    (cartItemId: number, qty: any) => {
      if (props.actions?.updateCartItem && props?.cartId) {
        props.actions.updateCartItem({
          cartId: props.cartId,
          cartItemId,
          qty,
        });
      }
    },
    [props?.cartId]
  );
  return (
    <div className="minicart-item">
      <div className="minicart-item-detail">
        <div
          className="product-photo"
          onClick={() => {
            RouterSingleton.push(`${props?.item?.product?.url_key}.html`);
          }}
        >
          <img src={props?.item?.product?.small_image?.url} />
        </div>
        <div className="product-item-detail">
          <h3
            className="product-name text-lg"
            onClick={() => {
              RouterSingleton.push(`${props?.item?.product?.url_key}.html`);
            }}
          >
            {props?.item?.product?.name}
          </h3>
          <ul className="product-options text-small text-gray list-reset">
            {Array.isArray(props?.item?.configurable_options) &&
              props?.item?.configurable_options.map((option: any) => {
                return (
                  <li
                    className="option-item"
                    key={option?.configurable_product_option_uid}
                  >
                    {option['option_label']}:
                    <span className="option-value">
                      {option['value_label']}
                    </span>
                  </li>
                );
              })}
          </ul>
          <div className="price-box">
            <p className="special-price">
              <UiExtension
                uiId="CURRENCY"
                price={props?.item?.prices?.price?.value}
              />
            </p>
            {/*<p className="old-price">700.000đ</p>*/}
            {/*<p className="special-price">500.000₫</p>*/}
            {props?.item?.product?.price_range?.maximum_price?.regular_price
              ?.value &&
              props?.item?.product?.price_range?.maximum_price?.regular_price
                ?.value >
                props?.item?.prices?.row_total_including_tax?.value && (
                <p className="old-price">
                  <UiExtension
                    uiId="CURRENCY"
                    price={
                      props?.item?.product?.price_range?.maximum_price
                        ?.regular_price?.value
                    }
                  />
                </p>
              )}
          </div>
          <div className="minicart-actions flex ion-justify-content-between ">
            <UiExtension
              uiId="CART_CONTENT_ITEM_QTY"
              qty={props?.item?.quantity}
              whenUpdateItemQty={(qty: number) => {
                updateQty(parseInt(props?.item.id), qty);
              }}
            />
            <button
              className="btn-link btn-delete"
              onClick={() => removeCartItem(parseInt(props.item?.id))}
            >
              <i className="fal fa-trash-alt" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
});

export default CartItem;
