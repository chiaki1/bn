import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useState } from 'react';

import { AppRoute } from '../../../../../router/Route';

const CartBrandItem: React.FC = combineHOC()((props) => {
  const [showBrand, setShowBrand] = useState(true);

  return (
    <div className="cart-vendor">
      <div className="cart-vendor-head flex-center ion-justify-content-between">
        <label className="label-input text-lg-semi">
          <input
            type="checkbox"
            checked={props?.checkedBrand(props?.itemCart?.brand_id || null)}
            onChange={() => {
              props?.unCheckBrandAction(
                props?.itemCart?.brand_id || null,
                props?.checkedBrand(props?.itemCart?.brand_id || null)
              );
            }}
          />
          <span className="label">
            <span
              onClick={() => {
                RouterSingleton.push(
                  '/' + AppRoute.BRAND_DETAIL + '/' + props?.itemCart?.brand_id
                );
              }}
            >
              {props?.itemCart?.name} (
              {props?.itemCart?.countItem ||
                props?.itemCart?.items?.length ||
                0}{' '}
              sản phẩm)
            </span>
          </span>
        </label>
        <button
          className="btn-link btn-arr-right"
          onClick={() => {
            setShowBrand(!showBrand);
          }}
        >
          {showBrand ? (
            <i className="far fa-chevron-right" />
          ) : (
            <i className="far fa-chevron-down" />
          )}
        </button>
      </div>
      {showBrand &&
        props?.itemCart?.items?.length > 0 &&
        props?.itemCart?.items?.map((item: any) => (
          <div className="cart-vendor-item" key={item?.id}>
            <div className="cart-checkbox">
              <label className="label-input text-lg-semi">
                <input
                  type="checkbox"
                  checked={!props?.listRemoveItem.includes(item?.id)}
                  onChange={() => {
                    props?.toggleItemCart(item?.id);
                  }}
                />
                <span className="label" />
              </label>
            </div>
            <UiExtension uiId="CART_ITEM" item={item} cartId={props?.cartId} />
          </div>
        ))}
    </div>
  );
});

export default CartBrandItem;
