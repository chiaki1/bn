import _ from 'lodash';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

const ContentItemQty: React.FC<{
  qty: number;
  whenUpdateItemQty: (qty: number) => void;
  canZero?: boolean;
  imme?: boolean;
}> = (props) => {
  const [qty, setQty] = useState(props.qty);
  const [isUpdatingInput, setIsUpdatingInput] = useState(false);

  const isValidNumber = useCallback((number: any) => {
    if (isNaN(number)) {
      return false;
    }
    if (props?.canZero) {
      return parseInt(number + '') >= 0;
    } else {
      return parseInt(number + '') > 0;
    }
  }, []);

  useEffect(() => {
    if (!isUpdatingInput) {
      setQty(props.qty);
    }
  }, [props.qty]);

  const handleInputChange = useCallback((event: any) => {
    setQty(event.target.value);
  }, []);

  const updateQty = useCallback(() => {
    try {
      if (isValidNumber(qty)) {
      } else {
        setQty(1);
      }
    } catch (e) {
      setQty(1);
    }
    setIsUpdatingInput(false);
  }, [qty]);

  const debounceUpdateQty = useMemo(
    () =>
      _.debounce(
        (qty: number) => {
          if (props.whenUpdateItemQty) {
            props.whenUpdateItemQty(qty);
          }
        },
        props?.imme ? 0 : 1234
      ),
    []
  );

  useEffect(() => {
    if (qty != props.qty && !isUpdatingInput) {
      debounceUpdateQty(qty);
    }
  }, [qty, isUpdatingInput]);

  return (
    <div className="flex form-qty">
      <input
        className="input-text qty-item"
        // type="number"
        // inputMode="decimal"
        value={qty}
        disabled={true}
        onChange={handleInputChange}
        onFocus={(event) => {
          event.target.select();
          setIsUpdatingInput(true);
        }}
        onBlur={updateQty}
      />
      <span
        className="qty-change qty-item  orderFirst"
        onClick={() => setQty(isValidNumber(qty - 1) ? qty - 1 : qty)}
      >
        <i className="fal fa-minus" />
      </span>
      <span className="qty-change qty-item " onClick={() => setQty(qty + 1)}>
        <i className="fal fa-plus" />
      </span>
    </div>
  );
};

export default ContentItemQty;
