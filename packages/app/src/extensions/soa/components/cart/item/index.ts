import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CART_ITEM_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CART_BRAND_ITEM',
    component: { lazy: React.lazy(() => import('./CartBrandItem')) },
  },
  {
    uiId: 'CART_ITEM',
    component: { lazy: React.lazy(() => import('./CartItem')) },
  },
  {
    uiId: 'CART_CONTENT_ITEM_QTY',
    component: { lazy: React.lazy(() => import('./ContentItemQty')) },
  },
];
