import { IonCol, IonHeader, IonRow, IonToolbar } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import IconPhone from '../../assets/images/icons/icon-phone.svg';

const HeaderChat: React.FC = combineHOC()((props) => {
  return (
    <IonHeader className="page-header ion-no-border header-account">
      <div className="header-content text-center">
        <IonToolbar color="nobg">
          <div className="header-chat flex-center flex-content-bw container">
            <button className="btn-link-white btn-back">
              <i className="fal fa-chevron-left"></i>
            </button>
            <h1 className="account-pages-title">Chat</h1>
            <img src={IconPhone} alt="" />
          </div>
        </IonToolbar>
      </div>
    </IonHeader>
  );
});

export default HeaderChat;
