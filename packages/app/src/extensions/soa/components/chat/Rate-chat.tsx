import { IonCol, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconClose from '../../assets/images/icons/icon-close.svg';

const RateChat: React.FC = combineHOC()((props) => {
  return (
    <div className="popup-rate-chat">
      <div className="container">
        <div className="rate-chat">
          <h3 className="title-rate-chat">
            Bạn vui lòng đánh giá chất lượng tư vấn <br /> của nhân viên hỗ trợ
          </h3>
          <ul className="items-star">
            <li className="item-star">
              <i className="fas fa-star"></i>
            </li>
            <li className="item-star">
              <i className="fas fa-star"></i>
            </li>{' '}
            <li className="item-star">
              <i className="fas fa-star"></i>
            </li>{' '}
            <li className="item-star">
              <i className="fas fa-star"></i>
            </li>{' '}
            <li className="item-star disabled">
              <i className="fas fa-star"></i>
            </li>
          </ul>
          <div className="comment-chat">
            <h4>Ý kiến của bạn</h4>
            <div className="textarea">
              <textarea rows={6}></textarea>
            </div>
          </div>
          <ul className="choose-option-comment">
            <li className="item-option-comment">Nhân viên nhiệt tình</li>
            <li className="item-option-comment">Tư vấn cẩn thận</li>
            <li className="item-option-comment">Nhân viên nhiệt tình</li>
          </ul>
          <div className="button-rate-chat">
            <button className="btn-reset-black btn-xl">Bỏ qua</button>
            <button className="btn-black btn-xl">Gửi ý kiến</button>
          </div>
        </div>
      </div>
      <button className="btn-close">
        <img src={iconClose} alt="" />
      </button>
    </div>
  );
});

export default RateChat;
