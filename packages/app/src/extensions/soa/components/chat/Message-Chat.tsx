import { IonCol, IonFooter, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconOtherimg from '../../assets/images/icons/icon-other-img.svg';
import iconSend from '../../assets/images/icons/icon-send.svg';
import imgAvatar from '../../assets/images/image/img-avatar-message.svg';

const ChatMessage: React.FC = combineHOC()((props) => {
  return (
    <div className="chat-message">
      <div className="container">
        <div className="message-box">
          <div className="message-left item-message flex">
            <div className="avatar">
              <img src={imgAvatar} alt="" />
            </div>
            <div className="text-message">
              <p>How are you?</p>
              <p>Haha that's terrifying 😂</p>
              <p>omg, this is amazing</p>
            </div>
          </div>
          <div className="message-right item-message flex">
            <div className="avatar">
              <img src={imgAvatar} alt="" />
            </div>
            <div className="text-message">
              <p>How are you?</p>
              <p>Haha that's terrifying 😂</p>
              <p>omg, this is amazing</p>
            </div>
          </div>
        </div>
        <div className="toolbar-chat">
          <div className="other-img-chat">
            <img src={iconOtherimg} alt="" />
          </div>
          <div className="input-chat">
            <input type="text" placeholder="value" />
          </div>
          <button className="submit">
            <img src={iconSend} alt="" />
          </button>
        </div>
      </div>
    </div>
  );
});

export default ChatMessage;
