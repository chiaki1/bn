import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CHAT_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CHAT_HOME',
    component: { lazy: React.lazy(() => import('./Home-Chat')) },
  },
  {
    uiId: 'CHAT_MESSAGE',
    component: { lazy: React.lazy(() => import('./Message-Chat')) },
  },
  {
    uiId: 'HEADER_CHAT',
    component: { lazy: React.lazy(() => import('./Header-Chat')) },
  },
  {
    uiId: 'RATE_CHAT',
    component: { lazy: React.lazy(() => import('./Rate-chat')) },
  },
];
