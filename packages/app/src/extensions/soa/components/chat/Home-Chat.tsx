import { IonCol, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import imgchathome from '../../assets/images/image/img-chat-home.svg';

const ChatHomePage: React.FC = combineHOC()((props) => {
  return (
    <div className="chat-home">
      <div className="container">
        <div className="img-chat-home">
          <img src={imgchathome} alt="" />
          <p>
            Chọn phương thức liên hệ <br />
            để chúng tôi có thể hỗ trợ bạn
          </p>
        </div>
        <div className="group-btn-chat">
          <button className="btn-fs ">Gọi điện thoại</button>
          <button className="btn-fs ">Chat</button>
        </div>
      </div>
    </div>
  );
});

export default ChatHomePage;
