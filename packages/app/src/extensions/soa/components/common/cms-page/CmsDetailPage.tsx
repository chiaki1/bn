import { IonContent } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withCmsContentData } from '../../../hoc/cms-content/withCmsContentData';

const CmsDetailPage: React.FC = combineHOC(withCmsContentData)((props) => {
  return (
    <>
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title={props.state?.accountPolicyData?.content_heading}
      />
      <IonContent>
        <div className="account-sidebar account-sidebar-cms">
          <div className="container">
            <div className="sidebar-person">
              <h2 className="title-account no-mr">
                {props.state?.accountPolicyData?.content_heading}
              </h2>
              <div className="btn-list-item">
                <div
                  dangerouslySetInnerHTML={{
                    __html: props.state?.accountPolicyData?.content || '',
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </>
  );
});

export default CmsDetailPage;
