import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CMS_DETAIL_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CMS_DETAIL_PAGE',
    component: { lazy: React.lazy(() => import('./CmsDetailPage')) },
  },
];
