import { IonCol, IonHeader, IonRow, IonToolbar } from '@ionic/react';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import iconCamera from '../../../assets/images/icons/icon-camera.svg';
import iconpoints from '../../../assets/images/icons/icon-points.svg';

const RewardPointsHeader: React.FC<{
  close?: () => void;
  title?: string;
}> = combineHOC(withAccountState)((props) => {
  const avatar = useMemo(() => {
    // @ts-ignore
    return props?.state?.accountState?.customer?.avatar || iconCamera;
  }, [props?.state?.accountState?.customer]);

  return (
    <IonHeader
      className="page-header ion-no-border header-account"
      translucent={false}
    >
      <div className="header-content text-center">
        <IonToolbar color="nobg">
          <div className="header-container flex-center">
            <button
              className="btn-link-white btn-back"
              onClick={() => {
                if (props?.close) {
                  props?.close();
                } else {
                  RouterSingleton.back();
                }
              }}
            >
              <i className="fal fa-chevron-left" />
            </button>
            <h1 className="account-pages-title">
              {props?.title || 'Dùng Reward Points'}
            </h1>
          </div>
        </IonToolbar>

        <div className="reward-points-header">
          <div className="box-reward-points flex flex-content-bw">
            <div className="reward-account flex flex-center">
              <div className="account-info">
                <div className="account-info-item flex-center pd-vertical-md">
                  <div className="customer-avatar flex-center-center rounded bg-gray">
                    {!!avatar ? (
                      <img className="img-cover round" src={avatar} alt="" />
                    ) : (
                      <img className="icon-camera" src={iconCamera} alt="" />
                    )}
                  </div>
                </div>
              </div>
              <div className="reward-account_name">
                <h3>
                  {props?.state?.accountState?.customer?.firstname +
                    ' ' +
                    props?.state?.accountState?.customer?.lastname}
                </h3>
                <p>Hạng: Member</p>
              </div>
            </div>
            <div className="points flex flex-center">
              {Intl.NumberFormat('en-EN').format(
                props?.state?.accountState?.customer?.reward_points?.balance
                  ?.points || 0
              )}{' '}
              <img src={iconpoints} alt="" />
            </div>
          </div>
        </div>
      </div>
    </IonHeader>
  );
});

export default RewardPointsHeader;
