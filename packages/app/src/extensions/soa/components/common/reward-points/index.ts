import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_REWARD_POINTS_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'REWARD_POINTS_HEADER',
    component: { lazy: React.lazy(() => import('./RewardPointsHeader')) },
  },
  {
    uiId: 'REWARD_POINTS_OPTION',
    component: { lazy: React.lazy(() => import('./RewardPointsOption')) },
  },
];
