import { IonContent, IonFooter, IonToolbar } from '@ionic/react';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withRewardPointData } from '@vjcspy/r/build/modules/account/hoc/withRewardPointData';
import { withRewardPointActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withRewardPointActions';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

import iconcheckwhite from '../../../assets/images/icons/icon-check-white.svg';
import iconcloseinput from '../../../assets/images/icons/icon-close-input.svg';

const RewardPointsOption: React.FC<{
  close?: () => void;
  pointAll: number;
  pointUsed: number;
  isUpdatingPoint: boolean;
}> = combineHOC(
  withRewardPointData,
  withRewardPointActions,
  withAccountState
)((props) => {
  const arrVal = useMemo(() => {
    if (
      // @ts-ignore
      props?.state?.accountState?.customer?.reward_points?.suggest_use &&
      Array.isArray(
        // @ts-ignore
        props?.state?.accountState?.customer?.reward_points?.suggest_use
      )
    ) {
      // @ts-ignore
      return props?.state?.accountState?.customer?.reward_points?.suggest_use;
    }
    return [];
  }, [props?.state?.accountState?.customer?.reward_points]);
  const [valPoint, setValPoint] = useState(props.pointUsed ?? '');
  const [checkSet, setCheckSet] = useState(false);

  const whenSubmit = useCallback(() => {
    if (
      typeof props.actions?.setRewardPointToCartWithPoint === 'function' &&
      valPoint
    ) {
      props.actions?.setRewardPointToCartWithPoint(valPoint);
    } else {
      if (typeof props.actions?.removeRewardPointFromCart === 'function') {
        props.actions?.removeRewardPointFromCart();
      }
    }
  }, [valPoint]);

  useEffect(() => {
    if (props?.isUpdatingPoint === false && checkSet) {
      setCheckSet(false);
      props.close();
    }
  }, [props?.isUpdatingPoint, checkSet]);

  const whenClear = useCallback(() => {
    if (typeof props.actions?.removeRewardPointFromCart === 'function') {
      props.actions?.removeRewardPointFromCart();
    }
  }, [props.onCancel]);

  const handleChange = useCallback(
    (e: any) => {
      if (props?.pointAll && e.target.value > props?.pointAll) {
        setValPoint(props?.pointAll);
      } else {
        setValPoint(e.target.value);
      }
    },
    [valPoint]
  );

  return (
    <>
      <IonContent>
        <div className="reward-points-options">
          <div className="container">
            <div className="form-address-add-item">
              <label htmlFor="">Sử dụng Reward Points</label>
              <div className="form-address-add-item_input">
                <input
                  type="number"
                  inputMode="decimal"
                  value={valPoint}
                  onChange={handleChange}
                  placeholder="Nhập số điểm"
                  disabled={!props?.pointAll}
                />
                <img
                  src={iconcloseinput}
                  alt=""
                  onClick={() => {
                    setValPoint('');
                  }}
                />
              </div>
            </div>
            <div className="swatch-attr swatch-size flex ">
              {!props?.pointAll && (
                <span className="error-message text-small">
                  Bạn không có point nào để sử dụng!
                </span>
              )}
              <div className="listbox">
                <ul className="swatch-list list-reset flex flex-wrap">
                  {arrVal
                    .filter((it: any) => it <= props?.pointAll)
                    .map((item: any) => (
                      <li
                        className="swatch-item label "
                        key={`pointxx-${item}`}
                      >
                        <label
                          className="label-input"
                          onClick={() => {
                            setValPoint(parseInt(item));
                          }}
                        >
                          <input name="amshopby" type="radio" />
                          <span className="label">
                            {Intl.NumberFormat('en-EN').format(item)}
                          </span>
                        </label>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </IonContent>
      <IonFooter class="footer-shipping footer-cart">
        <IonToolbar className="ion-no-padding">
          <div className="container">
            <button
              className="btn btn-black btn-fs btn-success-address"
              onClick={() => {
                whenSubmit();
                setCheckSet(true);
              }}
            >
              <img src={iconcheckwhite} alt="" className="pd-lft-rght" />
              Đồng ý
            </button>
          </div>
        </IonToolbar>
      </IonFooter>
    </>
  );
});

export default RewardPointsOption;
