import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import moment from 'moment/moment';
import React, { useMemo } from 'react';

const FlashSale: React.FC<{ flashSaleData: any }> = combineHOC()((props) => {
  const checkFlashSale = useMemo(() => {
    if (
      props?.flashSaleData?.catalog_rule?.schedule &&
      Array.isArray(props?.flashSaleData?.catalog_rule?.schedule) &&
      props?.flashSaleData?.catalog_rule?.schedule.filter(
        (item: any) =>
          item?.start_time &&
          item?.end_time &&
          moment().isBefore(moment(item?.end_time)) &&
          moment().isAfter(moment(item?.start_time))
      ).length > 0
    ) {
      return true;
    }
    return false;
  }, [props?.flashSaleData]);

  const flashSale = useMemo(() => {
    if (checkFlashSale) {
      return props?.flashSaleData?.catalog_rule?.schedule.filter(
        (item: any) =>
          item?.start_time &&
          item?.end_time &&
          moment().isBefore(moment(item?.end_time)) &&
          moment().isAfter(moment(item?.start_time))
      )[0];
    }
    return false;
  }, [checkFlashSale, props?.flashSaleData]);

  const imgBackground = useMemo(() => {
    if (props?.flashSaleData?.background_image) {
      return props?.flashSaleData?.background_image;
    } else {
      return props?.flashSaleData?.banner_image;
    }
  }, [props?.flashSaleData]);

  return (
    <>
      {checkFlashSale &&
        Array.isArray(flashSale?.products?.items) &&
        flashSale?.products?.items.length > 0 && (
          <div className={'home__flashsale mgb-block'}>
            <div
              className={'container'}
              style={{
                backgroundImage: 'url("' + imgBackground + '") ',
                backgroundSize: 'cover',
              }}
            >
              <UiExtension uiId="FLASH_SALE_COUNT_DOWN" flashSale={flashSale} />
              <UiExtension
                uiId="SLIDER_LIST_PRODUCT"
                listItems={flashSale?.products?.items || []}
              />
            </div>
          </div>
        )}
    </>
  );
});

export default FlashSale;
