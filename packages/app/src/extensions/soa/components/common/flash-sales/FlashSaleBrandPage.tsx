import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const FlashSaleBrandPage: React.FC = combineHOC()((props) => {
  if (!props?.flashSaleData) {
    return null;
  }
  return <UiExtension uiId="FLASH_SALE" flashSaleData={props?.flashSaleData} />;
});

export default FlashSaleBrandPage;
