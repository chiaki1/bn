import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_FLASH_SALE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'FLASH_SALE_HOME_PAGE',
    component: { lazy: React.lazy(() => import('./FlashSaleHomePage')) },
  },
  {
    uiId: 'FLASH_SALE_BRAND_PAGE',
    component: { lazy: React.lazy(() => import('./FlashSaleBrandPage')) },
  },
  {
    uiId: 'FLASH_SALE',
    component: { lazy: React.lazy(() => import('./FlashSale')) },
  },
  {
    uiId: 'FLASH_SALE_COUNT_DOWN',
    component: { lazy: React.lazy(() => import('./FlashSalesCountDown')) },
  },
];
