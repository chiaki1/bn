import { IonButton, IonCol, IonRow } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import moment, { Duration } from 'moment/moment';
import React, { useEffect, useMemo, useState } from 'react';

import { withResolveProductFilterCustom } from '../../../../../modules/catalog/hoc/router/withResolveProductFilterCustom';
import { AppRoute } from '../../../../../router/Route';

const FlashSalesCountDown: React.FC<{ flashSale: any }> = combineHOC(
  withResolveProductFilterCustom
)((props) => {
  const [countdown, setCountdown] = useState<Duration>();
  const eventTime = useMemo(() => {
    if (props?.flashSale?.end_time) {
      return moment(props?.flashSale?.end_time, 'YYYY/MM/DD hh:mm');
    }

    return undefined;
  }, [props?.flashSale]);

  useEffect(() => {
    let interval: any;
    if (eventTime && eventTime.isValid() && eventTime.diff(moment()) > 0) {
      interval = setInterval(() => {
        setCountdown(moment.duration(eventTime.diff(moment())));
      }, 1000);
    } else {
      setCountdown(undefined);
    }

    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  }, [eventTime]);

  return (
    <>
      <IonRow className="ion-justify-content-end mr-row fs-row-title">
        <IonCol class="no-padd text-right">
          <div
            className="countdown-timer ion-justify-content-end flex"
            style={{ minHeight: 32 }}
          >
            {!!countdown && (
              <>
                <IonButton
                  size="small"
                  color="secondary"
                  className="countdown-item hour ion-button-pd-xs"
                >
                  {(
                    (countdown.days() * 24 + countdown.hours() > 9
                      ? countdown.days() * 24 + countdown.hours()
                      : '0' + countdown.hours()) + ''
                  )
                    .split('')
                    .map((c, index) => (
                      <span key={index}>{c}</span>
                    ))}
                </IonButton>
                <IonButton
                  size="small"
                  color="secondary"
                  className="countdown-item minute ion-button-pd-xs"
                >
                  {(
                    (countdown.minutes() > 9
                      ? countdown.minutes()
                      : '0' + countdown.minutes()) + ''
                  )
                    .split('')
                    .map((c, index) => (
                      <span key={index}>{c}</span>
                    ))}
                </IonButton>
                <IonButton
                  size="small"
                  color="secondary"
                  className="countdown-item second ion-button-pd-xs"
                >
                  {(
                    (countdown.seconds() > 9
                      ? countdown.seconds()
                      : '0' + countdown.seconds()) + ''
                  )
                    .split('')
                    .map((c, index) => (
                      <span key={index}>{c}</span>
                    ))}
                </IonButton>
              </>
            )}
          </div>
          <IonButton
            className="ion-button-no-padd ion-button-pd-xs"
            color="nobg"
            onClick={() => {
              if (JSON.parse(props?.flashSale?.data_filters)?.filter) {
                props?.actions?.setFilterToStore(
                  JSON.parse(props?.flashSale?.data_filters)?.filter
                );
                RouterSingleton.push('/' + AppRoute.PRODUCTS_LIST);
              }
            }}
          >
            Xem thêm
          </IonButton>
        </IonCol>
      </IonRow>
    </>
  );
});

export default FlashSalesCountDown;
