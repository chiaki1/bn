import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import { withSoaHomeFlashSaleData } from '../../../hoc/home-page/withSoaHomeFlashSaleData';

const FlashSaleHomePage: React.FC = combineHOC(withSoaHomeFlashSaleData)(
  (props) => {
    if (
      !props?.flashSaleData ||
      (Array.isArray(props?.flashSaleData) && props?.flashSaleData.length === 0)
    ) {
      return null;
    }
    return (
      <>
        {props?.flashSaleData.map((flash: any) => (
          <UiExtension
            uiId="FLASH_SALE"
            flashSaleData={flash}
            key={flash?.block_id}
          />
        ))}
      </>
    );
  }
);

export default FlashSaleHomePage;
