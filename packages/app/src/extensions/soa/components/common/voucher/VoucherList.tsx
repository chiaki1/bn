import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import moment from 'moment/moment';
import React, { useCallback, useMemo } from 'react';

import imgVoucher from '../../../assets/images/image/voucher.png';

const VoucherList: React.FC<{
  voucherLists: [];
  handleAddCoupon: (coupon: any) => void;
  activeCoupon: any;
  voucherSelected?: any;
}> = combineHOC()((props) => {
  const handleAdd = useCallback((coupon: any) => {
    if (typeof props.handleAddCoupon === 'function') {
      props.handleAddCoupon(coupon);
    }
  }, []);

  const voucherActive = useMemo(() => {
    return props?.voucherLists.filter((item: any) => !item?.used);
  }, [props?.voucherLists]);

  return (
    <div className="voucher-list">
      <div className="container">
        <h3 className="text-lg adrs-deli">Lựa chọn mã giảm giá</h3>
        <div className="wrp-voucher-list">
          {voucherActive.length > 0 ? (
            <>
              {voucherActive.map((voucher: any) => (
                <div className="voucher-item" key={voucher?.name}>
                  <div className="box-voucher">
                    <img src={imgVoucher} alt="" />
                    <div className="voucher-item_desc flex flex-center-center flex-content-bw">
                      <div className="voucher-item_desc__txt">
                        <p className="voucher-item-title">
                          ({voucher?.name}) Voucher{' '}
                          <UiExtension
                            uiId="CURRENCY"
                            price={voucher?.discount_amount || 0}
                          />
                        </p>
                        <p className="text-xs">
                          Áp dụng:{' '}
                          {moment(voucher?.form_date).isValid()
                            ? moment(voucher?.form_date).format('DD/MM/YYYY')
                            : '...'}{' '}
                          -{' '}
                          {moment(voucher?.to_date).isValid()
                            ? moment(voucher?.to_date).format('DD/MM/YYYY')
                            : '...'}
                        </p>
                      </div>
                      {props?.voucherSelected &&
                      props?.voucherSelected[0]?.code === voucher?.code ? (
                        <button
                          className="btn btn-reset"
                          onClick={() => {
                            props?.handleRemoveCoupon();
                          }}
                        >
                          Hủy bỏ
                        </button>
                      ) : (
                        <button
                          className="btn btn-reset"
                          onClick={() => {
                            handleAdd(voucher?.code);
                          }}
                        >
                          Sử dụng
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              ))}
            </>
          ) : (
            <div className="voucher-item">
              <span>Hiện không có voucher nào khả dụng.</span>
            </div>
          )}
        </div>
      </div>
    </div>
  );
});

export default VoucherList;
