import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_VOUCHER_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'VOUCHER_LIST',
    component: { lazy: React.lazy(() => import('./VoucherList')) },
  },
  {
    uiId: 'VOUCHER_LIST_ON_PAGE',
    component: { lazy: React.lazy(() => import('./VoucherListOnPage')) },
  },
];
