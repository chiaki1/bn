import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import moment from 'moment/moment';
import React, { useCallback, useMemo } from 'react';

import imgVoucher from '../../../assets/images/image/voucher.png';

const VoucherList = combineHOC(withAccountState)((props) => {
  console.log('annnn', props);
  const voucherLists = useMemo(() => {
    let flag: any[] = [];
    if (
      Array.isArray(props.state?.accountState?.customer?.vouchers?.items) &&
      props.state?.accountState?.customer?.vouchers?.items.length
    ) {
      flag = props.state?.accountState?.customer?.vouchers?.items.filter(
        (item: any) => !item?.used
      );
    }
    return flag;
  }, [props.state?.accountState?.customer?.vouchers?.items]);

  return (
    <div className="voucher-list">
      <div className="container">
        <h3 className="text-lg adrs-deli">Danh sách mã giảm giá</h3>
        <div className="wrp-voucher-list">
          {voucherLists.length > 0 ? (
            <>
              {voucherLists
                .filter((item: any) => !item?.used)
                .map((voucher: any) => (
                  <div className="voucher-item" key={voucher?.name}>
                    <div className="box-voucher">
                      <img src={imgVoucher} alt="" />
                      <div className="voucher-item_desc flex flex-center-center flex-content-bw">
                        <div className="voucher-item_desc__txt">
                          <p className="voucher-item-title">
                            ({voucher?.name}) Voucher{' '}
                            <UiExtension
                              uiId="CURRENCY"
                              price={voucher?.discount_amount || 0}
                            />
                          </p>
                          <p className="text-xs">
                            Áp dụng:{' '}
                            {moment(voucher?.form_date).isValid()
                              ? moment(voucher?.form_date).format('DD/MM/YYYY')
                              : '...'}{' '}
                            -{' '}
                            {moment(voucher?.to_date).isValid()
                              ? moment(voucher?.to_date).format('DD/MM/YYYY')
                              : '...'}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
            </>
          ) : (
            <div className="voucher-item">
              <span>Hiện không có voucher nào khả dụng.</span>
            </div>
          )}
        </div>
      </div>
    </div>
  );
});

export default VoucherList;
