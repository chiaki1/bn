import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withSoaHomeAdditionalData } from '../../../hoc/home-page/withSoaHomeAdditionalData';

const AdditionalBlockHomePage = combineHOC(withSoaHomeAdditionalData)(
  (props) => {
    if (
      !props?.additionalBlockData ||
      (props?.additionalBlockData &&
        Array.isArray(props?.additionalBlockData) &&
        props?.additionalBlockData.length === 0)
    ) {
      return null;
    }
    return (
      <UiExtension
        uiId="ADDITIONAL_BOCK_CONTENT"
        additionalBlockData={props?.additionalBlockData || []}
      />
    );
  }
);

export default AdditionalBlockHomePage;
