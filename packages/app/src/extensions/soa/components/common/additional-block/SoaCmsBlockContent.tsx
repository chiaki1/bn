import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const SoaCmsBlockContent: React.FC<{ dataCmsContentCampaign: any }> =
  combineHOC()((props) => {
    return (
      <div className={'home__sales mgb-block'}>
        <div
          dangerouslySetInnerHTML={{
            __html: `${props.dataCmsContentCampaign?.html_content?.content}`,
          }}
        />
      </div>
    );
  });

export default SoaCmsBlockContent;
