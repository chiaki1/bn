import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ADDITIONAL_BLOCK_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ADDITIONAL_BOCK_HOME_PAGE',
    component: { lazy: React.lazy(() => import('./AdditionalBlockHomePage')) },
  },
  {
    uiId: 'ADDITIONAL_BOCK_BRAND_PAGE',
    component: { lazy: React.lazy(() => import('./AdditionalBlockBrandPage')) },
  },
  {
    uiId: 'ADDITIONAL_BOCK_CONTENT',
    component: { lazy: React.lazy(() => import('./AdditionalBlockContent')) },
  },
  {
    uiId: 'SOA_CMS_BLOCK_CONTENT',
    component: { lazy: React.lazy(() => import('./SoaCmsBlockContent')) },
  },
];
