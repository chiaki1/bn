import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import {
  BLOCK_CAMPAIGN_IMAGE_LISTING,
  BLOCK_CAMPAIGN_SLIDER,
  CMS_CONTENT_CAMPAIGN,
  TYPES_CONTENT_BANNER,
  TYPES_CONTENT_FLASH_SALE,
} from '../../../values/SOA_CONTENT_HOME_PAGE';

const AdditionalBlockContent: React.FC<{ additionalBlockData: any[] }> =
  combineHOC()((props) => {
    const ContentBlock = useMemo(() => {
      if (props?.additionalBlockData) {
        return (
          <>
            {props?.additionalBlockData
              .slice()
              .sort((a: any, b: any) => a.position - b.position)
              // .filter((it: any) => it?.type === TYPES_CONTENT_BANNER)
              .map((item: any) => {
                if (item?.type === TYPES_CONTENT_BANNER) {
                  switch (item?.component_banner) {
                    case BLOCK_CAMPAIGN_SLIDER?.component_banner:
                      return (
                        <UiExtension
                          uiId="HOME_BRAND"
                          dataBannersAdd={item}
                          key={item?.block_id}
                        />
                      );
                    case CMS_CONTENT_CAMPAIGN?.component_banner:
                      return (
                        <UiExtension
                          uiId="SOA_CMS_BLOCK_CONTENT"
                          dataCmsContentCampaign={item}
                          key={item?.block_id}
                        />
                      );
                    case BLOCK_CAMPAIGN_IMAGE_LISTING?.component_banner:
                      return (
                        <UiExtension
                          uiId="HOME_HOT_SALE"
                          dataBlockCampaignImageListing={item}
                          key={item?.block_id}
                        />
                      );
                  }
                } else if (item?.type === TYPES_CONTENT_FLASH_SALE) {
                  return (
                    <UiExtension
                      uiId="FLASH_SALE_BRAND_PAGE"
                      flashSaleData={item}
                      key={item?.block_id}
                    />
                  );
                }

                return null;
              })}
          </>
        );
      }
      return null;
    }, [props?.additionalBlockData]);

    if (
      !props?.additionalBlockData ||
      (props?.additionalBlockData &&
        Array.isArray(props?.additionalBlockData) &&
        props?.additionalBlockData.length === 0)
    ) {
      return null;
    }
    return <>{ContentBlock}</>;
  });

export default AdditionalBlockContent;
