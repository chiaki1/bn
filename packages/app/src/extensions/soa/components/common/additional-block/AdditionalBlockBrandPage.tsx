import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withSoaBrandDetailBlockData } from '../../../hoc/brand/withSoaBrandDetailBlockData';

const AdditionalBlockBrandPage = combineHOC(withSoaBrandDetailBlockData)(
  (props) => {
    if (
      !props?.dataBrandDetailBlocks ||
      (props?.dataBrandDetailBlocks &&
        Array.isArray(props?.dataBrandDetailBlocks) &&
        props?.dataBrandDetailBlocks.length === 0)
    ) {
      return null;
    }

    return (
      <UiExtension
        uiId="ADDITIONAL_BOCK_CONTENT"
        additionalBlockData={props?.dataBrandDetailBlocks || []}
      />
    );
  }
);

export default AdditionalBlockBrandPage;
