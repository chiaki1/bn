import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import arrowRight from '../../assets/images/icons/arrow-right.svg';

const QuestionDetail: React.FC = combineHOC()((props) => {
  return (
    <div className="question-detail">
      <div className="container">
        <div className="list-question">
          <h3 className="title-question-detail">Vận chuyển và đổi trả</h3>
          <div className="items-question">
            <div className="item-question">
              <div className="title-item-question flex-center flex-content-bw">
                <h4>Không vừa lòng có được đổi trả không?</h4>
                <img src={arrowRight} alt="" />
              </div>
              <div className="desc-item-question">
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software
                  like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
              </div>
            </div>
            <div className="item-question">
              <div className="title-item-question flex-center flex-content-bw">
                <h4>Đổi hàng có mất phí gì không?</h4>
                <img src={arrowRight} alt="" />
              </div>
            </div>
            <div className="item-question">
              <div className="title-item-question flex-center flex-content-bw">
                <h4>Hoàn tiền như thế nào?</h4>
                <img src={arrowRight} alt="" />
              </div>
            </div>
            <div className="item-question">
              <div className="title-item-question flex-center flex-content-bw">
                <h4>What are the signs of a toxic relationship?</h4>
                <img src={arrowRight} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default QuestionDetail;
