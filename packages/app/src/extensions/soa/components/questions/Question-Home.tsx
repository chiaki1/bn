import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import { AppRoute } from '../../../../router/Route';
import imgExchange from '../../assets/images/image/img-exchange.svg';
import imgOrther from '../../assets/images/image/img-orther.svg';
import imgPay from '../../assets/images/image/img-pay.svg';
import imgShip from '../../assets/images/image/img-ship.svg';
import imgShopping from '../../assets/images/image/img-shopping.svg';
import imgSize from '../../assets/images/image/img-size-guide.svg';

const QuestionHome: React.FC = combineHOC()((props) => {
  return (
    <div className="question-home">
      <div className="container">
        <ul className="items-question-home list-reset">
          <li
            className="item-question-home"
            onClick={() => RouterSingleton.push('/' + AppRoute.SIZE_GUIDE)}
          >
            <div className="img-question-home">
              <img src={imgSize} alt="" />
            </div>
            <h4>Hướng dẫn chọn size</h4>
          </li>
          <li
            className="item-question-home"
            onClick={() => RouterSingleton.push('/' + AppRoute.SHOPPING)}
          >
            <div className="img-question-home">
              <img src={imgShopping} alt="" />
            </div>
            <h4>Mua sắm</h4>
          </li>
          <li
            className="item-question-home"
            onClick={() => RouterSingleton.push('/' + AppRoute.PAY)}
          >
            <div className="img-question-home">
              <img src={imgPay} alt="" />
            </div>
            <h4>Thanh toán</h4>
          </li>
          <li
            className="item-question-home"
            onClick={() => RouterSingleton.push('/' + AppRoute.TRANSPORT)}
          >
            <div className="img-question-home">
              <img src={imgShip} alt="" />
            </div>
            <h4>Vận chuyển</h4>
          </li>
          <li
            className="item-question-home"
            onClick={() =>
              RouterSingleton.push('/' + AppRoute.EXCHANGE_AND_REFUND)
            }
          >
            <div className="img-question-home">
              <img src={imgExchange} alt="" />
            </div>
            <h4>Đổi và hoàn trả hàng hóa</h4>
          </li>
          <li
            className="item-question-home"
            onClick={() => RouterSingleton.push('/' + AppRoute.OTHER)}
          >
            <div className="img-question-home">
              <img src={imgOrther} alt="" />
            </div>
            <h4>Khác</h4>
          </li>
        </ul>
      </div>
    </div>
  );
});

export default QuestionHome;
