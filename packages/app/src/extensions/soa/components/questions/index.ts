import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_QUESTION_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'QUESTION_HOME',
    component: { lazy: React.lazy(() => import('./Question-Home')) },
  },
  /*{
    uiId: 'QUESTION_DETAIL',
    component: { lazy: React.lazy(() => import('./Question-Detail')) },
  },*/
];
