import { IonImg } from '@ionic/react';
import { withCustomerWishlistActions } from '@vjcspy/r/build/modules/account/hoc/wishlist/withCustomerWishlistActions';
import { withCustomerWishlistData } from '@vjcspy/r/build/modules/account/hoc/wishlist/withCustomerWishlistData';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import forEach from 'lodash/forEach';
import React, { useCallback, useMemo } from 'react';

import iconRecycleBin from '../../assets/images/icons/icon-Recycle-Bin.svg';

const WishList: React.FC = combineHOC(
  withCustomerWishlistData,
  withCustomerWishlistActions,
  withCustomer
)((props) => {
  const totalItems = useMemo(() => {
    let total = 0;
    if (Array.isArray(props.state?.wishlists)) {
      forEach(props.state.wishlists, (w: any) => {
        total += w.items_count;
      });
    }

    return total;
  }, [props?.state?.wishlists]);

  const removeWishlistItem = useCallback(
    (wishlistId: any, wishlistItemId: any) => {
      if (typeof props.actions.removeWishlistItem === 'function') {
        props.actions.removeWishlistItem(wishlistId, wishlistItemId);
      }
    },
    []
  );

  return (
    <div className="list-address">
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <div className="container">
        <h3 className="text-lg adrs-deli">Danh sách yêu thích</h3>
        <UiExtension uiId="WISH_LIST_TOOL_BAR" totalItems={totalItems} />
        <div className="item-wishlist">
          {props.state?.wishlists?.map((wishlist: any) => {
            return wishlist?.items_v2?.items?.map((wishlistItem: any) => {
              const isShowDiscount =
                wishlistItem.product?.price_range?.maximum_price?.discount
                  ?.amount_off &&
                wishlistItem.product?.price_range?.maximum_price?.discount
                  ?.amount_off > 0;
              return (
                <div className="minicart-item">
                  <div className="minicart-item-detail">
                    <div
                      className="product-photo"
                      onClick={() => {
                        RouterSingleton.push(
                          wishlistItem?.product?.url_key + '.html'
                        );
                      }}
                    >
                      <IonImg
                        class="center"
                        src={
                          wishlistItem?.product?.small_image?.url ??
                          wishlistItem?.product?.image?.url
                        }
                      />
                    </div>
                    <div className="product-item-detail">
                      <h4 className="name-branch">
                        {wishlistItem?.product?.brand?.name}
                      </h4>
                      <h3 className="product-name text-lg">
                        {wishlistItem?.product?.name}
                      </h3>
                      <div className="price-box">
                        <p className="special-price">
                          <UiExtension
                            uiId="CURRENCY"
                            price={
                              wishlistItem?.product?.price_range?.minimum_price
                                ?.regular_price?.value
                            }
                          />
                        </p>
                        {!!isShowDiscount && (
                          <p className="old-price">
                            {' '}
                            <UiExtension
                              uiId="CURRENCY"
                              price={
                                wishlistItem.product?.price_range?.maximum_price
                                  ?.final_price?.value
                              }
                            />
                          </p>
                        )}
                      </div>
                      <div className="add-remove-cart">
                        <button
                          className="add"
                          onClick={() => {
                            RouterSingleton.push(
                              wishlistItem?.product?.url_key + '.html'
                            );
                          }}
                        >
                          Thêm vào giỏ hàng
                        </button>
                        <button
                          className="remove"
                          onClick={() =>
                            removeWishlistItem(wishlist?.id, wishlistItem?.id)
                          }
                        >
                          <img src={iconRecycleBin} alt="" />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            });
          })}
        </div>
      </div>
    </div>
  );
});

export default WishList;
