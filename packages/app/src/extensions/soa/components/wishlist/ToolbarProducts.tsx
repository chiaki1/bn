import {
  IonButton,
  IonCol,
  IonContent,
  IonFooter,
  IonHeader,
  IonModal,
  IonRow,
  IonToolbar,
} from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useState } from 'react';

import iconSorter from '../../assets/images/icons/icon-sorter.svg';

const ToolbarProducts: React.FC<{ totalItems: any }> = combineHOC()((props) => {
  const [showModal, setShowModal] = useState(false);
  return (
    <div className="toolbar-product">
      <IonRow className="mr-row ion-align-items-center">
        <IonCol size="9">
          <p className="sum-products">{props?.totalItems || 0} Sản phẩm</p>
        </IonCol>
        <IonCol size="2">
          <img className="icon-sorter" src={iconSorter} />
        </IonCol>
        <IonCol size="1" className="p_static col-filter">
          <div className="filter-products modal_overlay">
            <IonModal
              isOpen={showModal}
              className="modal-filter modal-overlay modal-custom"
            >
              <IonHeader
                className="ion-no-border modal-header"
                translucent={false}
              >
                <div className="container">
                  <IonToolbar color="link">
                    <div className="flex-center ion-justify-content-between">
                      <h2 className="filter-title">Bộ lọc</h2>
                      <button
                        className="btn-link btn-close"
                        onClick={() => setShowModal(false)}
                      ></button>
                    </div>
                  </IonToolbar>
                </div>
              </IonHeader>
              {/*<IonContent fullscreen>*/}
              {/*  <FilterProduct />*/}
              {/*</IonContent>*/}
              <IonFooter class="modal-footer">
                <IonToolbar className="ion-no-padding">
                  <IonRow className="ion-row-btn">
                    <IonCol>
                      <button className="btn btn-reset-black btn-lg btn-fs">
                        Đặt lại
                      </button>
                    </IonCol>
                    <IonCol>
                      <button className="btn btn-black btn-lg btn-fs">
                        Áp dụng
                      </button>
                    </IonCol>
                  </IonRow>
                </IonToolbar>
              </IonFooter>
            </IonModal>
            <IonButton
              color={'link'}
              className="btn-filter"
              onClick={() => setShowModal(true)}
            ></IonButton>
          </div>
        </IonCol>
      </IonRow>
    </div>
  );
});

export default ToolbarProducts;
