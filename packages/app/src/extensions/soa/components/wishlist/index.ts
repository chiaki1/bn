import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_WISH_LISH_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'WISH_LIST',
    component: { lazy: React.lazy(() => import('./WishList')) },
  },
  {
    uiId: 'WISH_LIST_TOOL_BAR',
    component: { lazy: React.lazy(() => import('./ToolbarProducts')) },
  },
];
