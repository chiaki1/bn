import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_SEARCH_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CATEGORY_SEARCH_LIST_BRANCH',
    component: { lazy: React.lazy(() => import('./CategorySearchBrand')) },
  },
];
