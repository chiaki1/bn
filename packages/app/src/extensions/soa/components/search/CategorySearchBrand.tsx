import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import iconArrorRight from '../../assets/images/icons/arrow-right.svg';
import { withBrandListDataForSearch } from '../../hoc/brand/withBrandListDataForSearch';

const CategorySearchBrand: React.FC = combineHOC(withBrandListDataForSearch)(
  (props) => {
    return (
      <>
        <div className="brand__top">
          <div className="container">
            <div className="flex ion-justify-content-between block__title block__bb">
              <p className="block__headline ">THƯƠNG HIỆU</p>
              <p className="block__headline ">
                {props?.state?.brandSearchResult.length || 0} kết quả
              </p>
            </div>
          </div>
        </div>
        <div className="brand__list">
          <div className="container">
            {props?.state?.brandSearchResult?.map((items: any) => (
              <div
                className="brand__item flex-center flex-content-bw"
                key={items?.brand_id}
                onClick={() => {
                  if (items?.brand_id) {
                    RouterSingleton.push(`brand-details/${items?.brand_id}`);
                  }
                }}
              >
                <div className="banner-item-detail flex-center w100p">
                  <div className="brand__thumbnail p_relative img-block">
                    <img className="img-cover" src={items?.logo} />
                  </div>
                  <div className="brand-detail">
                    <h3 className="brand-name">{items?.name}</h3>
                    <span>{items?.count || 0} sản phẩm</span>
                  </div>
                </div>
                <button
                  className={'btn-link btn__view'}
                  onClick={() => {
                    if (items?.brand_id) {
                      RouterSingleton.push(`brand-details/${items?.brand_id}`);
                    }
                  }}
                >
                  <img src={iconArrorRight} />
                </button>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }
);

export default CategorySearchBrand;
