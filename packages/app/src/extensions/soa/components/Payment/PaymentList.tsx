import { IonCol, IonFooter, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import PaymentItem from './PaymentItem';

const PaymentList: React.FC = combineHOC()((props) => {
  return (
    <div className="shipping-list">
      <div className={'container'}>
        <div className={'wrap-address'}>
          <div className={'wrap-address-item flex '}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>
            <PaymentItem />
          </div>
          <div className={'wrap-address-item flex '}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>
            <PaymentItem />
          </div>
          <div className={'wrap-address-item flex '}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>
            <PaymentItem />
          </div>
          <div className={'wrap-address-item flex '}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>
            <PaymentItem />
          </div>
          <div className={'wrap-address-item flex '}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>
            <PaymentItem />
          </div>
          <div className={'wrap-address-item flex '}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>
            <PaymentItem />
          </div>
        </div>
      </div>
    </div>
  );
});

export default PaymentList;
