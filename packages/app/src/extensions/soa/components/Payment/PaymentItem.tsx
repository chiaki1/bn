import { IonCol, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const PaymentItem: React.FC = combineHOC()((props) => {
  return (
    <div className={'list-address'}>
      <div className={'list-address_item'}>
        <div className={'list-address_item__info'}>
          <h3 className={'item__info-name'}>
            Thẻ ATM nội địa (Internet banking)
          </h3>
          <p className={'item__info-address'}>
            Thẻ ATM nội địa (Internet banking)
          </p>
        </div>
      </div>
    </div>
  );
});

export default PaymentItem;
