import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_PAYMENT_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'PAYMENT_LIST',
    component: { lazy: React.lazy(() => import('./PaymentList')) },
  },
  {
    uiId: 'PAYMENT_ITEM',
    component: { lazy: React.lazy(() => import('./PaymentItem')) },
  },
];
