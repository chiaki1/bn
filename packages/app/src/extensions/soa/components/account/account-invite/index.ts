import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ACCOUNT_INVITE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ACCOUNT_INVITE_CONTAINER',
    component: { lazy: React.lazy(() => import('./AccountInviteContainer')) },
  },
];
