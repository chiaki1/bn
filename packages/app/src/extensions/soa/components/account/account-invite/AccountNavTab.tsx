import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const AccountNavTab: React.FC = combineHOC()((props) => {
  return (
    <div className="nav-tab mgb-block">
      <ul className="tab-items list-reset text-center full-2item">
        <li className="tab-item active">Mã giới thiệu của bạn</li>
        <li className="tab-item">Nhập mã giới thiệu</li>
      </ul>
    </div>
  );
});

export default AccountNavTab;
