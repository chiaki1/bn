import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconSprites from '../../../assets/images/icon-sprites.svg';

const InviteForm: React.FC = combineHOC()((props) => {
  return (
    <div className="form__invite">
      <div className="container">
        <div className="invite-form">
          <p className="text-center text-lg fw-600 qr-label">
            Nhập mã giới thiệu của bạn bè để hưởng ưu đãi lorem ipsum dolor sit
            amet consectetur
          </p>
          <h2 className="title-account">Nhập mã giới thiệu</h2>
          <form className="form-input-code form-inline-button">
            <div className="field no-mr">
              <input
                className="input-no-border"
                placeholder="Mã giới thiệu"
                type="text"
              />
            </div>
            <button className="btn-link btn-inline-field inline-flex items-center">
              <svg
                className="text-color-base fill-current"
                width="25"
                height="24"
              >
                <use xlinkHref={iconSprites + '#icon-qrcode'} />
              </svg>
            </button>
            <button className="btn btn-black btn-fs btn-submit">
              Xác nhận
            </button>
          </form>
        </div>
      </div>
    </div>
  );
});

export default InviteForm;
