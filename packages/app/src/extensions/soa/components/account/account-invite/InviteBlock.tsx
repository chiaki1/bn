import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconSprites from '../../../assets/images/icon-sprites.svg';

const InviteBlock: React.FC = combineHOC()((props) => {
  return (
    <div className="block__invite">
      <div className="container">
        <div className="qr-code text-center">
          <img src="https://via.placeholder.com/200x200" />
          <p className="text-lg fw-600 qr-des">
            Giới thiệu bạn bè để hưởng ưu đãi lorem ipsum dolor sit amet
            consectetur
          </p>
        </div>
        <div className="invite-code">
          <h2 className="title-account">Mã giới thiệu của bạn</h2>
          <div className="input-field input-field-icon-end flex-center-center input-field-lg text-main p_relative">
            SOA00485761
            <svg
              className="fill-transparent absolute icon-copy-code"
              width="25"
              height="24"
            >
              <use xlinkHref={iconSprites + '#icon-copy'} />
            </svg>
          </div>
          <p className="invite-link-text">Link giới thiệu</p>
          <form className="form-inline-button flex ion-justify-content-between pdb-indent-md">
            <div className="field no-mr">
              <input type="text" />
              <button className="btn-link btn-delete">
                <i className="fas fa-times-circle"></i>
              </button>
            </div>
            <button className="btn btn-reset btn-inline-field">
              <i className="fal fa-link"></i>
            </button>
          </form>
        </div>
        <ul className="social-share list-reset container-onlyl text-center flex flex-wrap">
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Messenger</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Facebook</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Tin nhắn</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Nhật ký</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Feed</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Story</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">Email</label>
          </li>
          <li className="social-item">
            <span className="social-logo">
              <img src="https://via.placeholder.com/200x200" />
            </span>
            <label className="social-label text-xs">SMS</label>
          </li>
        </ul>
      </div>
    </div>
  );
});

export default InviteBlock;
