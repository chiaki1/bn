import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useState } from 'react';
import QRCode from 'react-qr-code';
import {
  FacebookIcon,
  FacebookMessengerIcon,
  FacebookMessengerShareButton,
  FacebookShareButton,
  PinterestIcon,
  PinterestShareButton,
  TwitterIcon,
  TwitterShareButton,
} from 'react-share';

import { clientFacebookId } from '../../../../../values/AuthenIds';
import iconSprites from '../../../assets/images/icon-sprites.svg';

const AccountInviteContainer: React.FC = combineHOC()((props) => {
  const [activeTab, setActiveTab] = useState('info');
  return (
    <>
      <div className="nav-tab mgb-block">
        <ul className="tab-items list-reset text-center full-2item">
          <li
            className={clsx('tab-item', activeTab === 'info' && 'active')}
            onClick={() => {
              setActiveTab('info');
            }}
          >
            Mã giới thiệu của bạn
          </li>
          <li
            className={clsx('tab-item', activeTab === 'input' && 'active')}
            onClick={() => {
              setActiveTab('input');
            }}
          >
            Nhập mã giới thiệu
          </li>
        </ul>
      </div>
      {activeTab === 'info' && (
        <div className="block__invite">
          <div className="container">
            <div className="qr-code text-center">
              {/*<img src="https://via.placeholder.com/200x200" />*/}
              <QRCode value="https://saleoff.asia/" />
              <p className="text-lg fw-600 qr-des">
                Giới thiệu bạn bè để hưởng ưu đãi
              </p>
            </div>
            <div className="invite-code">
              <h2 className="title-account">Mã giới thiệu của bạn</h2>
              <div className="input-field input-field-icon-end flex-center-center input-field-lg text-main p_relative">
                SOA00485761
                <svg
                  className="fill-transparent absolute icon-copy-code"
                  width="25"
                  height="24"
                >
                  <use xlinkHref={iconSprites + '#icon-copy'} />
                </svg>
              </div>
              <p className="invite-link-text">Link giới thiệu</p>
              <form className="form-inline-button flex ion-justify-content-between pdb-indent-md">
                <div className="field no-mr">
                  <input
                    type="text"
                    value={'https://saleoff.asia/'}
                    onChange={() => {}}
                  />
                  {/*<button className="btn-link btn-delete">*/}
                  {/*  <i className="fas fa-times-circle" />*/}
                  {/*</button>*/}
                </div>
                <button className="btn btn-reset btn-inline-field">
                  <i className="fal fa-link" />
                </button>
              </form>
            </div>
            <ul className="social-share list-reset container-onlyl text-center flex flex-wrap">
              <FacebookShareButton
                url={'https://saleoff.asia/'}
                className="p-3"
              >
                <FacebookIcon size={'1.5rem'} round />
                <label className="label block text-xs text-gray">
                  Facebook
                </label>
              </FacebookShareButton>
              <TwitterShareButton url={'https://saleoff.asia/'} className="p-3">
                <TwitterIcon size={'1.5rem'} round />
                <label className="label block text-xs text-gray">Twitter</label>
              </TwitterShareButton>
              <PinterestShareButton
                media={'https://saleoff.asia/'}
                url={'abc123'}
              >
                <PinterestIcon size={'1.5rem'} round />
                <label className="label block text-xs text-gray">
                  Pinterest
                </label>
              </PinterestShareButton>
              <FacebookMessengerShareButton
                url={'https://saleoff.asia/'}
                appId={clientFacebookId}
                className="p-3"
              >
                <FacebookMessengerIcon size={'1.5rem'} round />
                <label className="label block text-xs text-gray">
                  Messenger
                </label>
              </FacebookMessengerShareButton>
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Messenger</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Facebook</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Tin nhắn</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Nhật ký</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Feed</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Story</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">Email</label>*/}
              {/*</li>*/}
              {/*<li className="social-item">*/}
              {/*  <span className="social-logo">*/}
              {/*    <img src="https://via.placeholder.com/200x200" />*/}
              {/*  </span>*/}
              {/*  <label className="social-label text-xs">SMS</label>*/}
              {/*</li>*/}
            </ul>
          </div>
        </div>
      )}

      {activeTab === 'input' && (
        <div className="form__invite">
          <div className="container">
            <div className="invite-form">
              <p className="text-center text-lg fw-600 qr-label">
                Nhập mã giới thiệu của bạn bè để hưởng ưu đãi
              </p>
              <h2 className="title-account">Nhập mã giới thiệu</h2>
              <form className="form-input-code form-inline-button">
                <div className="field no-mr">
                  <input
                    className="input-no-border"
                    placeholder="Mã giới thiệu"
                    type="text"
                  />
                </div>
                <button className="btn-link btn-inline-field inline-flex items-center">
                  <svg
                    className="text-color-base fill-current"
                    width="25"
                    height="24"
                  >
                    <use xlinkHref={iconSprites + '#icon-qrcode'} />
                  </svg>
                </button>
                <button className="btn btn-black btn-fs btn-submit">
                  Xác nhận
                </button>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  );
});

export default AccountInviteContainer;
