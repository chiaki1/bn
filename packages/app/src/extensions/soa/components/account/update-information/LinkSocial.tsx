import { IonCol, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const LinkSocial: React.FC = combineHOC()((props) => {
  return (
    <div className="list-social">
      <div className="container">
        <h3 className=" title-account-update">LIÊN KẾT MẠNG XÃ HỘI</h3>
        {/*<ul className="list-reset list-items-sociallink">*/}
        {/*  <li className="_item-sociallink flex-center flex-content-bw">*/}
        {/*    <label className="flex flex-center">*/}
        {/*      <img src="https://via.placeholder.com/20px20px" alt="" />*/}
        {/*      <span className="name-linksocial">Nguyễn Tiến Nam</span>*/}
        {/*    </label>*/}
        {/*    <button className="unlink"> Hủy liên kết</button>*/}
        {/*  </li>*/}
        {/*  <li className="_item-sociallink flex-center flex-content-bw">*/}
        {/*    <label className="flex flex-center">*/}
        {/*      <img src="https://via.placeholder.com/20px20px" alt="" />*/}
        {/*      <span className="name-linksocial">Nguyễn Tiến Nam</span>*/}
        {/*    </label>*/}
        {/*    <button className="unlink"> Hủy liên kết</button>*/}
        {/*  </li>*/}
        {/*  <li className="_item-sociallink flex-center flex-content-bw">*/}
        {/*    <label className="flex flex-center">*/}
        {/*      <img src="https://via.placeholder.com/20px20px" alt="" />*/}
        {/*      <span className="name-linksocial">Nguyễn Tiến Nam</span>*/}
        {/*    </label>*/}
        {/*    <button className="unlink"> Hủy liên kết</button>*/}
        {/*  </li>*/}
        {/*  <li className="_item-sociallink flex-center flex-content-bw">*/}
        {/*    <label className="flex flex-center">*/}
        {/*      <img src="https://via.placeholder.com/20px20px" alt="" />*/}
        {/*      <span className="name-linksocial">Nguyễn Tiến Nam</span>*/}
        {/*    </label>*/}
        {/*    <button className="unlink"> Hủy liên kết</button>*/}
        {/*  </li>*/}
        {/*  <li className="_item-sociallink flex-center flex-content-bw">*/}
        {/*    <label className="flex flex-center">*/}
        {/*      <img src="https://via.placeholder.com/20px20px" alt="" />*/}
        {/*      <span className="name-linksocial">Nguyễn Tiến Nam</span>*/}
        {/*    </label>*/}
        {/*    <button className="unlink"> Hủy liên kết</button>*/}
        {/*  </li>*/}
        {/*</ul>*/}
      </div>
    </div>
  );
});

export default LinkSocial;
