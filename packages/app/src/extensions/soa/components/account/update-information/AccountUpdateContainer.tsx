import { IonContent, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const AccountUpdateContainer: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Cập nhập thông tin"
        showIcon={false}
      />
      <IonContent>
        {/*<UiExtension uiId="AVATAR_UPDATE_INFO" />*/}
        <UiExtension uiId="UPDATE_INFO_FORM" />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default AccountUpdateContainer;
