import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ACCOUNT_UPDATE_INFO_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ACCOUNT_UPDATE_CONTAINER',
    component: { lazy: React.lazy(() => import('./AccountUpdateContainer')) },
  },
  {
    uiId: 'UPDATE_INFO_FORM',
    component: { lazy: React.lazy(() => import('./UpdateInfoForm')) },
  },
  {
    uiId: 'AVATAR_UPDATE_INFO',
    component: { lazy: React.lazy(() => import('./AvatarUpdateInfo')) },
  },
  {
    uiId: 'LINK_SOCIAL',
    component: { lazy: React.lazy(() => import('./LinkSocial')) },
  },
];
