import {
  Camera,
  CameraResultType,
  CameraSource,
  Photo,
} from '@capacitor/camera';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useState } from 'react';

const AvatarUpdateInfo: React.FC = combineHOC()((props) => {
  const [photo, setPhoto] = useState<any>('');
  const [base64Photo, setBase64Photo] = useState<any>('');
  const takePicture = async () => {
    const photos = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      // resultType: CameraResultType.Uri,
      source: CameraSource.Photos,
      quality: 100,
      correctOrientation: true,
      allowEditing: false,
    });

    if (photos?.base64String) {
      setPhoto('data:image/jpeg;base64, ' + photos?.base64String);
      setBase64Photo('data:image/jpeg;base64, ' + photos?.base64String);
    }
  };

  return (
    <div className="avatar-update-info">
      <div className="container">
        <div className="avatar-update">
          {photo ? (
            <img src={photo} alt="" />
          ) : (
            <img src="https://via.placeholder.com/120x120" alt="" />
          )}

          <button onClick={() => takePicture()}>Sửa</button>
        </div>
      </div>
      <hr className="h4" />
    </div>
  );
});

export default AvatarUpdateInfo;
