import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import {
  IonContent,
  IonDatetime,
  IonHeader,
  IonModal,
  IonToolbar,
} from '@ionic/react';
import { withAccountLoadingState } from '@vjcspy/r/build/modules/account/hoc/withAccountLoadingState';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import moment from 'moment/moment';
import React, { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import { AppRoute } from '../../../../../router/Route';
import iconSprites from '../../../assets/images/icon-sprites.svg';
import iconCamera from '../../../assets/images/icons/icon-camera.svg';
import iconedit from '../../../assets/images/icons/icon-edit.svg';
import { withAvatarAccountActions } from '../../../hoc/account/withAvatarAccountActions';
import { withSoaUpdateCustomerInfoActions } from '../../../hoc/account/withSoaUpdateCustomerInfoActions';

const UpdateInfoForm: React.FC = combineHOC(
  // withCustomer,
  withSoaUpdateCustomerInfoActions,
  withAccountLoadingState,
  withAccountState,
  withAvatarAccountActions
)((props) => {
  const [selectedDate, setSelectedDate] = useState<string>('');
  const [checkUpdate, setCheckUpdate] = useState<boolean>(false);
  const [showDatePicker, setShowDatePicker] = useState<boolean>(false);

  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    // @ts-ignore
    if (!props?.state?.accountState?.customer?.system_generate) {
      setValue(
        'firstname',
        props?.state?.accountState?.customer?.firstname || ''
      );
      setValue(
        'lastname',
        props?.state?.accountState?.customer?.lastname || ''
      );
      // setValue('email', props?.state?.accountState?.customer?.email || '');
      setValue('gender', props?.state?.accountState?.customer?.gender || '1');

      if (props?.state?.accountState?.customer?.date_of_birth) {
        setValue(
          'date_of_birth',
          moment(props?.state?.accountState?.customer?.date_of_birth).format(
            'DD/MM/YYYY'
          ) || ''
        );
        setSelectedDate(props?.state?.accountState?.customer?.date_of_birth);
      }
    }
  }, [props.state.accountState.customer]);

  useEffect(() => {
    if (
      props.state?.accountState?.loadingState?.info === false &&
      checkUpdate
    ) {
      RouterSingleton.push('/' + AppRoute.ACCOUNT);
    }
  }, [props.state?.accountState?.loadingState?.info]);

  const [photo, setPhoto] = useState<any>('');
  const [base64Photo, setBase64Photo] = useState<any>({
    base64_encoded_data: '',
    name: '',
    type: '',
  });

  const takePicture = async () => {
    const photos = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      // resultType: CameraResultType.Uri,
      source: CameraSource.Photos,
      quality: 100,
      correctOrientation: true,
      allowEditing: false,
    });

    if (photos?.base64String) {
      setBase64Photo({
        ...base64Photo,
        base64_encoded_data: photos?.base64String,
        name:
          props?.state?.accountState?.customer?.retail_telephone +
            '.' +
            photos?.format || '',
        type: 'image/' + photos?.format,
      });
      setPhoto(
        'data:image/' + photos?.format + ';base64, ' + photos?.base64String
      );
    }
  };

  const onSubmit = useCallback(
    (data: any) => {
      // setErrorDate(false);
      const input: any = {};
      input['lastname'] = data['lastname'];
      input['firstname'] = data['firstname'];
      // input['email'] = data['email'];
      input['gender'] = data['gender'];
      input['date_of_birth'] = data['date_of_birth']
        ? moment(data['date_of_birth'], 'DD/MM/YYYY').format('YYYY-MM-DD')
        : '';
      input['system_generate'] = false;

      if (base64Photo?.base64_encoded_data) {
        props?.actions?.saveCustomerAvatar(
          props?.state?.accountState?.customer?.retail_telephone +
            '.' +
            base64Photo?.type.replace('image/', '') || 'jpg',
          base64Photo
        );
      }

      if (typeof props.actions.updateCustomerInfo === 'function') {
        props.actions.updateCustomerInfo(input);
        setCheckUpdate(true);
      }
    },
    [base64Photo]
  );

  return (
    <>
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      {/*<div className="avatar-update-info">*/}
      {/*  <div className="container">*/}
      {/*    <div className="avatar-update bg-gray flex-center-center relative rounded overflow-hidden">*/}
      {/*      {photo ? (*/}
      {/*        <img src={photo} alt="" />*/}
      {/*      ) : (*/}
      {/*        <img*/}
      {/*          className="img-cover round"*/}
      {/*          src={*/}
      {/*            // @ts-ignore*/}
      {/*            props?.state?.accountState?.customer?.avatar || iconCamera*/}
      {/*          }*/}
      {/*        />*/}
      {/*      )}*/}
      {/*      <button*/}
      {/*        onClick={() => takePicture()}*/}
      {/*        className="absolute inset-0 w-full bg-transparent flex ion-align-items-end"*/}
      {/*      >*/}
      {/*        <span className="block w-full">Sửa</span>*/}
      {/*      </button>*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*  <hr className="h4" />*/}
      {/*</div>*/}
      <div className="List-input-account-update">
        <div className="container">
          <h3 className=" title-account-update">THÔNG TIN CÁ NHÂN</h3>
          <form>
            <div className="form-address-add-item">
              <label htmlFor="">Tên</label>
              <div className="form-address-add-item_input">
                <input
                  type="text"
                  {...register('lastname', {
                    required: true,
                  })}
                  placeholder="Nhập tên"
                  name="lastname"
                />
                <img src={iconedit} alt="" />
              </div>
              {errors.lastname && (
                <div className="error-message text-small">
                  <i className="fas fa-exclamation-circle" /> Vui lòng nhập tên.
                </div>
              )}
            </div>
            <div className="form-address-add-item">
              <label htmlFor="">Họ và tên đệm</label>
              <div className="form-address-add-item_input">
                <input
                  type="text"
                  {...register('firstname', {
                    required: true,
                  })}
                  placeholder="Nhập họ và tên đệm"
                />
                <img src={iconedit} alt="" />
              </div>
              {errors.firstname && (
                <div className="error-message text-small">
                  <i className="fas fa-exclamation-circle" /> Vui lòng nhập tên.
                </div>
              )}
            </div>
            <div className="form-address-add-item">
              <label htmlFor="">Số điện thoại</label>
              <div className="form-address-add-item_input">
                <input
                  type="number"
                  disabled
                  defaultValue={
                    props?.state?.accountState?.customer?.retail_telephone || ''
                  }
                  placeholder="Nhập số điện thoại"
                />
                <img src={iconedit} alt="" />
              </div>
            </div>

            <div className="form-address-add-item">
              <label htmlFor="">Giới tính</label>
              <div className="custom-select-address-add">
                <select
                  {...register('gender', {
                    required: true,
                  })}
                >
                  <option value="">Vui lòng chọn</option>
                  {/*<option value="0">Khác</option>*/}
                  <option value="1">Nam</option>
                  <option value="2">Nữ</option>
                </select>
              </div>
              {errors.gender && (
                <div className="error-message text-small">
                  <i className="fas fa-exclamation-circle" /> Vui lòng chọn giới
                  tính.
                </div>
              )}
            </div>
            <div className="form-address-add-item">
              <label htmlFor="">Ngày sinh</label>
              <div
                className="custom-select-address-add custom-datetime"
                onClick={() => {
                  if (!selectedDate) {
                    setShowDatePicker(!showDatePicker);
                  }
                }}
              >
                <input
                  type="text"
                  disabled
                  defaultValue={selectedDate}
                  placeholder="Chọn ngày tháng"
                  {...register('date_of_birth', {
                    required: true,
                  })}
                />
              </div>

              {errors.date_of_birth && (
                <div className="error-message text-small">
                  <i className="fas fa-exclamation-circle" /> Vui lòng chọn ngày
                  sinh tính.
                </div>
              )}
            </div>
          </form>
        </div>
        {/*<UiExtension uiId="LINK_SOCIAL" />*/}
        <div className="">
          <button
            className="btn btn-black btn-fs btn-success-address"
            onClick={handleSubmit(onSubmit)}
            disabled={props.state?.accountState?.loadingState?.info}
          >
            Cập nhật
          </button>
        </div>
        <hr className="h4" />
        {/*datepicker*/}
        <IonModal isOpen={showDatePicker}>
          <IonHeader className="ion-no-border modal-header" translucent={false}>
            <div className="container">
              <IonToolbar color="link">
                <div className="flex-center ion-justify-content-between">
                  <h2 className="filter-title">Lựa chọn ngày</h2>
                  <div
                    className="modal-close"
                    onClick={() => setShowDatePicker(false)}
                  >
                    <svg width="10" height="10">
                      <use xlinkHref={iconSprites + '#icon-close'} />
                    </svg>
                  </div>
                </div>
              </IonToolbar>
            </div>
          </IonHeader>
          <IonContent force-overscroll="false">
            <IonDatetime
              presentation="date"
              min="1970"
              max={moment().format('YYYY-MM-DD')}
              value={selectedDate}
              locale="vi-VN"
              // showClearButton={true}
              showDefaultButtons={true}
              onIonChange={(val) => {
                if (val?.detail?.value) {
                  setSelectedDate(
                    moment(val?.detail?.value).format('YYYY-MM-DD')
                  );
                  setValue(
                    'date_of_birth',
                    moment(val?.detail?.value).format('DD/MM/YYYY')
                  );
                }
              }}
            />
          </IonContent>
        </IonModal>
      </div>
    </>
  );
});

export default UpdateInfoForm;
