import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ACCOUNT_POLICY_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ACCOUNT_POLICY',
    component: { lazy: React.lazy(() => import('./AccountPolicy')) },
  },
  {
    uiId: 'CMS_CONTENT',
    component: { lazy: React.lazy(() => import('./CmsContent')) },
  },
];
