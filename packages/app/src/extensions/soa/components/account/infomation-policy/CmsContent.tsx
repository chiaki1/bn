import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import { withCmsContentData } from '../../../hoc/cms-content/withCmsContentData';

const CmsContent: React.FC = combineHOC(withCmsContentData)((props) => {
  return (
    <div className="account-sidebar account-sidebar-cms">
      <div className="container">
        <div className="sidebar-person">
          <h2 className="title-account no-mr">
            {props.state?.accountPolicyData?.content_heading}
          </h2>
          <div className="btn-list-item">
            <div
              dangerouslySetInnerHTML={{
                __html: props.state?.accountPolicyData?.content || '',
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
});

export default CmsContent;
