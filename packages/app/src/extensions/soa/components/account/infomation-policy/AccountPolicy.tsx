import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { AppRoute } from '../../../../../router/Route';

const AccountPolicy: React.FC = combineHOC()((props) => {
  return (
    <div className="account-sidebar account-sidebar-cms">
      <div className="container">
        <div className="sidebar-person">
          <h2 className="title-account no-mr">THÔNG TIN CHUNG</h2>
          <ul className="list-reset account-sidebar-list">
            <li
              onClick={() => RouterSingleton.push('/' + AppRoute.POLICY_RULES)}
            >
              <div className="btn-list-item btn-arrow-right">
                Chính sách và điều khoản
              </div>
            </li>
            <li
              onClick={() => RouterSingleton.push('/' + AppRoute.QUESTION_PAGE)}
            >
              <div className="btn-list-item btn-arrow-right">
                Câu hỏi thường gặp
              </div>
            </li>
            <li
              onClick={() =>
                RouterSingleton.push('/' + AppRoute.INFORMATION_SECURITY_POLICY)
              }
            >
              <div className="btn-list-item btn-arrow-right">
                Chính sách bảo mật thông tin
              </div>
            </li>
            <li onClick={() => RouterSingleton.push('/about')}>
              <div className="btn-list-item btn-arrow-right">Về chúng tôi</div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
});

export default AccountPolicy;
