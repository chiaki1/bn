import { IonModal } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconSprites from '../../../assets/images/icon-sprites.svg';

const AddressPopupConfirmDelete: React.FC<{
  onDeleteCustomerAdd: () => void;
  close: () => void;
  showModelDelete?: boolean;
}> = combineHOC()((props) => {
  return (
    <IonModal isOpen={props?.showModelDelete} className="modal-wrapper-center">
      <div
        className="text-center pd-indent ion-modal-content"
        style={{ padding: '100px 0px' }}
      >
        <div
          className="modal-close"
          onClick={() => {
            props?.close();
          }}
        >
          <svg width="23" height="23">
            <use xlinkHref={iconSprites + '#icon-close'} />
          </svg>
        </div>
        <p className="text-lg-semi">
          Bạn có chắc muốn xóa địa chỉ này <br /> khỏi sổ địa chỉ
        </p>
        <div className="grid grid-cols-2 gap-x-6">
          <div
            className="flex-center-center btn-xl btn btn-reset-black"
            onClick={() => {
              props?.close();
            }}
          >
            Hủy bỏ
          </div>
          <div
            className="flex-center-center btn-xl btn btn-main"
            onClick={() => {
              props?.onDeleteCustomerAdd();
            }}
          >
            Xóa
          </div>
        </div>
      </div>
    </IonModal>
  );
});

export default AddressPopupConfirmDelete;
