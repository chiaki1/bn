import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ADDRESS_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ADDRESS_LIST_ON_CHECKOUT',
    component: { lazy: React.lazy(() => import('./AddressListOnCheckout')) },
  },
  {
    uiId: 'ADDRESS_DETAIL_FORM',
    component: { lazy: React.lazy(() => import('./form/AddressDetailForm')) },
  },
  {
    uiId: 'ADDRESS_LIST_ON_PAGE',
    component: { lazy: React.lazy(() => import('./AddressListOnPage')) },
  },
  {
    uiId: 'ADDRESS_DETAIL_FORM_ON_PAGE',
    component: {
      lazy: React.lazy(() => import('./form/AddressDetailFormOnPage')),
    },
  },
  {
    uiId: 'ADDRESS_POPUP_CONFIRM_DELETE',
    component: {
      lazy: React.lazy(() => import('./AddressPopupConfirmDelete')),
    },
  },
];
