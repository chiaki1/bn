import {
  IonContent,
  IonFooter,
  IonModal,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { withCustomerAddressActions } from '@vjcspy/r/build/modules/account/hoc/withCustomerAddressActions';
import {
  getCustomerName,
  parseCustomerName,
} from '@vjcspy/r/build/modules/account/util/getCustomerName';
import { withCheckoutAddressActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressActions';
import { withCheckoutAddressData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressData';
import { withContentAddressActions } from '@vjcspy/r/build/modules/content/hoc/withContentAddressActions';
import { withContentAddressData } from '@vjcspy/r/build/modules/content/hoc/withContentAddressData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import find from 'lodash/find';
import isNumber from 'lodash/isNumber';
import isString from 'lodash/isString';
import map from 'lodash/map';
import size from 'lodash/size';
import split from 'lodash/split';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm, useWatch } from 'react-hook-form';

import iconcheckwhite from '../../../../assets/images/icons/icon-check-white.svg';

const AddressDetailForm: React.FC<{
  whenUpdate?: () => void;
  showPopup?: boolean;
}> = combineHOC(
  withCheckoutAddressData,
  withContentAddressData,
  withCheckoutAddressActions,
  withCustomer,
  withCustomerAddressActions,
  withContentAddressActions
)((props) => {
  const [checkedDefault, setCheckedDefault] = useState(false);
  const [showModelDelete, setShowModelDelete] = useState(false);
  const {
    control,
    handleSubmit,
    register,
    setValue,
    getValues,
    formState: { errors },
    clearErrors,
  } = useForm();

  useEffect(() => {
    setCheckedDefault(
      props.state?.customer?.default_shipping == props.state?.editAddressObj?.id
    );
    clearErrors();
  }, [props.state?.editAddressObj]);

  useEffect(() => {
    if (props.state?.isDeletedAddress) {
      cancelEdit();
    }
  }, [props.state?.isDeletedAddress]);

  const province = useWatch({
    control,
    name: 'province',
  });
  const district = useWatch({
    control,
    name: 'district',
  });
  const ward = useWatch({
    control,
    name: 'ward',
  });

  useEffect(() => {
    if (props.actions?.getProvinceDistrictWardData) {
      props.actions.getProvinceDistrictWardData();
    }
  }, []);

  // init form value
  useEffect(() => {
    if (
      !props?.state?.isUpdatingAddress &&
      props?.state?.isEditingAddress &&
      props.state?.editAddressObj &&
      props.state?.editAddressObj['id']
    ) {
      setValue('name', getCustomerName(props.state?.editAddressObj));
      setValue('telephone', props.state?.editAddressObj?.telephone ?? '');
      setValue(
        'province',
        props.state?.editAddressObj?.iz_address_province ?? ''
      );
      setValue(
        'district',
        props.state?.editAddressObj?.iz_address_district ?? ''
      );
      setValue('ward', props.state?.editAddressObj?.iz_address_ward ?? '');
      setValue('street', props.state?.editAddressObj?.street[0] ?? '');
    }

    if (!props.state?.editAddressObj?.id) {
      setValue('name', '');
      setValue('telephone', '');
      setValue('province', '');
      setValue('district', '');
      setValue('ward', '');
      setValue('street', '');
    }
  }, [props.state?.editAddressObj]);

  // Lấy options dựa vào lựa chọn của user
  const provinceOptions = useMemo(() => {
    if (typeof props.data?.ProvinceOptionsFactory === 'function') {
      return map(props.data?.ProvinceOptionsFactory(), (d: any) => d.value);
    }
    return [];
  }, []);

  const districtOptions = useMemo(() => {
    if (province && typeof props.data?.DistrictOptionsFactory === 'function') {
      return map(
        props.data?.DistrictOptionsFactory(province),
        (d: any) => d.value
      );
    }
    return [];
  }, [province]);

  const wardOptions = useMemo(() => {
    if (district && typeof props.data?.WardOptionsFactory === 'function') {
      return map(props.data?.WardOptionsFactory(district), (d: any) => d.value);
    }
    return [];
  }, [district]);
  // Set lại null nếu thay đổi giá trị tỉnh/thành
  useEffect(() => {
    const isExisted = find(districtOptions, (d) => d === district);
    if (!isExisted && district) {
      setValue('district', null, { shouldValidate: true });
    }
  }, [province]);
  useEffect(() => {
    const isExisted = find(wardOptions, (d) => d === ward);
    if (!isExisted && ward) {
      setValue('ward', null, { shouldValidate: true });
    }
  }, [district]);

  const cancelEdit = useCallback(() => {
    if (typeof props.actions?.cancelEditAddress === 'function') {
      props.actions.cancelEditAddress();
    }
  }, []);

  const onDeleteCustomerAdd = useCallback((addId?: any) => {
    if (
      typeof props.actions.deleteCustomerAddress === 'function' &&
      isNumber(addId)
    ) {
      props.actions.deleteCustomerAddress(addId);
      setShowModelDelete(false);
    }
  }, []);

  const onSubmit = useCallback(
    (data: any) => {
      const input: any = {};
      if (parseCustomerName(data.name)) {
        input['firstname'] = parseCustomerName(data.name)!.firstname;
        input['lastname'] = parseCustomerName(data.name)!.lastname;
      }
      input['iz_address_district'] = data.district;
      input['iz_address_ward'] = data.ward;
      input['iz_address_province'] = data.province;
      input['default_billing'] = checkedDefault;
      input['default_shipping'] = checkedDefault;
      input['street'] = [data.street];
      input['telephone'] = data.telephone;

      if (props.state?.editAddressObj['id']) {
        if (typeof props.actions.updateCustomerAddress === 'function') {
          props.actions.updateCustomerAddress(
            props.state?.editAddressObj['id'],
            input
          );
        }
      } else {
        if (typeof props.actions.createNewCustomerAddress === 'function') {
          props.actions.createNewCustomerAddress(input);
        }
      }
    },
    [props.state?.editAddressObj, checkedDefault]
  );

  return (
    <IonModal
      isOpen={props.state?.isEditingAddress}
      className="modal-wrapper-full"
    >
      <IonPage className="page-header-no-mrb">
        <UiExtension
          uiId="HEADER_ACCOUNT"
          goBack={() => cancelEdit()}
          title={
            props.state?.editAddressObj?.id ? 'Sửa địa chỉ' : 'Thêm địa chỉ mới'
          }
          showIcon={false}
        />
        <IonContent>
          <form>
            <div className="form-address-add">
              <div className="container">
                <div className="wrp-form-address-add">
                  <div className="form-address-add-item">
                    <label htmlFor="">Họ và tên</label>
                    <div className="form-address-add-item_input">
                      <input
                        type="text"
                        {...register('name', {
                          required: true,
                          validate: (value: any) =>
                            isString(value) && size(split(value, ' ')) > 1,
                        })}
                        placeholder="Nhập tên"
                      />
                      {errors.name && (
                        <div className="error-message text-small">
                          <i className="fas fa-exclamation-circle" /> Vui lòng
                          nhập tên.
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-address-add-item">
                    <label htmlFor="">Số điện thoại</label>
                    <div className="form-address-add-item_input">
                      <input
                        type="text"
                        {...register('telephone', {
                          required: true,
                          pattern:
                            /(086|096|097|098|032|033|034|035|036|037|038|039|088|091|094|083|084|085|081|082|089|090|093|070|076|077|078|079|092|058|056|099|059)+([0-9]{7})\b/u,
                        })}
                        placeholder="Số điện thoại"
                      />
                      {errors.telephone && (
                        <div className="error-message text-small">
                          <i className="fas fa-exclamation-circle" /> Số điện
                          thoại không hợp lệ
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-address-add-item">
                    <label htmlFor="">Tỉnh/Thành phố</label>
                    <div className="custom-select-address-add">
                      <select
                        {...register('province', {
                          required: true,
                        })}
                        disabled={
                          !Array.isArray(provinceOptions) ||
                          provinceOptions.length === 0
                        }
                        onChange={(e) => {
                          setValue('province', e.target.value ?? '');
                        }}
                      >
                        <option value="">Chọn Tỉnh/Thành phố:</option>
                        {Array.isArray(provinceOptions) &&
                          provinceOptions.map((item: any, key) => (
                            <option value={item || ''} key={item}>
                              {item}
                            </option>
                          ))}
                      </select>
                      {(errors.province || !getValues('province')) && (
                        <div className="error-message text-small">
                          <i className="fas fa-exclamation-circle" /> Vui lòng
                          chọn Tỉnh/ Thành phố
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-address-add-item">
                    <label htmlFor="">Quận/Huyện</label>
                    <div className="custom-select-address-add">
                      <select
                        {...register('district', {
                          required: true,
                        })}
                        value={getValues('district') || ''}
                        disabled={!getValues('province')}
                        onChange={(e) => {
                          if (e.target.value) {
                            setValue('district', e.target.value ?? '');
                            clearErrors('district');
                          }
                        }}
                      >
                        <option value="">Chọn Quận/Huyện:</option>
                        {Array.isArray(districtOptions) &&
                          districtOptions.map((item1: any) => (
                            <option value={item1} key={item1}>
                              {item1}
                            </option>
                          ))}
                      </select>
                      {errors.district && (
                        <div className="error-message text-small">
                          <i className="fas fa-exclamation-circle" /> Vui lòng
                          chọn Quận/ Huyện
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-address-add-item">
                    <label htmlFor="">Phường/Xã</label>
                    <div className="custom-select-address-add">
                      <select
                        {...register('ward', {
                          required: true,
                        })}
                        value={getValues('ward') || ''}
                        disabled={!getValues('district')}
                        onChange={(e) => {
                          if (e.target.value) {
                            setValue('ward', e.target.value ?? '');
                            clearErrors('ward');
                          }
                        }}
                      >
                        <option value="">Chọn Phường/Xã:</option>
                        {Array.isArray(wardOptions) &&
                          wardOptions.map((item2: any) => (
                            <option value={item2} key={item2}>
                              {item2}
                            </option>
                          ))}
                      </select>
                      {errors.ward && (
                        <div className="error-message text-small">
                          <i className="fas fa-exclamation-circle" /> Vui lòng
                          chọn Phường/ Xã
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-address-add-item">
                    <label htmlFor="">Địa chỉ chi tiết</label>
                    <div className="form-address-add-item_input">
                      <input
                        type="text"
                        {...register('street', {
                          required: true,
                        })}
                        placeholder="Địa chỉ chi tiết"
                      />
                    </div>
                    {errors.street && (
                      <div className="error-message text-small">
                        <i className="fas fa-exclamation-circle" /> Vui lòng
                        nhập địa chỉ chi tiết
                      </div>
                    )}
                  </div>
                  <label className="label-input">
                    <input
                      type="checkbox"
                      {...register('save_as_default')}
                      checked={checkedDefault}
                      onChange={(event) => {
                        setCheckedDefault(event.target.checked);
                      }}
                    />
                    <span className="label">Đặt làm mặc định</span>
                  </label>
                </div>
              </div>
            </div>
          </form>
        </IonContent>
        <IonFooter class="footer-shipping footer-cart">
          <IonToolbar className="ion-no-padding">
            {props.state?.editAddressObj?.id &&
              props.state?.editAddressObj?.id !=
                props?.state?.currentShippingAddress?.customer_address_id && (
                <button
                  className="btn btn-fs btn-white-main btn-delete-address"
                  onClick={() => {
                    // onDeleteCustomerAdd(props.state?.editAddressObj?.id);
                    setShowModelDelete(true);
                  }}
                  disabled={
                    props.state?.editAddressObj?.id ==
                    props?.state?.currentShippingAddress?.customer_address_id
                  }
                >
                  Xóa địa chỉ
                </button>
              )}

            <button
              className="btn btn-black btn-fs btn-success-address"
              onClick={handleSubmit(onSubmit)}
              disabled={props?.state?.isUpdatingAddress}
            >
              <img src={iconcheckwhite} alt="" className="pd-lft-rght" />
              Xác nhận
            </button>
          </IonToolbar>
        </IonFooter>

        <UiExtension
          uiId="ADDRESS_POPUP_CONFIRM_DELETE"
          close={() => setShowModelDelete(false)}
          onDeleteCustomerAdd={() =>
            onDeleteCustomerAdd(props.state?.editAddressObj?.id)
          }
          showModelDelete={showModelDelete}
        />
      </IonPage>
    </IonModal>
  );
});

export default AddressDetailForm;
