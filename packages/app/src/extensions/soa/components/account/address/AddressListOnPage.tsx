import { IonContent } from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { withCustomerAddress } from '@vjcspy/r/build/modules/account/hoc/withCustomerAddress';
import { withCustomerAddressActions } from '@vjcspy/r/build/modules/account/hoc/withCustomerAddressActions';
import { getCustomerName } from '@vjcspy/r/build/modules/account/util/getCustomerName';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import isNumber from 'lodash/isNumber';
import join from 'lodash/join';
import React, { useCallback, useEffect, useState } from 'react';

import iconplus from '../../../assets/images/icons/product-page/icon-plus.svg';

const AddressListOnCheckout: React.FC<{}> = combineHOC(
  withCustomerAddress,
  withCustomerAddressActions,
  withCustomer
)((props) => {
  const [showPopupAddress, setShowPopupAddress] = useState(false);
  const [dataEditAddress, setDataEditAddress] = useState({});

  const handleToggle = (flag: boolean) => {
    setShowPopupAddress(flag);
  };

  const editCustomerAddress = useCallback((address: any) => {
    setDataEditAddress(address);
    setShowPopupAddress(true);
  }, []);

  const isDefaultAddress = useCallback(
    (add: any) => {
      return props.state?.customer?.default_shipping == add.id;
    },
    [props.state?.customer?.default_shipping]
  );

  const onDeleteCustomerAdds = useCallback((addId?: any) => {
    if (
      typeof props.actions.deleteCustomerAddress === 'function' &&
      isNumber(addId)
    ) {
      props.actions.deleteCustomerAddress(addId);
    }
  }, []);

  const setDefaultAddress = useCallback((data: any) => {
    const input: any = {};
    input['firstname'] = data.firstname;
    input['lastname'] = data.lastname;
    input['iz_address_district'] = data.district;
    input['iz_address_ward'] = data.ward;
    input['iz_address_province'] = data.province;
    input['default_billing'] = true;
    input['default_shipping'] = true;
    input['street'] = data.street;
    input['telephone'] = data.telephone;

    if (data?.id && typeof props.actions.updateCustomerAddress === 'function') {
      props.actions.updateCustomerAddress(data?.id, input);
    }
  }, []);

  useEffect(() => {
    if (showPopupAddress) {
      setShowPopupAddress(false);
    }
  }, [props.state?.customerAddress]);

  return (
    <IonContent>
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <div className="shipping-list">
        <div className="container">
          <div className="wrap-address">
            {props.state?.customerAddress?.map((add: any) => {
              return (
                <div className="wrap-address-item flex " key={add?.id}>
                  <div className="list-address">
                    <div className="list-address_item">
                      <div className="list-address_item__info">
                        <h3 className="item__info-name">
                          {getCustomerName(add)} | {add.telephone}
                        </h3>
                        <p className="item__info-address">
                          {` ${join(add['street'], ' ')},
                                  ${add['iz_address_ward']},
                                  ${add['iz_address_district']},
                                  ${add['iz_address_province']}`}
                        </p>
                        <p className="item__info-edit flex flex-content-bw">
                          {isDefaultAddress(add) ? (
                            <button className="text-small">
                              Địa chỉ mặc định
                            </button>
                          ) : (
                            <button
                              className="text-main text-small"
                              onClick={() => {
                                setDefaultAddress(add);
                              }}
                            >
                              Đặt làm mặc định
                            </button>
                          )}

                          <button
                            className="text-main text-small"
                            onClick={() => {
                              editCustomerAddress(add);
                            }}
                          >
                            Sửa địa chỉ
                          </button>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <button
        type="button"
        className="btn-add-address flex-center"
        onClick={() => {
          setDataEditAddress({});
          setShowPopupAddress(true);
        }}
      >
        <img src={iconplus} alt="" />
        <span>Thêm địa chỉ mới</span>
      </button>

      <UiExtension
        uiId="ADDRESS_DETAIL_FORM_ON_PAGE"
        close={() => handleToggle(false)}
        dataEditAddress={dataEditAddress}
        onDeleteCustomerAdds={onDeleteCustomerAdds}
        showPopupAddress={showPopupAddress}
      />
    </IonContent>
  );
});

export default AddressListOnCheckout;
