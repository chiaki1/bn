import {
  IonContent,
  IonFooter,
  IonModal,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { withCustomerAddress } from '@vjcspy/r/build/modules/account/hoc/withCustomerAddress';
import { withCustomerAddressActions } from '@vjcspy/r/build/modules/account/hoc/withCustomerAddressActions';
import { getCustomerName } from '@vjcspy/r/build/modules/account/util/getCustomerName';
import { withCheckoutAddressActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressActions';
import { withCheckoutAddressData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import join from 'lodash/join';
import React, { useCallback, useEffect, useState } from 'react';

import { AppRoute } from '../../../../../router/Route';
import iconcheckwhite from '../../../assets/images/icons/icon-check-white.svg';
import iconplus from '../../../assets/images/icons/product-page/icon-plus.svg';

const AddressListOnCheckout: React.FC<{
  close: () => void;
  showPopup: boolean;
}> = combineHOC(
  withCustomerAddress,
  withCheckoutAddressData,
  withCheckoutAddressActions,
  withCustomerAddressActions,
  withCustomer
)((props) => {
  const [selectAddress, setSelectAddress] = useState('');

  const editCustomerAddress = useCallback((address: any) => {
    if (typeof props.actions.editAddress === 'function') {
      props.actions.editAddress(address);
    }
  }, []);

  const selectCustomerAdd = useCallback((addressId: any) => {
    if (
      typeof props.actions?.setBillingAddress === 'function' &&
      typeof props.actions?.setShippingAddress === 'function'
    ) {
      props.actions.setBillingAddress(undefined, addressId);
      props.actions.setShippingAddress(undefined, addressId);
      // props?.close();
      RouterSingleton.push(`/${AppRoute.CHECKOUT}`);
    }
  }, []);

  const isDefaultAddress = useCallback(
    (add: any) => {
      return props.state?.customer?.default_shipping == add.id;
    },
    [props.state?.customer?.default_shipping]
  );

  const setDefaultAddress = useCallback((data: any) => {
    const input: any = {};
    input['firstname'] = data.firstname;
    input['lastname'] = data.lastname;
    input['iz_address_district'] = data.district;
    input['iz_address_ward'] = data.ward;
    input['iz_address_province'] = data.province;
    input['default_billing'] = true;
    input['default_shipping'] = true;
    input['street'] = data.street;
    input['telephone'] = data.telephone;

    if (data?.id && typeof props.actions.updateCustomerAddress === 'function') {
      props.actions.updateCustomerAddress(data?.id, input);
    }
  }, []);

  useEffect(() => {
    if (props?.state?.currentShippingAddress?.customer_address_id) {
      setSelectAddress(
        props?.state?.currentShippingAddress?.customer_address_id
      );
    }
  }, [props?.state?.currentShippingAddress]);

  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Sổ địa chỉ"
        showIcon={false}
      />
      <IonContent>
        <div className="shipping-list">
          <div className="container">
            <div className="wrap-address">
              {props.state?.customerAddress?.map((add: any) => {
                return (
                  <div className="wrap-address-item flex " key={add?.id}>
                    <div className="address-checkbox">
                      <label className="label-input">
                        <input
                          name="amshopby"
                          type="radio"
                          id={add?.id}
                          value={add?.id}
                          onChange={() => {
                            setSelectAddress(add.id);
                          }}
                          checked={selectAddress == add.id}
                        />
                        <span className="label" />
                      </label>
                    </div>
                    <div className="list-address">
                      <div className="list-address_item">
                        <div className="list-address_item__info">
                          <h3 className="item__info-name">
                            {getCustomerName(add)} | {add.telephone}
                          </h3>
                          <p className="item__info-address">
                            {` ${join(add['street'], ' ')},
                                  ${add['iz_address_ward']},
                                  ${add['iz_address_district']},
                                  ${add['iz_address_province']}`}
                          </p>
                          <p className="item__info-edit flex flex-content-bw">
                            {isDefaultAddress(add) ? (
                              <button className="text-small">
                                Địa chỉ mặc định
                              </button>
                            ) : (
                              <button
                                className="text-main text-small"
                                onClick={() => {
                                  setDefaultAddress(add);
                                }}
                              >
                                Đặt làm mặc định
                              </button>
                            )}

                            <button
                              className="text-main text-small"
                              onClick={() => {
                                editCustomerAddress(add);
                              }}
                            >
                              Sửa địa chỉ
                            </button>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <button
          type="button"
          className="btn-add-address flex-center"
          onClick={() => editCustomerAddress({})}
        >
          <img src={iconplus} alt="" />
          <span>Thêm địa chỉ mới</span>
        </button>
      </IonContent>
      <IonFooter class="footer-shipping footer-cart">
        <IonToolbar className="ion-no-padding">
          <div className="container">
            <button
              className="btn btn-black btn-fs btn-success-address"
              onClick={() => {
                selectCustomerAdd(selectAddress);
              }}
            >
              <img src={iconcheckwhite} alt="" className="pd-lft-rght" />
              Đồng ý
            </button>
          </div>
        </IonToolbar>
      </IonFooter>

      <UiExtension
        uiId="ADDRESS_DETAIL_FORM"
        showPopup={props.state?.isEditingAddress}
      />
    </IonPage>
  );
});

export default AddressListOnCheckout;
