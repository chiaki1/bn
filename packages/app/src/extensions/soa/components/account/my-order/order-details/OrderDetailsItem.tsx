import { IonCol, IonImg, IonRow } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useEffect, useState } from 'react';

import iconclose from '../../../../assets/images/icons/icon-close.svg';
import iconplus from '../../../../assets/images/icons/product-page/icon-plus.svg';

const OrderDetailsItem: React.FC<{ itemCart: any }> = combineHOC()((props) => {
  const [showAll, setShowAll] = useState(false);
  const [itemShow, setItemShow] = useState(1);

  useEffect(() => {
    if (!showAll) {
      setItemShow(1);
    } else {
      setItemShow(props?.itemCart?.items?.length);
    }
  }, [showAll, props?.itemCart]);
  return (
    <div className="order-history-item">
      <div className="order-history-item_top flex flex-content-bw flex-center">
        <h3>
          {props?.itemCart?.name} (
          {props?.itemCart?.countItem || props?.itemCart?.items?.length || 0}{' '}
          sản phẩm)
        </h3>
        <div className=" text-small">
          {/*Phí vận chuyển <span className="text-main">100.000</span>*/}
        </div>
      </div>
      <div className="order-history-item_middle">
        {props?.itemCart?.items?.length > 0 &&
          props?.itemCart?.items?.slice(0, itemShow).map((item: any) => (
            <div key={item?.id}>
              {props?.buttonStatus()}
              <div className="order-history-item-product">
                <div className="minicart-item">
                  <div className="minicart-item-detail">
                    <div className="product-photo">
                      <IonImg class="center" src={item?.image} />
                    </div>
                    <div className="product-item-detail">
                      <h3 className="product-name text-lg">
                        {item?.product_name}
                      </h3>
                      {size(item?.selected_options) > 0 && (
                        <ul className="product-options text-small text-gray list-reset">
                          {item?.selected_options.map((se: any) => (
                            <li className="option-item" key={se?.value}>
                              {se?.label}:
                              <span className="option-value">{se?.value}</span>
                            </li>
                          ))}
                        </ul>
                      )}

                      <div className="price-box">
                        <p className="special-price">
                          <UiExtension
                            uiId="CURRENCY"
                            price={item?.product_sale_price.value || 0}
                          />
                        </p>
                        {item?.product_sale_price?.value <
                          item?.original_price.value && (
                          <p className="old-price">
                            <UiExtension
                              uiId="CURRENCY"
                              price={item?.original_price.value || 0}
                            />
                          </p>
                        )}
                      </div>
                      <span className="product-options text-gray list-reset flex">
                        Số lượng: {item?.quantity_ordered || 0}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
      {props?.itemCart?.items?.length > 1 && (
        <button
          type="button"
          className="btn-add-address flex-center"
          onClick={() => setShowAll(!showAll)}
        >
          {showAll ? (
            <>
              <img src={iconclose} alt="" />
              <span>Thu gọn</span>
            </>
          ) : (
            <>
              <img src={iconplus} alt="" />
              <span>Xem tất cả</span>
            </>
          )}
        </button>
      )}
    </div>
  );
});

export default OrderDetailsItem;
