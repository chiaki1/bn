import { combineHOC } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import moment from 'moment/moment';
import React, { useMemo } from 'react';

import iconPayment from '../../../../assets/images/icons/icon-payment.svg';

const OrderDetailMethodPay: React.FC<{ paymentMethod: any; orderItems: any }> =
  combineHOC()((props) => {
    const incrementId = useMemo(() => {
      if (
        props?.orderItems &&
        size(props?.orderItems) > 0 &&
        props?.orderItems[0]?.payment_data
      ) {
        return props?.orderItems[0]?.payment_data?.increment_id;
      }
      return null;
    }, [props?.orderItems]);
    const paidTime = useMemo(() => {
      if (
        props?.orderItems &&
        size(props?.orderItems && props?.orderItems[0]?.payment_data) > 0
      ) {
        return props?.orderItems[0]?.payment_data?.paid_time;
      }
      return null;
    }, [props?.orderItems]);
    return (
      <div className="shipping-method">
        <div className="container">
          <div className="promo-item">
            <div className="promo-btn flex-center btn-arrow-right">
              <span className="promo-logo">
                <img className="icon" src={iconPayment} />
              </span>
              <span className="promo-name text-lg">Phương thức thanh toán</span>
            </div>
            <div className="promo-name text-lg">
              {props?.paymentMethod?.name}
            </div>
            {incrementId && (
              <div className="promo-detail text-gray flex-center flex-content-bw">
                <p className="promo-des">Mã số giao dịch</p>
                <p className="promo-des">{incrementId}</p>
              </div>
            )}

            {paidTime && (
              <div className="promo-detail text-gray flex-center flex-content-bw">
                <p className="promo-des">Thời gian giao dịch:</p>
                <p className="promo-des">
                  {moment(paidTime).format('hh:mm DD/MM/YYYY')}
                </p>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  });

export default OrderDetailMethodPay;
