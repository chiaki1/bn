import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { formatCustomerAddress } from '@vjcspy/r/build/modules/account/util/formatCustomerAddress';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback } from 'react';

import iconmap from '../../../../assets/images/icons/icon-map.svg';

const OrderDetailsAddress: React.FC<{ address: any }> = combineHOC(
  withCustomer
)((props) => {
  const isDefaultAddress = useCallback(
    (add: any) => {
      return props.state?.customer?.default_shipping == add.customer_address_id;
    },
    [props.state?.customer?.default_shipping]
  );

  return (
    <div className="order-details-address">
      <div className="container">
        <div className={'cart-pay-header flex'}>
          <img src={iconmap} alt="" />
          <div className={'list-address'}>
            <div className={'list-address_item'}>
              <div className={'list-address_item__info'}>
                <h3 className={'item__info-name'}>
                  {props?.address?.firstname} {props?.address?.lastname}|{' '}
                  {props?.address?.telephone}
                </h3>
                <p className={'item__info-address'}>
                  {formatCustomerAddress(props.address)}
                </p>
                <p className={'item__info-edit flex flex-content-bw'}>
                  <button className={'text-xs'}>
                    {isDefaultAddress(props?.address) && 'Địa chỉ mặc định'}
                  </button>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr className="un-container" />
    </div>
  );
});

export default OrderDetailsAddress;
