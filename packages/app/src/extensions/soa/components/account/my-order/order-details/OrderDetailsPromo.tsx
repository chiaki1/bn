import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import iconVoucher from '../../../../assets/images/icons/icon-voucher.svg';

const OrderDetailsPromo: React.FC<{ vouchers: any }> = combineHOC()((props) => {
  return (
    <div className="promo__items">
      <div className="container">
        <div className="promo-item promo-voucher">
          <div className="promo-btn flex-center btn-arrow-right">
            <span className="promo-logo">
              <img className="icon" src={iconVoucher} />
            </span>
            <span className="promo-name text-lg">Mã giảm giá</span>
          </div>
          <div className="promo-name text-lg"></div>
          <div className="promo-detail text-gray flex flex-wrap ion-justify-content-between">
            <p className="promo-des">{props?.vouchers?.label}</p>
            <p className="no-mr">
              <UiExtension
                uiId="CURRENCY"
                price={props?.vouchers?.amount?.value || 0}
              />
            </p>
          </div>
        </div>
        {/*<div className="promo-item promo-reward">*/}
        {/*  <div className="promo-btn flex-center btn-arrow-right">*/}
        {/*    <span className="promo-logo">*/}
        {/*      <img className="icon" src={iconReward} />*/}
        {/*    </span>*/}
        {/*    <span className="promo-name text-lg">Dùng reward points</span>*/}
        {/*  </div>*/}
        {/*  <div className="promo-detail text-gray flex flex-wrap ion-justify-content-between">*/}
        {/*    <p className="promo-des">Sử dụng 1.000.000 reward points</p>*/}
        {/*  </div>*/}
        {/*</div>*/}
      </div>
    </div>
  );
});

export default OrderDetailsPromo;
