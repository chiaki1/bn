import { IonHeader, IonToolbar } from '@ionic/react';
import { withCustomerOrderPageInfo } from '@vjcspy/r/build/modules/account/hoc/my-orders/withCustomerOrderPageInfo';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback } from 'react';

import ACCOUNT from '../../../../../../values/extendable/ACCOUNT';

const OrderHistoryTab: React.FC = combineHOC(withCustomerOrderPageInfo)(
  (props) => {
    const setStatusFilter = (status: string) => {
      if (props.actions?.setFilterStatus) {
        props.actions.setFilterStatus(status);
      }
    };
    return (
      <IonHeader
        className="page-header ion-no-border header-account"
        translucent={false}
      >
        <div className="header-content text-center">
          <IonToolbar color="nobg">
            <div className="header-container flex-center ">
              <button
                className="btn-link-white btn-back"
                onClick={() => {
                  RouterSingleton.back();
                }}
              >
                <i className="fal fa-chevron-left" />
              </button>

              <h1 className="account-pages-title">Lịch sử đơn hàng</h1>

              {/*<button className="btn-link-white btn-info"></button>*/}
            </div>
          </IonToolbar>
        </div>
        <div className="order-history-tab nav-tab">
          <ul className="list-reset tab-items">
            {ACCOUNT.r('MY_ORDER_STATUS').map((status: any) => {
              return (
                <li className="item-nav" key={status.value}>
                  <div
                    className={clsx(
                      'tab-item',
                      (props.state?.pageFilter?.status == status.value ||
                        status.value?.includes(
                          props.state?.pageFilter?.status
                        )) &&
                        'active'
                    )}
                    onClick={() => setStatusFilter(status.value)}
                  >
                    <span>{status.title}</span>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </IonHeader>
    );
  }
);

export default OrderHistoryTab;
