import { IonImg } from '@ionic/react';
import { withReorderActions } from '@vjcspy/r/build/modules/account/hoc/my-orders/withReorderActions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useCallback, useMemo } from 'react';

import { AppRoute } from '../../../../../../router/Route';
import iconright from '../../../../assets/images/icons/arrow-right.svg';

const OrderHistoryItem: React.FC<{
  order: any;
}> = combineHOC(withReorderActions)((props) => {
  const goDetail = useCallback((orderId: any) => {
    if (orderId) {
      RouterSingleton.push(`/${AppRoute.ORDER_DETAIL}/${orderId}`);
    }
  }, []);

  const buttonStatus = useMemo(() => {
    switch (props?.order?.status_code) {
      case 'complete':
        return (
          <div
            className="status-order-history status-success flex flex-center flex-content-bw"
            onClick={() => goDetail(props.order.number)}
          >
            Giao hàng thành công
            <img
              src={iconright}
              alt=""
              onClick={() => goDetail(props.order.number)}
            />
          </div>
        );
      case 'canceled':
        return (
          <div
            className="status-order-history status-error flex flex-center flex-content-bw"
            onClick={() => goDetail(props.order.number)}
          >
            Đã hủy
            <img src={iconright} alt="" />
          </div>
        );
      case 'delivery':
        return (
          <div
            className="status-order-history status-warning flex flex-center flex-content-bw"
            onClick={() => goDetail(props.order.number)}
          >
            Đang giao hàng
            <img src={iconright} alt="" />
          </div>
        );
      case 'pending':
        return (
          <div
            className="status-order-history status-warning flex flex-center flex-content-bw"
            onClick={() => goDetail(props.order.number)}
          >
            Chờ xác nhận
            <img src={iconright} alt="" />
          </div>
        );
      case 'waiting_to_pickup':
        return (
          <div
            className="status-order-history status-warning flex flex-center flex-content-bw"
            onClick={() => goDetail(props.order.number)}
          >
            Chờ lấy hàng
            <img src={iconright} alt="" />
          </div>
        );
      case 'closed':
        return (
          <div
            className="status-order-history status-error flex flex-center flex-content-bw"
            onClick={() => goDetail(props.order.number)}
          >
            Đã đổi trả
            <img src={iconright} alt="" />
          </div>
        );
    }
    return (
      <div
        className="status-order-history status-warning flex flex-center flex-content-bw"
        onClick={() => goDetail(props.order.number)}
      >
        Xem chi tiết
        <img src={iconright} alt="" />
      </div>
    );
  }, [props.order]);

  const totalsItems = useMemo(() => {
    const count = props?.order?.items.reduce((res: any, item: any) => {
      return (res += item?.quantity_ordered);
    }, 0);
    return count;
  }, [props?.order?.items]);

  const listBrand = useMemo(() => {
    const list = props?.order?.items.reduce(
      (res: any, item: any) => {
        if (item?.brand?.name && !res.includes(item?.brand?.name)) {
          res.push(item?.brand?.name);
        }
        return res;
      },
      ['Other']
    );
    return list;
  }, [props?.order?.items]);

  if (!props?.order) {
    return null;
  }

  return (
    <div className="order-history-item">
      <div className="order-history-item_top flex flex-content-bw flex-center">
        <h3>{listBrand.toString()}</h3>
        <button
          className="text-main text-small"
          onClick={() => goDetail(props.order.number)}
        >
          Chi tiết
        </button>
      </div>
      <div className="order-history-item_middle">
        {buttonStatus}
        {props?.order?.items.slice(0, 1).map((item: any) => {
          return (
            <div className="order-history-item-product" key={item?.id}>
              <div className="minicart-item">
                <div className="minicart-item-detail">
                  <div className="product-photo">
                    <IonImg class="center" src={item?.image} />
                  </div>
                  <div className="product-item-detail">
                    <h3 className="product-name text-lg">
                      {item?.product_name}
                    </h3>
                    {item?.selected_options &&
                      size(item?.selected_options) > 0 && (
                        <ul className="product-options text-small text-gray list-reset">
                          {item?.selected_options.map((op: any) => (
                            <li className="option-item" key={op?.value}>
                              {op?.label}:
                              <span className="option-value">{op?.value}</span>
                            </li>
                          ))}
                        </ul>
                      )}

                    <div className="price-box">
                      <p className="special-price">
                        <UiExtension
                          uiId="CURRENCY"
                          price={item?.product_sale_price?.value || 0}
                        />
                      </p>
                      {item?.product_sale_price?.value <
                        item?.original_price.value && (
                        <p className="old-price">
                          <UiExtension
                            uiId="CURRENCY"
                            price={item?.original_price.value || 0}
                          />
                        </p>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="order-history-item_bottom">
        <ul className="item_bottom__items list-reset">
          <li className="item_bottom__item flex flex-content-bw">
            {props?.order?.items?.length} sản phẩm{' '}
            <span className="price text-main">
              <UiExtension
                uiId="CURRENCY"
                price={props?.order?.total?.grand_total?.value}
              />
            </span>
          </li>
          <li className="item_bottom__item flex flex-content-bw">
            Mã đơn hàng <span>#{props?.order?.number}</span>
          </li>
        </ul>
        <button
          className="btn-black btn-lg"
          onClick={() => {
            if (
              props.order.number &&
              typeof props?.actions?.reorderAction === 'function'
            ) {
              props?.actions?.reorderAction(props.order.number);
            }
          }}
        >
          Mua lại
        </button>
      </div>
      <hr className="un-container" />
    </div>
  );
});

export default OrderHistoryItem;
