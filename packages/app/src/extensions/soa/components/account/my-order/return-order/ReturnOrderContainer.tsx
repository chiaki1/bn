import { IonContent } from '@ionic/react';
import { withInitAccountState } from '@vjcspy/r/build/modules/account/hoc/withInitAccountState';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import moment from 'moment/moment';
import React, { useEffect, useMemo } from 'react';
import { useParams } from 'react-router';

import { withSoaCustomerOrderDetail } from '../../../../hoc/my-orders/withSoaCustomerOrderDetail';

const ReturnOrderContainer: React.FC = combineHOC(
  withSoaCustomerOrderDetail,
  withInitAccountState
)((props) => {
  const { orderId } = useParams<{
    orderId: any;
  }>();

  const totalsDiscount = useMemo(() => {
    if (size(props?.state.orderDetail?.total?.discounts) > 0) {
      const count = props?.state.orderDetail?.total?.discounts.reduce(
        (res: any, item: any) => {
          return (res += item?.amount?.value);
        },
        0
      );
      return count;
    }
    return 0;
  }, [props?.state.orderDetail?.total?.discounts]);

  useEffect(() => {
    if (
      typeof props.actions?.getOrderDetail === 'function' &&
      orderId &&
      props.state?.isResolvedAccountState === true
    ) {
      props.actions.getOrderDetail(orderId);
    }
  }, [orderId, props.state?.isResolvedAccountState]);

  return (
    <IonContent>
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <div className="code-exchange">
        <div className="container">
          <div className="flex flex-content-bw flex-center title-account">
            <h2>Mã đơn hàng</h2>
            <span className="code">{props?.state?.orderDetail?.number}</span>
          </div>
          <table className="table-order-summary table">
            <tbody>
              <tr>
                <th>Tổng tiền hàng:</th>
                <td>
                  <span className=" price">
                    <UiExtension
                      uiId="CURRENCY"
                      price={
                        props?.state?.orderDetail?.total?.grand_total?.value ||
                        0
                      }
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Phí vận chuyển:</th>
                <td>
                  <span className=" price">
                    <UiExtension
                      uiId="CURRENCY"
                      price={
                        props?.state?.orderDetail?.total?.total_shipping
                          ?.value || 0
                      }
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Giảm giá:</th>
                <td>
                  <span className=" price">
                    {props?.totalsDiscount ? '- ' : ' '}
                    <UiExtension uiId="CURRENCY" price={totalsDiscount || 0} />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Dùng điểm tích lũy:</th>
                <td>
                  <span className=" price">
                    {props?.state?.orderDetail?.rewardCurrencyAmount
                      ? '- '
                      : ' '}
                    <UiExtension
                      uiId="CURRENCY"
                      price={
                        props?.state?.orderDetail?.rewardCurrencyAmount || 0
                      }
                    />
                  </span>
                </td>
              </tr>

              <tr>
                <th>Thời gian đặt hàng:</th>
                <td>
                  <span className=" price">
                    {props?.state?.orderDetail?.order_date &&
                      moment(props?.state?.orderDetail?.order_date).format(
                        'DD/MM/YYYY hh:mm'
                      )}
                  </span>
                </td>
              </tr>
              {/*<tr>*/}
              {/*  <th>Giao hàng dự kiến:</th>*/}
              {/*  <td>*/}
              {/*    <span className=" price">*/}
              {/*      {moment(props?.state?.orderDetail?.order_date)*/}
              {/*        .add(3, 'd')*/}
              {/*        .format('DD/MM/YYYY hh:mm')}*/}
              {/*    </span>*/}
              {/*  </td>*/}
              {/*</tr>*/}
            </tbody>
          </table>
        </div>
        <hr />
      </div>
      <UiExtension
        uiId="LIST_BRANCH_RETURN"
        orderDetail={props?.state?.orderDetail}
      />
    </IonContent>
  );
});

export default ReturnOrderContainer;
