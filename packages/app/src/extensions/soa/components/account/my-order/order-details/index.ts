import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ORDER_DETAILS_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ORDER_DETAIL_CONTAINER',
    component: { lazy: React.lazy(() => import('./OrderDetailContainer')) },
  },
  {
    uiId: 'ORDER_DETAIL_METHOD_PAY',
    component: { lazy: React.lazy(() => import('./OrderDetailMethodPay')) },
  },
  {
    uiId: 'ORDER_DETAIL_ADDRESS',
    component: { lazy: React.lazy(() => import('./OrderDetailsAddress')) },
  },
  {
    uiId: 'ORDER_DETAIL_TOTALS',
    component: { lazy: React.lazy(() => import('./OrderDetailsTotals')) },
  },
  {
    uiId: 'ORDER_DETAIL_LIST',
    component: { lazy: React.lazy(() => import('./OrderDetailsList')) },
  },
  {
    uiId: 'ORDER_DETAIL_PROMO',
    component: { lazy: React.lazy(() => import('./OrderDetailsPromo')) },
  },
  {
    uiId: 'ORDER_DETAIL_ITEM',
    component: { lazy: React.lazy(() => import('./OrderDetailsItem')) },
  },
  {
    uiId: 'ORDER_DETAIL_FOOTER',
    component: { lazy: React.lazy(() => import('./OrderDetailFooter')) },
  },
];
