import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback } from 'react';

const ReorderItemQty: React.FC = combineHOC()((props) => {
  const isValidNumber = useCallback((number: any) => {
    if (isNaN(number)) {
      return false;
    }
    if (props?.canZero) {
      return parseInt(number + '') >= 0;
    } else {
      return parseInt(number + '') > 0;
    }
  }, []);

  return (
    <div className="minicart-actions flex ion-justify-content-between ">
      <div className="flex form-qty">
        <input
          className="input-text qty-item"
          value={props?.getQtyItem(
            props?.item?.id,
            props?.item?.quantity_ordered
          )}
          onChange={(event) => {}}
        />
        <span
          className="qty-change qty-item  orderFirst"
          onClick={() => {
            if (
              props?.listSelectItemWithQty[props?.item?.id] &&
              isValidNumber(
                props?.listSelectItemWithQty[props?.item?.id] - 1
              ) &&
              props?.listSelectItemWithQty[props?.item?.id] <=
                props?.item?.quantity_ordered
            ) {
              props?.onchangeQtyItem(
                props?.item?.id,
                props?.listSelectItemWithQty[props?.item?.id] - 1
              );
            }
          }}
        >
          <i className="fal fa-minus" />
        </span>
        <span
          className="qty-change qty-item "
          onClick={() => {
            if (
              props?.listSelectItemWithQty[props?.item?.id] &&
              isValidNumber(
                props?.listSelectItemWithQty[props?.item?.id] + 1
              ) &&
              props?.listSelectItemWithQty[props?.item?.id] <
                props?.item?.quantity_ordered
            ) {
              props?.onchangeQtyItem(
                props?.item?.id,
                props?.listSelectItemWithQty[props?.item?.id] + 1
              );
            }
          }}
        >
          <i className="fal fa-plus" />
        </span>
      </div>
    </div>
  );
});

export default ReorderItemQty;
