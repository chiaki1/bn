import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import moment from 'moment/moment';
import React, { useCallback, useMemo, useState } from 'react';

import iconright from '../../../../assets/images/icons/arrow-right.svg';

const OrderDetailsList: React.FC<{ orderDetail: any }> = combineHOC()(
  (props) => {
    const shippingDate = props.orderDetail?.order_date_with_timezone;
    const buttonStatus = useCallback(() => {
      switch (props?.orderDetail?.status_code) {
        case 'complete':
          return (
            <div className="status-order-history status-success flex flex-center flex-content-bw">
              <div className="txt-status">
                <span className="text-lg">Giao thành công</span>
                <span className="text-small">
                  Cập nhật:{' '}
                  {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
                </span>
              </div>
              {/*<img src={iconright} alt="" />*/}
            </div>
          );
        case 'canceled':
          return (
            <div className="status-order-history status-error flex flex-center flex-content-bw">
              <div className="txt-status">
                <span className="text-lg">Đã hủy</span>
                <span className="text-small">
                  Cập nhật:{' '}
                  {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
                </span>
              </div>
              {/*<img src={iconright} alt="" />*/}
            </div>
          );
        case 'delivery':
          return (
            <div className="status-order-history status-warning flex flex-center flex-content-bw">
              <div className="txt-status">
                <span className="text-lg">Đang giao hàng</span>
                <span className="text-small">
                  Cập nhật:{' '}
                  {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
                </span>
              </div>
              {/*<img src={iconright} alt="" />*/}
            </div>
          );
        case 'pending':
          return (
            <div className="status-order-history status-warning flex flex-center flex-content-bw">
              <div className="txt-status">
                <span className="text-lg">Chờ xác nhận</span>
                <span className="text-small">
                  Cập nhật:{' '}
                  {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
                </span>
              </div>
              {/*<img src={iconright} alt="" />*/}
            </div>
          );
        case 'waiting_to_pickup':
          return (
            <div className="status-order-history status-warning flex flex-center flex-content-bw">
              <div className="txt-status">
                <span className="text-lg">Chờ lấy hàng</span>
                <span className="text-small">
                  Cập nhật:{' '}
                  {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
                </span>
              </div>
              {/*<img src={iconright} alt="" />*/}
            </div>
          );
        case 'closed':
          return (
            <div className="status-order-history status-error flex flex-center flex-content-bw">
              <div className="txt-status">
                <span className="text-lg">Đã đổi trảosed</span>
                <span className="text-small">
                  Cập nhật:{' '}
                  {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
                </span>
              </div>
              {/*<img src={iconright} alt="" />*/}
            </div>
          );
      }
      return (
        <div className="status-order-history status-warning flex flex-center flex-content-bw">
          <div className="txt-status">
            <span className="text-lg">Chờ xác nhận</span>
            <span className="text-small">
              Cập nhật:{' '}
              {moment(shippingDate || moment()).format('HH:mm DD/MM/YYYY')}
            </span>
          </div>
          {/*<img src={iconright} alt="" />*/}
        </div>
      );
    }, [props?.orderDetail]);

    const listBrand = useMemo(() => {
      const listBrands: any[] = [];
      if (props?.orderDetail?.items) {
        props?.orderDetail?.items
          // .filter((it: any) => it?.product?.brand?.brand_id)
          .forEach((item: any, index: number) => {
            if (item?.brand?.brand_id) {
              listBrands[item?.brand?.brand_id] = {
                ...item?.brand,
                items: [],
                countItem: 0,
              };
            } else {
              listBrands[0] = {
                brand_id: null,
                name: 'Other',
                items: [],
                countItem: 0,
              };
            }
          });
      }
      return listBrands;
    }, [props?.orderDetail?.items]);

    const listItemShow = useMemo(() => {
      let listItems: any[] = [];
      const brand: any[] = listBrand;

      if (
        brand &&
        props?.orderDetail?.items &&
        props?.orderDetail?.items?.length > 0 &&
        Object.values(brand).length > 0
      ) {
        listItems = props?.orderDetail?.items.reduce(
          (result: any, item: any) => {
            if (item?.brand?.brand_id && result[item?.brand?.brand_id]) {
              const itemProducts = result[item?.brand?.brand_id]?.items;
              itemProducts.push(item);
              result[item?.brand?.brand_id] = {
                ...result[item?.brand?.brand_id],
                items: itemProducts,
                countItem: result[item?.brand?.brand_id]?.countItem + 1,
              };

              return result;
            } else {
              const itemProducts = result[0]?.items;
              itemProducts.push(item);
              result[0] = {
                ...result[0],
                items: itemProducts,
                countItem: result[0]?.countItem + 1,
              };
              return result;
            }
            return result;
          },
          brand
        );
      }
      return listItems?.reverse()?.filter((item: any) => item?.name);
    }, [props?.orderDetail?.items, listBrand]);
    return (
      <div className="order-history-list">
        <div className="container">
          <div className="order-Details-list-item">
            {listItemShow &&
              listItemShow.map((item: any) => (
                <UiExtension
                  uiId="ORDER_DETAIL_ITEM"
                  itemCart={item}
                  buttonStatus={buttonStatus}
                  key={item?.brand_id}
                />
              ))}
          </div>
          <hr className="un-container" />
        </div>
      </div>
    );
  }
);

export default OrderDetailsList;
