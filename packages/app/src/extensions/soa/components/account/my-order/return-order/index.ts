import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_EXCHANGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'REASON_EXCHANGE',
    component: { lazy: React.lazy(() => import('./ReasonExchange')) },
  },
  {
    uiId: 'LIST_BRANCH_RETURN',
    component: { lazy: React.lazy(() => import('./ListBrandReturn')) },
  },
  {
    uiId: 'RETURN_ORDER_CONTAINER',
    component: { lazy: React.lazy(() => import('./ReturnOrderContainer')) },
  },
  {
    uiId: 'RETURN_ORDER_BRAND_ITEM',
    component: { lazy: React.lazy(() => import('./ReorderBrandItem')) },
  },
  {
    uiId: 'RETURN_ORDER_ITEM_QTY',
    component: { lazy: React.lazy(() => import('./ReorderItemQty')) },
  },
];
