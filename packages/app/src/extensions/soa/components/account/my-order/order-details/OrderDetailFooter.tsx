import { IonCol, IonFooter, IonRow, IonToolbar } from '@ionic/react';
import { withReorderActions } from '@vjcspy/r/build/modules/account/hoc/my-orders/withReorderActions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useMemo } from 'react';

import { AppRoute } from '../../../../../../router/Route';

const OrderDetailFooter: React.FC<{
  totalsItems: any;
  totals: any;
  canCancel: boolean;
  orderNumber: string;
  orderStatusCode: any;
}> = combineHOC(withReorderActions)((props) => {
  const buttonAction = useMemo(() => {
    switch (props?.orderStatusCode) {
      case 'complete':
        return (
          <IonRow className="mr-row">
            <IonCol size="6">
              <div
                className={clsx(
                  'btn-reset-black btn-fs flex-center-center btn-pd-xs',
                  props?.canCancel && 'disabled'
                )}
                onClick={() => {
                  RouterSingleton.push(
                    '/' + AppRoute.RETURN_ORDER + '/' + props.orderNumber
                  );
                }}
              >
                Yêu cầu trả hàng
              </div>
            </IonCol>
            <IonCol size="6">
              <div
                className="btn-black btn-fs flex-center-center btn-pd-xs"
                onClick={() => {
                  if (
                    props.orderNumber &&
                    typeof props?.actions?.reorderAction === 'function'
                  ) {
                    props?.actions?.reorderAction(props.orderNumber);
                  }
                }}
              >
                Mua lại
              </div>
            </IonCol>
          </IonRow>
        );
      case 'canceled':
        return (
          <IonRow className="mr-row">
            <IonCol size="12">
              <div
                className="btn-black btn-fs flex-center-center btn-pd-xs"
                onClick={() => {
                  if (
                    props.orderNumber &&
                    typeof props?.actions?.reorderAction === 'function'
                  ) {
                    props?.actions?.reorderAction(props.orderNumber);
                  }
                }}
              >
                Mua lại
              </div>
            </IonCol>
          </IonRow>
        );
      case 'delivery':
        return (
          <IonRow className="mr-row">
            <IonCol size="12">
              <div
                className="btn-black btn-fs flex-center-center btn-pd-xs"
                onClick={() => RouterSingleton.push('/' + AppRoute.HOME)}
              >
                Tiếp tục mua sắm
              </div>
            </IonCol>
          </IonRow>
        );
      case 'pending 1':
        return (
          <IonRow className="mr-row">
            <IonCol size="12">
              <div
                className="btn-black btn-fs flex-center-center btn-pd-xs"
                onClick={() => RouterSingleton.push('/' + AppRoute.HOME)}
              >
                Tiếp tục mua sắm
              </div>
            </IonCol>
          </IonRow>
        );
    }
    return (
      <IonRow className="mr-row">
        <IonCol size="12">
          <div
            className="btn-black btn-fs flex-center-center btn-pd-xs"
            onClick={() => RouterSingleton.push('/' + AppRoute.HOME)}
          >
            Tiếp tục mua sắm
          </div>
        </IonCol>
      </IonRow>
    );
  }, [props?.orderStatusCode]);

  return (
    <IonFooter class="footer-cart">
      <div className="container">
        <p className="text-lg text-center cart-total">
          <span className="price-label">Thành tiền:</span>
          <span className="price text-main">
            <UiExtension uiId="CURRENCY" price={props?.totals || 0} />
          </span>
        </p>
        {/*<button className="btn-black btn-fs">Thanh toán</button>*/}
        <IonToolbar className="ion-no-padding">{buttonAction}</IonToolbar>
      </div>
    </IonFooter>
  );
});

export default OrderDetailFooter;
