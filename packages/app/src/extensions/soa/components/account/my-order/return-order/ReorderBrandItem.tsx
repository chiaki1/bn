import { IonImg } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useState } from 'react';

const ReorderBrandItem: React.FC = combineHOC()((props) => {
  const [showBrand, setShowBrand] = useState(true);

  return (
    <div className="cart-vendor">
      <div className="cart-vendor-head flex-center ion-justify-content-between">
        <label className="label-input text-lg-semi">
          <input
            type="checkbox"
            checked={!props?.checkedBrand(props?.itemCart?.brand_id || null)}
            onChange={() => {
              props?.unCheckBrandAction(
                props?.itemCart?.brand_id || null,
                props?.checkedBrand(props?.itemCart?.brand_id || null)
              );
            }}
          />
          <span className="label">
            {props?.itemCart?.name} (
            {props?.itemCart?.countItem || props?.itemCart?.items?.length || 0}{' '}
            sản phẩm)
          </span>
        </label>
        <button
          className="btn-link btn-arr-right"
          onClick={() => {
            setShowBrand(!showBrand);
          }}
        >
          {showBrand ? (
            <i className="far fa-chevron-right" />
          ) : (
            <i className="far fa-chevron-down" />
          )}
        </button>
      </div>
      {showBrand &&
        props?.itemCart?.items?.length > 0 &&
        props?.itemCart?.items?.map((item: any) => (
          <div className="cart-vendor-item" key={item?.id}>
            <div className="cart-checkbox">
              <label className="label-input text-lg-semi">
                <input
                  type="checkbox"
                  checked={props?.listSelectItem.includes(item?.id)}
                  onChange={() => {
                    props?.toggleItem(item?.id, item?.quantity_ordered);
                  }}
                />
                <span className="label" />
              </label>
            </div>
            <div className="minicart-item">
              <div className="minicart-item-detail">
                <div className="product-photo">
                  <IonImg class="center" src={item?.image} />
                </div>
                <div className="product-item-detail">
                  <h3 className="product-name text-lg">{item?.product_name}</h3>
                  {size(item?.selected_options) > 0 && (
                    <ul className="product-options text-small text-gray list-reset">
                      {item?.selected_options.map((se: any) => (
                        <li className="option-item" key={se?.value}>
                          {se?.label}:
                          <span className="option-value">{se?.value}</span>
                        </li>
                      ))}
                    </ul>
                  )}
                  <div className="price-box">
                    <p className="special-price">
                      <UiExtension
                        uiId="CURRENCY"
                        price={item?.product_sale_price.value || 0}
                      />
                    </p>
                    {item?.product_sale_price?.value <
                      item?.original_price.value && (
                      <p className="old-price">
                        <UiExtension
                          uiId="CURRENCY"
                          price={item?.original_price.value || 0}
                        />
                      </p>
                    )}
                  </div>
                  <UiExtension
                    uiId="RETURN_ORDER_ITEM_QTY"
                    item={item}
                    listSelectItemWithQty={props?.listSelectItemWithQty}
                    onchangeQtyItem={props?.onchangeQtyItem}
                    getQtyItem={props?.getQtyItem}
                  />
                </div>
              </div>
            </div>
          </div>
        ))}
    </div>
  );
});

export default ReorderBrandItem;
