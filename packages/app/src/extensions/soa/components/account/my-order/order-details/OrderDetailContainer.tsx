import { IonContent } from '@ionic/react';
import { withInitAccountState } from '@vjcspy/r/build/modules/account/hoc/withInitAccountState';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import first from 'lodash/first';
import size from 'lodash/size';
import React, { useEffect, useMemo } from 'react';
import { useParams } from 'react-router';

import { withSoaCustomerOrderDetail } from '../../../../hoc/my-orders/withSoaCustomerOrderDetail';

const OrderDetailContainer: React.FC = combineHOC(
  withSoaCustomerOrderDetail,
  withInitAccountState
)((props) => {
  const { orderId } = useParams<{
    orderId: any;
  }>();
  useEffect(() => {
    if (
      typeof props.actions?.getOrderDetail === 'function' &&
      orderId &&
      props.state?.isResolvedAccountState === true
    ) {
      props.actions.getOrderDetail(orderId);
    }
  }, [orderId, props.state?.isResolvedAccountState]);

  const paymentMethod = useMemo(() => {
    const firstPayment: any = first(props.state?.orderDetail?.payment_methods);
    if (firstPayment) {
      return firstPayment.name;
    }

    return null;
  }, [props.state?.orderDetail]);

  const totalsItems = useMemo(() => {
    const count = props?.state.orderDetail?.items.reduce(
      (res: any, item: any) => {
        return (res += item?.quantity_ordered);
      },
      0
    );
    return count;
  }, [props?.state.orderDetail?.items]);

  const totalsDiscount = useMemo(() => {
    if (size(props?.state.orderDetail?.total?.discounts) > 0) {
      const count = props?.state.orderDetail?.total?.discounts.reduce(
        (res: any, item: any) => {
          return (res += item?.amount?.value);
        },
        0
      );
      return count;
    }
    return 0;
  }, [props?.state.orderDetail?.total?.discounts]);

  if (!props?.state.orderDetail) {
    return null;
  }

  return (
    <>
      <IonContent>
        <UiExtension uiId="MY_ACCOUNT_GUARD" />
        <UiExtension
          uiId="DEFAULT_LOADING"
          loading={
            !props.state?.orderDetail ||
            props.state?.loading ||
            props?.state?.isUpdatingTotals
          }
        />
        <UiExtension
          uiId="ORDER_DETAIL_ADDRESS"
          address={props?.state?.orderDetail?.shipping_address}
        />
        <UiExtension
          uiId="ORDER_DETAIL_LIST"
          orderDetail={props?.state?.orderDetail}
        />
        {size(props?.state?.orderDetail?.total?.discounts) > 0 && (
          <UiExtension
            uiId="ORDER_DETAIL_PROMO"
            vouchers={props?.state?.orderDetail?.total?.discounts[0]}
          />
        )}

        {size(props?.state?.orderDetail?.payment_methods) > 0 && (
          <UiExtension
            uiId="ORDER_DETAIL_METHOD_PAY"
            paymentMethod={props?.state?.orderDetail?.payment_methods[0]}
            orderItems={props?.state?.orderDetail?.items}
          />
        )}

        <UiExtension
          uiId="ORDER_DETAIL_TOTALS"
          number={props?.state?.orderDetail?.number}
          orderDate={props?.state?.orderDetail?.order_date_with_timezone}
          totals={props?.state?.orderDetail?.total}
          totalsDiscount={totalsDiscount}
          statusOrder={props?.state?.orderDetail?.status}
          rewardCurrencyAmount={
            props?.state?.orderDetail?.reward_currency_amount
          }
        />
      </IonContent>
      <UiExtension
        uiId="ORDER_DETAIL_FOOTER"
        totalsItems={totalsItems}
        totals={props?.state?.orderDetail?.total?.grand_total?.value}
        canCancel={props?.state?.orderDetail?.can_cancel}
        orderNumber={props?.state?.orderDetail?.number}
        orderStatusCode={props?.state?.orderDetail?.status_code}
      />
    </>
  );
});

export default OrderDetailContainer;
