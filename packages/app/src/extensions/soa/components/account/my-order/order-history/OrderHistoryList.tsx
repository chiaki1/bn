import { IonContent } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useMemo } from 'react';

import { withSoaCustomerOrdersCustomer } from '../../../../hoc/my-orders/withSoaCustomerOrdersCustomer';

const OrderHistoryList: React.FC = combineHOC(withSoaCustomerOrdersCustomer)(
  (props) => {
    const ORDERS = useMemo(() => {
      if (size(props?.state?.orders) === 0) {
        return (
          <div className="order-history-list">
            <p>Không có đơn đặt hàng nào tại đây.</p>
          </div>
        );
      } else {
        return (
          <>
            {props?.state?.orders?.map((order: any) => (
              <UiExtension
                uiId="ORDER_HISTORY_ITEM"
                key={order.id}
                order={order}
              />
            ))}
          </>
        );
      }
    }, [props.state?.orders]);

    return (
      <IonContent>
        <UiExtension
          uiId="DEFAULT_LOADING"
          loading={
            !props?.state?.pageInfoRes ||
            (props?.state?.isLoading && props?.state?.orders?.length === 0) ||
            props?.state?.isUpdatingTotals
          }
        />
        <div className="order-history-list">
          <div className="container">
            <div className="order-history-list-item">
              <div className="order-history-item">
                {ORDERS}
                {!!(
                  props.state?.pageInfoRes?.total_pages &&
                  props.state?.pageInfoRes?.total_pages ==
                    props?.state?.pageFilter?.currentPage
                ) && (
                  <div className="myOrder-seen__List text-center pd-vertical">
                    Bạn đã xem hết danh sách
                  </div>
                )}
                <UiExtension
                  uiId="ION_INFINITE_LOADING"
                  handleLoadMorePage={props.actions?.handleLoadMorePage}
                  isDone={
                    !!props.state?.pageInfoRes?.total_pages &&
                    props.state?.pageInfoRes?.total_pages ==
                      props?.state?.pageFilter?.currentPage
                  }
                  isLoading={props.state?.isLoading}
                />
              </div>
            </div>
          </div>
        </div>
      </IonContent>
    );
  }
);

export default OrderHistoryList;
