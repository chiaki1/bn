import { IonModal, IonPage } from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useEffect, useMemo, useState } from 'react';

import { AppRoute } from '../../../../../../router/Route';
import close from '../../../../assets/images/icons/icon-close.svg';
import { withReturnOrderActions } from '../../../../hoc/account/withReturnOrderActions';
import { withCancelOrderListBrandData } from '../../../../hoc/cart/withCancelOrderListBrandData';

const ReturnOrderContainer: React.FC<{ orderDetail: any }> = combineHOC(
  withCancelOrderListBrandData,
  withReturnOrderActions,
  withCustomer
)((props) => {
  const [statusDisable, setStatusDisable] = useState(true);
  const [selectImage, setSelectImage] = useState([]);
  const [comment, setComment] = useState('');
  const [showPopup, setShowPopup] = useState(false);

  const submitReturn = useCallback(() => {
    const input: any = {};
    if (typeof props?.actions?.saveReturnOrder === 'function') {
      const itemSelected: any[] = [];
      if (
        props?.state?.listSelectItemWithQty &&
        Object.keys(props?.state?.listSelectItemWithQty)
      ) {
        Object.keys(props?.state?.listSelectItemWithQty).forEach(
          (item: any) => {
            itemSelected.push({
              order_item_uid: item,
              quantity_to_return: props?.state?.listSelectItemWithQty[item],
            });
          }
        );
      }

      input['contact_email'] = props?.state?.customer?.email;
      input['order_uid'] = props?.orderDetail?.id;
      input['comment_text'] = comment;
      input['items'] = itemSelected;
      input['attachment'] = selectImage;

      props?.actions?.saveReturnOrder(input);
    }
  }, [
    comment,
    props?.orderDetail,
    selectImage,
    props?.state?.listSelectItemWithQty,
  ]);

  useEffect(() => {
    if (
      props?.state?.listSelectItem &&
      props?.state?.listSelectItem.length > 0 &&
      selectImage.length > 0 &&
      comment.length > 0
    ) {
      setStatusDisable(false);
    }
  }, [props?.state?.listSelectItem, selectImage]);

  useEffect(() => {
    if (props?.state?.statusAction) {
      setShowPopup(true);
    }
  }, [props?.state?.statusAction]);

  return (
    <div className="cart-vendor">
      <div className="container">
        {props?.state?.listItemShow &&
          props?.state?.listItemShow.map((item: any) => (
            <UiExtension
              uiId="RETURN_ORDER_BRAND_ITEM"
              itemCart={item}
              orderId={props?.orderDetail?.id}
              key={item?.name}
              toggleItem={props?.actions?.toggleItem}
              listSelectItem={props?.state?.listSelectItem}
              listSelectItemWithQty={props?.state?.listSelectItemWithQty}
              checkedBrand={props?.actions?.checkedBrand}
              unCheckBrandAction={props?.actions?.unCheckBrandAction}
              onchangeQtyItem={props?.actions?.onchangeQtyItem}
              getQtyItem={props?.actions?.getQtyItem}
            />
          ))}
      </div>

      <UiExtension
        uiId="REASON_EXCHANGE"
        setComment={setComment}
        setSelectImage={setSelectImage}
      />
      <div className="button-exchange button-address">
        <button
          className={clsx(
            'btn btn-black btn-fs btn-success-address ',
            statusDisable && 'button-disabled',
            props?.state?.statusButton && 'button-disabled'
          )}
          onClick={() => {
            submitReturn();
          }}
        >
          Xác nhận đổi trả
        </button>
      </div>

      <IonModal isOpen={showPopup}>
        <IonPage className="page-header-no-mrb">
          <div className="popup-exchange">
            <div className="container">
              <div className="desc-popup">
                <p>
                  Chúng tôi đã nhận được yêu cầu trả hàng của bạn. Bộ phận chăm
                  sóc khách hàng của chúng tôi sẽ liên hệ với bạn trong thời
                  gian sớm nhất
                </p>
                <button
                  className="btn btn-black btn-fs btn-success-address"
                  onClick={() => {
                    setShowPopup(false);
                    setTimeout(() => {
                      RouterSingleton.push('/' + AppRoute.HOME);
                    }, 1300);
                  }}
                >
                  Đóng
                </button>
                <button
                  className="btn-close"
                  onClick={() => {
                    setShowPopup(false);
                    setTimeout(() => {
                      RouterSingleton.push('/' + AppRoute.HOME);
                    }, 1300);
                  }}
                >
                  <img src={close} alt="" />
                </button>
              </div>
            </div>
          </div>
        </IonPage>
      </IonModal>
    </div>
  );
});

export default ReturnOrderContainer;
