import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useEffect, useState } from 'react';

import iconImages from '../../../../assets/images/icons/icon-images.svg';

const ReasonExchange: React.FC = combineHOC()((props) => {
  const [photo, setPhoto] = useState<any>('');
  const [photo1, setPhoto1] = useState<any>('');
  const [photo2, setPhoto2] = useState<any>('');
  const [base64Photo, setBase64Photo] = useState<any>({
    base64_encoded_data: '',
    name: '',
    type: '',
  });
  const [base64Photo1, setBase64Photo1] = useState<any>({
    base64_encoded_data: '',
    name: '',
    type: '',
  });
  const [base64Photo2, setBase64Photo2] = useState<any>({
    base64_encoded_data: '',
    name: '',
    type: '',
  });

  const takePicture = async () => {
    const photos = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      // resultType: CameraResultType.Uri,
      source: CameraSource.Photos,
      quality: 100,
      correctOrientation: true,
      allowEditing: false,
    });

    if (photos?.base64String) {
      setBase64Photo({
        ...base64Photo,
        base64_encoded_data: photos?.base64String,
        name:
          props?.state?.accountState?.customer?.retail_telephone +
            '.' +
            photos?.format || '',
        type: 'image/' + photos?.format,
      });
      setPhoto(
        'data:image/' + photos?.format + ';base64, ' + photos?.base64String
      );
    }
  };

  const takePicture1 = async () => {
    const photos = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      // resultType: CameraResultType.Uri,
      source: CameraSource.Photos,
      quality: 100,
      correctOrientation: true,
      allowEditing: false,
    });

    if (photos?.base64String) {
      setBase64Photo1({
        ...base64Photo,
        base64_encoded_data: photos?.base64String,
        name:
          props?.state?.accountState?.customer?.retail_telephone +
            '.' +
            photos?.format || '',
        type: 'image/' + photos?.format,
      });
      setPhoto1(
        'data:image/' + photos?.format + ';base64, ' + photos?.base64String
      );
    }
  };

  const takePicture2 = async () => {
    const photos = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      // resultType: CameraResultType.Uri,
      source: CameraSource.Photos,
      quality: 100,
      correctOrientation: true,
      allowEditing: false,
    });

    if (photos?.base64String) {
      setBase64Photo2({
        ...base64Photo,
        base64_encoded_data: photos?.base64String,
        name:
          props?.state?.accountState?.customer?.retail_telephone +
            '.' +
            photos?.format || '',
        type: 'image/' + photos?.format,
      });
      setPhoto2(
        'data:image/' + photos?.format + ';base64, ' + photos?.base64String
      );
    }
  };

  useEffect(() => {
    if (
      base64Photo?.base64_encoded_data.length > 0 ||
      base64Photo1?.base64_encoded_data.length > 0 ||
      base64Photo2?.base64_encoded_data.length > 0
    ) {
      const dataImage: any = [];
      if (base64Photo?.base64_encoded_data.length > 0) {
        dataImage.push({
          file: base64Photo?.name,
          content: {
            ...base64Photo,
          },
        });
      }
      if (base64Photo1?.base64_encoded_data.length > 0) {
        dataImage.push({
          file: base64Photo1?.name,
          content: {
            ...base64Photo1,
          },
        });
      }
      if (base64Photo2?.base64_encoded_data.length > 0) {
        dataImage.push({
          file: base64Photo2?.name,
          content: {
            ...base64Photo2,
          },
        });
      }
      props?.setSelectImage(dataImage);
    }
  }, [base64Photo, base64Photo1, base64Photo2]);

  return (
    <div className="reason-exchange">
      <hr />
      <div className="container">
        <div className="desc-exchange">
          <h3 className="title-reason">Lý do trao đổi</h3>
          <div className="area-exchange">
            <textarea
              placeholder="Lý do trả sản phẩm"
              rows={5}
              onChange={(e) => {
                props?.setComment(e.target.value);
              }}
            />
          </div>
        </div>
        <div className="images-exchange">
          <h3 className="title-reason">Hình ảnh sản phẩm</h3>
          <div className="items-images-exchange">
            <div className="item-images-exchange" onClick={() => takePicture()}>
              {photo ? (
                <img src={photo} alt="" />
              ) : (
                <img src={iconImages} alt="" />
              )}
            </div>
            <div
              className="item-images-exchange"
              onClick={() => takePicture1()}
            >
              {photo1 ? (
                <img src={photo1} alt="" />
              ) : (
                <img src={iconImages} alt="" />
              )}
            </div>
            <div
              className="item-images-exchange"
              onClick={() => takePicture2()}
            >
              {photo2 ? (
                <img src={photo2} alt="" />
              ) : (
                <img src={iconImages} alt="" />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default ReasonExchange;
