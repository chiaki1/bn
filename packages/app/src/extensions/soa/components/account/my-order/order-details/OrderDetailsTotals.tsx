import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import moment from 'moment/moment';
import React from 'react';

const OrderDetailsTotals: React.FC<{
  number: any;
  totals: any;
  orderDate: any;
  totalsDiscount: any;
  statusOrder: any;
  rewardCurrencyAmount: any;
}> = combineHOC()((props) => {
  return (
    <div className="order-detail-code">
      <hr />
      <div className="container">
        <div className="order-detail-content">
          <div className="flex flex-content-bw flex-center title-account">
            <h2>Mã đơn hàng</h2>
            <span className="code">#{props?.number}</span>
          </div>
          <table className="table-order-summary table">
            <tbody>
              <tr>
                <th>Tổng tiền hàng:</th>
                <td>
                  <span className=" price">
                    <UiExtension
                      uiId="CURRENCY"
                      price={props?.totals?.subtotal?.value || 0}
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Phí vận chuyển:</th>
                <td>
                  <span className=" price">
                    <UiExtension
                      uiId="CURRENCY"
                      price={props?.totals?.total_shipping?.value || 0}
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Giảm giá:</th>
                <td>
                  <span className=" price">
                    {props?.totalsDiscount ? '- ' : ' '}
                    <UiExtension
                      uiId="CURRENCY"
                      price={props?.totalsDiscount || 0}
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Dùng điểm tích lũy:</th>
                <td>
                  <span className=" price">
                    {props?.rewardCurrencyAmount ? '- ' : ' '}
                    <UiExtension
                      uiId="CURRENCY"
                      price={props?.rewardCurrencyAmount || 0}
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Thời gian đặt hàng:</th>
                <td>
                  <span className=" price">
                    {props?.orderDate &&
                      moment(props?.orderDate).format('HH:mm DD/MM/YYYY')}
                  </span>
                </td>
              </tr>
              {/*{props?.statusOrder !== 'Complete' && (*/}
              {/*  <tr>*/}
              {/*    <th>Giao hàng dự kiến:</th>*/}
              {/*    <td>*/}
              {/*      <span className=" price">*/}
              {/*        {moment(props?.orderDate)*/}
              {/*          .add(3, 'd')*/}
              {/*          .format('hh:mm DD/MM/YYYY')}*/}
              {/*      </span>*/}
              {/*    </td>*/}
              {/*  </tr>*/}
              {/*)}*/}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
});

export default OrderDetailsTotals;
