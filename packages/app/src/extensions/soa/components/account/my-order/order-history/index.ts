import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ORDER_HISTORY_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ORDER_HISTORY_ITEM',
    component: { lazy: React.lazy(() => import('./OrderHistoryItem')) },
  },
  {
    uiId: 'ORDER_HISTORY_LIST',
    component: { lazy: React.lazy(() => import('./OrderHistoryList')) },
  },
  {
    uiId: 'ORDER_HISTORY_TAB',
    component: { lazy: React.lazy(() => import('./OrderHistoryTab')) },
  },
];
