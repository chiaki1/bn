import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import moment from 'moment/moment';
import React, { useCallback, useMemo, useState } from 'react';

import { toastErrorMessage } from '../../../../../modules/ui/util/toast/toastErrorMessage';
import historypoint from '../../../assets/images/icons/history-point.svg';
import iconpoints from '../../../assets/images/icons/icon-points.svg';
import { withExchangePointActions } from '../../../hoc/account/withExchangePointActions';

const TabsPoints: React.FC = combineHOC(
  withAccountState,
  withExchangePointActions
)((props) => {
  const [activeTab, setActiveTab] = useState('list');
  const [showPopup, setShowPopup] = useState(false);
  const [exchangeActiveCurrent, setExchangeActiveCurrent] = useState({});

  const checkDisableButtonPoint = useMemo(() => {
    if (
      !props?.state?.accountState?.customer?.reward_points?.balance?.points ||
      props?.state?.accountState?.customer?.reward_points?.balance?.points === 0
    ) {
      return true;
    }
    return false;
  }, [props?.state?.accountState?.customer]);

  const exchangePoint = useMemo(() => {
    if (
      // @ts-ignore
      props?.state?.accountState?.customer?.reward_points?.exchange_vouchers
    ) {
      return (
        // @ts-ignore
        props?.state?.accountState?.customer?.reward_points?.exchange_vouchers
      );
    }
    return [];
  }, [props?.state?.accountState?.customer]);

  const exchangeHistory = useMemo(() => {
    if (
      // @ts-ignore
      props?.state?.accountState?.customer?.reward_points?.exchange_history
    ) {
      return (
        // @ts-ignore
        props?.state?.accountState?.customer?.reward_points?.exchange_history
      );
    }
    return [];
  }, [props?.state?.accountState?.customer]);

  const setDataPopup = useCallback(
    (exchange: any) => {
      if (
        props?.state?.accountState?.customer?.reward_points?.balance?.points &&
        exchange?.point &&
        props?.state?.accountState?.customer?.reward_points?.balance?.points >
          exchange?.point
      ) {
        setShowPopup(true);
        setExchangeActiveCurrent(exchange);
      } else {
        toastErrorMessage('Bạn không đủ point để thực hiện đổi!');
      }
    },
    [props?.state?.accountState?.customer]
  );

  const closePopup = useCallback(() => {
    setShowPopup(false);
    setExchangeActiveCurrent({});
  }, []);

  return (
    <div className="tab-points nav-tab">
      <div className="container">
        <ul className="tab-items list-reset">
          <li
            className={clsx('tab-item ', activeTab === 'list' && 'active')}
            onClick={() => {
              setActiveTab('list');
            }}
          >
            Đổi điểm
          </li>
          <li
            className={clsx('tab-item ', activeTab === 'history' && 'active')}
            onClick={() => {
              setActiveTab('history');
            }}
          >
            Lịch sử sử dụng
          </li>
        </ul>
        {activeTab === 'list' && (
          <div className="tab-exchanges wrp-voucher-list">
            {exchangePoint.length > 0 ? (
              <>
                {exchangePoint.map((item: any) => (
                  <div className="voucher-item" key={item?.exchange_id}>
                    <div className="box-voucher">
                      <img
                        className="img-cover"
                        src={item?.image}
                        alt="show image"
                        style={{ height: '120px' }}
                      />

                      <div className="voucher-item_desc ">
                        <div className="voucher-item_desc__txt ">
                          <p className="voucher-item-title">{item?.name}</p>
                          <p className="text-xs text-light">
                            Áp dụng:{' '}
                            {Array.isArray(item?.rule?.schedule) &&
                            item?.rule?.schedule.length > 0 &&
                            moment(
                              item?.rule?.schedule[0]?.start_time
                            ).isValid()
                              ? moment(
                                  item?.rule?.schedule[0]?.start_time
                                ).format('DD/MM/YYYY')
                              : '...'}{' '}
                            -{' '}
                            {Array.isArray(item?.rule?.schedule) &&
                            item?.rule?.schedule.length > 0 &&
                            moment(item?.rule?.schedule[0]?.end_time).isValid()
                              ? moment(
                                  item?.rule?.schedule[0]?.end_time
                                ).format('DD/MM/YYYY')
                              : '...'}
                          </p>
                        </div>
                        <div className="price-exchange-points flex flex-content-bw">
                          <p className="price flex flex-center">
                            <img src={iconpoints} alt="" />
                            <span>{item.point?.toLocaleString('vi-VN')}</span>
                          </p>
                          <button
                            className={clsx(
                              checkDisableButtonPoint && 'disabled'
                            )}
                            disabled={checkDisableButtonPoint}
                            onClick={() => {
                              setDataPopup(item);
                            }}
                          >
                            Đổi ngay
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}{' '}
              </>
            ) : (
              <span>Không có voucher nào khả dụng.</span>
            )}
          </div>
        )}

        {activeTab === 'history' && (
          <div className="tab-history_points">
            {exchangeHistory && exchangeHistory.length > 0 ? (
              <>
                {exchangeHistory.map((item: any) => (
                  <div
                    className="points-history-item flex flex-content-bw"
                    key={item?.date}
                  >
                    <div className="item_desc flex pr-12">
                      <span className="inline-flex items-center ion-justify-content-center img-tag rounded">
                        <img src={historypoint} alt="" />
                      </span>
                      <div className="item_img-name__title">
                        <h3>{item?.change_reason}</h3>
                        <small>
                          {moment(item?.date).format('DD/MM/YYYY HH:mm:ss')}
                        </small>
                      </div>
                    </div>
                    <div className="item_points ">
                      <img src={iconpoints} alt="" />
                      <p className="item_points">
                        {item.points_change?.toLocaleString('vi-VN')}
                      </p>
                    </div>
                  </div>
                ))}
              </>
            ) : (
              <span> Bạn chưa có lịch sử đổi điểm nào.</span>
            )}
          </div>
        )}

        {showPopup && (
          <UiExtension
            uiId="POPUP_CHANGE_POINTS"
            close={closePopup}
            exchangeActiveCurrent={exchangeActiveCurrent}
            exchangePointActions={props?.actions?.exchangePointActions}
          />
        )}
      </div>
    </div>
  );
});

export default TabsPoints;
