import { IonBackdrop, IonCol, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconSprites from '../../../assets/images/icon-sprites.svg';

const PopupExchangePoints: React.FC<{
  close: any;
  exchangeActiveCurrent: any;
  exchangePointActions: any;
}> = combineHOC()((props) => {
  return (
    <>
      <IonBackdrop tappable={true} visible={true} stopPropagation={true} />
      <div className="modalv2 modal-center">
        <div
          className="modalv2-content max-w-340 rounded-8"
          onClick={() => {
            props?.close();
          }}
        >
          <span className="inline-block absolute top-13 right-20 text-gray cursor-pointer">
            <svg width="13" height="13">
              <use xlinkHref={iconSprites + '#icon-close'} />
            </svg>
          </span>
          <div className="pd-indent">
            <h4 className="mt-0">
              Bạn có muốn đổi{' '}
              {props?.exchangeActiveCurrent.point?.toLocaleString('vi-VN')}{' '}
              point?
            </h4>
            <IonRow className="mr-row">
              <IonCol size="6">
                <button
                  className=" btn-reset btn-fs"
                  type="submit"
                  onClick={() => {
                    props?.close();
                  }}
                >
                  Quay lại
                </button>
              </IonCol>
              <IonCol size="6">
                <button
                  className="btn-black btn-reset btn-fs"
                  type="submit"
                  onClick={() => {
                    if (
                      props?.exchangeActiveCurrent?.exchange_id &&
                      typeof props?.exchangePointActions === 'function'
                    ) {
                      props?.exchangePointActions(
                        props?.exchangeActiveCurrent?.exchange_id
                      );
                    }
                  }}
                >
                  Tiếp tục
                </button>
              </IonCol>
            </IonRow>
          </div>
        </div>
      </div>
    </>
  );
});

export default PopupExchangePoints;
