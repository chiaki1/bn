import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_POINTS_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'TAB_POINTS',
    component: { lazy: React.lazy(() => import('./TabsPoints')) },
  },
  {
    uiId: 'POPUP_CHANGE_POINTS',
    component: { lazy: React.lazy(() => import('./PopupExchangePoints')) },
  },
];
