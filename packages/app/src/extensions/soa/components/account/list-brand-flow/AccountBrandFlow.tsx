import { IonContent, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withSoaFollowBrandListData } from '../../../hoc/brand/withSoaFollowBrandListData';

const AccountBrandFlow: React.FC = combineHOC(withSoaFollowBrandListData)(
  (props) => {
    if (!props.state?.dataFollowBrand) {
      return null;
    }
    if (
      props.state?.dataFollowBrand &&
      Array.isArray(props.state?.dataFollowBrand) &&
      props.state?.dataFollowBrand.length === 0
    ) {
      return (
        <div className="brand__list">
          <div className="container pt-16">
            <span>Bạn đang không theo dõi thương hiệu nào!</span>
          </div>
        </div>
      );
    }
    return (
      <div className="brand__list">
        <div className="container">
          {props.state?.dataFollowBrand?.map((items: any) => (
            <UiExtension
              uiId="BRAND_ITEM"
              itemsData={items}
              key={items?.brand_id}
            />
          ))}
        </div>
      </div>
    );
  }
);

export default AccountBrandFlow;
