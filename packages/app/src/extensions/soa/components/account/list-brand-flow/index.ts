import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ACCOUNT_BRAND_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ACCOUNT_BRAND_FLOW',
    component: { lazy: React.lazy(() => import('./AccountBrandFlow')) },
  },
];
