import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const InstructSize: React.FC = combineHOC()((props) => {
  return (
    <div className="instruct-size">
      <div className="container">
        <div className="table-size">
          <div dangerouslySetInnerHTML={{ __html: props.contentGuide || '' }} />
        </div>
      </div>
    </div>
  );
});

export default InstructSize;
