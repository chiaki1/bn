import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_MY_SIZE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'MY_SIZE',
    component: { lazy: React.lazy(() => import('./MySize')) },
  },
  {
    uiId: 'ADD_NEW_SIZE',
    component: { lazy: React.lazy(() => import('./AddNewsSize')) },
  },
  {
    uiId: 'INSTRUCT_SIZE',
    component: { lazy: React.lazy(() => import('./InstructSize')) },
  },
];
