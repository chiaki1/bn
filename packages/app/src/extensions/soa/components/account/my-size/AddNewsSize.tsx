import {
  IonCol,
  IonContent,
  IonFooter,
  IonPage,
  IonRow,
  IonToolbar,
} from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useForm, useWatch } from 'react-hook-form';

import { withMySizeData } from '../../../hoc/account/withMySizeData';
import { withMySizesActions } from '../../../hoc/account/withMySizesActions';

const AddNewsSize = combineHOC(
  withMySizeData,
  withMySizesActions
)((props) => {
  const [step, setStep] = useState(1);
  const [gender, setGender] = useState('men');
  const [dataSize, setDataSize] = useState<any>({});
  const [inputSave, setInputSave] = useState<any>({});
  const {
    handleSubmit,
    register,
    setValue,
    getValues,
    formState: { errors },
  } = useForm();

  const dataQuestionAdditionalAdult = useMemo(() => {
    if (size(props?.state?.listQuestionSize?.additional_question) > 0) {
      const array = props?.state?.listQuestionSize?.additional_question.filter(
        (sta: any) => sta?.status && sta?.age === 'adult'
      );

      if (array && array.length > 0) {
        return array;
      }
    }
    return [];
  }, [props?.state?.listQuestionSize]);

  const dataQuestionAdditionalChildren = useMemo(() => {
    if (size(props?.state?.listQuestionSize?.additional_question) > 0) {
      const array = props?.state?.listQuestionSize?.additional_question.filter(
        (sta: any) => sta?.status && sta?.age === 'children'
      );

      if (array && array.length > 0) {
        return array;
      }
    }
    return [];
  }, [props?.state?.listQuestionSize]);

  useEffect(() => {
    if (
      props?.state?.listQuestionSize?.sex_question &&
      Array.isArray(props?.state?.listQuestionSize?.sex_question) &&
      props?.state?.listQuestionSize?.sex_question.length > 0 &&
      !gender
    ) {
      setGender(
        props?.state?.listQuestionSize?.sex_question[0]?.value || 'men'
      );
    }
  }, []);

  useEffect(() => {
    if (props?.state?.mySizeEditingData?.answer_id) {
      setValue('name', props?.state?.mySizeEditingData?.name);
      setValue('gender', props?.state?.mySizeEditingData?.sex);
      setValue('height', props?.state?.mySizeEditingData?.height);
      setValue('weight', props?.state?.mySizeEditingData?.weight);
      setValue('form', props?.state?.mySizeEditingData?.form);
      setValue('waist', props?.state?.mySizeEditingData?.waist);

      if (
        props?.state?.mySizeEditingData?.sex === 'boy' ||
        props?.state?.mySizeEditingData?.sex === 'girl'
      ) {
        setValue('children1', props?.state?.mySizeEditingData?.form || '');
      } else {
        setValue('adult1', props?.state?.mySizeEditingData?.form || '');
        setValue('adult2', props?.state?.mySizeEditingData?.waist || '');
      }
      setGender(props?.state?.mySizeEditingData?.sex);
      setDataSize({
        name: props?.state?.mySizeEditingData?.name || '',
        // sex: props?.state?.mySizeEditingData?.sex,
        // height_answer: props?.state?.mySizeEditingData?.height,
        // weight_answer: props?.state?.mySizeEditingData?.weight,
        // form_answer: props?.state?.mySizeEditingData?.form,
        // waist_answer: props?.state?.mySizeEditingData?.waist || null,
      });
    }
  }, []);

  const setValueToInput = useCallback((name: any, value: any) => {
    setValue(name, value);
  }, []);

  const onClickNext = useCallback(
    (steps: any) => {
      if (gender === 'boy' || gender === 'girl') {
        if (steps === 2) {
          setStep(steps + 2);
        } else {
          setStep(steps + 1);
        }
      } else {
        setStep(steps + 1);
      }
    },
    [gender, step]
  );

  const onClickBack = useCallback(() => {
    if (gender === 'boy' || gender === 'girl') {
      if (step === 4) {
        setStep(step - 2);
      } else {
        setStep(step - 1);
      }
    } else {
      setStep(step - 1);
    }
  }, [gender, step]);

  const onSubmit = useCallback(
    (data: any) => {
      let heightId = null;
      let weightId = null;
      switch (gender) {
        case 'men':
          heightId =
            props?.state?.listQuestionSize?.men_height_question?.question_id.toString();
          weightId =
            props?.state?.listQuestionSize?.men_weight_question?.question_id.toString();
          break;
        case 'women':
          heightId =
            props?.state?.listQuestionSize?.women_height_question?.question_id.toString();
          weightId =
            props?.state?.listQuestionSize?.women_weight_question?.question_id.toString();
          break;
        case 'boy':
          heightId =
            props?.state?.listQuestionSize?.boy_height_question?.question_id.toString();
          weightId =
            props?.state?.listQuestionSize?.boy_weight_question?.question_id.toString();
          break;
        case 'girl':
          heightId =
            props?.state?.listQuestionSize?.girl_height_question?.question_id.toString();
          weightId =
            props?.state?.listQuestionSize?.girl_weight_question?.question_id.toString();
          break;
      }
      let input: any = {};

      if (data?.height && data?.weight && heightId && weightId) {
        input = {
          height_answer: {
            question_id: heightId,
            answer: data?.height,
          },
          weight_answer: {
            question_id: weightId,
            answer: data?.weight,
          },
          sex: gender,
        };
        setDataSize({ ...dataSize, ...input, name: data?.name || '' });
      }
      // setDataSize({ ...dataSize, name: data?.name || '' });

      if (
        data?.adult1 &&
        dataQuestionAdditionalAdult.length > 0 &&
        dataQuestionAdditionalAdult[0]?.question_id
      ) {
        input = {
          ...input,
          form_answer: {
            question_id: dataQuestionAdditionalAdult[0]?.question_id,
            answer: data?.adult1,
          },
        };
        setDataSize({ ...dataSize, ...input, name: data?.name || '' });
      }
      if (
        data?.adult2 &&
        dataQuestionAdditionalAdult.length > 1 &&
        dataQuestionAdditionalAdult[1]?.question_id
      ) {
        input = {
          ...input,
          waist_answer: {
            question_id: dataQuestionAdditionalAdult[1]?.question_id,
            answer: data?.adult2,
          },
        };
        setDataSize({ ...dataSize, ...input, name: data?.name || '' });
      }
      if (
        data?.children1 &&
        dataQuestionAdditionalChildren.length > 0 &&
        dataQuestionAdditionalChildren[0]?.question_id
      ) {
        input = {
          ...input,
          form_answer: {
            question_id: dataQuestionAdditionalChildren[0]?.question_id,
            answer: data?.children1,
          },
        };
        setDataSize({ ...dataSize, ...input, name: data?.name || '' });
      }

      if (
        typeof props?.actions?.prepareAnswer === 'function' &&
        input?.weight_answer?.question_id &&
        input?.height_answer?.question_id
      ) {
        props?.actions?.prepareAnswer(input);
        setInputSave({ ...input, name: data?.name });
        onClickNext(step);
      }
    },
    [gender, props?.state?.listQuestionSize, dataQuestionAdditionalAdult, step]
  );

  const saveQuestions = useCallback(() => {
    if (props?.state?.mySizeEditingData?.answer_id) {
      if (
        inputSave &&
        typeof props?.actions?.editAnswer === 'function' &&
        inputSave?.weight_answer?.question_id
      ) {
        props?.actions?.editAnswer({
          ...inputSave,
          size_id: props?.state?.mySizeEditingData?.answer_id,
        });
      }
    } else {
      if (
        inputSave &&
        typeof props?.actions?.saveAnswer === 'function' &&
        inputSave?.weight_answer?.question_id
      ) {
        props?.actions?.saveAnswer({ ...inputSave, set_as_default: false });
      }
    }
  }, [inputSave, props?.state?.mySizeEditingData]);
  const showContentMainQuestion = useMemo(() => {
    switch (gender) {
      case 'men':
        return (
          <>
            {props?.state?.listQuestionSize?.men_height_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.men_height_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.men_height_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('height', {
                      required:
                        !props?.state?.listQuestionSize?.men_height_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.height && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.men_height_question?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.men_height_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('height', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
            {props?.state?.listQuestionSize?.men_weight_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.men_weight_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.men_weight_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('weight', {
                      required:
                        !props?.state?.listQuestionSize?.men_weight_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.weight && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.men_weight_question?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.men_weight_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('weight', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
          </>
        );
        break;
      case 'women':
        return (
          <>
            {props?.state?.listQuestionSize?.women_height_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.women_height_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.women_height_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('height', {
                      required:
                        !props?.state?.listQuestionSize?.women_height_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.height && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.women_height_question
                      ?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.women_height_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('height', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
            {props?.state?.listQuestionSize?.women_weight_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.women_weight_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.women_weight_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('weight', {
                      required:
                        !props?.state?.listQuestionSize?.women_weight_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.weight && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.women_weight_question
                      ?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.women_weight_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('weight', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
          </>
        );
        break;
      case 'boy':
        return (
          <>
            {props?.state?.listQuestionSize?.boy_height_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.boy_height_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.boy_height_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('height', {
                      required:
                        !props?.state?.listQuestionSize?.boy_height_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.height && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.boy_height_question?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.boy_height_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('height', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
            {props?.state?.listQuestionSize?.boy_weight_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.boy_weight_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.boy_weight_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('weight', {
                      required:
                        !props?.state?.listQuestionSize?.boy_weight_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.weight && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.boy_weight_question?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.boy_weight_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('weight', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
          </>
        );
        break;
      case 'girl':
        return (
          <>
            {props?.state?.listQuestionSize?.girl_height_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.girl_height_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.girl_height_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('height', {
                      required:
                        !props?.state?.listQuestionSize?.girl_height_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.height && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.girl_height_question
                      ?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.girl_height_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('height', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
            {props?.state?.listQuestionSize?.girl_weight_question
              ?.question_id && (
              <div
                className="form-item-size"
                key={
                  props?.state?.listQuestionSize?.girl_weight_question
                    ?.question_id
                }
              >
                <label htmlFor="">
                  {props?.state?.listQuestionSize?.girl_weight_question?.title}
                </label>
                <div className="form-address-add-item_input">
                  <input
                    type="text"
                    {...register('weight', {
                      required:
                        !props?.state?.listQuestionSize?.girl_weight_question
                          ?.can_skip,
                    })}
                  />
                </div>
                {errors?.weight && (
                  <div className="error-message text-small mb-12">
                    <i className="fas fa-exclamation-circle" /> Vui lòng nhập
                    thông tin để tiếp tục.
                  </div>
                )}
                <div className="listbox-option list-reset flex flex-wrap">
                  {size(
                    props?.state?.listQuestionSize?.girl_weight_question
                      ?.options
                  ) > 0 &&
                    props?.state?.listQuestionSize?.girl_weight_question?.options.map(
                      (it: any) => (
                        <li
                          className="options-item"
                          key={it?.value}
                          onClick={() => setValueToInput('weight', it?.value)}
                        >
                          <label>{it?.title}</label>
                        </li>
                      )
                    )}
                </div>
              </div>
            )}
          </>
        );
        break;
    }
  }, [gender, props?.state?.listQuestionSize, errors]);

  const showContentAdditionalQuestion = useMemo(() => {
    if (step === 2) {
      if (gender === 'boy' || gender === 'girl') {
        if (
          dataQuestionAdditionalChildren.length > 0 &&
          dataQuestionAdditionalChildren[0]?.question_id
        ) {
          return (
            <div className="form-item-size">
              <div className="shipping-list">
                <div className="container">
                  <h3 className="text-lg">
                    {dataQuestionAdditionalChildren[0]?.title}
                  </h3>
                  <div className="wrap-address">
                    {size(dataQuestionAdditionalChildren[0]?.options) > 0 &&
                      dataQuestionAdditionalChildren[0]?.options.map(
                        (it: any) => (
                          <div
                            className="wrap-s-itemelect flex "
                            key={it?.value}
                          >
                            <div className="address-checkbox">
                              <label className="label-input">
                                <input
                                  type="radio"
                                  {...register('children1', {})}
                                  value={it?.value}
                                />
                                <span className="label">{it?.title}</span>
                              </label>
                            </div>
                          </div>
                        )
                      )}
                  </div>
                </div>
              </div>
            </div>
          );
        }
      } else {
        if (
          dataQuestionAdditionalAdult.length > 0 &&
          dataQuestionAdditionalAdult[0]?.question_id
        ) {
          return (
            <div className="form-item-size">
              <div className="shipping-list">
                <div className="container">
                  <h3 className="text-lg">
                    {dataQuestionAdditionalAdult[0]?.title}
                  </h3>
                  <div className="wrap-address">
                    {size(dataQuestionAdditionalAdult[0]?.options) > 0 &&
                      dataQuestionAdditionalAdult[0]?.options.map((it: any) => (
                        <div className="wrap-s-itemelect flex " key={it?.value}>
                          <div className="address-checkbox">
                            <label className="label-input">
                              <input
                                type="radio"
                                {...register('adult1', {})}
                                value={it?.value}
                              />
                              <span className="label">{it?.title}</span>
                            </label>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          );
        }
      }
    } else {
      if (
        dataQuestionAdditionalAdult.length >= 2 &&
        dataQuestionAdditionalAdult[1]?.question_id
      ) {
        return (
          <div className="form-item-size">
            <div className="shipping-list">
              <div className="container">
                <h3 className="text-lg">
                  {dataQuestionAdditionalAdult[1]?.title}
                </h3>
                <div className="wrap-address">
                  {size(dataQuestionAdditionalAdult[1]?.options) > 0 &&
                    dataQuestionAdditionalAdult[1]?.options.map((it: any) => (
                      <div className="wrap-s-itemelect flex " key={it?.value}>
                        <div className="address-checkbox">
                          <label className="label-input">
                            <input
                              type="radio"
                              {...register('adult2', {})}
                              value={it?.value}
                            />
                            <span className="label">{it?.title}</span>
                          </label>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
        );
      }
    }

    return <></>;
  }, [
    gender,
    dataQuestionAdditionalAdult,
    dataQuestionAdditionalChildren,
    step,
  ]);

  const getDataQuestion = useCallback(
    (type: string, value: any) => {
      let titleQues = '';
      let titleValue = '';
      if (type === 'gender') {
        titleValue =
          props?.state?.listQuestionSize?.sex_question.filter(
            (item: any) => item?.value === value
          ) &&
          props?.state?.listQuestionSize?.sex_question.filter(
            (item: any) => item?.value === value
          ).length > 0
            ? props?.state?.listQuestionSize?.sex_question.filter(
                (item: any) => item?.value === value
              )[0]?.label
            : '';
      } else if (type === 'form') {
        if (gender === 'boy' || gender === 'girl') {
          titleQues = dataQuestionAdditionalChildren[0]?.title;
          titleValue =
            dataQuestionAdditionalChildren[0]?.options.filter(
              (item: any) => item?.value === value
            ) &&
            dataQuestionAdditionalChildren[0]?.options.filter(
              (item: any) => item?.value === value
            ).length > 0
              ? dataQuestionAdditionalChildren[0]?.options.filter(
                  (item: any) => item?.value === value
                )[0]?.title
              : '';
        } else {
          titleQues = dataQuestionAdditionalAdult[0]?.title;
          titleValue =
            dataQuestionAdditionalAdult[0]?.options.filter(
              (item: any) => item?.value === value
            ) &&
            dataQuestionAdditionalAdult[0]?.options.filter(
              (item: any) => item?.value === value
            ).length > 0
              ? dataQuestionAdditionalAdult[0]?.options.filter(
                  (item: any) => item?.value === value
                )[0]?.title
              : '';
        }
      } else if (type === 'waist') {
        titleQues = dataQuestionAdditionalAdult[1]?.title;
        titleValue =
          dataQuestionAdditionalAdult[1]?.options.filter(
            (item: any) => item?.value === value
          ) &&
          dataQuestionAdditionalAdult[1]?.options.filter(
            (item: any) => item?.value === value
          ).length > 0
            ? dataQuestionAdditionalAdult[1]?.options.filter(
                (item: any) => item?.value === value
              )[0]?.title
            : '';
      } else {
        switch (gender) {
          case 'men':
            if (type === 'height') {
              titleQues =
                props?.state?.listQuestionSize?.men_height_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.men_height_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.men_height_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.men_height_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }
            if (type === 'weight') {
              titleQues =
                props?.state?.listQuestionSize?.men_weight_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.men_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.men_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.men_weight_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }
            break;
          case 'women':
            if (type === 'height') {
              titleQues =
                props?.state?.listQuestionSize?.women_height_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.women_height_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.women_height_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.women_height_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }

            if (type === 'weight') {
              titleQues =
                props?.state?.listQuestionSize?.women_weight_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.women_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.women_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.women_weight_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }
            break;
          case 'boy':
            if (type === 'height') {
              titleQues =
                props?.state?.listQuestionSize?.boy_height_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.boy_height_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.boy_height_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.boy_height_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }

            if (type === 'weight') {
              titleQues =
                props?.state?.listQuestionSize?.boy_weight_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.boy_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.boy_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.boy_weight_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }
            break;
          case 'girl':
            if (type === 'height') {
              titleQues =
                props?.state?.listQuestionSize?.girl_height_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.girl_height_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.girl_height_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.girl_height_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }

            if (type === 'weight') {
              titleQues =
                props?.state?.listQuestionSize?.girl_weight_question?.title;
              titleValue =
                props?.state?.listQuestionSize?.girl_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ) &&
                props?.state?.listQuestionSize?.girl_weight_question?.options.filter(
                  (item: any) => item?.value === value
                ).length > 0
                  ? props?.state?.listQuestionSize?.girl_weight_question?.options.filter(
                      (item: any) => item?.value === value
                    )[0]?.title
                  : '';
            }

            break;
        }
      }

      return {
        titleQues: titleQues,
        titleValue: titleValue,
      };
    },
    [props?.state?.listQuestionSize, gender]
  );

  const confirmData = useMemo(() => {
    if (Object.values(dataSize)) {
      return (
        <>
          {dataSize?.name && (
            <div className="form-item-size">
              <label htmlFor=""> Họ và tên</label>
              <div className="custom-select-address-add px-16">
                <h3> - {dataSize?.name}</h3>
              </div>
            </div>
          )}
          {dataSize?.sex && (
            <div className="form-item-size">
              <label htmlFor=""> Giới tính</label>
              <div className="custom-select-address-add px-16">
                <h3>
                  - {getDataQuestion('gender', dataSize?.sex)?.titleValue}
                </h3>
              </div>
            </div>
          )}
          {dataSize?.height_answer && (
            <div className="form-item-size">
              <label htmlFor="">
                {' '}
                {
                  getDataQuestion('height', dataSize?.height_answer?.answer)
                    ?.titleQues
                }
              </label>
              <div className="custom-select-address-add px-16">
                <h3>
                  -{' '}
                  {getDataQuestion('height', dataSize?.height_answer?.answer)
                    ?.titleValue || dataSize?.height_answer?.answer}
                </h3>
              </div>
            </div>
          )}
          {dataSize?.weight_answer && (
            <div className="form-item-size">
              <label htmlFor="">
                {' '}
                {
                  getDataQuestion('weight', dataSize?.weight_answer?.answer)
                    ?.titleQues
                }
              </label>
              <div className="custom-select-address-add px-16">
                <h3>
                  -{' '}
                  {getDataQuestion('weight', dataSize?.weight_answer?.answer)
                    ?.titleValue || dataSize?.weight_answer?.answer}
                </h3>
              </div>
            </div>
          )}
          {dataSize?.form_answer && (
            <div className="form-item-size">
              <label htmlFor="">
                {' '}
                {
                  getDataQuestion('form', dataSize?.form_answer?.answer)
                    ?.titleQues
                }
              </label>
              <div className="custom-select-address-add px-16">
                <h3>
                  -{' '}
                  {getDataQuestion('form', dataSize?.form_answer?.answer)
                    ?.titleValue || dataSize?.form_answer?.answer}
                </h3>
              </div>
            </div>
          )}
          {dataSize?.waist_answer && (
            <div className="form-item-size">
              <label htmlFor="">
                {' '}
                {
                  getDataQuestion('waist', dataSize?.waist_answer?.answer)
                    ?.titleQues
                }
              </label>
              <div className="custom-select-address-add px-16">
                <h3>
                  -{' '}
                  {getDataQuestion('waist', dataSize?.waist_answer?.answer)
                    ?.titleValue || dataSize?.waist_answer?.answer}
                </h3>
              </div>
            </div>
          )}
        </>
      );
    }
    return <></>;
  }, [dataSize]);

  return (
    <IonPage>
      <IonContent>
        <div className="add-new-size">
          <div className="container">
            <form>
              {step === 1 && (
                <>
                  <h3 className="text-lg adrs-deli">Nhập số đo của bạn</h3>
                  <div className="form-item-size">
                    <label htmlFor="">Vui lòng cho biết TÊN của bạn (*)</label>
                    <div className="custom-select-address-add">
                      <input
                        type="text"
                        {...register('name', {
                          required: true,
                        })}
                      />
                    </div>
                    {errors?.name && (
                      <div className="error-message text-small mb-12">
                        <i className="fas fa-exclamation-circle" /> Vui lòng
                        nhập thông tin để tiếp tục.
                      </div>
                    )}
                  </div>
                  {props?.state?.listQuestionSize?.sex_question && (
                    <>
                      <div className="form-item-size">
                        <label htmlFor="">
                          Vui lòng cho biết GIỚI TÍNH của bạn (*)
                        </label>
                        <div className="custom-select-address-add">
                          <select
                            {...register('gender', {
                              required: true,
                            })}
                            onChange={(e) => {
                              setValue('gender', e.target.value ?? '');
                              setValue('height', '');
                              setValue('weight', '');
                              setGender(e.target.value ?? 'men');
                              if (
                                e.target.value === 'boy' ||
                                e.target.value === 'girl'
                              ) {
                                setValue('adult1', '');
                                setValue('adult2', '');
                              } else {
                                setValue('children1', '');
                              }
                            }}
                          >
                            {props?.state?.listQuestionSize?.sex_question.map(
                              (item: any) => (
                                <option value={item?.value} key={item?.value}>
                                  {item?.label}
                                </option>
                              )
                            )}
                          </select>
                        </div>
                      </div>
                    </>
                  )}
                  {showContentMainQuestion}
                </>
              )}

              {step === 2 && <>{showContentAdditionalQuestion}</>}
              {step === 3 && <>{showContentAdditionalQuestion}</>}
              {step === 4 && <>{confirmData}</>}
            </form>
          </div>
        </div>
      </IonContent>

      <IonFooter class="footer-cart">
        {step >= 2 && step < 4 && (
          <div>
            <p
              className="text-lg text-center"
              onClick={() => {
                onClickNext(step);
              }}
            >
              <span className="price text-main">Bỏ qua câu hỏi này</span>
            </p>
          </div>
        )}

        <IonToolbar className="ion-no-padding">
          <div className="container">
            {props?.state?.tempSize && (
              <p className="text-lg text-center cart-total">
                <span className="price-label">Size ước tính :</span>
                <span className="price text-main">
                  {props?.state?.tempSize}
                </span>
              </p>
            )}

            <IonRow className="mr-row">
              {step === 1 && (
                <IonCol size="12">
                  <div
                    className="btn-black btn-fs flex-center-center btn-pd-xs"
                    onClick={handleSubmit(onSubmit)}
                  >
                    Tiếp tục
                  </div>
                </IonCol>
              )}
              {step > 1 && step < 4 && (
                <>
                  <IonCol size="6">
                    <button
                      className=" btn-reset btn-fs"
                      type="submit"
                      onClick={() => {
                        onClickBack();
                      }}
                    >
                      Quay lại
                    </button>
                  </IonCol>
                  <IonCol size="6">
                    <button
                      className="btn-black btn-reset btn-fs"
                      type="submit"
                      onClick={handleSubmit(onSubmit)}
                    >
                      Tiếp tục
                    </button>
                  </IonCol>
                </>
              )}

              {step >= 4 && (
                <>
                  <IonCol size="6">
                    <button
                      className=" btn-reset btn-fs"
                      type="submit"
                      onClick={() => {
                        onClickBack();
                      }}
                    >
                      Quay lại
                    </button>
                  </IonCol>
                  <IonCol size="6">
                    <button
                      className="btn-black btn-reset btn-fs"
                      type="submit"
                      onClick={() => {
                        saveQuestions();
                      }}
                    >
                      Lưu số đo
                    </button>
                  </IonCol>
                </>
              )}
            </IonRow>
          </div>
        </IonToolbar>
      </IonFooter>
    </IonPage>
  );
});

export default AddNewsSize;
