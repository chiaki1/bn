import { IonContent } from '@ionic/react';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import size from 'lodash/size';
import React, { useEffect, useState } from 'react';

import { AppRoute } from '../../../../../router/Route';
import iconplus from '../../../assets/images/icons/product-page/icon-plus.svg';
import { withMySizeData } from '../../../hoc/account/withMySizeData';
import { withMySizesActions } from '../../../hoc/account/withMySizesActions';

const MySize: React.FC = combineHOC(
  withAccountState,
  withMySizeData,
  withMySizesActions
)((props) => {
  const [activeTab, setActiveTab] = useState('size');
  useEffect(() => {
    if (typeof props?.actions?.clearAddressEditing === 'function') {
      props?.actions?.clearAddressEditing();
    }
  }, []);
  return (
    <IonContent>
      <div className="form-address">
        <div className="nav-tab mgb-block">
          <ul className="tab-items list-reset text-center full-2item">
            <li
              className={clsx('tab-item', activeTab === 'size' && 'active')}
              onClick={() => {
                setActiveTab('size');
              }}
            >
              Size của bạn
            </li>
            <li
              className={clsx('tab-item', activeTab === 'guide' && 'active')}
              onClick={() => {
                setActiveTab('guide');
              }}
            >
              Hướng dẫn chọn size
            </li>
          </ul>
        </div>
        {activeTab === 'size' && (
          <div className="container">
            <div className="wrap-address">
              {size(props?.state?.mySizeData) > 0 ? (
                <>
                  {props?.state?.mySizeData.map((item: any) => {
                    return (
                      <div
                        className="wrap-address-item flex flex-center"
                        key={item?.answer_id}
                      >
                        <div className="address-checkbox">
                          <label className="label-input">
                            <input
                              name="amshopby"
                              type="radio"
                              checked={item?.active === 1}
                              onChange={() => {}}
                            />
                            <span className="label" />
                          </label>
                        </div>

                        <div className="list-info-size">
                          <h3 className="item__info-name">{item?.name}</h3>
                          <p className="item__info-sex text-lg">
                            <label>Giới tính: </label>
                            <span className="text-main">{item?.sex}</span>
                          </p>
                          <div className="py-8">
                            <ul className="item__info-size list-reset flex">
                              <li>
                                <label>Chiều cao: </label>
                                <span className="text-main">
                                  {item?.height}
                                </span>
                              </li>
                              <li style={{ paddingLeft: '24px' }}>
                                <label>Cân nặng: </label>
                                <span className="text-main">
                                  {item?.weight}
                                </span>
                              </li>
                            </ul>
                          </div>

                          <p className="item__info-edit flex flex-content-bw">
                            {item?.active === 1 ? (
                              <button className="text-small">
                                Size mặc định
                              </button>
                            ) : (
                              <button
                                className="text-main text-small"
                                onClick={() => {
                                  if (
                                    typeof props?.actions?.setSizeAsDefault ==
                                    'function'
                                  ) {
                                    props?.actions?.setSizeAsDefault(
                                      item?.answer_id
                                    );
                                  }
                                }}
                              >
                                Đặt làm mặc định
                              </button>
                            )}

                            <button
                              className="text-main text-small"
                              onClick={() => {
                                if (
                                  typeof props?.actions?.setAddressEditing ===
                                  'function'
                                ) {
                                  props?.actions?.setAddressEditing(item);
                                  RouterSingleton.push(
                                    '/' + AppRoute.MY_SIZE_INPUT_PAGE
                                  );
                                }
                              }}
                            >
                              Sửa số đo
                            </button>
                          </p>
                        </div>
                      </div>
                    );
                  })}
                </>
              ) : (
                <span>Bạn chưa có thông tin về size.</span>
              )}
            </div>
            <button
              type="button"
              className="btn-add-address flex-center"
              onClick={() => {
                RouterSingleton.push('/' + AppRoute.MY_SIZE_INPUT_PAGE);
              }}
            >
              <img src={iconplus} alt="" />
              <span>Thêm Size mới</span>
            </button>
          </div>
        )}

        {activeTab === 'guide' && (
          <UiExtension
            uiId="INSTRUCT_SIZE"
            contentGuide={props?.state?.listQuestionSize?.size_guide?.content}
          />
        )}
      </div>
    </IonContent>
  );
});

export default MySize;
