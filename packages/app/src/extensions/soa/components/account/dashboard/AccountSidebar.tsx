import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import { AppRoute } from '../../../../../router/Route';

const AccountSidebar: React.FC = combineHOC()((props) => {
  return (
    <div className="account-sidebar">
      <div className="container">
        <div className="sidebar-person">
          <h2 className="title-account no-mr">CÁ NHÂN</h2>
          <ul className="list-reset account-sidebar-list">
            <li
              onClick={() => RouterSingleton.push('/' + AppRoute.ADDRESS_LIST)}
            >
              <div className="btn-list-item btn-arrow-right">Sổ địa chỉ</div>
            </li>
            <li
              onClick={() =>
                RouterSingleton.push('/' + AppRoute.WISH_LIST_PAGE)
              }
            >
              <div className="btn-list-item btn-arrow-right">
                Sản phẩm yêu thích
              </div>
            </li>
            <li
              onClick={() => RouterSingleton.push('/' + AppRoute.MY_SIZE_PAGE)}
            >
              <div className="btn-list-item btn-arrow-right">Số đo của tôi</div>
            </li>
            <li
              onClick={() =>
                RouterSingleton.push('/' + AppRoute.ACCOUNT_BRAND_FLOW_PAGE)
              }
            >
              <div className="btn-list-item btn-arrow-right">
                Thương hiệu đang theo dõi
              </div>
            </li>
            <li
              onClick={() =>
                RouterSingleton.push(`/${AppRoute.ACCOUNT_INVITE}`)
              }
            >
              <div className="btn-list-item btn-arrow-right">Mã giới thiệu</div>
            </li>
          </ul>
        </div>
      </div>
      <hr className="h4" />
    </div>
  );
});

export default AccountSidebar;
