import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_ACCOUNT_DASHBOARD_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ACCOUNT_INFO',
    component: { lazy: React.lazy(() => import('./AccountInfo')) },
  },
  {
    uiId: 'ACCOUNT_VOUCHER',
    component: { lazy: React.lazy(() => import('./VoucherReward')) },
  },
  {
    uiId: 'ORDER_HISTORY',
    component: { lazy: React.lazy(() => import('./OrderHistory')) },
  },
  {
    uiId: 'ACCOUNT_SIDEBAR',
    component: { lazy: React.lazy(() => import('./AccountSidebar')) },
  },
  {
    uiId: 'CMS_SIDEBAR',
    component: { lazy: React.lazy(() => import('./CmsSidebar')) },
  },
];
