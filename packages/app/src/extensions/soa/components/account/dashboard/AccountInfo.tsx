import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { getCustomerName } from '@vjcspy/r/build/modules/account/util/getCustomerName';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useEffect, useMemo } from 'react';

import { AppRoute } from '../../../../../router/Route';
import iconCamera from '../../../assets/images/icons/person-circle-outline.svg';
import { withAvatarAccountActions } from '../../../hoc/account/withAvatarAccountActions';

const AccountInfo: React.FC = combineHOC(
  withCustomer,
  withAvatarAccountActions
)((props) => {
  const systemGenerate = useMemo(() => {
    // @ts-ignore
    return props?.state?.customer?.system_generate;
  }, []);

  const avatar = useMemo(() => {
    // @ts-ignore
    return props?.state?.customer?.avatar || iconCamera;
  }, [props?.state?.customer]);

  useEffect(() => {
    if (typeof props?.actions?.getCustomerDetailActions === 'function') {
      props?.actions?.getCustomerDetailActions();
    }
  }, []);

  return (
    <>
      {systemGenerate && (
        <div className="page-message message-success">
          <span>Bạn hãy hoàn tất thông tin tài khoản</span>
        </div>
      )}
      <div
        className="account-info"
        onClick={() => RouterSingleton.push('/' + AppRoute.ACCOUNT_UPDATE_INFO)}
      >
        <div className="container">
          <div className="account-info-item flex-center pd-vertical-md">
            <div className="customer-avatar flex-center-center rounded bg-gray">
              {!!avatar ? (
                <img className="img-cover round" src={avatar} alt="" />
              ) : (
                // @ts-ignore
                <img className="icon-camera" src={iconCamera} alt="" />
              )}
            </div>
            <div className="account-info-detail">
              <h3 className="customer-name fw-600">
                {systemGenerate
                  ? props?.state?.customer?.retail_telephone
                  : // @ts-ignore
                    getCustomerName(props.state.customer)}
              </h3>
              <p className="member-rating no-mr">Member</p>
            </div>
          </div>
        </div>
        <hr className="h4" />
      </div>
    </>
  );
});

export default AccountInfo;
