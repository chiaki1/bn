import { withCustomerOrderPageInfo } from '@vjcspy/r/build/modules/account/hoc/my-orders/withCustomerOrderPageInfo';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useCallback, useEffect, useState } from 'react';

import { AppRoute } from '../../../../../router/Route';
import ACCOUNT from '../../../../../values/extendable/ACCOUNT';
import iconSprites from '../../../assets/images/icon-sprites.svg';
import { withSoaCustomerOrders } from '../../../hoc/my-orders/withSoaCustomerOrders';

const OrderHistory: React.FC = combineHOC(
  withCustomerOrderPageInfo,
  withSoaCustomerOrders
)((props) => {
  const [countStatus, setCountStatus] = useState({
    wait_for_confirmation: 0,
    processing: 0,
    delivery: 0,
    delivered: 0,
    canceled: 0,
    closed: 0,
  });

  const handleGoOrderWithStatus = (status: string) => {
    if (props.actions?.setFilterStatus) {
      props.actions.setFilterStatus(status);
      RouterSingleton.push('/' + AppRoute.MY_ORDERS);
    }
  };

  useEffect(() => {
    props.actions.setFilterStatus('all');
  }, []);

  useEffect(() => {
    const checkSum = {
      wait_for_confirmation: 0,
      processing: 0,
      delivery: 0,
      delivered: 0,
      canceled: 0,
      closed: 0,
    };
    if (size(props?.state?.orders) > 0) {
      const pendding = props?.state?.orders.filter(
        (item: any) => item?.app_status === 'wait_for_confirmation'
      );

      if (pendding && pendding.length > 0) {
        checkSum.wait_for_confirmation = pendding.length;
      }

      const waiting = props?.state?.orders.filter(
        (item: any) => item?.app_status === 'processing'
      );

      if (waiting && waiting.length > 0) {
        checkSum.processing = waiting.length;
      }

      const delivery = props?.state?.orders.filter(
        (item: any) => item?.app_status === 'delivery'
      );

      if (delivery && delivery.length > 0) {
        checkSum.delivery = delivery.length;
      }

      const complete = props?.state?.orders.filter(
        (item: any) => item?.app_status === 'delivered'
      );

      if (complete && complete.length > 0) {
        checkSum.delivered = complete.length;
      }

      const canceled = props?.state?.orders.filter(
        (item: any) => item?.app_status === 'canceled'
      );

      if (canceled && canceled.length > 0) {
        checkSum.canceled = canceled.length;
      }

      const closed = props?.state?.orders.filter(
        (item: any) => item?.app_status === 'closed'
      );

      if (closed && closed.length > 0) {
        checkSum.closed = closed.length;
      }

      setCountStatus(checkSum);
    }
  }, [props?.state?.orders]);

  return (
    <div className="order-history">
      <div className="container">
        <div className="order-process">
          <h2
            className="title-account"
            // onClick={() => goToMyOrders()}
          >
            LỊCH SỬ ĐƠN HÀNG
          </h2>
          <ul className="tab-items list-reset tab-over-horizon container-onlyl">
            <li
              className="tab-item"
              onClick={() => handleGoOrderWithStatus('wait_for_confirmation')}
            >
              <span className="process-icon counter-parent">
                <svg className="fill-current" width="21" height="20">
                  <use xlinkHref={iconSprites + '#icon-clock'} />
                </svg>
                <span className="counter counter-main">
                  {countStatus?.wait_for_confirmation}
                </span>
              </span>
              <label className="process-label">Chờ xác nhận</label>
            </li>
            <li
              className="tab-item"
              onClick={() => handleGoOrderWithStatus('processing')}
            >
              <span className="process-icon counter-parent">
                <svg className="fill-current" width="21" height="20">
                  <use xlinkHref={iconSprites + '#icon-order-process'} />
                </svg>
                <span className="counter counter-main">
                  {countStatus?.processing}
                </span>
              </span>
              <label className="process-label">Đang xử lý</label>
            </li>
            <li
              className="tab-item"
              onClick={() => handleGoOrderWithStatus('delivery')}
            >
              <span className="process-icon counter-parent">
                <svg className="fill-current" width="23" height="16">
                  <use xlinkHref={iconSprites + '#icon-order-shipping'} />
                </svg>
                <span className="counter counter-main">
                  {countStatus?.delivery}
                </span>
              </span>
              <label className="process-label">Đang giao hàng</label>
            </li>
            <li
              className="tab-item"
              onClick={() => handleGoOrderWithStatus('delivered')}
            >
              <span className="process-icon counter-parent">
                <i className="fal fa-sync" />
                <span className="counter counter-main">
                  {countStatus?.delivered}
                </span>
              </span>
              <label className="process-label">Đã giao hàng</label>
            </li>
            <li
              className="tab-item"
              onClick={() => handleGoOrderWithStatus('canceled')}
            >
              <span className="process-icon counter-parent">
                <i className="fal fa-sync" />
                <span className="counter counter-main">
                  {countStatus?.canceled}
                </span>
              </span>
              <label className="process-label">Đã hủy</label>
            </li>
            <li
              className="tab-item"
              onClick={() => handleGoOrderWithStatus('closed')}
            >
              <span className="process-icon counter-parent">
                <i className="fal fa-sync" />
                <span className="counter counter-main">
                  {countStatus?.closed}
                </span>
              </span>
              <label className="process-label">Đổi trả hàng</label>
            </li>
          </ul>
        </div>
      </div>
      <hr className="h4" />
    </div>
  );
});

export default OrderHistory;
