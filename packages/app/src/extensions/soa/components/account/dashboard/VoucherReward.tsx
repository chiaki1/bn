import { IonCol, IonRow } from '@ionic/react';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import { AppRoute } from '../../../../../router/Route';
import iconSprites from '../../../assets/images/icon-sprites.svg';

const AccountVoucher: React.FC = combineHOC(
  withCustomer,
  withAccountState
)((props) => {
  const voucherLength = useMemo(() => {
    let flag: any = 0;
    if (
      Array.isArray(props.state?.accountState?.customer?.vouchers?.items) &&
      props.state?.accountState?.customer?.vouchers?.items.length
    ) {
      flag = props.state?.accountState?.customer?.vouchers?.items?.filter(
        (item: any) => !item?.used
      ).length;
    }
    return flag;
  }, [props.state?.accountState?.customer?.vouchers?.items]);

  return (
    <div className="account-voucher">
      <div className="container">
        <IonRow className="voucher-list">
          <IonCol size="6">
            <div
              className="voucher-item flex-center"
              onClick={() => RouterSingleton.push('/' + AppRoute.VOUCHER_PAGE)}
            >
              <div className="voucher-icon inline-flex items-center">
                <svg className="fill-current" width="28" height="16">
                  <use xlinkHref={iconSprites + '#icon-voucher'} />
                </svg>
              </div>
              <div className="voucher-item-detail">
                <p className="fw-600 voucher-item-name">Mã giảm giá</p>
                <p className="no-mr text-small">{voucherLength} mã giảm giá</p>
              </div>
            </div>
          </IonCol>
          <IonCol size="6">
            <div
              className="voucher-item flex-center"
              onClick={() => RouterSingleton.push('/' + AppRoute.POINTS_PAGE)}
            >
              <div className="voucher-icon inline-flex items-center">
                <svg className="fill-current" width="28" height="28">
                  <use xlinkHref={iconSprites + '#icon-reward'} />
                </svg>
              </div>
              <div className="voucher-item-detail">
                <p className="fw-600 voucher-item-name">Reward Points</p>
                <p className="no-mr text-small">
                  {props.state?.customer?.reward_points?.balance?.points?.toLocaleString(
                    'vi-VN'
                  ) ?? 0}{' '}
                  points
                </p>
              </div>
            </div>
          </IonCol>
        </IonRow>
      </div>
      <hr className="h4" />
    </div>
  );
});

export default AccountVoucher;
