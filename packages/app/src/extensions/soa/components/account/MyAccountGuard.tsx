import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withInitAccountState } from '@vjcspy/r/build/modules/account/hoc/withInitAccountState';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { AppRoute } from '../../../../router/Route';

const MyAccountGuard = combineHOC(
  withAccountState,
  withInitAccountState
)((props) => {
  useEffect(() => {
    if (
      props?.state?.accountState?.isResolvedCustomerState === true &&
      !props.state?.accountState?.customer
    ) {
      RouterSingleton.push('/' + AppRoute.ACCOUNT);
    }
  }, [props.state?.accountState]);
  return (
    <>
      <UiExtension
        uiId="DEFAULT_LOADING"
        loading={!props.state?.accountState?.isResolvedCustomerState}
      />
    </>
  );
});

export default MyAccountGuard;
