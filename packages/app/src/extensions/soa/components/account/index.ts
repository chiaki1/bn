import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

import { SOA_ACCOUNT_DASHBOARD_EXT_CFG } from './dashboard';
import { SOA_ACCOUNT_BRAND_EXT_CFG } from './list-brand-flow';
import { SOA_ORDER_DETAILS_PAGE_EXT_CFG } from './my-order/order-details';
import { SOA_ORDER_HISTORY_PAGE_EXT_CFG } from './my-order/order-history';
import { SOA_ACCOUNT_UPDATE_INFO_EXT_CFG } from './update-information';

export const SOA_ACCOUNT_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ACCOUNT_CONTAINER',
    component: { lazy: React.lazy(() => import('./AccountContainer')) },
  },
  {
    uiId: 'MY_ACCOUNT',
    component: { lazy: React.lazy(() => import('./MyAccount')) },
  },
  {
    uiId: 'FORM_LOGIN',
    component: { lazy: React.lazy(() => import('./login/FormLogin')) },
  },
  {
    uiId: 'ACCOUNT_LOGIN_CONTAINER',
    component: {
      lazy: React.lazy(() => import('./login/AccountLoginContainer')),
    },
  },
  {
    uiId: 'ACCOUNT_OTP_POPUP_FORM',
    component: { lazy: React.lazy(() => import('./login/FormOtpPopup')) },
  },
  {
    uiId: 'MY_ACCOUNT_GUARD',
    component: { lazy: React.lazy(() => import('./MyAccountGuard')) },
  },
  ...SOA_ACCOUNT_DASHBOARD_EXT_CFG,
  ...SOA_ACCOUNT_UPDATE_INFO_EXT_CFG,
  ...SOA_ACCOUNT_BRAND_EXT_CFG,
  ...SOA_ORDER_DETAILS_PAGE_EXT_CFG,
  ...SOA_ORDER_HISTORY_PAGE_EXT_CFG,
];
