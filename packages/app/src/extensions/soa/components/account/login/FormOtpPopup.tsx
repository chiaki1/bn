import {
  IonBackdrop,
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import OtpInput from 'react-otp-input';

let _interval: any;

const FormOtpPopup: React.FC<{
  close: () => void;
  requestOtp: (phone: string) => void;
  submitOtp: (info: any) => void;
  authInfo: any;
  phone: string;
  isLoading: boolean;
  verifyError: boolean;
  sendOtpRequest: boolean;
}> = combineHOC()((props) => {
  const [time, setTime] = useState(29);
  const [checkDisableButton, setCheckDisableButton] = useState(false);

  const [otp, setOtp] = useState<string>();
  const decreaseNum = useCallback(() => {
    setTime((prev) => {
      if (prev > 0) {
        return prev - 1;
      }
      return 0;
    });
  }, []);

  useEffect(() => {
    _interval = setInterval(decreaseNum, 1000);
    return () => {
      try {
        if (typeof _interval !== 'undefined') {
          clearInterval(_interval);
        }
      } catch {}
    };
  }, []);

  const onSubmit = useCallback(() => {
    const info = {
      otp,
      phone: props?.phone,
      userId: props?.authInfo?.user?.userId,
    };
    if (typeof props.submitOtp === 'function' && props?.isLoading !== true) {
      props.submitOtp(info);
    }
  }, [otp, props?.isLoading]);

  useEffect(() => {
    if (otp && otp?.length >= 4 && !props?.isLoading) {
      setCheckDisableButton(false);
    } else {
      setCheckDisableButton(true);
    }
  }, [otp, props?.isLoading]);

  const reSendOtp = useCallback(() => {
    if (typeof props.requestOtp === 'function' && props?.phone && time === 0) {
      setTime(29);
      if (_interval) {
        clearInterval(_interval);
      }
      _interval = setInterval(decreaseNum, 1000);
      props.requestOtp(props?.phone);
    }
  }, [time, decreaseNum]);

  useEffect(() => {
    if (props?.authInfo?.authorizationCode) {
      setTimeout(() => {
        setOtp(props.authInfo.authorizationCode);
      }, 3000);
    }
  }, [props?.authInfo]);
  return (
    <>
      <IonBackdrop tappable={true} visible={true} stopPropagation={true} />
      <div className="modalv2 modal-full">
        <IonPage className="modalv2-content">
          <IonHeader
            className="page-header ion-no-border header-account"
            translucent={false}
          >
            <div className="header-content text-center">
              <IonToolbar color="nobg">
                <div className="header-container flex-center ">
                  <button
                    className="btn-link-white btn-back"
                    onClick={() => {
                      props.close();
                    }}
                  >
                    <i className="fal fa-chevron-left" />
                  </button>
                  <h1 className="account-pages-title">Nhập mã xác minh</h1>

                  <button className="btn-link-white btn-info">
                    <i className="fal fa-info-circle" />
                  </button>
                </div>
              </IonToolbar>
            </div>
          </IonHeader>
          <IonContent>
            <div className="page-login-otp">
              <div className="container">
                <div className="form-otp text-center">
                  <p>Mã xác minh đã được gửi tới số điện thoại</p>
                  <p className="phone-number">{props?.phone}</p>
                  <div>
                    <p className="text-gray no-mr">
                      Vui lòng nhập đúng 4 số được gửi qua tin nhắn
                    </p>
                    {/*<p className="timer-otp">*/}
                    {/*  <i className="fal fa-clock"></i>*/}
                    {/*  <span className="text-gray">Hiệu lực OTP: </span>*/}
                    {/*  {time}s*/}
                    {/*</p>*/}
                  </div>
                  <div className="digit-group form">
                    <div className="digit-field flex-center-center">
                      <OtpInput
                        value={otp}
                        onChange={(o: any) => setOtp(o)}
                        numInputs={4}
                        inputStyle={{ width: '2em' }}
                        isInputNum={true}
                        className={clsx(
                          'digit-input size-lg',
                          props?.verifyError && 'mage-error'
                        )}
                      />
                    </div>
                    {props?.verifyError && (
                      <div className="error-message text-center pdt-indent-md mage-error">
                        Mã OTP không chính xác
                      </div>
                    )}

                    <button
                      className="btn btn-black btn-fs btn-submit"
                      disabled={checkDisableButton}
                      onClick={() => onSubmit()}
                    >
                      Tiếp tục
                    </button>
                    {/*Remove attr "disable" khi kh nhập đúng OTP*/}
                  </div>
                  <p className="text-left resend">
                    <span className="text-gray">
                      Không nhận được mã xác minh?
                    </span>
                    <button
                      className={clsx(
                        'btn-link',
                        time === 0 && 'text-main',
                        time > 0 && 'text-gray'
                      )}
                      onClick={() => reSendOtp()}
                    >
                      Gửi lại {time > 0 ? `sau ${time}s` : ''}
                    </button>
                  </p>
                </div>
              </div>
            </div>
          </IonContent>
        </IonPage>
      </div>
    </>
  );
});

export default FormOtpPopup;
