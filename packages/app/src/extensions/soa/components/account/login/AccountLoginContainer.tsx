import { IonContent, IonPage } from '@ionic/react';
import { UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const AccountLoginContainer: React.FC = () => {
  return (
    <IonPage className="page-header-no-mrb">
      <IonContent className="offset-bottom-0">
        <UiExtension
          uiId="HEADER_ACCOUNT"
          canBack={false}
          title={'Tài khoản'}
          showIcon={true}
        />
        <UiExtension uiId="FORM_LOGIN" />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
};

export default AccountLoginContainer;
