import { withPhoneAuthActions } from '@vjcspy/r/build/modules/account/hoc/phone/withPhoneAuthActions';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import { AppRoute } from '../../../../../router/Route';

const FormLogin: React.FC = combineHOC(
  withPhoneAuthActions,
  withAccountState
)((props) => {
  const [sendOtpRequest, setSendOtpRequest] = useState(false);
  const [checkDisableButton, setCheckDisableButton] = useState(true);
  const {
    setValue,
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = useCallback((data: any) => {
    if (typeof props.actions?.requestOtp === 'function') {
      props.actions.requestOtp(data.phone);
    }
  }, []);

  const handleToggle = (flag: boolean) => {
    setSendOtpRequest(flag);
    setCheckDisableButton(false);
  };

  useEffect(() => {
    if (props?.state?.isLoading) {
      setCheckDisableButton(props?.state?.isLoading);
    } else {
      setCheckDisableButton(false);
    }
  }, [props?.state?.isLoading, props.state?.isRequestSuccess]);

  // show modal
  useEffect(() => {
    if (props.state?.isRequestSuccess) {
      setSendOtpRequest(true);
    }
  }, [props.state?.isRequestSuccess]);

  useEffect(() => {
    if (props.state?.accountState?.token) {
      setSendOtpRequest(false);
    }
    if (props.state?.authInfo?.token) {
      setSendOtpRequest(false);
    }
  }, [props.state?.accountState?.token, props.state?.authInfo?.token]);

  if (props.state?.accountState?.customer) {
    if (RouterSingleton.pathname !== AppRoute.ACCOUNT) {
      RouterSingleton.push(`/${AppRoute.ACCOUNT}`);
    }
  }
  return (
    <div
      className="block__login bg-cover min-h-full"
      style={{
        backgroundImage: 'url("' + 'images/home/img_account_login.png' + '")',
      }}
    >
      <div className="login-container">
        <h2 className="login-title">
          Nhập số điện thoại để đăng ký / đăng nhập
        </h2>
        <form className="form-login form" onSubmit={handleSubmit(onSubmit)}>
          <div className="field">
            <input
              className={clsx('input-no-border', errors.phone && 'mage-error')}
              type="tel"
              placeholder="Số điện thoại"
              {...register('phone', {
                required: true,
                pattern:
                  /(086|087|096|097|098|032|033|034|035|036|037|038|039|088|091|094|083|084|085|081|082|089|090|093|070|076|077|078|079|092|058|056|099|059|012)+([0-9]{7})\b/u,
              })}
              onFocus={() => {
                setCheckDisableButton(false);
              }}
            />
            {errors.phone && (
              <div className="error-message text-small">
                <i className="fas fa-exclamation-circle" /> Số điện thoại không
                hợp lệ
              </div>
            )}
            {/*Add class="mage-error" cho input khi input invalid*/}
            {/*<div className="error-message text-small">*/}
            {/*  <i className="fas fa-exclamation-circle"></i> Số điện thoại không*/}
            {/*  hợp lệ*/}
            {/*</div>*/}
            {!checkDisableButton && (
              <button
                type="button"
                className="btn-link btn-delete"
                onClick={() => {
                  setValue('phone', '');
                  setCheckDisableButton(true);
                }}
              >
                <i className="fas fa-times-circle" />
              </button>
            )}
          </div>
          <button
            className="btn btn-black btn-fs"
            type="submit"
            disabled={checkDisableButton}
          >
            Tiếp tục
          </button>
        </form>
        <p className="text-small text-white text-center text-approve">
          Bằng việc chọn “Tiếp tục”, bạn đã đồng ý với
          <span className="text-underline">Điều khoản và Điều kiện</span> của
          SALEOFF.ASIA
        </p>
      </div>

      {sendOtpRequest && (
        <UiExtension
          uiId="ACCOUNT_OTP_POPUP_FORM"
          submitOtp={props.actions.submitOtp}
          requestOtp={props.actions.requestOtp}
          authInfo={props.state.authInfo}
          phone={props.state.phone}
          close={() => handleToggle(false)}
          isLoading={props?.state?.isLoading}
          verifyError={props?.state?.verifyError}
          sendOtpRequest={sendOtpRequest}
        />
      )}
    </div>
  );
});

export default FormLogin;
