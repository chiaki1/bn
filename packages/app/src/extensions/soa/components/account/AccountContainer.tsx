import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import { useMemo } from 'react';

const AccountContainer = combineHOC(withAccountState)((props) => {
  const CONTENT = useMemo(() => {
    if (!props.state?.accountState?.isResolvedCustomerState) {
      return null;
    }

    if (props.state?.accountState?.customer) {
      return <UiExtension uiId="MY_ACCOUNT" />;
    } else {
      return <UiExtension uiId="ACCOUNT_LOGIN_CONTAINER" />;
    }
  }, [
    props.state?.accountState?.customer,
    props.state?.accountState?.isResolvedCustomerState,
  ]);

  return <>{CONTENT}</>;
});

export default AccountContainer;
