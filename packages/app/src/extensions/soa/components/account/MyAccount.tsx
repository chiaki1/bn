import { IonContent, IonPage } from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const MyAccount: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER" />
      <IonContent>
        {/*<UiExtension uiId="MESSAGE_SUCCESS" />*/}
        <UiExtension uiId="ACCOUNT_INFO" />
        <UiExtension uiId="ACCOUNT_VOUCHER" />
        <UiExtension uiId="ORDER_HISTORY" />
        <UiExtension uiId="ACCOUNT_SIDEBAR" />
        <UiExtension uiId="CMS_SIDEBAR" />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default MyAccount;
