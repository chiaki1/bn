import {
  IonBackdrop,
  IonContent,
  IonFooter,
  IonHeader,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import React from 'react';

import iconSprites from '../../assets/images/icon-sprites.svg';

const CustomModal = React.memo<{
  show: boolean;
  handleBack: () => void;
  showBack: boolean;
  content: string;
  title: string;
  showHeader?: boolean;
}>((props) => {
  if (props?.show !== true) {
    return null;
  }

  return (
    <>
      <IonBackdrop tappable={true} visible={true} stopPropagation={true} />
      {/*Modal Show Full*/}
      {/*<div className="modalv2 modal-full">*/}
      {/*  <IonPage className="modalv2-content">*/}
      {/*    <IonHeader translucent={false} className="page-header">*/}
      {/*      <div className="header-content text-center">*/}
      {/*        <IonToolbar color="nobg" className="root-min-h-30">*/}
      {/*          <div className="header-container flex-center ">*/}
      {/*            {props?.showBack !== false && (*/}
      {/*              <button*/}
      {/*                className="btn-link-white btn-back"*/}
      {/*                onClick={props?.handleBack}*/}
      {/*              >*/}
      {/*                <i className="fal fa-chevron-left" />*/}
      {/*              </button>*/}
      {/*            )}*/}

      {/*            <h1 className="pages-title">title</h1>*/}
      {/*          </div>*/}
      {/*        </IonToolbar>*/}
      {/*      </div>*/}
      {/*      <div>*/}
      {/*        <a>{props?.content ?? ''}</a>*/}
      {/*      </div>*/}
      {/*    </IonHeader>*/}
      {/*    <IonContent>*/}
      {/*      <div className="container py-32">*/}
      {/*        <h4 className="mt-0">Giao hàng tiêu chuẩn</h4>*/}
      {/*        <p>*/}
      {/*          Lorem Ipsum is simply dummy text of the printing and typesetting*/}
      {/*          industry. Lorem Ipsum has been the industry's standard dummy*/}
      {/*          text ever since the 1500s, when an unknown printer took a galley*/}
      {/*          of type and scrambled it to make a type specimen book. It has*/}
      {/*          survived not only five centuries, but also the leap into*/}
      {/*          electronic typesetting, remaining essentially unchanged. It was*/}
      {/*          popularised in the 1960s with the release of Letraset sheets*/}
      {/*          containing Lorem Ipsum passages, and more recently with desktop*/}
      {/*          publishing software like Aldus PageMaker including versions of*/}
      {/*          Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and*/}
      {/*          typesetting industry. Lorem Ipsum has been the industry's*/}
      {/*          standard dummy text ever since the 1500s, when an unknown*/}
      {/*          printer took a galley of type and scrambled it to make a type*/}
      {/*          specimen book. It has survived not only five centuries, but also*/}
      {/*          the leap into electronic typesetting, remaining essentially*/}
      {/*          unchanged. It was popularised in the 1960s with the release of*/}
      {/*          Letraset sheets containing Lorem Ipsum passages, and more*/}
      {/*          recently with desktop publishing software like Aldus PageMaker*/}
      {/*          including versions of Lorem Ipsum.*/}
      {/*        </p>*/}
      {/*        <p>*/}
      {/*          Lorem Ipsum is simply dummy text of the printing and typesetting*/}
      {/*          industry. Lorem Ipsum has been the industry's standard dummy*/}
      {/*          text ever since the 1500s, when an unknown printer took a galley*/}
      {/*          of type and scrambled it to make a type specimen book. It has*/}
      {/*          survived not only five centuries, but also the leap into*/}
      {/*          electronic typesetting, remaining essentially unchanged. It was*/}
      {/*          popularised in the 1960s with the release of Letraset sheets*/}
      {/*          containing Lorem Ipsum passages, and more recently with desktop*/}
      {/*          publishing software like Aldus PageMaker including versions of*/}
      {/*          Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and*/}
      {/*          typesetting industry. Lorem Ipsum has been the industry's*/}
      {/*          standard dummy text ever since the 1500s, when an unknown*/}
      {/*          printer took a galley of type and scrambled it to make a type*/}
      {/*          specimen book. It has survived not only five centuries, but also*/}
      {/*          the leap into electronic typesetting, remaining essentially*/}
      {/*          unchanged. It was popularised in the 1960s with the release of*/}
      {/*          Letraset sheets containing Lorem Ipsum passages, and more*/}
      {/*          recently with desktop publishing software like Aldus PageMaker*/}
      {/*          including versions of Lorem Ipsum.*/}
      {/*        </p>*/}
      {/*        <p>Dự kiến giao hàng vào 20/11/2021</p>*/}
      {/*      </div>*/}
      {/*    </IonContent>*/}
      {/*    <IonFooter>footer</IonFooter>*/}
      {/*  </IonPage>*/}
      {/*</div>*/}

      {/*Modal Show Center*/}
      <div className="modalv2 modal-center">
        <div className="modalv2-content max-w-340 rounded-8">
          <span className="inline-block absolute top-13 right-20 text-gray cursor-pointer">
            <svg width="13" height="13">
              <use xlinkHref={iconSprites + '#icon-close'} />
            </svg>
          </span>
          <div className="pd-indent">
            <h4 className="mt-0">Giao hàng tiêu chuẩn</h4>
            <p>
              has survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </p>
            <p>Dự kiến giao hàng vào 20/11/2021</p>
          </div>
        </div>
      </div>

      {/*Modal Show Bottom*/}
      {/*<div className="modalv2 modal-end modal-scroll">*/}
      {/*  <div className="modalv2-content">*/}
      {/*    <span className="inline-block absolute top-13 right-20 text-gray cursor-pointer">*/}
      {/*      <svg width="13" height="13">*/}
      {/*        <use xlinkHref={iconSprites + '#icon-close'} />*/}
      {/*      </svg>*/}
      {/*    </span>*/}
      {/*    <div className="pd-indent">*/}
      {/*      <h4 className="mt-0">Giao hàng tiêu chuẩn</h4>*/}
      {/*      <p>*/}
      {/*        Lorem Ipsum is simply dummy text of the printing and typesetting*/}
      {/*        industry. Lorem Ipsum has been the industry's standard dummy text*/}
      {/*        ever since the 1500s, when an unknown printer took a galley of*/}
      {/*        type and scrambled it to make a type specimen book. It has*/}
      {/*        survived not only five centuries, but also the leap into*/}
      {/*        electronic typesetting, remaining essentially unchanged. It was*/}
      {/*        popularised in the 1960s with the release of Letraset sheets*/}
      {/*        containing Lorem Ipsum passages, and more recently with desktop*/}
      {/*        publishing software like Aldus PageMaker including versions of*/}
      {/*        Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and*/}
      {/*        typesetting industry. Lorem Ipsum has been the industry's standard*/}
      {/*        dummy text ever since the 1500s, when an unknown printer took a*/}
      {/*        galley of type and scrambled it to make a type specimen book. It*/}
      {/*        has survived not only five centuries, but also the leap into*/}
      {/*        electronic typesetting, remaining essentially unchanged. It was*/}
      {/*        popularised in the 1960s with the release of Letraset sheets*/}
      {/*        containing Lorem Ipsum passages, and more recently with desktop*/}
      {/*        publishing software like Aldus PageMaker including versions of*/}
      {/*        Lorem Ipsum.*/}
      {/*      </p>*/}
      {/*      <p>*/}
      {/*        Lorem Ipsum is simply dummy text of the printing and typesetting*/}
      {/*        industry. Lorem Ipsum has been the industry's standard dummy text*/}
      {/*        ever since the 1500s, when an unknown printer took a galley of*/}
      {/*        type and scrambled it to make a type specimen book. It has*/}
      {/*        survived not only five centuries, but also the leap into*/}
      {/*        electronic typesetting, remaining essentially unchanged. It was*/}
      {/*        popularised in the 1960s with the release of Letraset sheets*/}
      {/*        containing Lorem Ipsum passages, and more recently with desktop*/}
      {/*        publishing software like Aldus PageMaker including versions of*/}
      {/*        Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and*/}
      {/*        typesetting industry. Lorem Ipsum has been the industry's standard*/}
      {/*        dummy text ever since the 1500s, when an unknown printer took a*/}
      {/*        galley of type and scrambled it to make a type specimen book. It*/}
      {/*        has survived not only five centuries, but also the leap into*/}
      {/*        electronic typesetting, remaining essentially unchanged. It was*/}
      {/*        popularised in the 1960s with the release of Letraset sheets*/}
      {/*        containing Lorem Ipsum passages, and more recently with desktop*/}
      {/*        publishing software like Aldus PageMaker including versions of*/}
      {/*        Lorem Ipsum.*/}
      {/*      </p>*/}
      {/*      <p>Dự kiến giao hàng vào 20/11/2021</p>*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</div>*/}
    </>
  );
});

CustomModal.displayName = 'CustomModal';
export default CustomModal;
