import { IonContent, IonHeader, IonModal, IonToolbar } from '@ionic/react';
import React from 'react';

const DefaultModal = React.memo<{
  show: boolean;
  handleBack: () => void;
  showBack: boolean;
  content: string;
  title: string;
  children: any;
}>((props) => {
  // if (props?.show !== true) {
  //   return null;
  // }
  return (
    <IonModal isOpen={props?.show} className="modal-wrapper-full">
      <IonHeader
        className="page-header ion-no-border header-account"
        translucent={false}
      >
        <div className="header-content text-center">
          <IonToolbar color="nobg">
            <div className="header-container flex-center ">
              {props?.showBack !== false && (
                <button
                  className="btn-link-white btn-back"
                  onClick={props?.handleBack}
                >
                  <i className="fal fa-chevron-left" />
                </button>
              )}

              <h1 className="account-pages-title">{props?.title}</h1>
              {/*<button className="btn-link-white btn-info">*/}
              {/*  <i className="fal fa-info-circle" />*/}
              {/*</button>*/}
            </div>
          </IonToolbar>
        </div>
      </IonHeader>
      <IonContent>{props.children}</IonContent>
    </IonModal>
  );
});

DefaultModal.displayName = 'DefaultModal';
export default DefaultModal;
