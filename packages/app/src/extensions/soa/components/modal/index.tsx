import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_MODAL_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'DEFAULT_MODAL',
    component: { lazy: React.lazy(() => import('./DefaultModal')) },
  },
  // {
  //   uiId: 'DEFAULT_MODAL',
  //   component: { lazy: React.lazy(() => import('./ModalV2')) },
  //   priority: 90,
  // },
];
