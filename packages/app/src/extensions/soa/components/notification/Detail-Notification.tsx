import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const DetailNotification: React.FC = combineHOC()((props) => {
  return (
    <div className="detail-notification">
      <div className="img-detail-noti">
        <img src="https://via.placeholder.com/375x562" alt="" />
      </div>
      <div className="container">
        <div className="content-detail-noti">
          <button>Lưu vocher</button>
          <div className="desc-detail-noti">
            <div className="desc-detail-noti-text">
              <h4 className="desc-detail-noti-title">Nội dung chương trình</h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum
              </p>
            </div>
            <div className="desc-detail-noti-text">
              <h4 className="desc-detail-noti-title">Cách thức tham gia</h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default DetailNotification;
