import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconRight from '../../assets/images/icons/arrow-right.svg';
import iconSale from '../../assets/images/icons/icon-sale.svg';
import ItemNotification from './Item-Notification';

const ItemsNotification: React.FC = combineHOC()((props) => {
  return (
    <div className="container">
      <div className="items-notication">
        <ItemNotification />
        <ItemNotification />
        <ItemNotification />
        <ItemNotification />
      </div>
    </div>
  );
});

export default ItemsNotification;
