import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_NOTIFICATION_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'ITEM_NOTIFICATION',
    component: { lazy: React.lazy(() => import('./Item-Notification')) },
  },
  {
    uiId: 'NOTIFICATION_HOME',
    component: { lazy: React.lazy(() => import('./Notification-Home')) },
  },
  {
    uiId: 'NOTIFICATION_NEWS',
    component: { lazy: React.lazy(() => import('./Notification-News')) },
  },
  {
    uiId: 'UPDATE_ORDER',
    component: { lazy: React.lazy(() => import('./Update-Order')) },
  },
  {
    uiId: 'ITEMS_NOTIFICATION',
    component: { lazy: React.lazy(() => import('./Items-Notification')) },
  },
  {
    uiId: 'LIST_UPDATE_ORDER',
    component: { lazy: React.lazy(() => import('./List-Update-Order')) },
  },
  {
    uiId: 'DETAIL_NOTIFICATION',
    component: { lazy: React.lazy(() => import('./Detail-Notification')) },
  },
];
