import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const NotificationHome: React.FC = combineHOC()((props) => {
  return (
    <div className="notification-home">
      <UiExtension uiId="ITEMS_NOTIFICATION" />
      <hr className="h-8" />
      <UiExtension uiId="LIST_UPDATE_ORDER" />
    </div>
  );
});

export default NotificationHome;
