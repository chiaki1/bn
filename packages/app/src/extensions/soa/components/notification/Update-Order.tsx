import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const UpdateOrder: React.FC = combineHOC()((props) => {
  return (
    <div className="item-update-order">
      <div className="item-update-order-img">
        <img src="https://via.placeholder.com/60x90" />
      </div>
      <div className="item-update-order_desc">
        <h4 className="item-update-order__title">Chương trình khuyến mại</h4>
        <p className="text-update-order">
          Aliqua id fugiat <span className="texxt-main"> SOA00445786</span> quis
          id quis ad et. Sunt qui esse pariatur duis deser...
        </p>
        <p className="time-noti">16:10 30/10/2021</p>
      </div>
    </div>
  );
});

export default UpdateOrder;
