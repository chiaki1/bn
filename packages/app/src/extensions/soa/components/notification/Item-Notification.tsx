import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconRight from '../../assets/images/icons/arrow-right.svg';
import iconSale from '../../assets/images/icons/icon-sale.svg';

const ItemNotification: React.FC = combineHOC()((props) => {
  return (
    <div className="item-notification">
      <div className="item-noti-img">
        <img src={iconSale} alt="" />
      </div>
      <div className="flex desc">
        <div className="item-noti_desc">
          <h4 className="item-noti_desc__title">Chương trình khuyến mại</h4>
          <p className="text-noti">
            Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. Sunt
            qui esse pariatur duis deser...
          </p>
          <p className="time-noti">16:10 30/10/2021</p>
        </div>
        <div className="item-noti_right">
          <span className="item-noti_right__count">99</span>
          <img src={iconRight} className="item-noti_right__arrow" />
        </div>
      </div>
    </div>
  );
});

export default ItemNotification;
