import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconRight from '../../assets/images/icons/arrow-right.svg';
import iconSale from '../../assets/images/icons/icon-sale.svg';
import ItemNotification from './Item-Notification';
import UpdateOrder from './Update-Order';

const ListUpdateOrder: React.FC = combineHOC()((props) => {
  return (
    <div className="container">
      <div className="items-update-order">
        <h4 className="title-update-order">Cập nhật đơn hàng</h4>
        <UpdateOrder />
        <UpdateOrder />
        <UpdateOrder />
        <UpdateOrder />
        <UpdateOrder />
      </div>
    </div>
  );
});

export default ListUpdateOrder;
