import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import iconplus from '../../assets/images/icons/product-page/icon-plus.svg';

const NotificationNews: React.FC = combineHOC()((props) => {
  return (
    <div className={'form-address'}>
      <div className="nav-tab mgb-block">
        <ul className="tab-items list-reset text-center full-2item">
          <li className="tab-item active">Size của bạn</li>
          <li className="tab-item">Hướng dẫn chọn size</li>
        </ul>
      </div>
      <div className={'container'}>
        <div className={'wrap-address'}>
          <div className={'wrap-address-item flex flex-center'}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>

            <div className={'list-info-size'}>
              <h3 className={'item__info-name'}>Nguyễn Tiến Nam</h3>
              <p className={'item__info-sex'}>
                <label>Giới tính:</label>
                <span className="text-main">Nam</span>
              </p>
              <ul className={'item__info-size list-reset flex'}>
                <li>
                  <label>Chiều cao:</label>
                  <span className="text-main">180cm</span>
                </li>
                <li style={{ paddingLeft: '24px' }}>
                  <label>Cân nặng:</label>
                  <span className="text-main">180kg</span>
                </li>
              </ul>
              <p className={'item__info-edit flex flex-content-bw'}>
                <button className={'text-xs'}>Size mặc định</button>
                <button className={'text-main text-xs'}>Sửa số đo</button>
              </p>
            </div>
          </div>
          <div className={'wrap-address-item flex flex-center'}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>

            <div className={'list-info-size'}>
              <h3 className={'item__info-name'}>Nguyễn Tiến Nam</h3>
              <p className={'item__info-sex'}>
                <label>Giới tính:</label>
                <span className="text-main">Nam</span>
              </p>
              <ul className={'item__info-size list-reset flex'}>
                <li>
                  <label>Chiều cao:</label>
                  <span className="text-main">180cm</span>
                </li>
                <li style={{ paddingLeft: '24px' }}>
                  <label>Cân nặng:</label>
                  <span className="text-main">180kg</span>
                </li>
              </ul>
              <p className={'item__info-edit flex flex-content-bw'}>
                <button className={'text-xs'}>Size mặc định</button>
                <button className={'text-main text-xs'}>Sửa số đo</button>
              </p>
            </div>
          </div>
          <div className={'wrap-address-item flex flex-center'}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>

            <div className={'list-info-size'}>
              <h3 className={'item__info-name'}>Nguyễn Tiến Nam</h3>
              <p className={'item__info-sex'}>
                <label>Giới tính:</label>
                <span className="text-main">Nam</span>
              </p>
              <ul className={'item__info-size list-reset flex'}>
                <li>
                  <label>Chiều cao:</label>
                  <span className="text-main">180cm</span>
                </li>
                <li style={{ paddingLeft: '24px' }}>
                  <label>Cân nặng:</label>
                  <span className="text-main">180kg</span>
                </li>
              </ul>
              <p className={'item__info-edit flex flex-content-bw'}>
                <button className={'text-xs'}>Size mặc định</button>
                <button className={'text-main text-xs'}>Sửa số đo</button>
              </p>
            </div>
          </div>
          <div className={'wrap-address-item flex flex-center'}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>

            <div className={'list-info-size'}>
              <h3 className={'item__info-name'}>Nguyễn Tiến Nam</h3>
              <p className={'item__info-sex'}>
                <label>Giới tính:</label>
                <span className="text-main">Nam</span>
              </p>
              <ul className={'item__info-size list-reset flex'}>
                <li>
                  <label>Chiều cao:</label>
                  <span className="text-main">180cm</span>
                </li>
                <li style={{ paddingLeft: '24px' }}>
                  <label>Cân nặng:</label>
                  <span className="text-main">180kg</span>
                </li>
              </ul>
              <p className={'item__info-edit flex flex-content-bw'}>
                <button className={'text-xs'}>Size mặc định</button>
                <button className={'text-main text-xs'}>Sửa số đo</button>
              </p>
            </div>
          </div>
          <div className={'wrap-address-item flex flex-center'}>
            <div className="address-checkbox">
              <label className="label-input">
                <input name="amshopby" type="radio" />
                <span className="label"></span>
              </label>
            </div>

            <div className={'list-info-size'}>
              <h3 className={'item__info-name'}>Nguyễn Tiến Nam</h3>
              <p className={'item__info-sex'}>
                <label>Giới tính:</label>
                <span className="text-main">Nam</span>
              </p>
              <ul className={'item__info-size list-reset flex'}>
                <li>
                  <label>Chiều cao:</label>
                  <span className="text-main">180cm</span>
                </li>
                <li style={{ paddingLeft: '24px' }}>
                  <label>Cân nặng:</label>
                  <span className="text-main">180kg</span>
                </li>
              </ul>
              <p className={'item__info-edit flex flex-content-bw'}>
                <button className={'text-xs'}>Size mặc định</button>
                <button className={'text-main text-xs'}>Sửa số đo</button>
              </p>
            </div>
          </div>
        </div>
        <button type={'button'} className={'btn-add-address flex-center'}>
          <img src={iconplus} alt="" />
          <span>Thêm Size mới</span>
        </button>
      </div>
    </div>
  );
});

export default NotificationNews;
