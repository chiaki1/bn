import { ExtensionConfig } from '@vjcspy/ui-extension';

import { SOA_ADDRESS_EXT_CFG } from './account/address';
import { SOA_ACCOUNT_POLICY_PAGE_EXT_CFG } from './account/infomation-policy';
import { SOA_EXCHANGE_EXT_CFG } from './account/my-order/return-order';
import { SOA_MY_SIZE_EXT_CFG } from './account/my-size';
import { SOA_POINTS_EXT_CFG } from './account/points';
import { SOA_BRAND_EXT_CFG } from './brand';
import { SOA_CATALOG_CATEGORY_EXT_CFG } from './catalog/category';
import { SOA_CATEGORY_TREE_EXT_CFG } from './catalog/category-tree';
import { SOA_PRODUCT_LIST_ITEM_EXT_CFG } from './catalog/product-list-item';
import { SOA_SLIDER_LIST_EXT_CFG } from './catalog/slider-list-product';
import { SOA_CATEGORY_PAGE_EXT_CFG } from './category-page';
import { SOA_CHAT_EXT_CFG } from './chat';
import { SOA_ADDITIONAL_BLOCK_EXT_CFG } from './common/additional-block';
import { SOA_CMS_DETAIL_EXT_CFG } from './common/cms-page';
import { SOA_FLASH_SALE_EXT_CFG } from './common/flash-sales';
import { SOA_REWARD_POINTS_EXT_CFG } from './common/reward-points';
import { SOA_VOUCHER_EXT_CFG } from './common/voucher';
import { SOA_HEADER_COMMON_EXT_CFG } from './header';
import { SOA_HOME_EXT_CFG } from './home';
import { SOA_MODAL_EXT_CFG } from './modal';
import { SOA_NOTIFICATION_EXT_CFG } from './notification';
import { SOA_PRODUCT_EXT_CFG } from './product';
import { SOA_QUESTION_EXT_CFG } from './questions';
import { SOA_SEARCH_EXT_CFG } from './search';
import { NAV_BAR_CPT_CFG } from './tabbar';
import { SOA_WISH_LISH_EXT_CFG } from './wishlist';

export const SOA_CPT_EXT_CFG: ExtensionConfig[] = [
  ...SOA_HOME_EXT_CFG,
  ...NAV_BAR_CPT_CFG,
  ...SOA_BRAND_EXT_CFG,
  ...SOA_FLASH_SALE_EXT_CFG,
  ...SOA_SLIDER_LIST_EXT_CFG,
  ...SOA_ADDITIONAL_BLOCK_EXT_CFG,
  ...SOA_PRODUCT_LIST_ITEM_EXT_CFG,
  ...SOA_HEADER_COMMON_EXT_CFG,
  ...SOA_CATEGORY_TREE_EXT_CFG,
  ...SOA_ADDRESS_EXT_CFG,
  ...SOA_CATEGORY_PAGE_EXT_CFG,
  ...SOA_VOUCHER_EXT_CFG,
  ...SOA_REWARD_POINTS_EXT_CFG,
  ...SOA_ACCOUNT_POLICY_PAGE_EXT_CFG,
  ...SOA_CATALOG_CATEGORY_EXT_CFG,
  ...SOA_PRODUCT_EXT_CFG,
  ...SOA_POINTS_EXT_CFG,
  ...SOA_WISH_LISH_EXT_CFG,
  ...SOA_MY_SIZE_EXT_CFG,
  ...SOA_EXCHANGE_EXT_CFG,
  ...SOA_CHAT_EXT_CFG,
  ...SOA_NOTIFICATION_EXT_CFG,
  ...SOA_QUESTION_EXT_CFG,
  ...SOA_CMS_DETAIL_EXT_CFG,
  ...SOA_SEARCH_EXT_CFG,
  ...SOA_MODAL_EXT_CFG,
];
