import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconinstruct from '../../assets/images/icons/product-page/policy4.1ba90870.svg';
import { withMySizeData } from '../../hoc/account/withMySizeData';

const ProductSizeGuide = combineHOC(withMySizeData)((props) => {
  const checkMySize = useMemo(() => {
    if (
      props?.state?.mySizeData &&
      Array.isArray(props?.state?.mySizeData) &&
      props?.state?.mySizeData.length > 0
    ) {
      const getSize: any = props?.state?.mySizeData.find(
        (item: any) => item?.active === 1
      );
      if (getSize.size) {
        return getSize?.name + ' - ' + getSize.size;
      }
    }
    return null;
  }, [props?.state?.mySizeData]);

  return (
    <div className="product-options">
      <div className="container size-guide">
        <div
          className="swatch-attr instruct flex-center"
          onClick={() => RouterSingleton.push('/' + AppRoute.MY_SIZE_PAGE)}
        >
          <img src={iconinstruct} alt="" />
          {checkMySize ? (
            <span className="txt-instruct size-guide">
              Size của bạn là: {checkMySize}
            </span>
          ) : (
            <span className="txt-instruct size-guide">Hướng dẫn chọn size</span>
          )}
        </div>
      </div>
    </div>
  );
});

export default ProductSizeGuide;
