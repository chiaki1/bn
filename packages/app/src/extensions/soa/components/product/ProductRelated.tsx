import { IonicSlides, IonRow } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';
import { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';

const ProductRelated: React.FC<{ product: any }> = combineHOC()((props) => {
  return (
    <div className="product-related">
      <div className="container">
        {props.product?.related_products &&
          Array.isArray(props.product?.related_products) &&
          props.product.related_products.length > 0 && (
            <div className="product-related-item">
              <div className="product-related-item_top flex flex-content-bw flex-center">
                <h3 className="product-related-item-top_title">
                  Sản phẩm liên quan
                </h3>
                {/*<button className="btn-white-main">Xem thêm</button>*/}
              </div>
              <IonRow className="mr-row">
                <Swiper
                  modules={[
                    Autoplay,
                    Keyboard,
                    Pagination,
                    Scrollbar,
                    Zoom,
                    IonicSlides,
                  ]}
                  autoplay={false}
                  keyboard={true}
                  pagination={false}
                  scrollbar={false}
                  zoom={true}
                  slidesPerView={2.5}
                  className="container-onlyl swiper-slide-4 swiper-slide-h-full"
                >
                  {props.product?.related_products.map((item: any) => {
                    return (
                      <SwiperSlide key={item?.sku}>
                        <UiExtension uiId="PRODUCT_LIST_ITEM" product={item} />
                      </SwiperSlide>
                    );
                  })}
                </Swiper>
              </IonRow>
            </div>
          )}
      </div>
    </div>
  );
});

export default ProductRelated;
