import { IonicSlides } from '@ionic/react';
import { withCustomerWishlistActions } from '@vjcspy/r/build/modules/account/hoc/wishlist/withCustomerWishlistActions';
import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withCurrentProductState } from '@vjcspy/r/build/modules/catalog/hoc/product/withCurrentProductState';
import { withProductWishlistData } from '@vjcspy/r/build/modules/catalog/hoc/product/withProductWishlistData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useState } from 'react';
import { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';

import iconshare from '../../assets/images/icons/product-page/btn-share.svg';
import { withSoaCustomerWishlistData } from '../../hoc/wishlist/withSoaCustomerWishlistData';
import { useProductListItem } from '../../hook/product/useProductListItem';

const ProductMedia: React.FC<{ mediaGallery: any; sku: string }> = combineHOC(
  withCurrentProductState,
  withCustomerWishlistActions,
  withAccountState,
  withSoaCustomerWishlistData,
  withProductWishlistData
)((props) => {
  const [showPopup, setShowPopup] = useState(false);
  const { toggleWishlist } = useProductListItem(props);

  return (
    <div className="product-media img-scale scale-150 scale-skeleton">
      <ul className="product-links list-reset">
        <li className="product-links_item" onClick={() => setShowPopup(true)}>
          <img src={iconshare} alt="" />
        </li>
        <li
          className={clsx(
            'product-links_item',
            props.state?.productInWishlist && 'is-active'
          )}
          onClick={() =>
            toggleWishlist({
              sku: props.sku,
              quantity: 1,
            })
          }
        >
          {/*<img src={iconheart} alt="" />*/}
          <i className="far fa-heart"></i>
        </li>
      </ul>
      <div className="product-swiper image">
        <Swiper
          modules={[
            Autoplay,
            Keyboard,
            Pagination,
            Scrollbar,
            Zoom,
            IonicSlides,
          ]}
          autoplay={false}
          keyboard={true}
          pagination={true}
          scrollbar={false}
          zoom={true}
          className={'pagination-active-main pagination-left-bottom'}
        >
          {props.mediaGallery?.map((items: any) => {
            return (
              <SwiperSlide key={items.position}>
                <div className={'brand-item img-block'}>
                  <img src={items?.url} />
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>

      <UiExtension
        uiId="PRODUCT_SHARE_POPUP"
        show={showPopup}
        urlProduct={window.location.href}
        close={() => setShowPopup(false)}
      />
    </div>
  );
});

export default ProductMedia;
