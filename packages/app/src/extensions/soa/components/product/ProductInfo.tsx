import { withProductInfo } from '@vjcspy/r/build/modules/catalog/hoc/category/withProductInfo';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const ProductInfo: React.FC<{ product: any }> = combineHOC(withProductInfo)(
  (props) => {
    return (
      <div className="product-info">
        <div className="container">
          <h1 className="product-info-brand">{props.product?.brand?.name}</h1>
          <h4 className="product-info-name">{props.product?.name}</h4>
          <UiExtension
            uiId="PRODUCT_PRICE"
            product={{ price_range: props?.state?.productInfo?.priceRange }}
          />
        </div>
      </div>
    );
  }
);

export default ProductInfo;
