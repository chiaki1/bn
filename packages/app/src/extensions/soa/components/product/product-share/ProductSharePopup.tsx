import { IonModal } from '@ionic/react';
import React from 'react';
import {
  FacebookIcon,
  FacebookMessengerIcon,
  FacebookMessengerShareButton,
  FacebookShareButton,
  PinterestIcon,
  PinterestShareButton,
  TwitterIcon,
  TwitterShareButton,
} from 'react-share';

import { clientFacebookId } from '../../../../../values/AuthenIds';
import iconClose from '../../../assets/images/icons/icon-close.svg';

const ProductSharePopup: React.FC<{
  show: boolean;
  close: () => void;
  urlProduct: string;
}> = (props) => {
  // if (!props.show) {
  //   return null;
  // }

  return (
    <IonModal
      isOpen={props?.show}
      className="modal-wrapper-full root-bg-transparent modal-transparent modal-content-end"
    >
      <div className="popup-modal is-active share popup-share bg-white w-full pd-horizon py-32 relative">
        <div className="popup-modal__head border-b-2 pb-16 mb-16">
          <span className="font-semibold">Chia sẻ</span>
          <img
            className="popup-modal__icon"
            src={iconClose}
            onClick={() => props.close()}
          />
        </div>
        <div className="popup-modal__content p-3">
          <FacebookShareButton url={props.urlProduct} className="p-3">
            <FacebookIcon size={'1.5rem'} round />
            <label className="label block text-xs text-gray">Facebook</label>
          </FacebookShareButton>
          {/*<button className="react-share__ShareButton">*/}
          {/*  <img src={iconClose} alt="" />*/}
          {/*</button>*/}
          <TwitterShareButton url={props.urlProduct} className="p-3">
            <TwitterIcon size={'1.5rem'} round />
            <label className="label block text-xs text-gray">Twitter</label>
          </TwitterShareButton>
          <PinterestShareButton media={props.urlProduct} url={props.urlProduct}>
            <PinterestIcon size={'1.5rem'} round />
            <label className="label block text-xs text-gray">Pinterest</label>
          </PinterestShareButton>
          <FacebookMessengerShareButton
            url={props.urlProduct}
            appId={clientFacebookId}
            className="p-3"
          >
            <FacebookMessengerIcon size={'1.5rem'} round />
            <label className="label block text-xs text-gray">Messenger</label>
          </FacebookMessengerShareButton>
        </div>
      </div>
    </IonModal>
  );
};

export default ProductSharePopup;
