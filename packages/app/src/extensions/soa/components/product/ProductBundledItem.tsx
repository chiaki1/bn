import { IonText } from '@ionic/react';
import { useImageSizeBaseOnCfg } from '@vjcspy/chitility/build/hook/useImageSizeBaseOnCfg';
import { withProductInfo } from '@vjcspy/r/build/modules/catalog/hoc/category/withProductInfo';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const ProductBundledItem = combineHOC(withProductInfo)((props) => {
  const { height } = useImageSizeBaseOnCfg(
    ['product', 'default_image_w_h'],
    undefined,
    70,
    undefined
  );
  return (
    <li className="product-bundled_item">
      <label className="label-input text-lg-semi">
        <input type="checkbox" />
        <span className="label"></span>
      </label>
      <div className="wrp-product-bundled_item ">
        <div className="product-photo">
          <UiExtension
            uiId="IMAGE"
            height={height}
            width={70}
            src={props?.bundleItem?.product?.image?.url}
          />
        </div>
        <div className="product-item-detail">
          <h4 className="name-bracnh">CANIFA</h4>
          <h3 className="name-product">
            <span>Extra 15%</span>
            {props?.bundleItem?.product?.name}
          </h3>
          <div className="price-wrap flex flex-center">
            <IonText color="primary" className="special-price-bundled">
              500.000đ
            </IonText>
            <IonText className="old-price text-small">580.000đ</IonText>
          </div>
          <div className="option-combo flex flex-content-bw">
            <select>
              <option value="0">Màu sắc</option>
              <option value="1">Audi</option>
              <option value="2">BMW</option>
            </select>
            <select>
              <option value="0">Kích thước</option>
              <option value="1">Audi</option>
              <option value="2">BMW</option>
            </select>
          </div>
        </div>
      </div>
    </li>
  );
});
export default ProductBundledItem;
