import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useState } from 'react';

import arowright from '../../assets/images/icons/arrow-right.svg';

const ProductBrand: React.FC<{ product: any }> = (props) => {
  const [showPopup, setShowPopup] = useState(false);
  return (
    <>
      <div className="product-info__item " onClick={() => setShowPopup(true)}>
        <div className={'container'}>
          <div className={'product-desc_item flex-center flex-content-bw '}>
            <h3 className={'product-desc-item_title list-reset'}>
              Thương hiệu
            </h3>
            <>
              <img src={arowright} alt="" onClick={() => setShowPopup(true)} />
            </>
          </div>
        </div>
      </div>
      <UiExtension
        uiId="DEFAULT_MODAL"
        title="Thương hiệu"
        show={showPopup}
        content=""
        handleBack={() => setShowPopup(false)}
      >
        <div className="container desc-detail pt-16">
          <div className="content-description">
            <h3>{props.product?.brand?.name || ''}</h3>
            <div
              dangerouslySetInnerHTML={{
                __html: props.product?.brand?.intro?.content || '',
              }}
            />
          </div>
        </div>
      </UiExtension>
    </>
  );
};

export default ProductBrand;
