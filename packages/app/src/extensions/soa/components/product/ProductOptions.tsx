import { logger } from '@vjcspy/chitility/build/util/logger';
import { UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

const ProductOptions = React.memo<{ product: any }>((props) => {
  const Options = useMemo(() => {
    if (props.product) {
      if (props.product['__typename'] === 'ConfigurableProduct') {
        return <UiExtension uiId="PRODUCT_TYPE_OPTIONS_CONFIGURABLE" />;
      } else if (props.product['__typename'] === 'SimpleProduct') {
        return <UiExtension uiId="PRODUCT_TYPE_OPTIONS_SIMPLE" />;
      } else {
        logger.warn(
          'We not yet support this product type ' + props.product['__typename']
        );
        return null;
      }
    }
    return null;
  }, [props.product]);

  return <>{Options}</>;
});

ProductOptions.displayName = 'ProductOptions';
export default ProductOptions;
