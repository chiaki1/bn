import { IonicSlides } from '@ionic/react';
import { withRecentProductData } from '@vjcspy/r/build/modules/catalog/hoc/product/withRecentProductData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';
import { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';

import SliderItemProduct from '../catalog/slider-list-product/SliderItemProduct';

const RecentlyViewedProducts: React.FC<{ currentSku: any }> = combineHOC(
  withRecentProductData
)((props) => {
  return (
    <>
      {props?.state?.recentProducts && props?.state?.recentProducts.length > 0 && (
        <div className="product-desc">
          <div className="container">
            <div className="product-related-item">
              <div className="product-related-item_top flex flex-content-bw flex-center">
                <h3 className="product-related-item-top_title">Xem gần đây</h3>
                {/*<button className="btn-white-main">Xem thêm</button>*/}
              </div>
              {/*<IonRow className={'mr-row'}>*/}
              <Swiper
                modules={[
                  Autoplay,
                  Keyboard,
                  Pagination,
                  Scrollbar,
                  Zoom,
                  IonicSlides,
                ]}
                autoplay={false}
                keyboard={true}
                pagination={false}
                scrollbar={false}
                zoom={true}
                slidesPerView={2.5}
                className="container-onlyl swiper-slide-4 swiper-slide-h-full"
              >
                {props?.state?.recentProducts
                  .slice()
                  .reverse()
                  .filter((item: any) => props.currentSku != item.sku)
                  .map((item: any) => {
                    return (
                      <SwiperSlide
                        key={item?.id}
                        onClick={() => {
                          RouterSingleton.push('/' + item?.url_key + '.html');
                        }}
                      >
                        <SliderItemProduct product={item} />
                      </SwiperSlide>
                    );
                  })}
              </Swiper>
              {/*</IonRow>*/}
            </div>
          </div>
        </div>
      )}
    </>
  );
});
export default RecentlyViewedProducts;
