import { IonContent, IonPage } from '@ionic/react';
import { withInitProductInfo } from '@vjcspy/r/build/modules/catalog/hoc/category/withInitProductInfo';
import { withInitRecentProduct } from '@vjcspy/r/build/modules/catalog/hoc/product/withInitRecentProduct';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect, useRef } from 'react';

import { withSoaProductContainer } from '../../hoc/product/withSoaProductContainer';

const Product = combineHOC(
  withSoaProductContainer,
  withInitRecentProduct,
  withInitProductInfo
)((props) => {
  const contentRef = useRef<HTMLIonContentElement | null>(null);
  const scrollToTop = () => {
    contentRef.current && contentRef.current.scrollToTop();
  };

  useEffect(() => {
    scrollToTop();
    if (
      props.product &&
      typeof props.actions?.initRecentProduct === 'function'
    ) {
      props.actions?.initRecentProduct(props?.product as any);
    }
  }, [props.product?.sku]);

  return (
    <IonPage>
      <UiExtension uiId="HEADER_LOGO_INLINE" title="" showSearch={false} />
      <IonContent ref={contentRef} scrollEvents={true}>
        <UiExtension
          uiId="PRODUCT_MEDIA"
          mediaGallery={props?.product?.media_gallery}
          sku={props?.product?.sku}
        />
        <UiExtension uiId="PRODUCT_INFO" product={props.product} />
        <UiExtension uiId="PRODUCT_OPTIONS" product={props.product} />
        <UiExtension uiId="PRODUCT_SIZE_GUIDE" product={props.product} />
        <UiExtension uiId="PRODUCT_QTY" product={props.product} />
        <UiExtension uiId="PRODUCT_POLICY" product={props.product} />
        <UiExtension uiId="PRODUCT_BUNDLES" product={props.product} />
        <UiExtension uiId="DESCRIPTIONS" product={props.product} />
        <UiExtension uiId="PRODUCT_BRAND" product={props.product} />
        <UiExtension uiId="PRODUCT_RELATED" product={props.product} />
        <UiExtension
          uiId="RECENTLY_VIEWED_PRODUCTS"
          currentSku={props.product?.sku}
        />
      </IonContent>
      <UiExtension uiId="ADD_TO_CART" />
    </IonPage>
  );
});

export default Product;
