import { withProductInfo } from '@vjcspy/r/build/modules/catalog/hoc/category/withProductInfo';
import { withCurrentProductState } from '@vjcspy/r/build/modules/catalog/hoc/product/withCurrentProductState';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback, useEffect, useState } from 'react';

import btncountdown from '../../assets/images/icons/product-page/btn-count-down.svg';
import btncountup from '../../assets/images/icons/product-page/btn-count-up.svg';

const ProductQty = combineHOC(
  withProductInfo,
  withCurrentProductState
)((props) => {
  const [qty, setQty] = useState<any>(props?.state?.productInfo?.qty ?? 1);
  const [message, setMessage] = useState('');
  const productQty =
    (props.state.productInfo?.configurable?.variants &&
      Array.isArray(props.state.productInfo?.configurable?.variants) &&
      props.state.productInfo?.configurable?.variants?.length > 0 &&
      props.state.productInfo?.configurable?.variants[0]?.product
        ?.bms_saleable_qty) ||
    null;
  const updateQty = useCallback(
    (e) => {
      const value = e?.target?.value || e;
      if (!isNaN(value) && value > 0) {
        if (!!productQty && value > productQty) {
          setMessage('Chỉ còn lại ' + productQty + ' sản phẩm.');
          setQty(productQty);
        } else {
          setQty(value);
          setMessage('');
        }
      } else {
        setQty(1);
      }
    },
    [qty, productQty]
  );

  useEffect(() => {
    if (qty > 0 && props?.product?.id) {
      props?.actions?.setProductInfoQty(props.product.id, qty);
    }
  }, [qty]);

  useEffect(() => {
    setQty(1);
    setMessage('');
  }, [props.state.productInfo?.configurable?.variants]);

  return (
    <div className="product-options">
      <div className="container">
        <div className="swatch-attr product-quanlity flex-center mb-4">
          <label className="attr-label">Số lượng: </label>
          <div className="group-quanlity flex-center">
            <div className="btn-count" onClick={() => updateQty(qty - 1)}>
              <img src={btncountdown} alt="" />
            </div>
            <input
              type="number"
              inputMode="numeric"
              value={qty}
              onChange={updateQty}
            />
            <div className="btn-count" onClick={() => updateQty(qty + 1)}>
              <img src={btncountup} alt="" />
            </div>
          </div>
        </div>
        <p
          className="text-gray mt-0 text-small"
          style={{ color: '#ce0814', padding: '0 16px' }}
        >
          {message}
        </p>
      </div>
    </div>
  );
});

export default ProductQty;
