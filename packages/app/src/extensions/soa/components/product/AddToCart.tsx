import { IonBackdrop, IonFooter, IonToolbar } from '@ionic/react';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { withCurrentProductState } from '@vjcspy/r/build/modules/catalog/hoc/product/withCurrentProductState';
import { withAddToCartActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withAddToCartActions';
import { withAddToCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withAddToCartData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useMemo, useRef, useState } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconcart from '../../assets/images/icons/icon-cart.svg';
import iconchat from '../../assets/images/icons/icon-chat.svg';
import iconclose from '../../assets/images/icons/icon-close.svg';

const AddToCartSuccessModal = React.memo<{
  setShowSuccessModal: (show: boolean) => void;
  show: boolean;
  state: {
    productCartItem: any;
  };
}>((props) => {
  const goToCart = useCallback(async () => {
    props?.setShowSuccessModal(false);
    RouterSingleton.push(`/${AppRoute.CART}`);
  }, []);

  const closes = useCallback(async () => {
    props?.setShowSuccessModal(false);
  }, []);

  return (
    <>
      <IonBackdrop tappable={true} visible={true} stopPropagation={true} />
      <div className="modalv2 modal-end modal-scroll">
        <div className="modalv2-content">
          <div className="container">
            <div className="top-modal-product-success flex flex-content-bw">
              <p className="add-success">
                <i className="fas fa-check-circle" /> Thêm vào giỏ hàng thành
                công
              </p>
              <button className="btn-link" onClick={closes}>
                <img src={iconclose} alt="" />
              </button>
            </div>
            <div className="add-product-success">
              <span className="max-w-70">
                <UiExtension
                  uiId="IMAGE"
                  src={
                    props.state.productCartItem.product?.small_image?.url ?? ''
                  }
                />
              </span>
              <div className="info-product-success">
                <h3>{props.state.productCartItem.product.name}</h3>
                <p className="bracnh-product-success">
                  Cung cấp bởi:{' '}
                  <span>
                    {props.state.productCartItem.product?.brand?.name ?? ''}
                  </span>
                </p>
                <p className="price-product-success">
                  Giá:{' '}
                  <span className="text-main text-l ">
                    <UiExtension
                      uiId="CURRENCY"
                      price={
                        props.state.productCartItem?.prices?.price.value ?? 0
                      }
                    />
                  </span>
                </p>
              </div>
            </div>
            <IonFooter>
              <IonToolbar className="ion-no-padding">
                <button className="btn-fs btn-main" onClick={goToCart}>
                  Xem giỏ hàng
                </button>
              </IonToolbar>
            </IonFooter>
          </div>
        </div>
      </div>
    </>
  );
});

const AddToCart: React.FC = combineHOC(
  withCurrentProductState,
  withAddToCartActions,
  withAddToCartData
)((props) => {
  const isAddingToCArt = useMemo(() => {
    if (props.actions?.isAddingProductId) {
      return props.actions.isAddingProductId(props.state?.product?.id);
    }

    return false;
  }, [props.actions?.isAddingProductId, props.state?.product?.id]);

  const [showSuccessModal, setShowSuccessModal] = useState(false);

  const addToCart = useCallback(() => {
    if (isAddingToCArt) {
      return;
    }

    if (
      typeof props.actions?.prepareProductAddToCart === 'function' &&
      props.state?.product?.id &&
      props.actions?.openCart
    ) {
      props.actions.prepareProductAddToCart(props.state.product.id, () => {
        if (
          props.state?.productInfo?.product?.__typename ===
          'ConfigurableProduct'
        ) {
          if (
            props.state?.productInfo?.configurable?.super_attribute &&
            props?.state?.product?.configurable_options &&
            Array.isArray(props?.state?.product?.configurable_options) &&
            typeof props.state?.productInfo?.configurable?.super_attribute ===
              'object' &&
            Object.keys(props.state?.productInfo?.configurable?.super_attribute)
              ?.length >= props?.state?.product?.configurable_options.length
          ) {
            setShowSuccessModal(true);
          }
        } else {
          setShowSuccessModal(true);
        }
      });
    } else {
      logger.warning('could not found product in state', props);
    }
  }, [isAddingToCArt, props?.state.product]);

  const showChat = useCallback(() => {
    // @ts-ignore
    window?.Tawk_API?.maximize();
  }, []);

  return (
    <>
      {/*<div className="modalv2 modal-end modal-scroll">*/}
      {/*  <div className="modalv2-content">*/}
      {/*    <span className="inline-block absolute top-13 right-20 text-gray cursor-pointer">*/}
      {/*      <svg width="13" height="13">*/}
      {/*        <use xlinkHref={iconSprites + '#icon-close'} />*/}
      {/*      </svg>*/}
      {/*    </span>*/}
      {/*    <div className="pd-indent"></div>*/}
      {/*  </div>*/}
      {/*</div>*/}

      {props?.state?.productCartItem && showSuccessModal && (
        <AddToCartSuccessModal
          show={showSuccessModal}
          setShowSuccessModal={setShowSuccessModal}
          state={props.state}
        />
      )}
      <IonFooter>
        <IonToolbar className="ion-no-padding">
          <div className="add-to-cart-product">
            <div className={'group-sp-cart flex flex-content-bw'}>
              <div className={'add-to-cart-item btn-support '}>
                <button
                  type={'button'}
                  className={'btn-xl btn-reset flex-center-center'}
                  onClick={showChat}
                >
                  <img src={iconchat} alt="" />
                  Hỗ trợ
                </button>
              </div>
              <div className={'add-to-cart-item btn-add-to-cart '}>
                <button
                  type={'button'}
                  onClick={addToCart}
                  className={'btn-xl btn-reset flex-center-center'}
                  disabled={isAddingToCArt}
                >
                  <img src={iconcart} alt="" />
                  Thêm vào giỏ hàng
                </button>
              </div>
            </div>
          </div>
        </IonToolbar>
      </IonFooter>
      <UiExtension uiId="DEFAULT_LOADING" loading={isAddingToCArt} />
    </>
  );
});
export default AddToCart;
