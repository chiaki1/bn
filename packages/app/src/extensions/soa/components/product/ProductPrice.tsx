import { withProductDiscountPriceResolver } from '@vjcspy/r/build/modules/catalog/hoc/product/withProductDiscountPriceResolver';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const ProductPrice = combineHOC(withProductDiscountPriceResolver)((props) => {
  return (
    <div className="product-price flex">
      <p className="product-price_special list-reset">
        <UiExtension
          uiId="CURRENCY"
          price={props.product?.price_range?.minimum_price?.final_price?.value}
        />
      </p>
      {!!props?.isShowDiscount && (
        <p className="product-price_old-price list-reset old-price">
          <UiExtension
            uiId="CURRENCY"
            price={
              props.product?.price_range?.maximum_price?.regular_price?.value
            }
          />
        </p>
      )}
    </div>
  );
});

export default ProductPrice;
