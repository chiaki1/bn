import { withProductInfo } from '@vjcspy/r/build/modules/catalog/hoc/category/withProductInfo';
import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useEffect, useMemo } from 'react';

const SwatchList = combineHOC(withProductInfo)((props) => {
  const oneOption = useMemo(
    () =>
      props?.actions?.configurable?.isAttributeHasOneOption(
        props?.option?.attribute_code
      ),
    [props?.option]
  );

  useEffect(() => {
    if (oneOption?.uid) {
      props.actions.configurable.toggleConfigurableOption(
        props.option['attribute_code'],
        oneOption['uid'],
        true
      );
    }
  }, [oneOption]);

  const toggleOption = useCallback(
    (uid) => {
      if (oneOption?.uid) {
        props.actions.configurable.toggleConfigurableOption(
          props.option['attribute_code'],
          oneOption['uid'],
          true
        );
      } else {
        props.actions.configurable.toggleConfigurableOption(
          props.option['attribute_code'],
          uid
        );
      }
    },
    [props.actions.configurable.toggleConfigurableOption, oneOption?.uid]
  );
  return (
    <>
      <div className="product-options">
        <div className="container">
          <div className="swatch-attr swatch-color flex">
            <label className="attr-label">{props?.option?.label}</label>
            <div className="listbox flex">
              <div className="swatch-list list-reset flex">
                {props?.option['values'].map((value: any) => (
                  <button
                    style={{ background: value?.swatch_data?.value }}
                    className={clsx(
                      'swatch-item swatch-color',
                      props.state?.productInfo?.configurable?.super_attribute[
                        props.option['attribute_code']
                      ] === value['uid'] && 'active',
                      !props.fns.configurable.isOptionValueAvailable(
                        props.option['attribute_code'],
                        value['uid']
                      ) && 'outofstock'
                    )}
                    onClick={() => toggleOption(value['uid'])}
                    disabled={
                      !props.fns.configurable.isOptionValueAvailable(
                        props.option['attribute_code'],
                        value['uid']
                      )
                    }
                    key={value['uid']}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
});

export default SwatchList;
