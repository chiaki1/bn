import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CONFIGURABLE_OPTIONS_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'PRODUCT_TYPE_OPTIONS_CONFIGURABLE_OPTION_SWATCH_LIST',
    uiTags: ['PRODUCT_TYPE_OPTIONS_CONFIGURABLE_OPTION_SWATCH_LIST'],
    component: { lazy: React.lazy(() => import('./SwatchList')) },
    priorityFn: () => 10,
  },

  {
    uiId: 'PRODUCT_TYPE_OPTIONS_CONFIGURABLE_OPTION_TITLE_LIST',
    uiTags: ['PRODUCT_TYPE_OPTIONS_CONFIGURABLE_OPTION_TITLE_LIST'],
    component: { lazy: React.lazy(() => import('./TitleList')) },
    priorityFn: () => 10,
  },
];
