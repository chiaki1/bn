import { ExtensionConfig } from '@vjcspy/ui-extension';

import { SOA_CONFIGURABLE_OPTIONS_EXT_CFG } from './configurable-options';

export const SOA_OPTION_TYPE_EXT_CFG: ExtensionConfig[] = [
  ...SOA_CONFIGURABLE_OPTIONS_EXT_CFG,
];
