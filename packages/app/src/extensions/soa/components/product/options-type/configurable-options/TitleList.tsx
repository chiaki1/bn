import { withProductInfo } from '@vjcspy/r/build/modules/catalog/hoc/category/withProductInfo';
import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React from 'react';

const TitleList = combineHOC(withProductInfo)((props) => {
  return (
    <>
      <div className="product-options">
        <div className="container">
          <div className="swatch-attr swatch-size flex">
            <label className="attr-label">{props?.option?.label}</label>
            <div className="listbox">
              <div className="swatch-list list-reset flex flex-wrap">
                {props.option['values'].map((value: any) => (
                  <button
                    className={clsx(
                      'options-item',
                      props.state?.productInfo?.configurable?.super_attribute[
                        props.option['attribute_code']
                      ] === value['uid'] && 'active'
                    )}
                    onClick={() =>
                      props.actions.configurable.toggleConfigurableOption(
                        props.option['attribute_code'],
                        value['uid']
                      )
                    }
                    disabled={
                      !props.fns.configurable.isOptionValueAvailable(
                        props.option['attribute_code'],
                        value['uid']
                      )
                    }
                    key={value['uid']}
                  >
                    <label>{value['label']}</label>
                  </button>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
});

export default TitleList;
