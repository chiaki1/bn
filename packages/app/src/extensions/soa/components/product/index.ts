import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

import { SOA_OPTION_TYPE_EXT_CFG } from './options-type';

export const SOA_PRODUCT_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'PRODUCT',
    component: { lazy: React.lazy(() => import('./Product')) },
  },
  {
    uiId: 'PRODUCT_MEDIA',
    component: { lazy: React.lazy(() => import('./ProductMedia')) },
  },
  {
    uiId: 'PRODUCT_BUNDLES',
    component: { lazy: React.lazy(() => import('./ProductBundles')) },
  },
  {
    uiId: 'PRODUCT_BUNDLED_ITEM',
    component: { lazy: React.lazy(() => import('./ProductBundledItem')) },
  },
  {
    uiId: 'PRODUCT_BRAND',
    component: { lazy: React.lazy(() => import('./ProductBrand')) },
  },
  {
    uiId: 'PRODUCT_RELATED',
    component: { lazy: React.lazy(() => import('./ProductRelated')) },
  },
  {
    uiId: 'ADD_TO_CART',
    component: { lazy: React.lazy(() => import('./AddToCart')) },
  },
  {
    uiId: 'PRODUCT_INFO',
    component: { lazy: React.lazy(() => import('./ProductInfo')) },
    priorityFn: () => 10,
  },
  {
    uiId: 'PRODUCT_PRICE',
    component: { lazy: React.lazy(() => import('./ProductPrice')) },
    priorityFn: () => 10,
  },
  {
    uiId: 'PRODUCT_OPTIONS',
    component: { lazy: React.lazy(() => import('./ProductOptions')) },
  },
  {
    uiId: 'PRODUCT_SIZE_GUIDE',
    component: { lazy: React.lazy(() => import('./ProductSizeGuide')) },
  },
  {
    uiId: 'PRODUCT_POLICY',
    component: { lazy: React.lazy(() => import('./ProductPolicy')) },
  },
  {
    uiId: 'PRODUCT_QTY',
    component: { lazy: React.lazy(() => import('./ProductQty')) },
  },
  {
    uiId: 'RECENTLY_VIEWED_PRODUCTS',
    component: { lazy: React.lazy(() => import('./RecentlyViewedProducts')) },
  },
  {
    uiId: 'DESCRIPTIONS',
    component: { lazy: React.lazy(() => import('./Descriptions')) },
  },
  {
    uiId: 'PRODUCT_SHARE_POPUP',
    component: {
      lazy: React.lazy(() => import('./product-share/ProductSharePopup')),
    },
  },
  ...SOA_OPTION_TYPE_EXT_CFG,
];
