import { ProductInterface } from '@vjcspy/apollo';
import { UiExtension } from '@vjcspy/ui-extension';
import React, { useState } from 'react';

import arowright from '../../assets/images/icons/arrow-right.svg';

const Descriptions: React.FC<{
  product: ProductInterface;
}> = (props) => {
  const [showPopup, setShowPopup] = useState(false);
  return (
    <>
      <div className="product-info__item " onClick={() => setShowPopup(true)}>
        <div className="container">
          <div className="product-desc_item flex-center flex-content-bw ">
            <h3 className="product-desc-item_title list-reset">
              Mô tả sản phẩm
            </h3>
            <>
              <img src={arowright} alt="" onClick={() => setShowPopup(true)} />
            </>
          </div>
        </div>
      </div>
      <UiExtension
        uiId="DEFAULT_MODAL"
        title="Mô tả sản phẩm"
        show={showPopup}
        handleBack={() => setShowPopup(false)}
        content={props?.product?.description?.html}
      >
        <div className="container desc-detail pt-16 ">
          <div
            className="content-description"
            dangerouslySetInnerHTML={{
              __html:
                props?.product?.description?.html ||
                props?.product?.short_description?.html ||
                '',
            }}
          />
        </div>
      </UiExtension>
    </>
  );
};

export default Descriptions;
