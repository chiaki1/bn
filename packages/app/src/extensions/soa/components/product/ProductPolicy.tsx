import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import policy1 from '../../assets/images/icons/product-page/group-ic-policy1.svg';
import policy2 from '../../assets/images/icons/product-page/group-ic-policy2.svg';
import policy3 from '../../assets/images/icons/product-page/group-ic-policy3.svg';

const ProductPolicy = combineHOC()((props) => {
  return (
    <div className="product-options">
      <div className="container">
        <ul className="swatch-attr group-policy  list-reset flex flex-content-bw">
          <li className={'item-policy'}>
            <img
              src={policy1}
              alt=""
              style={{ width: '21px', height: '45px' }}
            />
            <label className="item-policy_txt">Sản phẩm chính hãng</label>
          </li>
          <li className="item-policy">
            <img
              src={policy2}
              alt=""
              style={{ width: '25px', height: '45px' }}
            />
            <label className="item-policy_txt">Giảm giá mỗi ngày</label>
          </li>
          <li className="item-policy">
            <img
              src={policy3}
              alt=""
              style={{ width: '25px', height: '45px' }}
            />
            <label className="item-policy_txt">15 ngày đổi trả</label>
          </li>
        </ul>
      </div>
      <hr className="h-8" />
    </div>
  );
});

export default ProductPolicy;
