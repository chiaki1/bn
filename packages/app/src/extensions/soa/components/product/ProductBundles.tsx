import { IonText } from '@ionic/react';
import { useImageSizeBaseOnCfg } from '@vjcspy/chitility/build/hook/useImageSizeBaseOnCfg';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import { withSoaProductBundlePack } from '../../hoc/product/withSoaProductBundlePack';

const ProductBundlePack = combineHOC()((props) => {
  const { pack, product } = props;

  const discountAmount = useMemo(() => {
    if (pack.discount_type == 1) {
      return `${pack.discount_amount}%`;
    } else if (pack.discount_type == 0) {
      return <UiExtension uiId="CURRENCY" price={pack.discount_amount} />;
    }
    return null;
  }, []);

  const { height } = useImageSizeBaseOnCfg(
    ['product', 'default_image_w_h'],
    undefined,
    70,
    undefined
  );

  return (
    <div className="product-bundled">
      <div className="container">
        <div className="product-bundled_top flex flex-content-bw">
          <h3 className="product-bundled_title list-reset uppercase">
            {pack['block_title']}
          </h3>
        </div>
        <div className="product-bundled-middle">
          <ul className="list-product-bundled list-reset label-input-gray">
            <li className="product-bundled_item">
              <label className="label-input text-lg-semi">
                <input type="checkbox" />
                <span className="label"></span>
              </label>
              <div className="wrp-product-bundled_item ">
                <div className="product-photo">
                  <UiExtension
                    uiId="IMAGE"
                    height={height}
                    width={70}
                    src={product?.image?.url}
                  />
                </div>
                <div className="product-item-detail">
                  <h4 className="name-bracnh">{product?.brand?.name}</h4>
                  <h3 className="name-product">{product?.name}</h3>
                  <div className="price-wrap flex flex-center">
                    <IonText color="primary" className="special-price-bundled">
                      500.000đ
                    </IonText>
                    <IonText className="old-price text-small">580.000đ</IonText>
                  </div>
                </div>
              </div>
            </li>
            <li className="separator text-small mb-16 or-style text-center">
              <span className="bg-white relative inline-block px-16">
                Mua kèm giảm thêm
              </span>
            </li>
            {pack?.items.map((bundlePackItem: any, index1: number) => (
              <UiExtension
                uiId="PRODUCT_BUNDLED_ITEM"
                key={index1}
                bundleItem={bundlePackItem}
                product={bundlePackItem?.product}
                discountType={pack.discount_type}
              />
            ))}
          </ul>
        </div>
        <div className="text-center border-b-2 mb-20 pb-16">
          <button className="product-bundled_seemore btn-white-main">
            Xem thêm
          </button>
        </div>

        <div className="product-bundled-bottom flex-center flex-content-bw">
          <div className="product-bundled-price">
            <div className="product-sum-price flex">
              <label className="text-price">Tổng tiền: </label>
              <div className="price-wrap flex flex-center">
                <IonText color="primary" className="special-price-bundled">
                  500.000đ
                </IonText>
                <IonText className="old-price text-xs">580.000đ</IonText>
              </div>
            </div>
            <div className="product-save-price flex flex-center">
              <label className={'text-price'}>Tiết kiệm: </label>
              <div className="price-wrap flex">
                <IonText color="primary" className="special-price-bundled">
                  500.000đ
                </IonText>
              </div>
            </div>
          </div>
          <button type="button" className="button btn-main btn-md">
            Mua combo
          </button>
        </div>
      </div>
      <hr className="h-8" />
    </div>
  );
});

const ProductBundles: React.FC = combineHOC(withSoaProductBundlePack)(
  (props) => {
    return (
      <>
        {Array.isArray(props?.state?.bundlePacks) &&
          props.state.bundlePacks.map((pack: any, index: number) => (
            <ProductBundlePack
              key={index}
              pack={pack}
              product={props.product}
            />
          ))}
      </>
    );
  }
);
export default ProductBundles;
