import { getPlatforms, isPlatform, useIonAlert } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { withAppVersion } from '../../hoc/update/withAppVersion';

const ForceUpdate = combineHOC(withAppVersion)((props) => {
  const [present] = useIonAlert();
  useEffect(() => {
    if (
      props?.hasNewVersion === true &&
      !getPlatforms().includes('mobileweb') &&
      (isPlatform('ios') || isPlatform('android'))
    ) {
      present({
        header: 'Có phiên bản mới',
        message: 'Xin hãy update lên phiên bản mới để có trải nghiệm tốt hơn.',
        buttons: [
          {
            text: 'Ok',
            handler: (d) => {
              if (typeof window !== 'undefined') {
                if (isPlatform('ios')) {
                  // @ts-ignore
                  window.location =
                    'https://apps.apple.com/us/app/id1526089274';
                  return false;
                } else if (isPlatform('android')) {
                  // @ts-ignore
                  // window.location = 'market://details?id=ggg.saleOfAsia';
                  window.location =
                    'https://play.google.com/store/apps/details?id=ggg.saleOfAsia';
                  return false;
                }
              }
            },
          },
        ],
        onDidDismiss: (e) => console.log('did dismiss'),
      });
    }
  }, [props?.hasNewVersion, present]);

  return <></>;
});

export default ForceUpdate;
