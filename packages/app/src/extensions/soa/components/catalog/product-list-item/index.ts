import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_PRODUCT_LIST_ITEM_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'PRODUCT_LIST_ITEM',
    component: { lazy: React.lazy(() => import('./ProductListItem')) },
  },
];
