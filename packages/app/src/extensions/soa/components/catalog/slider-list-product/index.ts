import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_SLIDER_LIST_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'SLIDER_LIST_PRODUCT',
    component: { lazy: React.lazy(() => import('./SliderListProduct')) },
  },
  {
    uiId: 'SLIDER_LIST_ITEMS_PRODUCT',
    component: { lazy: React.lazy(() => import('./SliderItemProduct')) },
  },
];
