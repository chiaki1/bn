import { IonicSlides, IonRow } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';
import { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';

import SliderItemProduct from './SliderItemProduct';

const SliderListProduct: React.FC<{ listItems: [] }> = combineHOC()((props) => {
  if (props?.listItems.length === 0) {
    return null;
  }

  return (
    <IonRow className="mr-row">
      <Swiper
        modules={[Autoplay, Keyboard, Pagination, Scrollbar, Zoom, IonicSlides]}
        autoplay={{
          delay: 3000,
          waitForTransition: true,
          pauseOnMouseEnter: false,
        }}
        keyboard={true}
        pagination={true}
        scrollbar={false}
        zoom={true}
        slidesPerView={2.5}
        spaceBetween={8}
        className="container-onlyl"
      >
        {props.listItems?.map((item: any) => {
          return (
            <SwiperSlide key={item?.id}>
              <SliderItemProduct product={item} />
            </SwiperSlide>
          );
        })}
      </Swiper>
    </IonRow>
  );
});

export default SliderListProduct;
