import { IonImg } from '@ionic/react';
import { FlashSaleProducts } from '@vjcspy/apollo-sale-off';
import { withProductDiscountPriceResolver } from '@vjcspy/r/build/modules/catalog/hoc/product/withProductDiscountPriceResolver';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

const SliderItemProduct: React.FC<{ product: FlashSaleProducts }> = combineHOC(
  withProductDiscountPriceResolver
)((props) => {
  return (
    <div
      className="product-item"
      onClick={() => {
        RouterSingleton.push('/' + props.product?.url_key + '.html');
      }}
    >
      <div className="product-photo img-scale scale-150">
        <IonImg class="center image" src={props.product?.image?.url} />
        {props?.isShowDiscount && (
          <label className="label-product text-small">{`-${parseInt(
            props.product?.price_range?.minimum_price?.discount?.percent_off ??
              props.product?.price_range?.maximum_price?.discount
                ?.percent_off ??
              0
          )}%`}</label>
        )}
      </div>

      <div className="product-item-detail pd-item-detail">
        <h3 className="brand-product text-small">
          {props.product?.brand?.name || ' '}
        </h3>
        <p className="product-name text-xs">{props.product?.name}</p>
        <UiExtension uiId="PRICE" priceRange={props.product.price_range} />
      </div>
    </div>
  );
});

export default SliderItemProduct;
