import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback, useState } from 'react';

import { withResolveProductFilterCustom } from '../../../../../modules/catalog/hoc/router/withResolveProductFilterCustom';
import { AppRoute } from '../../../../../router/Route';

const CategoryTree: React.FC<{ dataCategoryTree: any[]; brandId?: any }> =
  combineHOC(withResolveProductFilterCustom)(
    React.memo((props) => {
      const [listActive, setListActive] = useState<any[]>([]);

      const setToActive = useCallback(
        (val: any) => {
          if (listActive.includes(val)) {
            setListActive((prevState: any[]) => {
              return prevState.filter((item: any) => item !== val);
            });
          } else {
            setListActive((prevState: any[]) => {
              return [...prevState, val];
            });
          }
        },
        [listActive]
      );

      const goToPage = useCallback((url: any) => {
        if (url) {
          RouterSingleton.push('/' + url + '.html');
        }
      }, []);

      const goToList = useCallback(() => {
        if (props?.brandId) {
          props?.actions?.setFilterToStore({ brand: { eq: props?.brandId } });
          RouterSingleton.push('/' + AppRoute.PRODUCTS_LIST);
        }
      }, [props?.brandId]);

      if (!props?.dataCategoryTree || props?.dataCategoryTree?.length === 0) {
        return null;
      }
      return (
        <div className="nav-menu">
          <div className="container">
            <ul className="group-menu list-reset">
              <li className="item-menu parent level0">
                <div
                  className="menu-link"
                  onClick={() => {
                    goToList();
                  }}
                >
                  Tất cả sản phẩm
                </div>
              </li>
              {props?.dataCategoryTree?.map((item: any) => (
                <li
                  className={`item-menu parent level0 ${
                    listActive.includes(item?.id) && 'active'
                  }`}
                  key={`br-li0-${item?.id}`}
                  onClick={() => {
                    if (parseInt(item?.children_count) > 0) {
                      setToActive(item?.id);
                    } else {
                      goToPage(item?.url_path);
                    }
                  }}
                >
                  <div
                    className="menu-link"
                    onClick={() => {
                      if (parseInt(item?.children_count) > 0) {
                        setToActive(item?.id);
                      } else {
                        goToPage(item?.url_path);
                      }
                    }}
                  >
                    <span
                      onClick={() => {
                        goToPage(item?.url_path);
                      }}
                    >
                      {item?.name_on_app ?? item?.name}
                    </span>
                  </div>
                  {parseInt(item?.children_count) > 0 && (
                    <span
                      className="item-dropdown"
                      onClick={() => {
                        setToActive(item?.id);
                      }}
                    />
                  )}
                  {parseInt(item?.children_count) > 0 &&
                    listActive.includes(item?.id) && (
                      <ul className="groupmenu-drop lv1 list-reset">
                        {item?.children?.map((it_1: any) => (
                          <li
                            className={`item-menu parent level1 ${
                              listActive.includes(it_1?.id) && 'active'
                            }`}
                            key={`br-li1-${it_1?.id}`}
                            onClick={() => {
                              if (parseInt(it_1?.children_count) > 0) {
                                setToActive(it_1?.id);
                              } else {
                                goToPage(it_1?.url_path);
                              }
                            }}
                          >
                            <div
                              className="menu-link"
                              onClick={() => {
                                if (parseInt(it_1?.children_count) > 0) {
                                  setToActive(it_1?.id);
                                } else {
                                  goToPage(it_1?.url_path);
                                }
                              }}
                            >
                              <span
                                onClick={() => {
                                  goToPage(it_1?.url_path);
                                }}
                              >
                                {it_1?.name_on_app ?? it_1?.name}
                              </span>
                            </div>
                            {parseInt(it_1?.children_count) > 0 && (
                              <span
                                className="item-dropdown"
                                onClick={() => {
                                  setToActive(it_1?.id);
                                }}
                              />
                            )}
                            {parseInt(it_1?.children_count) > 0 &&
                              listActive.includes(it_1?.id) && (
                                <ul className="groupmenu-drop lv2 list-reset">
                                  {it_1?.children?.map((it_2: any) => (
                                    <li
                                      className={`item-menu parent level2  ${
                                        listActive.includes(it_2?.id) &&
                                        'active'
                                      } ${it_2?.id} ${it_2?.name}`}
                                      key={`br-li2-${it_2?.id}`}
                                      onClick={() => {
                                        if (
                                          parseInt(it_2?.children_count) > 0
                                        ) {
                                          setToActive(it_2?.id);
                                        } else {
                                          goToPage(it_2?.url_path);
                                        }
                                      }}
                                    >
                                      <div
                                        className="menu-link"
                                        onClick={() => {
                                          if (
                                            parseInt(it_2?.children_count) > 0
                                          ) {
                                            setToActive(it_2?.id);
                                          } else {
                                            goToPage(it_2?.url_path);
                                          }
                                        }}
                                      >
                                        <span
                                          onClick={() => {
                                            goToPage(it_2?.url_path);
                                          }}
                                        >
                                          {it_2?.name_on_app ?? it_2?.name}
                                        </span>
                                      </div>
                                      {parseInt(it_2?.children_count) > 0 && (
                                        <span
                                          className="item-dropdown"
                                          onClick={() => {
                                            setToActive(it_2?.id);
                                          }}
                                        />
                                      )}
                                      {parseInt(it_2?.children_count) > 0 &&
                                        listActive.includes(it_2?.id) && (
                                          <ul className="groupmenu-drop lv3 list-reset">
                                            {it_2?.children?.map(
                                              (it_3: any) => (
                                                <li
                                                  className={`item-menu parent level3  ${
                                                    listActive.includes(
                                                      it_3?.id
                                                    ) && 'active'
                                                  }`}
                                                  key={`br-li3-${it_3?.id}`}
                                                >
                                                  <div
                                                    className="menu-link"
                                                    onClick={() => {
                                                      goToPage(it_3?.url_path);
                                                    }}
                                                  >
                                                    {it_3?.name_on_app ??
                                                      it_3?.name}
                                                  </div>
                                                  {parseInt(
                                                    it_3?.children_count
                                                  ) > 0 && (
                                                    <span
                                                      className="item-dropdown"
                                                      onClick={() => {
                                                        setToActive(it_3?.id);
                                                      }}
                                                    />
                                                  )}
                                                  {parseInt(
                                                    it_3?.children_count
                                                  ) > 0 &&
                                                    listActive.includes(
                                                      it_3?.id
                                                    ) && (
                                                      <ul className="groupmenu-drop lv3 list-reset">
                                                        {it_3?.children?.map(
                                                          (it_4: any) => (
                                                            <li
                                                              className={`item-menu parent level4  ${
                                                                listActive.includes(
                                                                  it_4?.id
                                                                ) && 'active'
                                                              }`}
                                                              key={`br-li4-${it_4?.id}`}
                                                            >
                                                              <div
                                                                className="menu-link"
                                                                onClick={() => {
                                                                  goToPage(
                                                                    it_4?.url_path
                                                                  );
                                                                }}
                                                              >
                                                                {it_4?.name_on_app ??
                                                                  it_4?.name}
                                                              </div>
                                                              {parseInt(
                                                                it_4?.children_count
                                                              ) > 0 && (
                                                                <span
                                                                  className="item-dropdown"
                                                                  onClick={() => {
                                                                    setToActive(
                                                                      it_4?.id
                                                                    );
                                                                  }}
                                                                />
                                                              )}
                                                            </li>
                                                          )
                                                        )}
                                                      </ul>
                                                    )}
                                                </li>
                                              )
                                            )}
                                          </ul>
                                        )}
                                    </li>
                                  ))}
                                </ul>
                              )}
                          </li>
                        ))}
                      </ul>
                    )}
                </li>
              ))}
            </ul>
          </div>
        </div>
      );
    })
  );

export default CategoryTree;
