import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CATEGORY_TREE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CATEGORY_TREE',
    component: { lazy: React.lazy(() => import('./CategoryTree')) },
  },
];
