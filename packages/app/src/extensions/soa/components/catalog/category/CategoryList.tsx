import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useMemo, useState } from 'react';

import { withCategoryListData } from '../../../hoc/category/withCategoryListData';

const MenuChildItem = React.memo<{ category: any }>((props) => {
  const [showChild, setShowChild] = useState(false);
  const hasChild = useMemo(
    () =>
      Array.isArray(props.category?.children) &&
      props.category?.children.length > 0,
    []
  );
  const selectCat = useCallback(
    (cat: any) => {
      if (Array.isArray(cat?.children) && cat.children.length > 0) {
        setShowChild(!showChild);
      } else {
        RouterSingleton.push(`/${cat.url_path}${cat.url_suffix}`);
      }
    },
    [showChild]
  );
  return (
    <li
      className={clsx('item-menu parent level1', showChild && 'active')}
      onClick={() => selectCat(props?.category)}
    >
      <span className="menu-link">
        {props.category?.name_on_app ?? props.category?.name}
      </span>
      {hasChild && (
        <>
          <span className="item-dropdown" />
          <ul className="groupmenu-drop lv2 list-reset">
            {showChild &&
              props?.category?.children.map((cat: any) => (
                <li
                  className="item-menu parent level2"
                  onClick={() => selectCat(cat)}
                  key={cat.uid}
                >
                  <span className="menu-link">
                    {cat.name_on_app ?? cat.name}
                  </span>
                </li>
              ))}
          </ul>
        </>
      )}
    </li>
  );
});

const MenuItem = React.memo<{
  category: any;
}>((props) => {
  const [showChild, setShowChild] = useState(false);
  const hasChild = useMemo(
    () =>
      Array.isArray(props.category?.children) &&
      props.category?.children.length > 0,
    []
  );

  const selectCat = useCallback(() => {
    const cat = props?.category;
    if (Array.isArray(cat?.children) && cat.children.length > 0) {
      setShowChild(!showChild);
    } else {
      RouterSingleton.push(`/${cat.url_path}${cat.url_suffix}`);
    }
  }, [showChild]);

  return (
    <li className={clsx('item-menu parent level0', showChild && 'active')}>
      <span className="menu-link" onClick={() => selectCat()}>
        {props?.category?.name_on_app ?? props?.category?.name}
      </span>
      {hasChild && (
        <>
          <span className="item-dropdown" onClick={() => selectCat()} />
          {showChild && (
            <ul className="groupmenu-drop lv1 list-reset">
              <li
                className="item-menu parent level1"
                onClick={() => {
                  if (props?.category.url_path) {
                    RouterSingleton.push(
                      `/${props?.category.url_path}${props?.category.url_suffix}`
                    );
                  }
                }}
              >
                <span className="menu-link">Tất cả sản phẩm</span>
                {/*<span className="item-dropdown"></span>*/}
              </li>
              {props?.category?.children?.map((cat: any) => (
                <MenuChildItem category={cat} key={cat.uid} />
              ))}
            </ul>
          )}
        </>
      )}
    </li>
  );
});

const CategoryList = combineHOC(withCategoryListData)((props) => {
  return (
    <>
      <div className="nav-tab mgb-block">
        <div className="container">
          <ul className="tab-items list-reset text-center">
            {props?.state?.categories?.map((cat: any) => (
              <li
                className={clsx(
                  'tab-item',
                  props?.state?.activeCat?.uid === cat?.uid && 'active'
                )}
                key={cat?.uid}
                onClick={() => props?.actions?.setActiveCat(cat)}
              >
                {cat?.name_on_app ?? cat?.name}
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="nav-menu">
        <div className="container">
          <ul className="group-menu list-reset">
            <li
              className="item-menu parent level0"
              onClick={() => {
                if (props?.state?.activeCat?.url_path) {
                  RouterSingleton.push(
                    `/${props?.state?.activeCat?.url_path}${props?.state?.activeCat?.url_suffix}`
                  );
                }
              }}
            >
              <span className="menu-link">Tất cả sản phẩm</span>
            </li>
            {props?.state?.activeCat?.children?.map((cat: any) => (
              <MenuItem category={cat} key={cat.uid} />
            ))}
          </ul>
        </div>
      </div>
    </>
  );
});

export default CategoryList;
