import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CATALOG_CATEGORY_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CATEGORY_LIST',
    component: { lazy: React.lazy(() => import('./CategoryList')) },
  },
];
