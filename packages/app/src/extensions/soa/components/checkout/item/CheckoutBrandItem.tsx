import { withShippingMethodData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withShippingMethodData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useState } from 'react';

const CheckoutBrandItem: React.FC = combineHOC(withShippingMethodData)(
  (props) => {
    const [showBrand, setShowBrand] = useState(true);
    return (
      <div className="summary-item-vendor">
        <div className="cart-vendor-head flex-center ion-justify-content-between">
          <h4 className="vendor-name">
            {props?.itemCart?.name} (
            {props?.itemCart?.countItem || props?.itemCart?.items?.length || 0}{' '}
            sản phẩm)
          </h4>

          <button
            className="btn-link btn-arr-right"
            onClick={() => {
              setShowBrand(!showBrand);
            }}
          >
            {showBrand ? (
              <i className="far fa-chevron-right" />
            ) : (
              <i className="far fa-chevron-down" />
            )}
          </button>
        </div>
        <div className="summary-item-price text-small">
          <p className="no-mr">
            Tổng cộng:{' '}
            <span className="text-main">
              <UiExtension
                uiId="CURRENCY"
                price={props?.itemCart?.totalsBrand ?? 0}
              />
            </span>
          </p>
          <p className="no-mr">
            Phí vận chuyển:{' '}
            <span className="text-main">
              <UiExtension
                uiId="CURRENCY"
                price={props.state?.selectedShippingMethod?.amount?.value ?? 0}
              />
            </span>
          </p>
        </div>
        {showBrand &&
          props?.itemCart?.items?.length > 0 &&
          props?.itemCart?.items?.map((item: any) => (
            <div className="order-vendor-item" key={item?.id}>
              <UiExtension uiId="CHECKOUT_ITEM" item={item} />
            </div>
          ))}
      </div>
    );
  }
);

export default CheckoutBrandItem;
