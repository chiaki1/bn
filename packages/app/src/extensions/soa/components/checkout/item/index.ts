import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CHECKOUT_ITEM_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CHECKOUT_BRAND_ITEM',
    component: { lazy: React.lazy(() => import('./CheckoutBrandItem')) },
  },
  {
    uiId: 'CHECKOUT_ITEM',
    component: { lazy: React.lazy(() => import('./CheckoutItem')) },
  },
];
