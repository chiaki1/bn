import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const CheckoutItem: React.FC = combineHOC()((props) => {
  return (
    <div className="minicart-item">
      <div className="minicart-item-detail">
        <div className="product-photo">
          <img src={props?.item?.product?.small_image?.url} />
        </div>
        <div className="product-item-detail">
          <h3 className="product-name text-lg">{props?.item?.product?.name}</h3>
          <ul className="product-options text-small text-gray list-reset">
            {Array.isArray(props?.item?.configurable_options) &&
              props?.item?.configurable_options.map((option: any) => {
                return (
                  <li
                    className="option-item"
                    key={option?.configurable_product_option_uid}
                  >
                    {option['option_label']}:
                    <span className="option-value">
                      {option['value_label']}
                    </span>
                  </li>
                );
              })}
          </ul>
          <div className="price-box">
            <p className="special-price">
              <UiExtension
                uiId="CURRENCY"
                price={props?.item?.prices?.price?.value}
              />
            </p>
            {props?.item?.product?.price_range?.maximum_price?.regular_price
              ?.value &&
              props?.item?.product?.price_range?.maximum_price?.regular_price
                ?.value > props?.item?.prices?.price?.value && (
                <p className="old-price">
                  <UiExtension
                    uiId="CURRENCY"
                    price={
                      props?.item?.product?.price_range?.maximum_price
                        ?.regular_price?.value
                    }
                  />
                </p>
              )}
          </div>
          <div className="details-qty text-small text-gray">
            <label className="label-qty">Số lượng: </label>
            <span className="value-qty">{props?.item?.quantity}</span>
          </div>
        </div>
      </div>
    </div>
  );
});

export default CheckoutItem;
