import { IonFooter, IonToolbar } from '@ionic/react';
import { animateCSS } from '@vjcspy/chitility/build/util/animateCSS';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { withCheckoutOrderActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutOrderActions';
import { withCheckoutOrderData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutOrderData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback } from 'react';

import { withDefaultPlaceOrderBehavior } from '../../../../modules/checkout/hoc/withDefaultPlaceOrderBehavior';
import { withVnPayPlaceOrderBehavior } from '../../../../modules/checkout/hoc/withVnPayPlaceOrderBehavior';
import { toastErrorMessage } from '../../../../modules/ui/util/toast/toastErrorMessage';

const FooterCheckout: React.FC<{
  scrollToPaymentMethod?: () => void;
  scrollToShippingMethod?: () => void;
}> = combineHOC(
  withCheckoutCartData,
  withCheckoutOrderData,
  withCheckoutOrderActions,
  withDefaultPlaceOrderBehavior,
  withVnPayPlaceOrderBehavior
)((props) => {
  const checkDataPlaceOrderCart = useCallback(() => {
    if (!props?.state?.cart?.selected_payment_method?.code) {
      toastErrorMessage('Vui lòng chọn phương thức thanh toán.');
      animateCSS('.shake-payment', 'shakeX');
      if (typeof props?.scrollToPaymentMethod === 'function') {
        props?.scrollToPaymentMethod();
      }
      return false;
    }
    if (
      !props?.state?.cart?.shipping_addresses[0]?.selected_shipping_method
        ?.method_code
    ) {
      toastErrorMessage('Vui lòng chọn phương thức giao hàng.');
      animateCSS('.shake-shipping-method', 'shakeX');
      if (typeof props?.scrollToShippingMethod === 'function') {
        props?.scrollToShippingMethod();
      }
      return false;
    }
    return true;
  }, [props.state?.cart]);

  const onSubmit = useCallback(() => {
    if (
      checkDataPlaceOrderCart() &&
      typeof props.actions.placeOrderWithHookAction === 'function'
    ) {
      props.actions.placeOrderWithHookAction();
    }
  }, [props.actions?.placeOrderWithHookAction, props.state?.cart]);

  return (
    <IonFooter class="footer-cart">
      <UiExtension
        uiId="DEFAULT_LOADING"
        loading={props.state.isPlacingOrder}
      />
      <IonToolbar className="ion-no-padding">
        <div className="container">
          <p className="text-lg text-center cart-total">
            <span className="price-label">
              Tổng cộng ({props?.state?.cart?.total_quantity || 0} sản phẩm):
            </span>
            <span className="price text-main">
              <UiExtension
                uiId="CURRENCY"
                price={props?.state?.cart?.prices?.grand_total?.value || 0}
              />
            </span>
          </p>
          <button
            className="btn-black btn-fs"
            disabled={props.state.isPlacingOrder}
            onClick={() => onSubmit()}
          >
            Thanh toán
          </button>
        </div>
      </IonToolbar>
    </IonFooter>
  );
});

export default FooterCheckout;
