import { IonContent } from '@ionic/react';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { withIsResolvedCart } from '@vjcspy/r/build/modules/checkout/hoc/cart/withIsResolvedCart';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useRef } from 'react';

import { useIonContentScrollElem } from '../../../../modules/ui/hook/useIonContentScrollElem';

const CheckoutContentContainer: React.FC = combineHOC(
  withCheckoutCartData,
  withIsResolvedCart
)((props) => {
  const { scrollTo } = useIonContentScrollElem();
  const contentRefPayment = useRef<any>();
  const contentRefShipping = useRef<any>();

  const scrollToPaymentMethod = () => {
    if (contentRefPayment?.current?.offsetTop) {
      scrollTo(contentRefPayment?.current?.offsetTop);
    }
  };

  const scrollToShippingMethod = () => {
    if (contentRefShipping?.current?.offsetTop) {
      scrollTo(contentRefPayment?.current?.offsetTop);
    }
  };
  return (
    <>
      <IonContent scrollEvents={true}>
        <UiExtension
          uiId="DEFAULT_LOADING"
          loading={
            !props.state?.isResolvedCart ||
            (props.state?.isResolvedCart && props.state?.isUpdatingTotals)
          }
        />
        <UiExtension uiId="SHIPPING_ADDRESS" />
        <UiExtension uiId="SUMMARY_BRAND" />
        <UiExtension uiId="CART_PROMO" />
        <div ref={contentRefPayment}>
          <UiExtension
            uiId="PAYMENT_METHOD"
            selectedPaymentMethod={props?.state?.cart?.selected_payment_method}
            subtotals={props.state?.cart?.prices?.grand_total?.value}
          />
        </div>
        <div ref={contentRefShipping}>
          <UiExtension uiId="SHIPPING_METHOD" />
        </div>

        <UiExtension uiId="ORDER_SUMMARY" />
      </IonContent>
      <UiExtension
        uiId="FOOTER_CHECKOUT"
        scrollToPaymentMethod={scrollToPaymentMethod}
        scrollToShippingMethod={scrollToShippingMethod}
      />
    </>
  );
});

export default CheckoutContentContainer;
