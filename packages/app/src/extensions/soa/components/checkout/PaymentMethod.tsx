import { animateCSS } from '@vjcspy/chitility/build/util/animateCSS';
import { withCheckoutAddressData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useState } from 'react';

import { useIonContentScrollElem } from '../../../../modules/ui/hook/useIonContentScrollElem';
import { toastErrorMessage } from '../../../../modules/ui/util/toast/toastErrorMessage';
import iconSprites from '../../assets/images/icon-sprites.svg';

const PaymentMethod: React.FC<{
  selectedPaymentMethod: any;
  subtotals: any;
}> = combineHOC(withCheckoutAddressData)((props) => {
  const [showPopupPayment, setShowPopupPayment] = useState(false);
  const { scrollTo } = useIonContentScrollElem();
  const handleToggle = (flag: boolean) => {
    setShowPopupPayment(flag);
  };

  const showPopupCheck = useCallback(() => {
    if (!props?.state?.currentShippingAddress) {
      toastErrorMessage('Vui lòng chọn địa chỉ giao hàng trước.');
      animateCSS('.shake-shipping-address', 'shakeX');
      scrollTo(0);
    } else {
      setShowPopupPayment(true);
    }
  }, [props?.state?.currentShippingAddress, scrollTo]);

  return (
    <div className="shipping-method shake-payment">
      <div className="container animate__animated animate__fadeIn">
        <div className="promo-item">
          <div
            className="promo-btn flex-center btn-arrow-right"
            onClick={() => showPopupCheck()}
          >
            <span className="promo-logo">
              <svg width="21" height="14">
                <use xlinkHref={iconSprites + '#icon-card'} />
              </svg>
            </span>
            <span className="promo-name text-lg">Phương thức thanh toán</span>
          </div>
          <div className="promo-detail text-gray ">
            <p className="promo-des">{props?.selectedPaymentMethod?.title}</p>
            {/*<p className="promo-des">Giảm giá 100.000đ trên tổng đơn hàng</p>*/}
          </div>
        </div>
      </div>
      <UiExtension
        uiId="CHECKOUT_POPUP_PAYMENT"
        close={() => handleToggle(false)}
        selectedPaymentCode={props?.selectedPaymentMethod?.code}
        showPopupPayment={showPopupPayment}
      />
    </div>
  );
});

export default PaymentMethod;
