import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { withShippingMethodData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withShippingMethodData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useMemo } from 'react';

const OrderSummary: React.FC = combineHOC(
  withCheckoutCartData,
  withShippingMethodData
)((props) => {
  const discountAmount = useMemo(() => {
    let count = 0;
    if (
      props.state?.cart?.prices?.discounts &&
      size(props.state?.cart?.prices?.discounts) > 0
    ) {
      props.state?.cart?.prices?.discounts.forEach((discount) => {
        if (discount?.amount?.value) {
          // @ts-ignore
          count = count + discount?.amount?.value;
        }
      });
    } else {
      count = 0;
    }

    return count;
  }, [props?.state?.cart]);

  const discountFromPoint = useMemo(() => {
    let count = 0;

    if (props.state?.cart?.applied_reward_points?.money?.value) {
      count = count + props.state?.cart?.applied_reward_points?.money?.value;
    }

    return count;
  }, [props?.state?.cart]);

  return (
    <div className="order-summary">
      <hr />
      <div className="container">
        <div className="order-summary-content">
          <h2 className="title-account">Thông tin thanh toán</h2>
          <table className="table-order-summary table">
            <tbody>
              <tr>
                <th>Tổng tiền hàng:</th>
                <td>
                  <span className="text-main price">
                    <UiExtension
                      uiId="CURRENCY"
                      price={
                        props.state?.cart?.prices?.subtotal_including_tax?.value
                      }
                    />
                  </span>
                </td>
              </tr>
              <tr>
                <th>Phí vận chuyển:</th>
                <td>
                  <span className="text-main price">
                    <UiExtension
                      uiId="CURRENCY"
                      price={
                        props.state?.selectedShippingMethod?.amount?.value ?? 0
                      }
                    />
                  </span>
                </td>
              </tr>

              <tr>
                <th>Giảm giá: </th>
                <td>
                  <span className="text-main price">
                    {discountAmount ? '- ' : ' '}
                    <UiExtension uiId="CURRENCY" price={discountAmount ?? 0} />
                  </span>
                </td>
              </tr>

              <tr>
                <th>Dùng điểm tích lũy: </th>
                <td>
                  <span className="text-main price">
                    {discountFromPoint ? '- ' : ' '}
                    <UiExtension
                      uiId="CURRENCY"
                      price={discountFromPoint ?? 0}
                    />
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
});

export default OrderSummary;
