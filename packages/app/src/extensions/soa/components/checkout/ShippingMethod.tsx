import { animateCSS } from '@vjcspy/chitility/build/util/animateCSS';
import { withCheckoutAddressData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressData';
import { withShippingMethodData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withShippingMethodData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useState } from 'react';

import { useIonContentScrollElem } from '../../../../modules/ui/hook/useIonContentScrollElem';
import { toastErrorMessage } from '../../../../modules/ui/util/toast/toastErrorMessage';
import iconSprites from '../../assets/images/icon-sprites.svg';

const ShippingMethod: React.FC = combineHOC(
  withShippingMethodData,
  withCheckoutAddressData
)((props) => {
  const { scrollTo } = useIonContentScrollElem();
  const [showPopupShippingMethod, setShowPopupShippingMethod] = useState(false);
  const handleToggle = (flag: boolean) => {
    setShowPopupShippingMethod(flag);
  };
  const showPopupCheck = useCallback(() => {
    if (!props?.state?.currentShippingAddress?.customer_address_id) {
      toastErrorMessage('Vui lòng chọn địa chỉ giao hàng trước.');
      animateCSS('.shake-shipping-address', 'shakeX');
      scrollTo(0);
    } else {
      setShowPopupShippingMethod(true);
    }
  }, [props?.state?.currentShippingAddress, scrollTo]);

  return (
    <div className="delivery-method shake-shipping-method">
      <div className="container animate__animated animate__fadeIn">
        <div className="promo-item">
          <div
            className="promo-btn flex-center btn-arrow-right"
            onClick={() => showPopupCheck()}
          >
            <span className="promo-logo">
              <svg width="23" height="16">
                <use xlinkHref={iconSprites + '#icon-order-shipping'} />
              </svg>
            </span>
            <span className="promo-name text-lg">Phương thức vận chuyển</span>
          </div>
          {props?.state?.selectedShippingMethod?.carrier_title && (
            <div className="promo-detail text-gray flex flex-wrap ion-justify-content-between">
              <p className="promo-des">
                {props?.state?.selectedShippingMethod?.carrier_title}
              </p>
            </div>
          )}
        </div>
      </div>

      <UiExtension
        uiId="CHECKOUT_SHIPPING_METHOD_PAYMENT"
        close={() => handleToggle(false)}
        dataShippingMethod={props?.state}
        showPopupShippingMethod={showPopupShippingMethod}
      />
    </div>
  );
});

export default ShippingMethod;
