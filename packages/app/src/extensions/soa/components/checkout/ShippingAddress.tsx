import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { formatCustomerAddress } from '@vjcspy/r/build/modules/account/util/formatCustomerAddress';
import { withCheckoutAddressData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutAddressData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useState } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconSprites from '../../assets/images/icon-sprites.svg';

const ShippingAddress: React.FC = combineHOC(
  withCustomer,
  withCheckoutAddressData
)((props) => {
  const isDefaultAddress = useCallback(
    (add: any) => {
      return props.state?.customer?.default_shipping == add.customer_address_id;
    },
    [props.state?.customer?.default_shipping]
  );

  return (
    <>
      {!!props.state?.currentShippingAddress ? (
        <div className="shake-shipping-address shipping-address animate__animated animate__fadeIn">
          <UiExtension
            uiId="DEFAULT_LOADING"
            loading={props?.state?.isUpdatingAddress}
          />
          <div className="container ">
            <div className="shipping-address-items">
              <span className="icon-address">
                <svg className="fill-current" width="16" height="21">
                  <use xlinkHref={iconSprites + '#icon-location'} />
                </svg>
              </span>
              <p className="text-lg-semi customer-info">
                {/*{getCustomerName(props?.state?.currentShippingAddress)}*/}
                {props.state?.currentShippingAddress?.firstname}{' '}
                {props.state?.currentShippingAddress?.lastname}
                <span className="separator">|</span>
                {props.state?.currentShippingAddress?.telephone}
              </p>
              <p className="customer-address text-gray">
                {formatCustomerAddress(props.state?.currentShippingAddress)}
              </p>
              <div className="flex-center ion-justify-content-between">
                <p className="text-small text-gray no-mr">
                  {isDefaultAddress(props?.state?.currentShippingAddress) &&
                    'Địa chỉ mặc định'}
                </p>

                <button
                  className="text-small btn-link text-main"
                  onClick={() => {
                    RouterSingleton.push(`/${AppRoute.ADDRESS_LIST_PAGE}`);
                  }}
                >
                  Đổi địa chỉ nhận hàng
                </button>
              </div>
            </div>
          </div>
          <hr />
        </div>
      ) : (
        <div className="shake-shipping-address shipping-address">
          <div className="container">
            <div className="shipping-address-items">
              <span className="icon-address">
                <i className="far fa-map-marker-alt"></i>
              </span>
              <p className="text-lg-semi customer-info">
                Bạn chưa chọn địa chỉ nhận hàng!
              </p>

              <div className="flex-center ion-justify-content-between">
                <p className="text-xs text-gray no-mr" />
                <button
                  className="text-xs btn-link text-main"
                  onClick={() => {
                    RouterSingleton.push(`/${AppRoute.ADDRESS_LIST_PAGE}`);
                  }}
                >
                  Chọn địa chỉ nhận hàng
                </button>
              </div>
            </div>
          </div>
          <hr />
        </div>
      )}
    </>
  );
});

export default ShippingAddress;
