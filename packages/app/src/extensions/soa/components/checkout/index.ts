import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

import { SOA_CHECKOUT_COMPLETE_CPT_CFG } from './complete';
import { SOA_CHECKOUT_ITEM_EXT_CFG } from './item';
import { SOA_POPUP_CHECKOUT_EXT_CFG } from './popup';

export const SOA_CHECKOUT_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CHECKOUT_CONTENT_CONTAINER',
    component: { lazy: React.lazy(() => import('./CheckoutContentContainer')) },
  },
  {
    uiId: 'CHECKOUT_STEP_GUARD',
    component: { lazy: React.lazy(() => import('./CheckoutGuard')) },
    priorityFn: () => 100,
  },
  {
    uiId: 'SHIPPING_ADDRESS',
    component: { lazy: React.lazy(() => import('./ShippingAddress')) },
  },
  {
    uiId: 'SUMMARY_BRAND',
    component: { lazy: React.lazy(() => import('./SummaryBrand')) },
  },
  {
    uiId: 'PAYMENT_METHOD',
    component: { lazy: React.lazy(() => import('./PaymentMethod')) },
  },
  {
    uiId: 'SHIPPING_METHOD',
    component: { lazy: React.lazy(() => import('./ShippingMethod')) },
  },
  {
    uiId: 'ORDER_SUMMARY',
    component: { lazy: React.lazy(() => import('./OrderSummary')) },
  },
  {
    uiId: 'FOOTER_CHECKOUT',
    component: { lazy: React.lazy(() => import('./FooterCheckout')) },
  },
  ...SOA_CHECKOUT_ITEM_EXT_CFG,
  ...SOA_POPUP_CHECKOUT_EXT_CFG,
  ...SOA_CHECKOUT_COMPLETE_CPT_CFG,
];
