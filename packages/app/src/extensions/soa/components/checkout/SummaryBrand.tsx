import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withCartListBrandData } from '../../hoc/cart/withCartListBrandData';

const SummaryBrand: React.FC = combineHOC(
  withCartListBrandData,
  withCheckoutCartData
)((props) => {
  return (
    <div className="summary-vendor">
      <div className="container">
        {props?.state?.listItemShow.map((itemCart: any) => (
          <UiExtension
            uiId="CHECKOUT_BRAND_ITEM"
            itemCart={itemCart}
            cartId={props?.state?.cart?.id}
            key={itemCart?.name}
          />
        ))}
      </div>
    </div>
  );
});

export default SummaryBrand;
