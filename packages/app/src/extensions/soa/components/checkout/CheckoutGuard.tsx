import { withAccountState } from '@vjcspy/r/build/modules/account/hoc/withAccountState';
import { withInitAccountState } from '@vjcspy/r/build/modules/account/hoc/withInitAccountState';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { AppRoute } from '../../../../router/Route';

const CheckoutGuard: React.FC = combineHOC(
  withCheckoutCartData,
  withAccountState,
  withInitAccountState
)(
  React.memo((props) => {
    useEffect(() => {
      if (props.state?.isResolvedCart && props.state.isResolvedAccountState) {
        if (
          !props.state.accountState?.token ||
          !props.state.cart?.total_quantity ||
          props.state.cart?.total_quantity === 0
        ) {
          RouterSingleton.push(`/${AppRoute.CART}`);
        }
      }
    }, [props.state.isResolvedCart, props.state.isResolvedAccountState]);

    return (
      <>
        {(!props.state.accountState.isResolvedCustomerState ||
          !props.state.isResolvedCart) && (
          <UiExtension uiId="DEFAULT_LOADING" loading={true} />
        )}
      </>
    );
  })
);

export default CheckoutGuard;
