import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback } from 'react';

import { AppRoute } from '../../../../../router/Route';

const CompleteHeader: React.FC = combineHOC()((props) => {
  const goHome = useCallback(() => {
    RouterSingleton.replace(`/${AppRoute.HOME}`);
  }, []);
  return (
    <UiExtension
      uiId="HEADER_ACCOUNT"
      goBack={() => goHome()}
      title="Mua hàng thành công"
      showIcon={false}
    />
  );
});

export default CompleteHeader;
