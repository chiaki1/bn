import { IonContent } from '@ionic/react';
import { withCheckoutOrderData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutOrderData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import ROUTES from '@vjcspy/web/src/values/extendable/ROUTES';
import React, { useCallback } from 'react';

import { AppRoute } from '../../../../../router/Route';
import iconSprites from '../../../assets/images/icon-sprites.svg';

const CompleteContent: React.FC = combineHOC(withCheckoutOrderData)((props) => {
  const goHome = useCallback(() => {
    RouterSingleton.replace(`/${AppRoute.HOME}`);
  }, []);
  return (
    <IonContent fullscreen>
      <div className="order-page flex flex-col justify-content items-center">
        <div className="container w-full">
          <div className="pd-vertical text-center">
            <svg className="" width="134" height="134">
              <use xlinkHref={iconSprites + '#icon-success'} />
            </svg>
            <h2 className="text-success mb-16">Đặt hàng thành công!</h2>
            <p className="text-lg-semi mt-0 mb-4">Cám ơn bạn đã đặt hàng!</p>
            <p className="text-small mt-0 mb-18">
              Bật thông báo để nhận các thông tin cấp nhật đơn hàng.
            </p>
            <p className="text-blue font-semibold mt-0 mb-28">
              <span
                className="cursor-pointer"
                onClick={() =>
                  RouterSingleton.push(
                    '/' +
                      AppRoute.ORDER_DETAIL +
                      '/' +
                      props?.state?.completeOrderNumber
                  )
                }
              >
                Chi tiết đơn hàng
              </span>
            </p>
            <p className="text-small mt-0 mb-32 or-style">
              <span className="px-16 inline-block bg-white relative">Hoặc</span>
            </p>
            <button
              className="btn-lg btn-black w-full"
              onClick={() => RouterSingleton.replace(`/${AppRoute.HOME}`)}
            >
              Tiếp tục mua sắm
            </button>
            {/*{props?.state?.completeOrderNumber && (*/}
            {/*  <div className="order-number">*/}
            {/*    Mã đơn đặt hàng:{' '}*/}
            {/*    <strong>{props?.state?.completeOrderNumber}</strong>*/}
            {/*  </div>*/}
            {/*)}*/}
          </div>
          {/*Checkout Fail*/}
          {/*<div className="pd-vertical text-center">*/}
          {/*  <svg className="" width="134" height="134">*/}
          {/*    <use xlinkHref={iconSprites + '#icon-error'} />*/}
          {/*  </svg>*/}
          {/*  <h2 className="text-main mb-16">Đặt hàng không thành công</h2>*/}
          {/*  <p className="text-lg-semi mt-0 mb-4">*/}
          {/*    Có lỗi xảy ra trong quá trình thanh toán!*/}
          {/*  </p>*/}
          {/*  <p className="text-small mt-0 mb-18">Vui lòng thử lại</p>*/}
          {/*  <p className="text-blue font-semibold mt-0 mb-28">*/}
          {/*    <span className="cursor-pointer">Chi tiết đơn hàng</span>*/}
          {/*  </p>*/}
          {/*  <p className="text-small mt-0 mb-32 or-style">*/}
          {/*    <span className="px-16 inline-block bg-white relative">Hoặc</span>*/}
          {/*  </p>*/}
          {/*  <button className="btn-lg btn-black w-full mb-16">*/}
          {/*    Thanh toán lại*/}
          {/*  </button>*/}
          {/*  <p className="mt-0 font-semibold">Hủy</p>*/}
          {/*</div>*/}
          {/*End Checkout Fail*/}
        </div>
      </div>
    </IonContent>
  );
});

export default CompleteContent;
