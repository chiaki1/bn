import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CHECKOUT_COMPLETE_CPT_CFG: ExtensionConfig[] = [
  {
    uiId: 'COMPLETE_HEADER',
    component: { lazy: React.lazy(() => import('./CompleteHeader')) },
    priority: 100,
  },
  {
    uiId: 'COMPLETE_CONTENT',
    component: { lazy: React.lazy(() => import('./CompleteContent')) },
    priority: 100,
  },
];
