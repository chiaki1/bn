import {
  IonContent,
  IonFooter,
  IonModal,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import { withShippingMethodActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withShippingMethodActions';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useState } from 'react';

import iconcheckwhite from '../../../assets/images/icons/icon-check-white.svg';

const PopupShipping: React.FC<{
  dataShippingMethod: any;
  close: () => void;
  showPopupShippingMethod: boolean;
}> = combineHOC(withShippingMethodActions)((props) => {
  const [selectShippingMethod, setSelectShippingMethod] = useState({
    carrier_code:
      props.dataShippingMethod?.selectedShippingMethod?.carrier_code || '',
    method_code:
      props.dataShippingMethod?.selectedShippingMethod?.method_code || '',
  });

  const saveSelectedShippingMethod = useCallback(() => {
    if (
      typeof props.actions?.setShippingMethodAction === 'function' &&
      selectShippingMethod?.carrier_code &&
      selectShippingMethod?.method_code
    ) {
      props.actions.setShippingMethodAction(
        selectShippingMethod?.carrier_code,
        selectShippingMethod?.method_code
      );
      props?.close();
    }
  }, [selectShippingMethod]);

  return (
    <IonModal
      isOpen={props?.showPopupShippingMethod}
      className="modal-wrapper-full"
    >
      <IonPage className="page-header-no-mrb">
        <UiExtension
          uiId="HEADER_ACCOUNT"
          goBack={() => props.close()}
          title="Phương thức vận chuyển"
          showIcon={false}
        />
        <IonContent>
          <div className="shipping-list">
            <div className="container">
              <h3 className="text-lg adrs-deli">
                Lựa chọn phương thức vận chuyển
              </h3>
              {props.dataShippingMethod?.availableShippingMethod.map(
                (method: any) => (
                  <div className="wrap-address" key={method.carrier_code}>
                    <div className="wrap-address-item flex ">
                      <div className="address-checkbox">
                        <label className="label-input">
                          <input
                            name="amshopby"
                            type="radio"
                            id={method.carrier_code + '_' + method.method_code}
                            value={
                              method.carrier_code + '_' + method.method_code
                            }
                            onChange={() =>
                              setSelectShippingMethod({
                                carrier_code: method.carrier_code,
                                method_code: method.method_code,
                              })
                            }
                            checked={
                              method?.carrier_code ===
                                selectShippingMethod?.carrier_code &&
                              method?.method_code ===
                                selectShippingMethod?.method_code
                            }
                          />
                          <span className="label">
                            {/*<h4 className="item__info-name">*/}
                            {/*  {method.method_title}*/}
                            {/*</h4>*/}
                            <div className="list-address">
                              <div className="list-address_item">
                                <div className="list-address_item__info">
                                  <h3 className="item__info-name">
                                    {method.method_title} |
                                    <span className="text-main">
                                      <UiExtension
                                        uiId="CURRENCY"
                                        price={method?.price_incl_tax?.value}
                                      />
                                    </span>
                                  </h3>
                                  <p className={'item__info-address'}>
                                    {/*Giao hàng sau 2-3 ngày làm việc, không tính thứ 7,*/}
                                    {/*chủ nhật*/}
                                  </p>
                                  <p
                                    className={'item__info-address text-small'}
                                  >
                                    {/*Dự kiến giao hàng vào 20/11/2021*/}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                )
              )}
            </div>
          </div>
        </IonContent>
        <IonFooter class="footer-shipping footer-cart">
          <IonToolbar className="ion-no-padding">
            <div className="container">
              <button
                className="btn btn-black btn-fs btn-success-address"
                onClick={() => {
                  saveSelectedShippingMethod();
                }}
              >
                <img src={iconcheckwhite} alt="" className="pd-lft-rght" />
                Đồng ý
              </button>
            </div>
          </IonToolbar>
        </IonFooter>
      </IonPage>
    </IonModal>
  );
});

export default PopupShipping;
