import {
  IonContent,
  IonFooter,
  IonModal,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { withPaymentMethodActions } from '@vjcspy/r/build/modules/checkout/hoc/cart/withPaymentMethodActions';
import { withPaymentMethodData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withPaymentMethodData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useCallback, useState } from 'react';

import iconcheckwhite from '../../../assets/images/icons/icon-check-white.svg';

const PopupPayment: React.FC<{
  selectedPaymentCode: any;
  close: () => void;
  showPopupPayment: boolean;
}> = combineHOC(
  withPaymentMethodData,
  withPaymentMethodActions,
  withCheckoutCartData
)((props) => {
  const [selectPayment, setSelectPayment] = useState(
    props?.selectedPaymentCode || ''
  );
  const saveSelectedPayment = useCallback((paymentMethod) => {
    if (
      typeof props.actions?.setPaymentMethodAction === 'function' &&
      paymentMethod
    ) {
      props.actions.setPaymentMethodAction(paymentMethod);
      props?.close();
    }
  }, []);
  return (
    <IonModal isOpen={props?.showPopupPayment} className="modal-wrapper-full">
      <IonPage className="page-header-no-mrb">
        <UiExtension
          uiId="HEADER_ACCOUNT"
          goBack={() => props.close()}
          title="Phương thức thanh toán"
          showIcon={false}
        />
        <IonContent>
          <div className="shipping-list">
            <div className="container">
              <div className="wrap-address">
                {props?.state?.availablePaymentMethods &&
                  props?.state?.availablePaymentMethods.map((item: any) => (
                    <div className="wrap-address-item flex " key={item?.code}>
                      <div className="address-checkbox">
                        <label className="label-input">
                          <input
                            name="amshopby"
                            type="radio"
                            id={item?.code}
                            value={item?.code}
                            onChange={() => {
                              setSelectPayment(item?.code);
                            }}
                            checked={selectPayment === item?.code}
                          />
                          <span className="label">
                            <h4 className="item__info-name">{item?.title}</h4>
                          </span>
                        </label>
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </IonContent>
        <IonFooter class="footer-shipping footer-cart">
          <IonToolbar className="ion-no-padding">
            <div className="container">
              <button
                className="btn btn-black btn-fs btn-success-address"
                onClick={() => {
                  saveSelectedPayment(selectPayment);
                }}
              >
                <img src={iconcheckwhite} alt="" className="pd-lft-rght" />
                Đồng ý
              </button>
            </div>
          </IonToolbar>
        </IonFooter>
      </IonPage>
    </IonModal>
  );
});

export default PopupPayment;
