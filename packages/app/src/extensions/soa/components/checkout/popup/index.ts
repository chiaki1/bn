import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_POPUP_CHECKOUT_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CHECKOUT_POPUP_PAYMENT',
    component: { lazy: React.lazy(() => import('./PopupPayment')) },
  },
  {
    uiId: 'CHECKOUT_SHIPPING_METHOD_PAYMENT',
    component: { lazy: React.lazy(() => import('./PopupShipping')) },
  },
];
