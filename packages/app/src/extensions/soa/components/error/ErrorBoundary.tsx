import { IonContent } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import React, { Component } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconSprites from '../../assets/images/icon-sprites.svg';

export default class ErrorBoundary extends Component {
  // // Initializing the state
  state = {
    error: false,
  };

  componentDidCatch(error: any) {
    // Changing the state to true
    // if some error occurs
    this.setState({
      error: true,
    });
  }

  render() {
    if (this.state.error) {
      // You can render any custom fallback UI
      return (
        <>
          <IonContent>
            <div className="cart__items text-center pt-40">
              <div className="container">
                <svg className="fill-text-gray" width="100" height="92">
                  <use xlinkHref={iconSprites + '#icon-cart-empty'} />
                </svg>
                <h3 className="mb-20 text-lg-semi">Vui lòng thử lại.</h3>
                <div
                  className="btn-main btn-fs inline-flex items-center ion-justify-content-center btn-pd-xs max-w-160"
                  onClick={() => {
                    RouterSingleton.push(AppRoute.HOME);
                    window?.location?.reload();
                  }}
                >
                  Quay lại mua hàng
                </div>
              </div>
            </div>
          </IonContent>
        </>
      );
    }

    // @ts-ignore
    return this.props.children;
  }
}
