import { IonHeader, IonToolbar } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const HeaderOnlyText: React.FC<{
  canBack: boolean;
  title: string;
}> = combineHOC()((props) => {
  return (
    <IonHeader
      className="page-header ion-no-border header-account"
      translucent={false}
    >
      <div className="header-content text-center">
        <IonToolbar color="nobg">
          <div className="header-container flex-center ">
            {props?.canBack && (
              <button
                className="btn-link-white btn-back"
                onClick={() => {
                  RouterSingleton.back();
                }}
              >
                <i className="fal fa-chevron-left" />
              </button>
            )}

            <h1
              className="account-pages-title"
              style={{ width: props?.canBack ? undefined : '100%' }}
            >
              {props?.title}
            </h1>
            {/*<button className="btn-link-white btn-info">*/}
            {/*  <i className="fal fa-info-circle" />*/}
            {/*</button>*/}
          </div>
        </IonToolbar>
      </div>
    </IonHeader>
  );
});

export default HeaderOnlyText;
