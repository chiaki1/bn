import { IonHeader, IonIcon, IonToolbar } from '@ionic/react';
import { withSearchBarContainer } from '@vjcspy/r/build/modules/catalog/hoc/category/withSearchBarContainer';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import { search } from 'ionicons/icons';
import React, { useEffect, useState } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconBack from '../../assets/images/icons/icon-arrow-left-white.svg';
import iconCart from '../../assets/images/icons/icon-cart.svg';
import iconNoti from '../../assets/images/icons/icon-noti.svg';
import logo from '../../assets/images/logo.svg';

const HeaderLogoInline: React.FC = combineHOC(
  withCheckoutCartData,
  withSearchBarContainer
)((props) => {
  return (
    <IonHeader className="page-header ion-no-padding header-logo-inline">
      <IonToolbar mode="ios" className="ion-no-padding">
        <div className="header-content text-center">
          <div className={'flex-center-center header-container'}>
            {RouterSingleton.pathname !== '/' + AppRoute.HOME &&
              props?.showBack !== false && (
                <button
                  className="btn-link btn-back"
                  onClick={() => {
                    RouterSingleton.back();
                  }}
                >
                  <img src={iconBack} />
                </button>
              )}

            <div className="block-search">
              <div
                className="logo"
                onClick={() => RouterSingleton.push('/' + AppRoute.HOME)}
              >
                <img className="max-h-18" src={logo} />
              </div>
            </div>
            <div className="header__action noti__wrapper btn-action-xs">
              {/*<button*/}
              {/*  className="p_relative btn-link"*/}
              {/*  onClick={() => {*/}
              {/*    RouterSingleton.push('/' + AppRoute.NOTIFICATION_PAGE);*/}
              {/*  }}*/}
              {/*>*/}
              {/*  <img src={iconNoti} />*/}
              {/*  /!*<span color="white" className="count-number text-xs">*!/*/}
              {/*  /!*  99*!/*/}
              {/*  /!*</span>*!/*/}
              {/*</button>*/}
            </div>
            <div
              className="header__action cart__wrapper"
              onClick={() => RouterSingleton.go('/' + AppRoute.CART)}
            >
              <button className="p_relative btn-link">
                <img src={iconCart} />
                {!!props.state?.cart?.items && (
                  <span color="white" className="count-number text-xs">
                    {props.state?.cart?.items?.length}
                  </span>
                )}
              </button>
            </div>
          </div>
        </div>
      </IonToolbar>
    </IonHeader>
  );
});

export default HeaderLogoInline;
