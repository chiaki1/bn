import { IonHeader, IonIcon, IonToolbar } from '@ionic/react';
import { withSearchBarContainer } from '@vjcspy/r/build/modules/catalog/hoc/category/withSearchBarContainer';
import { withCheckoutCartData } from '@vjcspy/r/build/modules/checkout/hoc/cart/withCheckoutCartData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import { search } from 'ionicons/icons';
import React, { useState } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconBack from '../../assets/images/icons/icon-arrow-left-white.svg';
import iconCart from '../../assets/images/icons/icon-cart.svg';
import logo from '../../assets/images/logo.svg';

const Header: React.FC = combineHOC(
  withCheckoutCartData,
  withSearchBarContainer
)((props) => {
  const [searchText, setSearchText] = useState(props.searchString ?? '');
  // useEffect(() => {
  //   if (typeof props.handleChange === 'function') {
  //     props.handleChange(searchText);
  //   }
  // }, [searchText]);
  return (
    <IonHeader className={'page-header ion-no-padding'}>
      <IonToolbar mode="ios" className="ion-no-padding">
        <div className="header-content text-center">
          <div
            className="logo"
            onClick={() => RouterSingleton.push('/' + AppRoute.HOME)}
          >
            <img className="mb-16" src={logo} />
          </div>
          <div className={'flex-center-center header-container'}>
            {RouterSingleton.pathname !== '/' + AppRoute.HOME &&
              props?.showBack !== false && (
                <button
                  className="btn-link btn-back"
                  onClick={() => {
                    RouterSingleton.back();
                  }}
                >
                  <img src={iconBack} />
                </button>
              )}

            <div className="block-search">
              {RouterSingleton.pathname != '/' + AppRoute.ACCOUNT &&
                RouterSingleton.pathname != '/' + AppRoute.ADDRESS_LIST &&
                props?.showSearch !== false && (
                  <div className="form-minisearch p_relative">
                    <label className="label flex-center">
                      <IonIcon
                        slot="start"
                        icon={search}
                        onClick={() => {
                          if (typeof props.handleChange === 'function') {
                            props.handleChange(searchText);
                          }
                          // RouterSingleton.push(`/${AppRoute.PRODUCTS}`);
                        }}
                      />
                    </label>
                    <input
                      className="input-search"
                      type="text"
                      placeholder="Tìm sản phẩm, thương hiệu..."
                      onChange={(e) => {
                        setSearchText(e.target.value!);
                      }}
                      onKeyDown={(e) => {
                        if (e.key === 'Enter') {
                          if (typeof props.handleChange === 'function') {
                            props.handleChange(searchText);
                          }
                        }
                      }}
                      onClick={(e) => {
                        if (
                          RouterSingleton.pathname !== `/${AppRoute.PRODUCTS}`
                        ) {
                          if (typeof props.handleChange === 'function') {
                            props.handleChange('');
                          }
                          RouterSingleton.push(`/${AppRoute.PRODUCTS}`);
                        }
                      }}
                    />
                  </div>
                )}
            </div>
            <div className="header__action noti__wrapper">
              {/*<button*/}
              {/*  className="p_relative btn-link"*/}
              {/*  onClick={() => {*/}
              {/*    RouterSingleton.push('/' + AppRoute.NOTIFICATION_PAGE);*/}
              {/*  }}*/}
              {/*>*/}
              {/*  <img src={iconNoti} />*/}
              {/*  /!*<span color="white" className="count-number text-xs">*!/*/}
              {/*  /!*  99*!/*/}
              {/*  /!*</span>*!/*/}
              {/*</button>*/}
            </div>
            <div
              className="header__action cart__wrapper"
              onClick={() => RouterSingleton.go('/' + AppRoute.CART)}
            >
              <button className="p_relative btn-link">
                <img src={iconCart} />
                {!!props.state?.cart?.items && (
                  <span color="white" className="count-number text-xs">
                    {props.state?.cart?.items?.length}
                  </span>
                )}
              </button>
            </div>
          </div>
        </div>
      </IonToolbar>
    </IonHeader>
  );
});

export default Header;
