import { IonHeader, IonIcon, IonToolbar } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const HeaderAccount: React.FC<{
  goBack: () => void;
  title: string;
  showIcon?: boolean;
}> = combineHOC()((props) => {
  return (
    <IonHeader
      className="page-header ion-no-border header-account"
      translucent={false}
    >
      <div className="header-content text-center">
        <IonToolbar color="nobg">
          <div className="header-container flex-center ">
            {props?.goBack && (
              <button
                className="btn-link-white btn-back"
                onClick={() => {
                  props.goBack();
                }}
              >
                <i className="fal fa-chevron-left" />
              </button>
            )}

            <h1 className="account-pages-title">{props?.title}</h1>

            <button className="btn-link-white btn-info">
              {props?.showIcon && <i className="fal fa-info-circle" />}
            </button>
          </div>
        </IonToolbar>
      </div>
    </IonHeader>
  );
});

export default HeaderAccount;
