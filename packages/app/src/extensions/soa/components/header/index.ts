import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_HEADER_COMMON_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'HEADER',
    component: { lazy: React.lazy(() => import('./Header')) },
  },
  {
    uiId: 'HEADER_LOGO_INLINE',
    component: { lazy: React.lazy(() => import('./HeaderLogoInline')) },
  },
  {
    uiId: 'HEADER_ACCOUNT',
    component: { lazy: React.lazy(() => import('./HeaderAccount')) },
  },
  {
    uiId: 'HEADER_ONLY_TEXT',
    component: { lazy: React.lazy(() => import('./HeaderOnlyText')) },
  },
];
