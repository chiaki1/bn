import { IonBackdrop } from '@ionic/react';
import { Registry } from '@vjcspy/chitility';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useEffect, useState } from 'react';

import { withResolveProductFilterCustom } from '../../../../modules/catalog/hoc/router/withResolveProductFilterCustom';
import { AppRoute } from '../../../../router/Route';
import iconSprites from '../../assets/images/icon-sprites.svg';
import { withSoaPopupHomePageData } from '../../hoc/home-page/withSoaPopupHomePageData';

const INIT_POPUP_HOME = 'INIT_POPUP_HOME';
const HomePopup: React.FC<{
  dataBannersAdd: {};
}> = combineHOC(
  withSoaPopupHomePageData,
  withResolveProductFilterCustom
)((props) => {
  const [showPopup, setShowPopup] = useState(false);

  useEffect(() => {
    if (props?.state?.popupsHomePage?.data_filters) {
      setShowPopup(true);
    }
  }, [props?.state?.popupsHomePage]);

  useEffect(() => {
    const isInited = Registry.getInstance().registry(INIT_POPUP_HOME);
    if (isInited !== true) {
      Registry.getInstance().register(INIT_POPUP_HOME, true);
      if (props?.actions?.getPopupHomePage) {
        props?.actions?.getPopupHomePage();
      }
    }
  }, []);

  return (
    <>
      {showPopup && (
        <>
          <IonBackdrop tappable={true} visible={true} stopPropagation={true} />
          <div className="modalv2 modal-center modal-transparent">
            <div className="modalv2-content max-w-284">
              <span
                onClick={() => {
                  setShowPopup(false);
                }}
                className="inline-flex items-center ion-justify-content-center absolute -top-20 -right-20 bg-white cursor-pointer w-34 h-34 rounded-4 text-black"
              >
                <svg width="18" height="18">
                  <use xlinkHref={iconSprites + '#icon-close'} />
                </svg>
              </span>
              <div
                className="img-block rounded-13 overflow-hidden"
                onClick={() => {
                  if (
                    props?.state?.popupsHomePage?.data_filters &&
                    JSON.parse(props?.state?.popupsHomePage?.data_filters)
                      ?.filter
                  ) {
                    props?.actions?.setFilterToStore(
                      JSON.parse(props?.state?.popupsHomePage?.data_filters)
                        ?.filter
                    );
                    RouterSingleton.push('/' + AppRoute.PRODUCTS_LIST);
                  }
                }}
              >
                <img src={props?.state?.popupsHomePage?.image ?? ''} />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
});

export default HomePopup;
