import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_HOME_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'HOME_BANNER',
    component: { lazy: React.lazy(() => import('./Banner')) },
  },
  {
    uiId: 'HOME_TOP_SEARCH',
    component: { lazy: React.lazy(() => import('./TopSearch')) },
  },
  {
    uiId: 'HOME_BRAND',
    component: { lazy: React.lazy(() => import('./HomeBrand')) },
  },
  {
    uiId: 'HOME_HOT_SALE',
    component: { lazy: React.lazy(() => import('./HotSale')) },
  },
  {
    uiId: 'HOME_SUGGESTION',
    component: { lazy: React.lazy(() => import('./HomeSuggestion')) },
  },
  {
    uiId: 'HOME_POPUP',
    component: { lazy: React.lazy(() => import('./HomePopup')) },
  },
];
