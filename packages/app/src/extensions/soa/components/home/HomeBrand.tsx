import { IonicSlides } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';
import { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';

import { withResolveProductFilterCustom } from '../../../../modules/catalog/hoc/router/withResolveProductFilterCustom';
import { AppRoute } from '../../../../router/Route';

const HomeBrand: React.FC<{
  dataBannersAdd: {};
}> = combineHOC(withResolveProductFilterCustom)((props) => {
  if (
    !props?.dataBannersAdd?.banners ||
    (props?.dataBannersAdd?.banners &&
      Array.isArray(props?.dataBannersAdd?.banners) &&
      props?.dataBannersAdd?.banners.length === 0)
  ) {
    return null;
  }

  return (
    <div className={'home__brands mgb-block'}>
      <h2 className={'home__title2 text-main text-center'}>
        {props?.dataBannersAdd?.title}
      </h2>
      <Swiper
        modules={[Autoplay, Keyboard, Pagination, Scrollbar, Zoom, IonicSlides]}
        autoplay={{
          delay: 3000,
          waitForTransition: true,
          pauseOnMouseEnter: false,
        }}
        keyboard={true}
        pagination={true}
        scrollbar={false}
        zoom={true}
        className="pagination-active-main"
      >
        {props?.dataBannersAdd?.banners
          .slice()
          .sort((a: any, b: any) => a.position - b.position)
          .map((item: any) => (
            <SwiperSlide key={`campain-${item?.banner_id}`}>
              <div
                className="brand-item img-block"
                onClick={() => {
                  if (JSON.parse(item?.data_filters)?.filter) {
                    props?.actions?.setFilterToStore(
                      JSON.parse(item?.data_filters)?.filter
                    );
                    RouterSingleton.push('/' + AppRoute.PRODUCTS_LIST);
                  }
                }}
              >
                <img src={item?.banner_image} alt={item?.name} />
              </div>
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
});

export default HomeBrand;
