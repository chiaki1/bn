import { IonImg, IonText } from '@ionic/react';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const ProductItem: React.FC = combineHOC()((props) => {
  return (
    <div className="product-item">
      <div className="product-photo">
        <IonImg class="center" src="https://via.placeholder.com/165x210" />
        <label className="label-product text-small">-45%</label>
      </div>

      <div className="product-item-detail pd-item-detail">
        <h3 className="brand-product text-small">MANGO</h3>
        <p className="product-name text-xs">Áo khoác lót lông</p>
        <div className="price-wrap">
          <p className="old-price text-xs">580.000đ</p>
          <IonText color="primary" className="special-price">
            500.000đ
          </IonText>
        </div>
      </div>
    </div>
  );
});

export default ProductItem;
