import { IonButton, IonCol, IonRow } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withResolveProductFilterCustom } from '../../../../modules/catalog/hoc/router/withResolveProductFilterCustom';
import { AppRoute } from '../../../../router/Route';
import { withListProductHotSaleData } from '../../hoc/home-page/withListProductHotSaleData';

const HotSale: React.FC<{ dataBlockCampaignImageListing: {} }> = combineHOC(
  withListProductHotSaleData,
  withResolveProductFilterCustom
)((props) => {
  return (
    <div className="home__hotsales">
      <div className="sales-item img-block">
        <img src={props.dataBlockCampaignImageListing?.banner_image} />
      </div>
      <div className="product-sales">
        <IonRow className="product-sales-list">
          {props?.state?.listProductHotSaleData?.products?.items?.map(
            (item, index) => (
              <IonCol size="4" key={item?.id}>
                <UiExtension uiId="PRODUCT_LIST_ITEM" product={item} />
              </IonCol>
            )
          )}
        </IonRow>
        <div className="text-center">
          <IonButton
            size="small"
            color="secondary"
            className="btn-view-all"
            onClick={() => {
              if (
                JSON.parse(props?.dataBlockCampaignImageListing?.data_filters)
                  ?.filter
              ) {
                props?.actions?.setFilterToStore(
                  JSON.parse(props?.dataBlockCampaignImageListing?.data_filters)
                    ?.filter
                );
                RouterSingleton.push(`/${AppRoute.PRODUCTS_LIST}`);
              }
            }}
          >
            Xem tất cả
          </IonButton>
        </div>
      </div>
    </div>
  );
});

export default HotSale;
