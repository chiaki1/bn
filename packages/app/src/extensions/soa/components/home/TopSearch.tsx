import { IonCol, IonImg, IonLabel, IonRow } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

import { withSoaHomeCategoryListData } from '../../hoc/home-page/withSoaHomeCategoryListData';

const TopSearch = combineHOC(withSoaHomeCategoryListData)((props) => {
  if (
    !props.topSearchData ||
    !props.topSearchData?.category_list ||
    (props.topSearchData?.category_list &&
      Array.isArray(props.topSearchData?.category_list) &&
      props.topSearchData?.category_list.length === 0)
  ) {
    return null;
  }
  const categoryList = useMemo(() => {
    if (!props.topSearchData?.category_list) {
      return null;
    }
    const arr = props.topSearchData.category_list;
    let leng = arr.length;
    let n = 9;
    if (leng < n) {
      return props.topSearchData.category_list;
    } else {
      const result: any = [];
      const taken = new Array(n);
      while (n--) {
        const x = Math.floor(Math.random() * leng);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --leng in taken ? taken[leng] : leng;
      }
      return result;
    }
  }, props.topSearchData.category_list);
  return (
    <div className="home__topsearch">
      <h2 className="home__title text-white text-center">
        {props.topSearchData?.title}
      </h2>
      <IonRow className="category-top">
        {categoryList.map((item: any) => (
          <IonCol
            size={'4'}
            className="text-center category-item"
            key={item?.name}
            onClick={() => {
              RouterSingleton.push(`/${item.url_path}.html`);
            }}
          >
            <IonImg
              className="category-icon mr-auto"
              class="center"
              src={item?.icon}
            />
            <IonLabel className="category-name">
              {item?.name_on_app?.toUpperCase() ?? item?.name?.toUpperCase()}
            </IonLabel>
          </IonCol>
        ))}
      </IonRow>
    </div>
  );
});

export default TopSearch;
