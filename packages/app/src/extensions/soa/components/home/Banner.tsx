import { IonicSlides } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';
import { Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react.js';

import { withResolveProductFilterCustom } from '../../../../modules/catalog/hoc/router/withResolveProductFilterCustom';
import { AppRoute } from '../../../../router/Route';
import { withSoaHomeTopBannerData } from '../../hoc/home-page/withSoaHomeTopBannerData';

const HomeBanner = combineHOC(
  withSoaHomeTopBannerData,
  withResolveProductFilterCustom
)(
  React.memo((props) => {
    if (
      !props.topBannerData ||
      !props.topBannerData?.banners ||
      (props.topBannerData?.banners &&
        Array.isArray(props.topBannerData?.banners) &&
        props.topBannerData?.banners.length === 0)
    ) {
      return null;
    }
    return (
      <div className={'home__brands mgb-block'}>
        <Swiper
          modules={[
            Autoplay,
            Keyboard,
            Pagination,
            Scrollbar,
            Zoom,
            IonicSlides,
          ]}
          autoplay={{
            delay: 3000,
            waitForTransition: true,
            pauseOnMouseEnter: false,
          }}
          keyboard={true}
          pagination={true}
          scrollbar={false}
          zoom={true}
          className="pagination-active-main"
        >
          {props.topBannerData?.banners.map((image: any) => (
            <SwiperSlide key={image?.banner_id}>
              <div
                className="brand-item img-block"
                onClick={() => {
                  if (JSON.parse(image?.data_filters)?.filter) {
                    props?.actions?.setFilterToStore(
                      JSON.parse(image?.data_filters)?.filter
                    );
                    RouterSingleton.push('/' + AppRoute.PRODUCTS_LIST);
                  }
                }}
              >
                <img src={image?.banner_image} alt={image?.name} />
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    );
  })
);

export default HomeBanner;
