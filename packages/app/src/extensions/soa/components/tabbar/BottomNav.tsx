import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import React, { useCallback } from 'react';

export interface BottomNavActionData {
  label?: string;
  icon?: React.ReactNode;
  iconActive?: React.ReactNode;
  value: string;
  activeValues?: any[];
}

const BottomNav: React.FC<{
  nav: BottomNavActionData[];
  index: string;
  handleChange: (event: any, newValue: any) => void;
}> = (props) => {
  const isActive = useCallback(
    (value: any, activeValues?: any[]) => {
      if (Array.isArray(activeValues)) {
        return activeValues.includes(props.index);
      } else {
        return props.index === value;
      }
    },
    [props.index]
  );

  return (
    <BottomNavigation
      showLabels
      sx={{
        height: 70,
        '& .MuiBottomNavigationAction-label': {
          paddingTop: '5px',
          fontSize: '10px',
          width: 70,
        },
        '& .Mui-selected': {
          fontSize: '10px !important',
          fontWeight: 400,
          color: '#C12229',
        },
        '& .MuiBottomNavigationAction-iconOnly': {
          minHeight: 24,
        },
      }}
      value={props.index}
      onChange={props.handleChange}
    >
      {props.nav.map((n) => (
        <BottomNavigationAction
          key={n.value}
          label={n.label}
          value={n.value}
          icon={isActive(n.value, n.activeValues) ? n.iconActive : n.icon}
        />
      ))}
    </BottomNavigation>
  );
};

export default BottomNav;
