import { IonFooter, IonToolbar } from '@ionic/react';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import ShoppingCartRoundedIcon from '@mui/icons-material/ShoppingCartRounded';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import React, { useCallback, useMemo } from 'react';

import { useBottomNavBarContext } from '../../../../modules/ui/context';
import { AppRoute } from '../../../../router/Route';
import iconSprites from '../../assets/images/icon-sprites.svg';
import iconAccount from '../../assets/images/icons/icon-account.svg';
import iconAccountActive from '../../assets/images/icons/icon-account_active.svg';
import iconBrand from '../../assets/images/icons/icon-brand.svg';
import iconBrandActive from '../../assets/images/icons/icon-brand_active.svg';
import iconCart from '../../assets/images/icons/icon-cart.svg';
import iconCategory from '../../assets/images/icons/icon-category.svg';
import iconCategoryActive from '../../assets/images/icons/icon-category_active.svg';
import iconChat from '../../assets/images/icons/icon-chat.svg';
import iconChatActive from '../../assets/images/icons/icon-chat_active.svg';
import iconHome from '../../assets/images/icons/icon-home.svg';
import iconHomeActive from '../../assets/images/icons/icon-home_active.svg';
// @ts-ignore
import BottomNav from './BottomNav';

const BottomNavBarMui: React.FC<{
  hiddenWhenScroll: boolean;
}> = (props) => {
  const bottomNavBarContextValue = useBottomNavBarContext();

  const bottomNavData = useMemo(() => {
    return [
      {
        label: 'Trang chủ',
        icon: <img src={iconHome} alt="home" />,
        iconActive: <img src={iconHomeActive} alt="home" />,
        value: AppRoute.HOME,
      },
      {
        label: 'Danh mục',
        icon: <img src={iconCategory} alt="category" />,
        iconActive: <img src={iconCategoryActive} alt="category" />,
        value: AppRoute.CATEGORY_LIST,
        activeValues: [AppRoute.CATEGORY_LIST, AppRoute.PRODUCTS],
      },
      {
        label: 'Thương hiệu',
        icon: <img src={iconBrand} alt="brand" />,
        iconActive: <img src={iconBrandActive} alt="brand" />,
        value: AppRoute.BRAND,
      },
      {
        label: 'Giỏ hàng',
        icon: (
          <svg className="text-light" width="22" height="22">
            <use xlinkHref={iconSprites + '#cart-added'} />
          </svg>
        ),
        iconActive: (
          <svg className="text-main" width="22" height="22">
            <use xlinkHref={iconSprites + '#cart-added'} />
          </svg>
        ),
        value: AppRoute.CART,
      },
      {
        label: 'Cá nhân',
        icon: <img src={iconAccount} alt="account" />,
        iconActive: <img src={iconAccountActive} alt="account" />,
        value: AppRoute.ACCOUNT,
      },
    ];
  }, []);

  const handleChange = useCallback(
    (event: any, newValue) => {
      if (typeof bottomNavBarContextValue.setIndex == 'function') {
        bottomNavBarContextValue.setIndex(newValue);
      }

      RouterSingleton.push('/' + newValue);
    },
    [bottomNavBarContextValue.setIndex]
  );

  return (
    <IonFooter>
      <IonToolbar className="ion-no-padding">
        <BottomNav
          nav={bottomNavData}
          index={bottomNavBarContextValue.index}
          handleChange={handleChange}
        />
      </IonToolbar>
    </IonFooter>
  );
};

export default BottomNavBarMui;
