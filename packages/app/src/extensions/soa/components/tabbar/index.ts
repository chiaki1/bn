import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const NAV_BAR_CPT_CFG: ExtensionConfig[] = [
  {
    uiId: 'BOTTOM_NAV_BAR',
    uiTags: ['BOTTOM_NAV_BAR'],
    component: { lazy: React.lazy(() => import('./BottomNavBarMui')) },
    hoc: [],
    priority: () => 1,
  },
];
