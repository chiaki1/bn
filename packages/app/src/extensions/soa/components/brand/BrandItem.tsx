import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import find from 'lodash/find';
import React, { useCallback, useEffect, useState } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconArrorRight from '../../assets/images/icons/arrow-right.svg';
import { withSoaFollowBrandListActions } from '../../hoc/brand/withSoaFollowBrandListActions';
import { withSoaFollowBrandListData } from '../../hoc/brand/withSoaFollowBrandListData';

const BrandItem: React.FC<{ itemsData?: any; state: any }> = combineHOC(
  withSoaFollowBrandListData,
  withSoaFollowBrandListActions,
  withCustomer
)((props) => {
  const [isCheckFollow, setCheckFollow] = useState<boolean>(false);
  useEffect(() => {
    if (
      props.state?.dataFollowBrand &&
      Array.isArray(props.state?.dataFollowBrand) &&
      props.state?.dataFollowBrand.length > 0
    ) {
      const isExisted = props.state.dataFollowBrand.find(
        (item: any) => item?.brand_id === props.itemsData?.brand_id
      );

      if (isExisted) {
        setCheckFollow(true);
      } else {
        setCheckFollow(false);
      }
    } else {
      setCheckFollow(false);
    }
  }, [props.state?.dataFollowBrand]);

  const setFollowBrandAction = useCallback((item: any) => {
    if (!props.state?.customer) {
      // save after go to login
      RouterSingleton.redirect('/' + AppRoute.ACCOUNT, `/${AppRoute.BRAND}`);
    } else {
      if (typeof props.actions?.setFollowBrand === 'function') {
        props.actions.setFollowBrand(item);
      }
    }
  }, []);
  const setUnFollowBrandAction = useCallback((item: any) => {
    if (typeof props.actions?.setUnFollowBrand === 'function') {
      props.actions?.setUnFollowBrand(item);
    }
  }, []);

  return (
    <div className="brand__item flex-center flex-content-bw">
      <div className="banner-item-detail flex-center w100p">
        <div
          className="brand__thumbnail p_relative img-block"
          onClick={() => {
            if (props.itemsData?.brand_id) {
              RouterSingleton.push(
                `brand-details/${props.itemsData?.brand_id}`
              );
            }
          }}
        >
          <img className="img-cover" src={props.itemsData?.logo} />
        </div>
        <div className="brand-detail">
          <h3
            className="brand-name"
            onClick={() => {
              if (props.itemsData?.brand_id) {
                RouterSingleton.push(
                  `brand-details/${props.itemsData?.brand_id}`
                );
              }
            }}
          >
            {props.itemsData?.name}
          </h3>
          {!isCheckFollow ? (
            <button
              className="btn-main round btn-md"
              onClick={() => {
                setFollowBrandAction(props.itemsData?.brand_id);
              }}
            >
              Theo dõi
            </button>
          ) : (
            <button
              className="btn-main round btn-md btn-reset"
              onClick={() => {
                setUnFollowBrandAction(props.itemsData?.brand_id);
              }}
            >
              Đã theo dõi
            </button>
          )}
        </div>
      </div>
      <button
        className="btn-link btn__view"
        onClick={() => {
          if (props.itemsData?.brand_id) {
            RouterSingleton.push(`brand-details/${props.itemsData?.brand_id}`);
          }
        }}
      >
        <img src={iconArrorRight} />
      </button>
    </div>
  );
});

export default BrandItem;
