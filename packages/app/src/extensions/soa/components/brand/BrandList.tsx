import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withBrandListData } from '../../hoc/brand/withBrandListData';

const BrandList = combineHOC(withBrandListData)((props) => {
  if (!props.state?.brandListData?.brandList) {
    return null;
  }
  return (
    <div className="brand__list">
      <div className="container">
        {props.state?.brandListData?.brandList?.map((items: any) => (
          <UiExtension
            uiId="BRAND_ITEM"
            itemsData={items}
            key={items?.brand_id}
          />
        ))}
      </div>
    </div>
  );
});

export default BrandList;
