import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

import { SOA_BRAND_DETAIL_EXT_CFG } from './detail';

export const SOA_BRAND_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'BRAND_ITEM',
    component: { lazy: React.lazy(() => import('./BrandItem')) },
  },
  {
    uiId: 'BRAND_TOP',
    component: { lazy: React.lazy(() => import('./BrandTop')) },
  },
  {
    uiId: 'BRAND_LIST',
    component: { lazy: React.lazy(() => import('./BrandList')) },
  },
  {
    uiId: 'BRAND_LIST_CONTAINER',
    component: { lazy: React.lazy(() => import('./BrandListContainer')) },
  },
  ...SOA_BRAND_DETAIL_EXT_CFG,
];
