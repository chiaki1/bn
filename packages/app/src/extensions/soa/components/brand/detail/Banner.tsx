import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

import { withSoaBrandDetailData } from '../../../hoc/brand/withSoaBrandDetailData';

const BrandBanner: React.FC = combineHOC(withSoaBrandDetailData)((props) => {
  if (!props?.dataBrandDetail?.top_banner) {
    return null;
  }
  return (
    <div className="banner-brand img-block">
      <img
        src={props?.dataBrandDetail?.top_banner}
        alt={props?.dataBrandDetail?.name}
      />
    </div>
  );
});

export default BrandBanner;
