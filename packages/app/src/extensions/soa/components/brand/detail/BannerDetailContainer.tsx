import { IonContent } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';

import { withSoaBrandDetailContainer } from '../../../hoc/brand/withSoaBrandDetailContainer';

const BannerDetailContainer: React.FC = combineHOC(withSoaBrandDetailContainer)(
  (props) => {
    const { brandId } = useParams<{
      brandId: any;
    }>();
    const [activeTab, setActiveTab] = useState<string>('home');

    useEffect(() => {
      if (typeof props.actions?.getDataBrandDetail === 'function' && brandId) {
        props.actions?.getDataBrandDetail(brandId);
      }
    }, [brandId]);
    return (
      <IonContent>
        <div className="nav-tab mgb-block">
          <div className="container">
            <ul className="tab-items list-reset text-center">
              <li
                className={`tab-item ${activeTab === 'home' && 'active'}`}
                onClick={() => setActiveTab('home')}
              >
                Trang chủ
              </li>
              <li
                className={`tab-item ${activeTab === 'category' && 'active'}`}
                onClick={() => setActiveTab('category')}
              >
                Sản phẩm
              </li>
              <li
                className={`tab-item ${activeTab === 'new' && 'active'}`}
                onClick={() => setActiveTab('new')}
              >
                Hàng mới về
              </li>
            </ul>
          </div>
        </div>
        {activeTab === 'home' && <UiExtension uiId="BRAND_HOME_TAB" />}
        {activeTab === 'category' && <UiExtension uiId="BRAND_CATEGORY_TAB" />}
        {activeTab === 'new' && (
          <UiExtension uiId="BRAND_NEW_TAB" brandId={brandId} />
        )}
      </IonContent>
    );
  }
);

export default BannerDetailContainer;
