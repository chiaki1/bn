import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';

import { withSoaBrandDetailContainer } from '../../../hoc/brand/withSoaBrandDetailContainer';

const BrandHomeTab: React.FC = combineHOC(withSoaBrandDetailContainer)(
  (props) => {
    const { brandId } = useParams<{
      brandId: any;
    }>();

    useEffect(() => {
      if (typeof props.actions?.getDataBrandDetail === 'function' && brandId) {
        props.actions?.getDataBrandDetail(brandId);
      }
    }, [brandId]);
    return (
      <>
        <UiExtension uiId="BANNER" />
        <UiExtension uiId="BRAND_INFO" />
        <UiExtension uiId="ADDITIONAL_BOCK_BRAND_PAGE" />
        <UiExtension uiId="BRAND_PRODUCT_SUGGESTION" brandId={brandId} />
      </>
    );
  }
);

export default BrandHomeTab;
