import { IonCol, IonRow } from '@ionic/react';
import { withCustomer } from '@vjcspy/r/build/modules/account/hoc/withCustomer';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback, useEffect, useState } from 'react';

import { AppRoute } from '../../../../../router/Route';
import { withSoaBrandDetailData } from '../../../hoc/brand/withSoaBrandDetailData';
import { withSoaFollowBrandListActions } from '../../../hoc/brand/withSoaFollowBrandListActions';
import { withSoaFollowBrandListData } from '../../../hoc/brand/withSoaFollowBrandListData';

const BrandInfo: React.FC = combineHOC(
  withSoaBrandDetailData,
  withSoaFollowBrandListData,
  withSoaFollowBrandListActions,
  withCustomer
)((props) => {
  const [isCheckFollow, setCheckFollow] = useState<boolean>(false);
  useEffect(() => {
    if (
      props.state?.dataFollowBrand &&
      Array.isArray(props.state?.dataFollowBrand) &&
      props.state?.dataFollowBrand.length > 0
    ) {
      const isExisted = props.state.dataFollowBrand.find(
        (item: any) => item?.brand_id === props.dataBrandDetail?.brand_id
      );

      if (isExisted) {
        setCheckFollow(true);
      } else {
        setCheckFollow(false);
      }
    } else {
      setCheckFollow(false);
    }
  }, [props.state?.dataFollowBrand]);

  const setFollowBrandAction = useCallback((item: any) => {
    if (!props.state?.customer) {
      // save after go to login
      RouterSingleton.redirect(
        '/' + AppRoute.ACCOUNT,
        `/${AppRoute.BRAND_DETAIL}/` + props?.dataBrandDetail?.brand_id
      );
    } else {
      if (typeof props.actions?.setFollowBrand === 'function') {
        props.actions.setFollowBrand(item);
      }
    }
  }, []);
  const setUnFollowBrandAction = useCallback((item: any) => {
    if (typeof props.actions?.setUnFollowBrand === 'function') {
      props.actions?.setUnFollowBrand(item);
    }
  }, []);

  if (!props?.dataBrandDetail) {
    return null;
  }
  return (
    <div className="brand__info sticky top-0 bg-white z-10">
      <div className="container">
        <IonRow className="ion-align-items-center">
          <IonCol size="8" className="flex-center">
            <div className="brand__thumbnail img-block flex-shrink">
              <img
                className="img-cover"
                src={props?.dataBrandDetail?.logo}
                alt={props?.dataBrandDetail?.name}
              />
            </div>
            <div className="brand-info-detail">
              <h5 className="brand-name">{props?.dataBrandDetail?.name}</h5>
              <p className="sum-item text-small flex-center no-mr">
                <span className="count text-lg">
                  {props?.dataBrandDetail?.follow_number}
                </span>
                <span className="sum-item-name">Đang theo dõi</span>
              </p>
            </div>
          </IonCol>
          <IonCol size="4" className="no-padd">
            <div className="brand-info-right text-right">
              {!isCheckFollow ? (
                <button
                  className="btn-main round btn-follow btn-follow-brand"
                  onClick={() => {
                    setFollowBrandAction(props.dataBrandDetail?.brand_id);
                  }}
                >
                  Theo dõi
                </button>
              ) : (
                <button
                  className="btn-main round btn-reset btn-follow-brand"
                  onClick={() => {
                    setUnFollowBrandAction(props.dataBrandDetail?.brand_id);
                  }}
                >
                  Đã theo dõi
                </button>
              )}
              <p className="sum-item text-small flex-center ion-justify-content-end mgb-0 mrt-5">
                <span className="count text-lg">
                  {props?.dataBrandDetail?.total_product}
                </span>
                <span className="sum-item-name">Sản phẩm</span>
              </p>
            </div>
          </IonCol>
        </IonRow>
      </div>
    </div>
  );
});

export default BrandInfo;
