import { IonCol, IonRow } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withSoaBrandNewProductData } from '../../../hoc/brand/withSoaBrandNewProductData';

const BrandNewTab: React.FC = combineHOC(withSoaBrandNewProductData)(
  (props) => {
    if (!props?.state?.dataNewsProductBrand) {
      return null;
    }
    return (
      <div className="home__suggestion">
        <div className="container">
          <div className="flex ion-justify-content-between block__title">
            <h2 className="block__headline text-main">Hàng mới về</h2>
          </div>
          <IonRow className="mr-row">
            {props?.state?.dataNewsProductBrand.map((item: any) => (
              <IonCol size="6" key={item?.id}>
                <UiExtension uiId="PRODUCT_LIST_ITEM" product={item} />
              </IonCol>
            ))}
          </IonRow>
        </div>
      </div>
    );
  }
);

export default BrandNewTab;
