import { IonCol, IonRow } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect, useState } from 'react';

import { useIsScrollBottom } from '../../../../../modules/ui/hook/useIsScrollBottom';
import iconSpinner from '../../../assets/images/icons/icon-spinner.svg';
import { withSoaBrandSuggestionData } from '../../../hoc/brand/withSoaBrandSuggestionData';

const BrandProductSuggestion: React.FC = combineHOC(withSoaBrandSuggestionData)(
  (props) => {
    const { isBottom } = useIsScrollBottom();

    useEffect(() => {
      if (
        typeof props?.actions?.debounceLoadMoreDataSuggest === 'function' &&
        isBottom &&
        props?.state?.pageInfo?.current_page <
          props?.state?.pageInfo?.total_pages
      ) {
        props?.actions?.debounceLoadMoreDataSuggest(
          props?.state?.pageInfo?.current_page + 1
        );
      }
    }, [isBottom]);

    useEffect(() => {
      if (typeof props.actions?.getProductSuggest === 'function') {
        props.actions?.getProductSuggest(1);
      }
    }, []);

    if (
      !props?.state?.dataSuggestionProduct ||
      props?.state?.dataSuggestionProduct?.length === 0
    ) {
      return null;
    }
    return (
      <div className="home__suggestion">
        <div className="container">
          <div className="flex ion-justify-content-between block__title">
            <h2 className="block__headline text-main pdt-indent-md">
              GỢI Ý CHO BẠN
            </h2>
          </div>
          <IonRow className="mr-row">
            {props?.state?.dataSuggestionProduct.map((item: any) => (
              <IonCol size="6" key={item?.id}>
                <UiExtension uiId="PRODUCT_LIST_ITEM" product={item} />
              </IonCol>
            ))}
          </IonRow>
          {props?.state?.pageInfo?.current_page ==
            props?.state?.pageInfo?.total_pages && (
            <div className="myOrder-seen__List text-center pd-vertical">
              Bạn đã xem hết danh sách
            </div>
          )}
          {props?.state?.pageInfo?.current_page <
            props?.state?.pageInfo?.total_pages && (
            <div className="text-center">
              <img src={iconSpinner} alt="" />
            </div>
          )}
        </div>
      </div>
    );
  }
);

export default BrandProductSuggestion;
