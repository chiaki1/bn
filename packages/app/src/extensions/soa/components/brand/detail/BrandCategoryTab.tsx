import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';
import { useParams } from 'react-router';

import { withSoaBrandDetailCategoryData } from '../../../hoc/brand/withSoaBrandDetailCategoryData';

const BrandCategoryTab: React.FC = combineHOC(withSoaBrandDetailCategoryData)(
  (props) => {
    const { brandId } = useParams<{
      brandId: any;
    }>();

    if (
      !props?.dataBrandDetailCategory ||
      props?.dataBrandDetailCategory?.length === 0
    ) {
      return null;
    }
    return (
      <UiExtension
        uiId="CATEGORY_TREE"
        dataCategoryTree={props?.dataBrandDetailCategory}
        brandId={brandId || null}
      />
    );
  }
);

export default BrandCategoryTab;
