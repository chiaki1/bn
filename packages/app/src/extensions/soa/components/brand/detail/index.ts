import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_BRAND_DETAIL_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'BANNER',
    component: { lazy: React.lazy(() => import('./Banner')) },
  },
  {
    uiId: 'BRAND_INFO',
    component: { lazy: React.lazy(() => import('./BrandInfo')) },
  },
  {
    uiId: 'BRAND_DETAIL_CONTAINER',
    component: { lazy: React.lazy(() => import('./BannerDetailContainer')) },
  },
  {
    uiId: 'BRAND_PRODUCT_SUGGESTION',
    component: { lazy: React.lazy(() => import('./BrandProductSuggestion')) },
  },
  {
    uiId: 'BRAND_HOME_TAB',
    component: { lazy: React.lazy(() => import('./BrandHomeTab')) },
  },
  {
    uiId: 'BRAND_CATEGORY_TAB',
    component: { lazy: React.lazy(() => import('./BrandCategoryTab')) },
  },
  {
    uiId: 'BRAND_NEW_TAB',
    component: { lazy: React.lazy(() => import('./BrandNewTab')) },
  },
];
