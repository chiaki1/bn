import { IonContent } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const BrandListContainer: React.FC = combineHOC()((props) => {
  return (
    <IonContent>
      <UiExtension uiId="BRAND_TOP" />
      <UiExtension uiId="BRAND_LIST" />
    </IonContent>
  );
});

export default BrandListContainer;
