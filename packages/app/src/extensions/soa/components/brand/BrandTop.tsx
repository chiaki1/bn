import { combineHOC } from '@vjcspy/ui-extension';
import React from 'react';

const BrandTop: React.FC = combineHOC()((props) => {
  return (
    <div className="brand__top">
      <div className="container">
        <div className="flex ion-justify-content-between block__title block__bb">
          <h2 className="block__headline text-main">THƯƠNG HIỆU NỔI BẬT</h2>
          {/*<button className="btn-link">Xem thêm</button>*/}
        </div>
      </div>
    </div>
  );
});

export default BrandTop;
