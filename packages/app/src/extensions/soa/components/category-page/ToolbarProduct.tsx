import { IonCol, IonRow } from '@ionic/react';
import { withProductsTotalsData } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsTotalsData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const ToolbarProduct: React.FC<{ categoryChildren: any }> = combineHOC(
  withProductsTotalsData
)((props) => {
  return (
    <div className="toolbar-product">
      <div className="container">
        <IonRow className="mr-row ion-align-items-center">
          <IonCol size="4">
            <div className="toolbar-product-item">
              <UiExtension
                uiId="CATEGORY_CHILDREN"
                categoryChildren={props?.categoryChildren}
              />
            </div>
          </IonCol>
          <IonCol size="4">
            {/*<img className="icon-sorter" src={iconSorter} />*/}
            <div className="toolbar-product-item">
              <UiExtension
                uiId="CATEGORY_PRODUCTS_SORTER"
                totalCount={props?.productTotals}
                rangePriceFilters={props?.rangePriceFilters}
              />
            </div>
          </IonCol>
          <IonCol size="4" className="p_static col-filter">
            <div className="filter-products modal_overlay toolbar-product-item">
              <UiExtension
                uiId="CATEGORY_AGGREGATIONS"
                totalCount={props?.productTotals}
                rangePriceFilters={props?.rangePriceFilters}
              />
            </div>
          </IonCol>
        </IonRow>
      </div>
    </div>
  );
});

export default ToolbarProduct;
