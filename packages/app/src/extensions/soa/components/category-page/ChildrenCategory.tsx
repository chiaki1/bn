import { useIonActionSheet } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC } from '@vjcspy/ui-extension';
import size from 'lodash/size';
import React, { useMemo } from 'react';

import { AppRoute } from '../../../../router/Route';
import iconArrow from '../../assets/images/icons/icon-arrow-select.svg';

const ChildrenCategory: React.FC<{ categoryChildren: any }> = combineHOC()(
  (props) => {
    const [present] = useIonActionSheet();
    const listChildrenCategory = useMemo(() => {
      const buttons: any = [];
      if (
        props.categoryChildren &&
        Array.isArray(props.categoryChildren) &&
        size(props.categoryChildren) > 0
      ) {
        props.categoryChildren.map((item: any) => {
          buttons.push({
            text: item?.name_on_app ?? item?.name,
            handler: () => {
              RouterSingleton.push('/' + item?.url_path + '.html');
            },
          });
        });
      }
      buttons.push({ text: 'Hủy', role: 'cancel' });
      return buttons;
    }, [props.categoryChildren]);

    if (
      !props.categoryChildren ||
      (Array.isArray(props.categoryChildren) &&
        size(props.categoryChildren) === 0) ||
      RouterSingleton.pathname.includes(AppRoute.PRODUCTS)
    ) {
      return (
        <div className="category-list-item flex-center-center ">{'...'}</div>
      );
    }

    return (
      <>
        {/*<IonCol size="4">*/}
        <div
          className="category-list-item flex-center-center "
          onClick={() =>
            present({
              buttons: listChildrenCategory,
              header: 'Phân loại',
            })
          }
        >
          {/*<img className="icon-sorter" src={iconSorter} />*/}
          Phân loại
          <img src={iconArrow} alt="" />
        </div>
        {/*This popup click b div sapxep add class active on opup*/}
        {/*</IonCol>*/}
      </>
    );
  }
);

export default ChildrenCategory;
