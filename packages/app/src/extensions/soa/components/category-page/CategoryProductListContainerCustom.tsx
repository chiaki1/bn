import { IonContent } from '@ionic/react';
import { withProductsFilterActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsFilterActions';
import { withProductsTotalsData } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsTotalsData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { withAppProductsContainerCustom } from '../../../../modules/catalog/hoc/products/withAppProductsContainerCustom';

const CategoryProductListContainerCustom: React.FC = combineHOC(
  withAppProductsContainerCustom,
  withProductsTotalsData,
  withProductsFilterActions
)((props) => {
  useEffect(() => {
    if (typeof props?.actions?.resetFilterInfo === 'function') {
      props?.actions?.resetFilterInfo();
    }
    if (typeof props?.actions?.removeSearchStringAction === 'function') {
      props?.actions?.removeSearchStringAction();
    }
  }, []);
  return (
    <IonContent>
      <UiExtension
        uiId="TOOLBAR_PRODUCTS"
        rangePriceFilters={props.state?.rangePriceFilters}
      />
      <div className="brand__top">
        <div className="container">
          <div className="flex ion-justify-content-between">
            <p className="block__headline ">Số lượng</p>
            <p className="block__headline ">
              {props?.productTotals || 0} sản phẩm
            </p>
          </div>
        </div>
      </div>
      <UiExtension
        uiId="PRODUCT_WRAPPERS"
        isLoading={props.state?.isLoading}
        products={props.state?.products}
        handleLoadMorePage={props.actions?.handleLoadMorePage}
        isDone={props.state?.isDone}
        pageFilterInfo={props?.state?.pageFilterInfo}
        totalPage={props?.state?.totalPage}
      />
    </IonContent>
  );
});

export default CategoryProductListContainerCustom;
