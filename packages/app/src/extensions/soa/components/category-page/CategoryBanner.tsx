import { withCategoryData } from '@vjcspy/r/build/modules/catalog/hoc/products/withCategoryData';
import { withProductsFilterActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsFilterActions';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

const CategoryBanner: React.FC = combineHOC(
  withCategoryData,
  withProductsFilterActions
)((props) => {
  useEffect(() => {
    if (typeof props?.actions?.removeSearchStringAction === 'function') {
      props?.actions?.removeSearchStringAction();
    }
  }, []);

  if (!props?.category?.app_background_image) {
    return null;
  }
  return (
    <div className="banner-brand img-block">
      <img
        src={props?.category?.app_background_image}
        alt={props?.category?.name}
      />
    </div>
  );
});

export default CategoryBanner;
