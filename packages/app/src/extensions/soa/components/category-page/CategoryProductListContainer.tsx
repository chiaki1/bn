import { IonContent } from '@ionic/react';
import { withProductsTotalsData } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsTotalsData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { withAppProductsContainer } from '../../../../modules/catalog/hoc/products/withAppProductsContainer';
import { withResolveProductFilterFromUrl } from '../../../../modules/catalog/hoc/router/withResolveProductFilterFromUrl';
import { AppRoute } from '../../../../router/Route';

const CategoryProductListContainer: React.FC = combineHOC(
  withResolveProductFilterFromUrl,
  withAppProductsContainer,
  withProductsTotalsData
)((props) => {
  useEffect(() => {
    if (typeof props?.actions?.resetFilterInfo === 'function') {
      props?.actions?.resetFilterInfo();
    }
  }, []);
  if (
    RouterSingleton.pathname === `/${AppRoute.PRODUCTS}` &&
    !props?.state?.isSearching
  ) {
    return (
      <IonContent>
        <div className="product-items">
          <div className="container">
            <div className="myOrder-seen__List text-center pd-vertical">
              Vui lòng nhập ký tự muốn tìm kiếm.
            </div>
          </div>
        </div>
      </IonContent>
    );
  }
  return (
    <IonContent>
      {!RouterSingleton.pathname.includes(AppRoute.PRODUCTS) && (
        <UiExtension uiId="CATEGORY_BANNER" />
      )}
      {RouterSingleton.pathname === `/${AppRoute.PRODUCTS}` && (
        <>
          <UiExtension uiId="CATEGORY_SEARCH_LIST_BRANCH" />
          <div className="brand__top">
            <div className="container">
              <div className="flex ion-justify-content-between block__title block__bb">
                <p className="block__headline ">SẢN PHẨM</p>
                <p className="block__headline ">
                  {props?.productTotals || 0} kết quả
                </p>
              </div>
            </div>
          </div>
        </>
      )}
      <UiExtension
        uiId="TOOLBAR_PRODUCTS"
        rangePriceFilters={props.state?.rangePriceFilters}
        categoryChildren={props.state?.categoryInfo?.children}
      />
      {!RouterSingleton.pathname.includes(AppRoute.PRODUCTS) && (
        <>
          <div className="brand__top">
            <div className="container">
              <div className="flex ion-justify-content-between">
                <p className="block__headline ">
                  {props?.state?.categoryInfo?.name_on_app ??
                    props?.state?.categoryInfo?.name}
                </p>
                <p className="block__headline ">
                  {props?.productTotals || 0} sản phẩm
                </p>
              </div>
            </div>
          </div>
        </>
      )}
      {/*<UiExtension uiId="CATEGORY_PRODUCTS_FILTER_SELECTED" />*/}
      <UiExtension
        uiId="PRODUCT_WRAPPERS"
        isLoading={props.state?.isLoading}
        products={props.state?.products}
        handleLoadMorePage={props.actions?.handleLoadMorePage}
        isDone={props.state?.isDone}
        pageFilterInfo={props?.state?.pageFilterInfo}
        totalPage={props?.state?.totalPage}
      />
    </IonContent>
  );
});

export default CategoryProductListContainer;
