import { IonCol, useIonActionSheet } from '@ionic/react';
import { SortEnum } from '@vjcspy/apollo';
import { withProductContainerActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductContainerActions';
import { combineHOC } from '@vjcspy/ui-extension';
import React, { useCallback, useEffect, useState } from 'react';

import iconArrow from '../../assets/images/icons/icon-arrow-select.svg';

const Sorter: React.FC = combineHOC(withProductContainerActions)((props) => {
  const PRICE_ASC = 'Giá từ thấp đến cao';
  const PRICE_DESC = 'Giá từ cao đến thấp';
  const NAME_ASC = 'Khuyến mại từ cao đến thấp';
  const NAME_DESC = 'Khuyến mại từ thấp đến cao';
  const NEW_DESC = 'Sản phẩm mới nhất';

  const [present, dismiss] = useIonActionSheet();
  const [sortValue, setSortValue] = useState('');

  const setSorter = useCallback(
    (sort: any) => {
      if (typeof props.actions?.setFilterInfo === 'function') {
        props.actions.setFilterInfo({
          sort,
          currentPage: 1,
        });
      }
    },
    [props.pageFilterInfo]
  );

  useEffect(() => {
    switch (sortValue) {
      case PRICE_ASC:
        return setSorter({
          price: SortEnum.Asc,
        });
      case PRICE_DESC:
        return setSorter({
          price: SortEnum.Desc,
        });
      case NAME_ASC:
        return setSorter({
          name: SortEnum.Asc,
        });
      case NAME_DESC:
        return setSorter({
          name: SortEnum.Desc,
        });
      case NEW_DESC:
        return setSorter({
          new: SortEnum.Desc,
        });
    }
  }, [sortValue]);

  return (
    <>
      {/*<IonCol size="4">*/}
      <div
        className="category-list-item flex-center-center "
        onClick={() =>
          present({
            buttons: [
              {
                text: PRICE_ASC,
                role: sortValue === PRICE_ASC ? 'selected' : '',
                handler: () => {
                  setSortValue(PRICE_ASC);
                },
              },
              {
                text: PRICE_DESC,
                role: sortValue === PRICE_DESC ? 'selected' : '',
                handler: () => {
                  setSortValue(PRICE_DESC);
                },
              },
              {
                text: NAME_ASC,
                role: sortValue === NAME_ASC ? 'selected' : '',
                handler: () => {
                  setSortValue(NAME_ASC);
                },
              },
              {
                text: NAME_DESC,
                role: sortValue === NAME_DESC ? 'selected' : '',
                handler: () => {
                  setSortValue(NAME_DESC);
                },
              },
              {
                text: NEW_DESC,
                role: sortValue === NEW_DESC ? 'selected' : '',
                handler: () => {
                  setSortValue(NEW_DESC);
                },
              },
              { text: 'Hủy', role: 'cancel' },
            ],
            header: 'Sắp xếp',
          })
        }
      >
        {/*<img className="icon-sorter" src={iconSorter} />*/}
        Sắp xếp
        <img src={iconArrow} alt="" />
      </div>
      {/*This popup click b div sapxep add class active on opup*/}
      {/*</IonCol>*/}
    </>
  );
});

export default Sorter;
