import Slider from '@mui/material/Slider';
import { withProductsAggregationActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsAggregationActions';
import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import _ from 'lodash';
import React, { useCallback, useEffect, useState } from 'react';

const AggregationPrice: React.FC<{
  aggregation: any; // pass from parent
  attribute: any; // pass from parent
  filter: any; // pass from parent
  rangePriceFilters?: any;
}> = combineHOC(withProductsAggregationActions)((props) => {
  const [show, setShow] = useState(true);
  const [defaultRangeValue, setDefaultRangeValue] = useState<{
    min: number;
    max: number;
  }>({ min: 0, max: 0 });
  const [changeRangeValue, setChangeRangeValue] = useState<number[]>([]);

  useEffect(() => {
    if (props?.filter?.data?.eq && props?.filter?.data?.eq.includes('_')) {
      const defaultValue = props?.filter?.data?.eq.split('_');
      if (defaultValue[0] && defaultValue[1]) {
        setChangeRangeValue([
          parseInt(defaultValue[0]),
          parseInt(defaultValue[1]),
        ]);
      }
    } else {
      if (props?.aggregation?.options.length > 0) {
        const defaultValue = [];
        const count = parseInt(props?.aggregation?.options.length) - 1;
        if (props?.aggregation?.options[0]?.value) {
          defaultValue.push(
            // @ts-ignore
            parseInt(props?.aggregation?.options[0]?.value.split('_')[0])
          );
        }
        if (props?.aggregation?.options[count]?.value) {
          defaultValue.push(
            // @ts-ignore
            parseInt(props?.aggregation?.options[count]?.value.split('_')[1])
          );
        }
        setChangeRangeValue(defaultValue);
      }
    }

    if (props.rangePriceFilters) {
      setDefaultRangeValue(props.rangePriceFilters);
    }
  }, []);
  const onChangeRangeSuccess = useCallback((val: any) => {
    let valuePrice = '';
    if (val && val[1]) {
      valuePrice = val[0] + '_' + val[1];
      props.actions.toggleAggregationItem(
        props.aggregation['attribute_code'],
        valuePrice
      );
    }
  }, []);

  return (
    <div className={clsx('filter-item dropdown-parent', show && 'active')}>
      <div
        className="list-filter-head"
        onClick={() => {
          setShow(!show);
        }}
      >
        <h3
          className={clsx('filter-option-title', show && 'active')}
          onClick={() => {
            setShow(!show);
          }}
        >
          {props.aggregation?.label}
        </h3>
      </div>
      {show && (
        <div className="list-filter-content price">
          {defaultRangeValue.max && changeRangeValue && (
            <Slider
              min={defaultRangeValue.min}
              max={defaultRangeValue.max}
              color="secondary"
              valueLabelDisplay="on"
              value={changeRangeValue}
              onChange={(event, value) => {
                // @ts-ignore
                setChangeRangeValue(value);
              }}
              onChangeCommitted={(event, value) => {
                onChangeRangeSuccess(value);
              }}
              valueLabelFormat={(value) =>
                `${Intl.NumberFormat('en-EN').format(value)}`
              }
            />
          )}
        </div>
      )}
    </div>
  );
});

export default AggregationPrice;
