import { withProductsFiltersData } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsFiltersData';
import { withNavigatorAttributeData } from '@vjcspy/r/build/modules/catalog/hoc/withNavigatorAttributeData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useMemo } from 'react';

const Aggregation: React.FC<{
  attributeCode: string; // pass from parent
  aggregation: any; // pass from parent
  attribute: any; // pass from hoc
  filters: any[]; // pass from hoc
  rangePriceFilters?: any;
}> = combineHOC(
  withNavigatorAttributeData,
  withProductsFiltersData
)((props) => {
  const filter = useMemo(() => {
    if (!props.attribute || !Array.isArray(props.filters)) {
      return null;
    }
    return props.filters.find(
      (f: any) => f.code === props.attribute?.attribute_code
    );
  }, [props.attribute, props.filters]);

  const AGGREGATION_CONTENT = useMemo(() => {
    if (!props.attribute) {
      return null;
    }

    if (
      Array.isArray(props.attribute['swatches']) &&
      props.attribute['swatches'].length > 0
    ) {
      const swatchType = props.attribute?.swatches[0]?.type;
      if (swatchType == 1) {
        return (
          <UiExtension
            uiId="CATEGORY_AGGREGATION_SWATCH"
            attribute={props.attribute}
            filter={filter}
            aggregation={props.aggregation}
          />
        );
      } else {
        return (
          <UiExtension
            uiId="CATEGORY_AGGREGATION_TEXT"
            attribute={props.attribute}
            filter={filter}
            aggregation={props.aggregation}
          />
        );
      }
    } else {
      if (props.attribute.input_type === 'price') {
        return (
          <UiExtension
            uiId="CATEGORY_AGGREGATION_PRICE"
            attribute={props.attribute}
            filter={filter}
            aggregation={props.aggregation}
            rangePriceFilters={props?.rangePriceFilters}
          />
        );
      }
      return (
        <UiExtension
          uiId="CATEGORY_AGGREGATION_TEXT"
          attribute={props.attribute}
          filter={filter}
          aggregation={props.aggregation}
        />
      );
    }
  }, [props.attribute, filter, props.filters]);

  return <> {AGGREGATION_CONTENT}</>;
});

export default Aggregation;
