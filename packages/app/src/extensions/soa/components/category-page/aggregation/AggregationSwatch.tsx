import { withProductsAggregationActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsAggregationActions';
import { isAttributeFilterSelected } from '@vjcspy/r/build/modules/catalog/util/isAttributeFilterSelected';
import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useState } from 'react';

const AggregationSwatch: React.FC<{
  aggregation: any; // pass from parent
  attribute: any; // pass from parent
  filter: any; // pass from parent
}> = combineHOC(withProductsAggregationActions)((props) => {
  const [show, setShow] = useState(true);
  return (
    <>
      <div
        className={clsx('filter-item dropdown-parent', show && 'active')}
        datatype="attr-colors"
      >
        <h3
          className={clsx('filter-option-title', show && 'active')}
          onClick={() => {
            setShow(!show);
          }}
        >
          {props.aggregation?.label}
        </h3>
        {show && (
          <div className="filter-option-content">
            <form className="filter-form">
              <ul className="am-filter-items list-reset">
                {props.aggregation['options'].map((option: any) => {
                  const swatch = props.attribute.swatches.find(
                    (s: any) => s['option_id'] == option['value']
                  );
                  if (!swatch) {
                    return null;
                  }
                  const isChecked = isAttributeFilterSelected(
                    option.value,
                    props.filter
                  );

                  return (
                    <li className="am-filter-item" key={option['value']}>
                      <label className="label-input">
                        <input
                          name="amshopby"
                          type="checkbox"
                          defaultChecked={isChecked}
                        />
                        <span
                          className="label"
                          onClick={() =>
                            props.actions.toggleAggregationItem(
                              props.aggregation['attribute_code'],
                              option.value
                            )
                          }
                        >
                          {option['label']}
                          <span
                            className="swatch-color"
                            style={{ background: swatch['value'] }}
                          />
                        </span>
                      </label>
                    </li>
                  );
                })}
              </ul>
            </form>
          </div>
        )}
      </div>
    </>
  );
});

export default AggregationSwatch;
