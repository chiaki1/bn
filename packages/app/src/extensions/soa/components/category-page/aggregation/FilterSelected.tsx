import { withProductsFiltersData } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsFiltersData';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import _ from 'lodash';
import React from 'react';

const FilterSelected: React.FC = combineHOC(withProductsFiltersData)(
  (props) => {
    if (!Array.isArray(props.filters) || _.size(props.filters) === 0) {
      return null;
    }
    return (
      <div className="category-list-filter">
        <ul>
          {props.filters.map((filter: any) => (
            <UiExtension
              key={filter.code}
              attributeCode={filter.code}
              uiId="CATEGORY_PRODUCTS_FILTER_SELECTED_ITEM"
              filter={filter}
            />
          ))}
        </ul>
      </div>
    );
  }
);

export default FilterSelected;
