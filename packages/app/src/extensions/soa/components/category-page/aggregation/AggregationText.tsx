import { withProductsAggregationActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsAggregationActions';
import { isAttributeFilterSelected } from '@vjcspy/r/build/modules/catalog/util/isAttributeFilterSelected';
import { combineHOC } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

const AggregationText: React.FC<{
  aggregation: any; // pass from parent
  attribute: any; // pass from parent
  filter: any; // pass from parent
}> = combineHOC(withProductsAggregationActions)((props) => {
  const { t } = useTranslation('common');
  const [show, setShow] = useState(true);
  const isBoolAttr = useMemo(() => {
    return props.attribute?.input_type === 'boolean';
  }, [props.attribute]);
  return (
    <div
      className={clsx('filter-item dropdown-parent', show && 'active')}
      datatype="attr-brands"
    >
      <h3
        className={clsx('filter-option-title', show && 'active')}
        onClick={() => {
          setShow(!show);
        }}
      >
        {props.aggregation?.label}
      </h3>

      {show && (
        <div className="filter-option-content">
          <form className="filter-form">
            <ul className="am-filter-items list-reset">
              {props.aggregation?.options.map((option: any) => {
                const isChecked = isAttributeFilterSelected(
                  option.value,
                  props.filter
                );
                return (
                  <li className="am-filter-item" key={option.value}>
                    <label className="label-input">
                      <input
                        // name="amshopby0"
                        type="checkbox"
                        checked={isChecked}
                        onChange={() => {}}
                      />
                      <span
                        className="label"
                        onClick={() =>
                          props.actions.toggleAggregationItem(
                            props.aggregation['attribute_code'],
                            option.value
                          )
                        }
                      >
                        {isBoolAttr
                          ? option.label === '1'
                            ? t('yes')
                            : option.label
                          : option.label}
                        ({option.count})
                      </span>
                      {/*className={clsx(isChecked && 'is-active')}*/}
                    </label>
                  </li>
                );
              })}
            </ul>
          </form>
        </div>
      )}
    </div>
  );
});

export default AggregationText;
