import {
  IonButton,
  IonCol,
  IonContent,
  IonFooter,
  IonHeader,
  IonModal,
  IonPage,
  IonToolbar,
} from '@ionic/react';
import { useOutsideClickHandle } from '@vjcspy/chitility/build/hook/useOutsideClickHandle';
import { withProductContainerActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductContainerActions';
import { withProductsAggregationsData } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsAggregationsData';
import { withProductsFilterActions } from '@vjcspy/r/build/modules/catalog/hoc/products/withProductsFilterActions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import clsx from 'clsx';
import React, { useCallback, useRef, useState } from 'react';

import { withAppProductsFilterActionsCustom } from '../../../hoc/product/withAppProductsFilterActionsCustom';

const Aggregations: React.FC<{
  totalCount?: number;
  rangePriceFilters?: any;
}> = combineHOC(
  withProductsAggregationsData,
  withProductContainerActions,
  withProductsFilterActions,
  withAppProductsFilterActionsCustom
)((props) => {
  const [showFilters, setShowFilters] = useState(false);
  const filtersRef = useRef<any>();
  useOutsideClickHandle(filtersRef, () => setShowFilters(false));

  const clearFilters = useCallback(() => {
    if (RouterSingleton.pathname.includes('products-list')) {
      if (typeof props.actions.clearFiltersCustom === 'function') {
        props.actions.clearFiltersCustom();
        RouterSingleton.replace('/products-list');
      }
    } else {
      if (typeof props.actions.clearFilters === 'function') {
        props.actions.clearFilters();
      }
    }

    setShowFilters(false);
  }, []);

  const applyFilters = useCallback(() => {
    setShowFilters(false);
  }, []);

  return (
    <>
      <IonModal
        isOpen={showFilters}
        className="root-bg-transparent modal-wrapper-full aggregation-only"
      >
        <IonPage className="max-w-265 mr-0 bg-white">
          {/*<div ref={filtersRef}>*/}
          <IonHeader className="ion-no-border modal-header" translucent={false}>
            <div className="container">
              <IonToolbar color="link" p-0>
                <div className="flex-center ion-justify-content-between">
                  <h2 className="filter-title">Bộ lọc</h2>
                  <button
                    className="btn-link btn-close"
                    onClick={() => setShowFilters(false)}
                  />
                </div>
              </IonToolbar>
            </div>
          </IonHeader>
          <IonContent>
            <div className="block__filter" ref={filtersRef}>
              <div className="container">
                <div className="filter-options">
                  {props.aggregations &&
                    props.aggregations?.map((aggregation: any) => (
                      <UiExtension
                        uiId="CATEGORY_AGGREGATION"
                        key={aggregation.attribute_code}
                        attributeCode={aggregation.attribute_code}
                        aggregation={aggregation}
                        rangePriceFilters={props?.rangePriceFilters}
                      />
                    ))}
                </div>
                {/*<div className="list-filter-group-bottom">*/}
                {/*  <span>Kết quả: {props.totalCount} sản phầm</span>*/}
                {/*</div>*/}
              </div>
            </div>
          </IonContent>
          <IonFooter>
            <IonToolbar color="link" className="root-min-h-30" p-0>
              <div className="grid grid-cols-2 gap-x-8 py-8 px-12">
                <button
                  className="btn btn-reset-black btn-lg btn-fs-32"
                  onClick={clearFilters}
                >
                  Đặt lại
                </button>
                <button
                  className="btn btn-black btn-lg btn-fs-32"
                  onClick={applyFilters}
                >
                  Áp dụng
                </button>
              </div>
            </IonToolbar>
          </IonFooter>
        </IonPage>
      </IonModal>
      <div className="flex-center-center" onClick={() => setShowFilters(true)}>
        Bộ lọc
        <IonButton color={'link'} className="btn-filter" />
      </div>

      <div className={clsx(showFilters && 'is-active')} />
    </>
  );
});

export default Aggregations;
