import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_CATEGORY_PAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'CATEGORY_PRODUCT_LIST_CONTAINER',
    component: {
      lazy: React.lazy(() => import('./CategoryProductListContainer')),
    },
  },
  {
    uiId: 'CATEGORY_PRODUCT_LIST_CONTAINER_CUSTOM',
    component: {
      lazy: React.lazy(() => import('./CategoryProductListContainerCustom')),
    },
  },
  {
    uiId: 'TOOLBAR_PRODUCTS',
    component: { lazy: React.lazy(() => import('./ToolbarProduct')) },
  },
  {
    uiId: 'PRODUCT_WRAPPERS',
    component: { lazy: React.lazy(() => import('./ProductWrapper')) },
  },
  {
    uiId: 'CATEGORY_BANNER',
    component: { lazy: React.lazy(() => import('./CategoryBanner')) },
  },
  {
    uiId: 'CATEGORY_AGGREGATIONS',
    uiTags: ['CATEGORY_AGGREGATIONS'],
    component: { lazy: React.lazy(() => import('./aggregation/Aggregations')) },
  },
  {
    uiId: 'CATEGORY_AGGREGATION',
    component: { lazy: React.lazy(() => import('./aggregation/Aggregation')) },
  },
  {
    uiId: 'CATEGORY_AGGREGATION_TEXT',
    component: {
      lazy: React.lazy(() => import('./aggregation/AggregationText')),
    },
  },
  {
    uiId: 'CATEGORY_AGGREGATION_PRICE',
    component: {
      lazy: React.lazy(() => import('./aggregation/AggregationPrice')),
    },
  },
  {
    uiId: 'CATEGORY_AGGREGATION_SWATCH',
    component: {
      lazy: React.lazy(() => import('./aggregation/AggregationSwatch')),
    },
  },
  {
    uiId: 'CATEGORY_PRODUCTS_FILTER_SELECTED',
    component: {
      lazy: React.lazy(() => import('./aggregation/FilterSelected')),
    },
  },
  {
    uiId: 'CATEGORY_PRODUCTS_FILTER_SELECTED_ITEM',
    component: {
      lazy: React.lazy(() => import('./aggregation/FilterSelectedItem')),
    },
  },
  {
    uiId: 'CATEGORY_PRODUCTS_SORTER',
    component: { lazy: React.lazy(() => import('./Sorter')) },
  },
  {
    uiId: 'CATEGORY_CHILDREN',
    component: { lazy: React.lazy(() => import('./ChildrenCategory')) },
  },
];
