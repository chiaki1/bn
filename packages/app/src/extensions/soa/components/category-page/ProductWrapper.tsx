import { IonCol, IonRow } from '@ionic/react';
import { PageFilterInfo } from '@vjcspy/r/build/modules/catalog/store/products/products.state';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect, useState } from 'react';

import { useIsScrollBottom } from '../../../../modules/ui/hook/useIsScrollBottom';
import iconSpinner from '../../assets/images/icons/icon-spinner.svg';

const ProductWrapper: React.FC<{
  props: any;
  isLoading: boolean;
  handleLoadMorePage: () => void;
  isDone: boolean;
  pageFilterInfo: PageFilterInfo;
  totalPage: any;
}> = combineHOC()((props) => {
  const [isShowLoading, setIsShowLoading] = useState(false);

  const { isBottom } = useIsScrollBottom();

  useEffect(() => {
    if (
      typeof props?.handleLoadMorePage === 'function' &&
      isBottom &&
      !props?.isDone
    ) {
      props.handleLoadMorePage();
    }
  }, [isBottom]);

  useEffect(() => {
    if (!props?.products && props?.isLoading === true) {
      setIsShowLoading(true);
    } else {
      setIsShowLoading(false);
    }
  }, [props?.products, props?.isLoading]);

  if (props?.products && props?.products?.length === 0 && !props?.isLoading) {
    return (
      <div className="myOrder-seen__List text-center pd-vertical">
        Không tìm thấy sản phẩm phù hợp
      </div>
    );
  }

  if (!props?.products) {
    return null;
  }

  return (
    <div className="product-items">
      <UiExtension uiId="DEFAULT_LOADING" loading={isShowLoading} />
      <div className="container">
        <IonRow className="mr-row">
          {props?.products?.map((item: any) => (
            <IonCol size="6" key={item?.id}>
              <UiExtension
                uiId="PRODUCT_LIST_ITEM"
                product={item}
                item={item}
              />
            </IonCol>
          ))}
        </IonRow>
        {props?.isDone && (
          <div className="myOrder-seen__List text-center pd-vertical">
            Bạn đã xem hết danh sách
          </div>
        )}
        {!props?.isDone && (
          <div className="text-center">
            <img src={iconSpinner} alt="" />
          </div>
        )}
      </div>
    </div>
  );
});

export default ProductWrapper;
