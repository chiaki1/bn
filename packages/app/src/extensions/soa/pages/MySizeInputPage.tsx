import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const MySizeInputPage: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <>
      <IonPage className="page-header-no-mrb">
        <UiExtension uiId="MY_ACCOUNT_GUARD" />
        <UiExtension
          uiId="HEADER_ACCOUNT"
          goBack={() => RouterSingleton.back()}
          title="Thêm size mới"
        />
        <IonContent>
          <UiExtension uiId="ADD_NEW_SIZE" />
        </IonContent>
      </IonPage>
    </>
  );
});

export default MySizeInputPage;
