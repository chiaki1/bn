import { IonContent, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const QuestionPage: React.FC = () => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Câu hỏi thường gặp"
      />
      <IonContent>
        <UiExtension uiId="QUESTION_HOME" />
        <UiExtension uiId="QUESTION_DETAIL" />
      </IonContent>
    </IonPage>
  );
};

export default QuestionPage;
