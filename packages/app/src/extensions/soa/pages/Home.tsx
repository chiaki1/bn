import { IonBackdrop, IonContent, IonFooter, IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';
import { AppRoute } from '../../../router/Route';
import { withSoaHomePageData } from '../hoc/home-page/withSoaHomePageData';

const Home: React.FC = combineHOC(withSoaHomePageData)((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex(AppRoute.HOME);
  }, []);

  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER" />
      <IonContent>
        <UiExtension uiId="HOME_POPUP" />
        <UiExtension uiId="HOME_BANNER" />
        <UiExtension uiId="FLASH_SALE_HOME_PAGE" />
        <UiExtension uiId="HOME_TOP_SEARCH" />
        <UiExtension uiId="ADDITIONAL_BOCK_HOME_PAGE" />
        <UiExtension uiId="HOME_SUGGESTION" />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />

      {/*Popup Home*/}
      {/*<IonBackdrop tappable={true} visible={true} stopPropagation={true} />*/}
      {/*<div className="modalv2 modal-center modal-no-w modal-transparent">*/}
      {/*  <div className="modalv2-content">*/}
      {/*    <span className="inline-flex items-center ion-justify-content-center absolute -top-20 -right-20 bg-white cursor-pointer w-34 h-34 rounded-4 text-black">*/}
      {/*      <svg width="18" height="18">*/}
      {/*        <use xlinkHref={iconSprites + '#icon-close'} />*/}
      {/*      </svg>*/}
      {/*    </span>*/}
      {/*    <div className="img-block rounded-13 overflow-hidden">*/}
      {/*      <img src="https://via.placeholder.com/284x359" />*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</div>*/}
      {/*End Popup Home*/}

      {/*Splash screen*/}
      {/*<IonContent>*/}
      {/*  <div className="text-center py-16 h-80vh flex flex-col ion-justify-content-center ion-align-items-center">*/}
      {/*    <img className="pb-40" src={welcomeIcon} />*/}
      {/*    <img src={welcomeLogo} />*/}
      {/*  </div>*/}
      {/*</IonContent>*/}

      {/*<IonFooter className="ion-no-border">*/}
      {/*  <p className="text-small m-0 pb-40 text-center px-16">*/}
      {/*    Công ty TNHH SALEOFF.ASIA*/}
      {/*  </p>*/}
      {/*</IonFooter>*/}
      {/*End Splash screen*/}
    </IonPage>
  );
});

export default Home;
