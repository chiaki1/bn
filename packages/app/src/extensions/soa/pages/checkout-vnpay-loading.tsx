import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

import { withVnpayCheckingStatus } from '../../../modules/checkout/hoc/withVnpayCheckingStatus';

const CheckoutVnpayLoading = combineHOC(withVnpayCheckingStatus)((props) => {
  return (
    <>
      <UiExtension
        uiId="DEFAULT_LOADING"
        loading={props?.state?.loading}
        mess="Đang chờ thông tin thanh toán..."
      />
    </>
  );
});

export default CheckoutVnpayLoading;
