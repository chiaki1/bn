import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const PointsPage: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <UiExtension uiId="REWARD_POINTS_HEADER" title="Điểm của bạn" />
      <IonContent>
        <UiExtension uiId="TAB_POINTS" />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default PointsPage;
