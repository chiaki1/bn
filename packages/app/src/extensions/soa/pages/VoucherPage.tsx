import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const VoucherPage: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title={'Mã giảm giá'}
      />
      <IonContent>
        <UiExtension uiId="VOUCHER_LIST_ON_PAGE" />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default VoucherPage;
