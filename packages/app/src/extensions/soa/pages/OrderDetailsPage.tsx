import { IonContent, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const OrderDetailsPage: React.FC = combineHOC()(() => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Chi tiết đơn hàng"
        showIcon={false}
      />

      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <UiExtension uiId="ORDER_DETAIL_CONTAINER" />
    </IonPage>
  );
});

export default OrderDetailsPage;
