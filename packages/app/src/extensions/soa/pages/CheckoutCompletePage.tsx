import { IonPage } from '@ionic/react';
import { UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const CheckoutCompletePage: React.FC = () => {
  return (
    <>
      <IonPage className="ion-page" id="main-content">
        <UiExtension uiId="COMPLETE_HEADER" />
        <UiExtension uiId="COMPLETE_CONTENT" />
      </IonPage>
    </>
  );
};

export default CheckoutCompletePage;
