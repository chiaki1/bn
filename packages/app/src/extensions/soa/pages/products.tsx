import { IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const Products: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER" />
      <UiExtension uiId="CATEGORY_PRODUCT_LIST_CONTAINER" />
      {/*<UiExtension uiId="BOTTOM_NAV_BAR" />*/}
    </IonPage>
  );
});

export default Products;
