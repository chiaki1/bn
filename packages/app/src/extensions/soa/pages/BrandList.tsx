import { IonContent, IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';
import { AppRoute } from '../../../router/Route';

const BrandList: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex(AppRoute.BRAND);
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER" />
      <UiExtension uiId="BRAND_LIST_CONTAINER" />
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default BrandList;
