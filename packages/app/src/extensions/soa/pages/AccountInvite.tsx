import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const AccountInvite: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Mã giới thiệu"
      />
      <IonContent>
        <UiExtension uiId="ACCOUNT_INVITE_CONTAINER" />
      </IonContent>
    </IonPage>
  );
});

export default AccountInvite;
