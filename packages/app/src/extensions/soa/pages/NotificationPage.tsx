import { IonContent, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const NotificationPage: React.FC = () => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Thông báo"
        showIcon={false}
      />
      <IonContent>
        <UiExtension uiId="NOTIFICATION_HOME" />
        {/*<UiExtension uiId="DETAIL_NOTIFICATION" />*/}
      </IonContent>
    </IonPage>
  );
};

export default NotificationPage;
