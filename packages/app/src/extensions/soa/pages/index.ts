import { ExtensionConfig } from '@vjcspy/ui-extension';
import React from 'react';

export const SOA_PAGES_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'HOME_PAGE',
    component: { lazy: React.lazy(() => import('./Home')) },
  },
  {
    uiId: 'BRAND_PAGE',
    component: { lazy: React.lazy(() => import('./BrandList')) },
  },
  {
    uiId: 'BRAND_DETAIL_PAGE',
    component: { lazy: React.lazy(() => import('./BrandDetail')) },
  },
  {
    uiId: 'PRODUCT_PAGE',
    component: { lazy: React.lazy(() => import('./product')) },
  },
  {
    uiId: 'LOGIN_PAGE',
    component: { lazy: React.lazy(() => import('./LoginPage')) },
  },
  {
    uiId: 'ACCOUNT_PAGE',
    component: { lazy: React.lazy(() => import('./AccountPage')) },
  },
  {
    uiId: 'ACCOUNT_INVITE_PAGE',
    component: { lazy: React.lazy(() => import('./AccountInvite')) },
  },
  {
    uiId: 'CART_PAGE',
    component: { lazy: React.lazy(() => import('./CartPage')) },
  },
  {
    uiId: 'CHECKOUT_PAGE',
    component: { lazy: React.lazy(() => import('./CheckoutPage')) },
  },
  {
    uiId: 'PRODUCTS_PAGE',
    component: { lazy: React.lazy(() => import('./products')) },
  },
  {
    uiId: 'VOUCHER_PAGE',
    component: { lazy: React.lazy(() => import('./VoucherPage')) },
  },
  {
    uiId: 'REWARD_POINTS',
    component: { lazy: React.lazy(() => import('./reward-points')) },
  },
  {
    uiId: 'POINTS_PAGE',
    component: { lazy: React.lazy(() => import('./PointsPage')) },
  },
  // {
  //   uiId: 'ACCOUNT_INFORMATION_POLICY_PAGE',
  //   component: {
  //     lazy: React.lazy(() => import('./AccountInformationPolicyPage')),
  //   },
  // },
  {
    uiId: 'POLICY_RULE_PAGE',
    component: { lazy: React.lazy(() => import('./PolicyRulePage')) },
  },
  {
    uiId: 'INFORMATION_SECURITY_POLICY_PAGE',
    component: {
      lazy: React.lazy(() => import('./InformationSecurityPolicyPage')),
    },
  },
  {
    uiId: 'ABOUT_PAGE',
    component: {
      lazy: React.lazy(() => import('./AboutPage')),
    },
  },
  {
    uiId: 'ORDER_DETAILS_PAGE',
    component: { lazy: React.lazy(() => import('./OrderDetailsPage')) },
  },
  {
    uiId: 'ORDER_HISTORY_PAGE',
    component: { lazy: React.lazy(() => import('./MyOrderHistoryPage')) },
  },
  {
    uiId: 'ACCOUNT_UPDATE_INFO',
    component: { lazy: React.lazy(() => import('./AccountUpdateInfo')) },
  },
  {
    uiId: 'WISHLIST_PAGE',
    component: {
      lazy: React.lazy(() => import('./WishListPage')),
    },
  },
  {
    uiId: 'PRODUCTS_LIST_PAGE',
    component: {
      lazy: React.lazy(() => import('./ProductsListPage')),
    },
  },
  {
    uiId: 'MY_SIZE_PAGE',
    component: { lazy: React.lazy(() => import('./MySizePage')) },
  },
  {
    uiId: 'MY_SIZE_INPUT_PAGE',
    component: { lazy: React.lazy(() => import('./MySizeInputPage')) },
  },
  {
    uiId: 'CHECKOUT_COMPLETE_PAGE',
    component: {
      lazy: React.lazy(() => import('./CheckoutCompletePage')),
    },
  },
  {
    uiId: 'ADDRESS_LIST',
    component: {
      lazy: React.lazy(() => import('./AddressPage')),
    },
  },
  {
    uiId: 'ACCOUNT_BRAND_FLOW_PAGE',
    component: {
      lazy: React.lazy(() => import('./AccountBrandFlowPage')),
    },
  },
  {
    uiId: 'MY_ORDERS_PAGE',
    component: {
      lazy: React.lazy(() => import('./MyOrderHistoryPage')),
    },
  },
  {
    uiId: 'RETURN_ORDER_PAGE',
    component: { lazy: React.lazy(() => import('./ReturnOrderPage')) },
  },
  {
    uiId: 'CHAT_MESSAGE_PAGE',
    component: { lazy: React.lazy(() => import('./Chat')) },
  },
  {
    uiId: 'NOTIFICATION_PAGE',
    component: { lazy: React.lazy(() => import('./NotificationPage')) },
  },
  {
    uiId: 'QUESTION_PAGE',
    component: { lazy: React.lazy(() => import('./QuestionPage')) },
  },
  {
    uiId: 'CMS_DETAIL_PAGES',
    component: { lazy: React.lazy(() => import('./CmsDetailPage')) },
  },
  {
    uiId: 'ADDRESS_LIST_PAGE',
    component: { lazy: React.lazy(() => import('./AddressListPage')) },
  },
  {
    uiId: 'CHECKOUT_VNPAY_LOADING',
    component: { lazy: React.lazy(() => import('./checkout-vnpay-loading')) },
  },
  {
    uiId: 'SPLASH',
    component: { lazy: React.lazy(() => import('./splash')) },
  },
  {
    uiId: 'SIZE_GUIDE',
    component: { lazy: React.lazy(() => import('./SizeGuide')) },
  },
  {
    uiId: 'SHOPPING',
    component: { lazy: React.lazy(() => import('./Shopping')) },
  },
  {
    uiId: 'PAY',
    component: { lazy: React.lazy(() => import('./Pay')) },
  },
  {
    uiId: 'TRANSPORT',
    component: { lazy: React.lazy(() => import('./Transport')) },
  },
  {
    uiId: 'EXCHANGE_AND_REFUND',
    component: { lazy: React.lazy(() => import('./ExchangeAndRefund')) },
  },
  {
    uiId: 'OTHER',
    component: { lazy: React.lazy(() => import('./Other')) },
  },
];
