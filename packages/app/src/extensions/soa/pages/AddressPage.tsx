import { IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const AddressPage: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER" />
      <UiExtension uiId="ADDRESS_LIST_ON_PAGE" />
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default AddressPage;
