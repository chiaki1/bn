import { IonContent, IonFooter, IonPage } from '@ionic/react';
import { Registry } from '@vjcspy/chitility';
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router';

import { AppRoute } from '../../../router/Route';
import welcomeIcon from '../assets/images/welcomeIcon.svg';
import welcomeLogo from '../assets/images/welcomeLogo.svg';

const INIT_SPLASH = 'INIT_SPLASH';
const Splash = React.memo((props) => {
  const [show, setShow] = useState(false);
  const location = useHistory();

  const goHome = useCallback(() => {
    setTimeout(() => {
      location.replace(`/${AppRoute.HOME}`);
    }, 1500);
  }, [location]);

  useEffect(() => {
    const isInited = Registry.getInstance().registry(INIT_SPLASH);
    if (isInited !== true) {
      Registry.getInstance().register(INIT_SPLASH, true);
      setShow(true);
      goHome();
    }
  }, []);

  if (!show) {
    return null;
  }
  return (
    <>
      <IonPage>
        <IonContent>
          <div className="text-center py-16 h-80vh flex flex-col ion-justify-content-center ion-align-items-center animate__animated animate__pulse">
            <img className="pb-40" src={welcomeIcon} />
            <img src={welcomeLogo} />
          </div>
        </IonContent>
        <IonFooter className="ion-no-border">
          <p className="text-small m-0 pb-40 text-center px-16 text-black bg-white">
            Công ty Cổ phần Quince Việt Nam
          </p>
        </IonFooter>
      </IonPage>
    </>
  );
});

Splash.displayName = 'Splash';
export default Splash;
