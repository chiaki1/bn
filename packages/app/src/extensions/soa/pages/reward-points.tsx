import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const RewardPoints: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="REWARD_POINTS_HEADER" />
      <IonContent>
        <UiExtension uiId="REWARD_POINTS_OPTION" />
      </IonContent>
      <UiExtension uiId="SHIPPING_FOOTER" />
    </IonPage>
  );
});

export default RewardPoints;
