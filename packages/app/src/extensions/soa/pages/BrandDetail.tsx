import { IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const BrandDetail: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER" />
      <UiExtension uiId="BRAND_DETAIL_CONTAINER" />
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default BrandDetail;
