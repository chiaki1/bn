import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const Product: React.FC = combineHOC()((props) => {
  return (
    <>
      <UiExtension uiId="PRODUCT" />
    </>
  );
});

export default Product;
