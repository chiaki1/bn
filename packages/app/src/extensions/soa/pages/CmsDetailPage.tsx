import { IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const CmsDetailPage: React.FC = combineHOC()(() => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);

  const { identifier } = useParams<{
    identifier: any;
  }>();

  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="CMS_DETAIL_PAGE" identifier={identifier} />
    </IonPage>
  );
});

export default CmsDetailPage;
