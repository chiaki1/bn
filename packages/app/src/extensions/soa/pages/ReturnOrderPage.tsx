import { IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const ReturnOrderPage: React.FC = () => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title="Trả hàng"
      />
      <UiExtension uiId="RETURN_ORDER_CONTAINER" />
    </IonPage>
  );
};

export default ReturnOrderPage;
