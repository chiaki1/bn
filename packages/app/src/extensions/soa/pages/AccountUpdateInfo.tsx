import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';
import { AppRoute } from '../../../router/Route';

const AccountUpdateInfo: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex(AppRoute.ACCOUNT);
  }, []);
  return <UiExtension uiId="ACCOUNT_UPDATE_CONTAINER" />;
});

export default AccountUpdateInfo;
