import { IonPage } from '@ionic/react';
import { UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const CheckoutPage: React.FC = () => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER_ONLY_TEXT" canBack={true} title="Thanh toán" />
      <UiExtension uiId="CHECKOUT_STEP_GUARD" />
      <UiExtension uiId="CHECKOUT_CONTENT_CONTAINER" />
    </IonPage>
  );
};

export default CheckoutPage;
