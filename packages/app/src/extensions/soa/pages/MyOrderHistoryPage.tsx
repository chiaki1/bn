import { IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';

const MyOrderHistoryPage: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex('');
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="MY_ACCOUNT_GUARD" />
      <UiExtension uiId="ORDER_HISTORY_TAB" />
      <UiExtension uiId="ORDER_HISTORY_LIST" />

      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default MyOrderHistoryPage;
