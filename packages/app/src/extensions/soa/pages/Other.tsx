import { IonContent, IonHeader, IonPage } from '@ionic/react';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React, { useEffect } from 'react';

import { useBottomNavBarContext } from '../../../modules/ui/context';
import { AppRoute } from '../../../router/Route';
import { OTHER } from '../values/SOA_CONTENT_HOME_PAGE';

const Other: React.FC = combineHOC()((props) => {
  const bottomContext = useBottomNavBarContext();
  useEffect(() => {
    bottomContext.setIndex(AppRoute.ACCOUNT);
  }, []);
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension
        uiId="HEADER_ACCOUNT"
        goBack={() => RouterSingleton.back()}
        title={'Câu hỏi thường gặp'}
      />
      <IonContent>
        <UiExtension uiId="CMS_CONTENT" identifier={OTHER} />
      </IonContent>
      <UiExtension uiId="BOTTOM_NAV_BAR" />
    </IonPage>
  );
});

export default Other;
