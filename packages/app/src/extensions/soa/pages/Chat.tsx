import { IonContent, IonPage } from '@ionic/react';
import { UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const ChatMessagePage: React.FC = () => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER_CHAT" />
      <IonContent>
        {/*<UiExtension uiId="CHAT_HOME" />*/}
        <UiExtension uiId="CHAT_MESSAGE" />
        {/*<UiExtension uiId="RATE_CHAT" />*/}
      </IonContent>
    </IonPage>
  );
};

export default ChatMessagePage;
