import { IonPage } from '@ionic/react';
import { combineHOC, UiExtension } from '@vjcspy/ui-extension';
import React from 'react';

const CartPage: React.FC = combineHOC()((props) => {
  return (
    <IonPage className="page-header-no-mrb">
      <UiExtension uiId="HEADER_ONLY_TEXT" canBack={false} title="Giỏ hàng" />
      <UiExtension uiId="CART_CONTENT_CONTAINER" />
    </IonPage>
  );
});

export default CartPage;
