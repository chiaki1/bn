export interface SoaBrandContentState {
  dataBrand?: any;
  followBrands?: [];
  listAllBrands?: [];
  loadingState: {
    loadingFollowBrand?: boolean;
  };
}

export const soaBrandContentStateFactory = (): SoaBrandContentState => ({
  loadingState: {},
});
