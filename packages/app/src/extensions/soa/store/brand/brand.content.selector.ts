import { SoaBrandContentState } from './brand.content.state';

export const selectBrandData = (state: {
  soa: { brand: SoaBrandContentState };
}) => state.soa?.brand?.dataBrand || {};

export const selectBrandCategoryData = (state: {
  soa: { brand: SoaBrandContentState };
}) => state.soa?.brand?.dataBrand?.categories || [];

export const selectBrandBlocksData = (state: {
  soa: { brand: SoaBrandContentState };
}) => state.soa?.brand?.dataBrand?.blocks || [];

export const selectFollowBrand = (state: {
  soa: { brand: SoaBrandContentState };
}) => state.soa.brand.followBrands;

export const selectListAllBrands = (state: {
  soa: { brand: SoaBrandContentState };
}) => state.soa.brand.listAllBrands;
