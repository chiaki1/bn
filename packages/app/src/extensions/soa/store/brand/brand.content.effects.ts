import { gotCustomerDetail } from '@vjcspy/r/build/modules/account/store/account.actions';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import { map } from 'rxjs/operators';

import { soaSetFollowBrandAfter } from './brand.content.actions';

const getBrandAfterLogin$ = createEffect((action$) =>
  action$.pipe(
    ofType(gotCustomerDetail),
    map((action) => {
      if (action?.payload?.customer?.follow_brands) {
        return soaSetFollowBrandAfter({
          data: action?.payload?.customer?.follow_brands,
        });
      }

      return soaSetFollowBrandAfter({
        data: [],
      });
    })
  )
);

export const SOA_BRAND_EFFECTS = [getBrandAfterLogin$];
