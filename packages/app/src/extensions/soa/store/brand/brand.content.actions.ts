import { createAction } from '@vjcspy/r/build/util/createAction';

const PREFIX = 'soa_brand_page';

const SOA_GET_DATA_BRAND_LIST = 'SOA_GET_DATA_BRAND_LIST';
export const soaGetDataBrandList = createAction(
  SOA_GET_DATA_BRAND_LIST,
  PREFIX
);

const SOA_GET_DATA_BRAND_LIST_AFTER = 'SOA_GET_DATA_BRAND_LIST_AFTER';
export const soaGetDataBrandListAfterAction = createAction<{
  data: any;
}>(SOA_GET_DATA_BRAND_LIST_AFTER, PREFIX);

const SOA_GET_DATA_BRAND_LIST_ERROR = 'SOA_GET_DATA_BRAND_LIST_ERROR';
export const soaGetDataBrandListErrorAction = createAction<{
  error: Error;
}>(SOA_GET_DATA_BRAND_LIST_ERROR, PREFIX);

const SOA_GET_DATA_BRAND_PAGE = 'SOA_GET_DATA_BRAND_PAGE';
export const soaGetDataBrandPage = createAction(
  SOA_GET_DATA_BRAND_PAGE,
  PREFIX
);

const SOA_GET_DATA_BRAND_PAGE_AFTER = 'SOA_GET_DATA_BRAND_PAGE_AFTER';
export const soaGetDataBrandPageAfterAction = createAction<{
  data: any;
}>(SOA_GET_DATA_BRAND_PAGE_AFTER, PREFIX);

const SOA_GET_DATA_BRAND_PAGE_ERROR = 'SOA_GET_DATA_BRAND_PAGE_ERROR';
export const soaGetDataBrandPageErrorAction = createAction<{
  error: Error;
}>(SOA_GET_DATA_BRAND_PAGE_ERROR, PREFIX);

const SOA_SET_FOLLOW_BRAND = 'SOA_SET_FOLLOW_BRAND';
export const soaSetFollowBrand = createAction<{
  brandId: any;
}>(SOA_SET_FOLLOW_BRAND, PREFIX);

const SOA_SET_FOLLOW_BRAND_AFTER = 'SOA_SET_FOLLOW_BRAND_AFTER';
export const soaSetFollowBrandAfter = createAction<{
  data: any;
}>(SOA_SET_FOLLOW_BRAND_AFTER, PREFIX);

const SOA_SET_FOLLOW_BRAND_ERROR = 'SOA_SET_FOLLOW_BRAND_ERROR';
export const soaSetFollowBrandError = createAction<{
  error: Error;
}>(SOA_SET_FOLLOW_BRAND_ERROR, PREFIX);

const SOA_SET_UN_FOLLOW_BRAND = 'SOA_SET_UN_FOLLOW_BRAND';
export const soaSetUnFollowBrand = createAction<{
  brandId: any;
}>(SOA_SET_UN_FOLLOW_BRAND, PREFIX);

const SOA_SET_UN_FOLLOW_BRAND_AFTER = 'SOA_SET_UN_FOLLOW_BRAND_AFTER';
export const soaSetUnFollowBrandAfter = createAction<{
  data: any;
}>(SOA_SET_UN_FOLLOW_BRAND_AFTER, PREFIX);

const SOA_SET_UN_FOLLOW_BRAND_ERROR = 'SOA_SET_UN_FOLLOW_BRAND_ERROR';
export const soaSetUnFollowBrandError = createAction<{
  error: Error;
}>(SOA_SET_UN_FOLLOW_BRAND_ERROR, PREFIX);

const SOA_CLEAR_LIST_BRAND = 'SOA_CLEAR_LIST_BRAND';
export const soaClearListFlowBrand = createAction(SOA_CLEAR_LIST_BRAND, PREFIX);
