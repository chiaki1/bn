import { createReducer } from '@reduxjs/toolkit';

import {
  soaClearListFlowBrand,
  soaGetDataBrandListAfterAction,
  soaGetDataBrandPage,
  soaGetDataBrandPageAfterAction,
  soaSetFollowBrand,
  soaSetFollowBrandAfter,
  soaSetFollowBrandError,
  soaSetUnFollowBrand,
  soaSetUnFollowBrandAfter,
  soaSetUnFollowBrandError,
} from './brand.content.actions';
import { soaBrandContentStateFactory } from './brand.content.state';

export const soaBrandContentReducer = createReducer(
  soaBrandContentStateFactory(),
  (builder) => {
    builder
      .addCase(soaGetDataBrandPage, (state) => {
        state.dataBrand = {};
      })
      .addCase(soaGetDataBrandPageAfterAction, (state, action) => {
        state.dataBrand = action.payload.data;
      })
      .addCase(soaGetDataBrandListAfterAction, (state, action) => {
        state.listAllBrands = action.payload.data;
      })
      .addCase(soaSetFollowBrand, (state) => {
        state.loadingState.loadingFollowBrand = true;
      })
      .addCase(soaSetFollowBrandAfter, (state, action) => {
        state.followBrands = action.payload.data;
        state.loadingState.loadingFollowBrand = false;
      })
      .addCase(soaSetFollowBrandError, (state) => {
        state.loadingState.loadingFollowBrand = false;
      })
      .addCase(soaSetUnFollowBrand, (state) => {
        state.loadingState.loadingFollowBrand = true;
      })
      .addCase(soaSetUnFollowBrandAfter, (state, action) => {
        state.followBrands = action.payload.data;
        state.loadingState.loadingFollowBrand = false;
      })
      .addCase(soaSetUnFollowBrandError, (state) => {
        state.loadingState.loadingFollowBrand = false;
      })
      .addCase(soaClearListFlowBrand, (state) => {
        state.followBrands = [];
      });
  }
);
