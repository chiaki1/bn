export interface SoaProductInterface {
  bundlePackSelectedItems: any[];
}

export const SoaProductFactory = (): SoaProductInterface => ({
  bundlePackSelectedItems: [],
});
