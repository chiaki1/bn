import { createReducer } from '@reduxjs/toolkit';

import { SoaProductFactory } from './product.state';

export const soaProductReducer = createReducer(
  SoaProductFactory(),
  (builder) => {}
);
