import {
  addWishListAfterAction,
  addWishListErrorAction,
  removeWishListAfterAction,
  removeWishListErrorAction,
} from '@vjcspy/r/build/modules/account/store/wishlisht/wishlist.actions';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import { EMPTY } from 'rxjs';
import { map } from 'rxjs/operators';

import { toastErrorMessage } from '../../../../modules/ui/util/toast/toastErrorMessage';
import { toastInfoMessage } from '../../../../modules/ui/util/toast/toastInfoMessage';

const addToWishlistAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(addWishListAfterAction),
    map(() => {
      toastInfoMessage('Đã thêm vào danh sách yêu thích thành công.');
      return EMPTY;
    })
  )
);

const addToWishlistError$ = createEffect((action$) =>
  action$.pipe(
    ofType(addWishListErrorAction),
    map(() => {
      toastErrorMessage('Có lỗi xảy ra vui lòng thử lại');
      return EMPTY;
    })
  )
);

const removeWishlistAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(removeWishListAfterAction),
    map(() => {
      toastInfoMessage('Đã xóa sản phầm khỏi danh sách yêu thích thành công.');
      return EMPTY;
    })
  )
);

const removeWishlistError$ = createEffect((action$) =>
  action$.pipe(
    ofType(removeWishListErrorAction),
    map(() => {
      toastErrorMessage('Có lỗi xảy ra vui lòng thử lại');
      return EMPTY;
    })
  )
);

export const SOA_CATALOG_EFFECTS = [
  addToWishlistAfter$,
  addToWishlistError$,
  removeWishlistAfter$,
  removeWishlistError$,
];
