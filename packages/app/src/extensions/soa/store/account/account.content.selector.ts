import { SoaAccountContentState } from './account.content.state';

export const selectMySize = (state: {
  soa: { account: SoaAccountContentState };
}) => state.soa?.account?.mySize || [];

export const selectMySizeEditing = (state: {
  soa: { account: SoaAccountContentState };
}) => state.soa?.account?.mySizeEditing || [];

export const selectListQuestionSize = (state: {
  soa: { account: SoaAccountContentState };
}) => state.soa?.account?.listQuestionSize || [];
