export interface SoaAccountContentState {
  mySize?: [];
  mySizeEditing?: any;
  listQuestionSize?: any;
}

export const soaAccountContentStateFactory = (): SoaAccountContentState => ({
  mySize: [],
  mySizeEditing: {},
  listQuestionSize: {},
});
