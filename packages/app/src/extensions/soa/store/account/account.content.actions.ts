import {
  createAction,
  generateAction,
} from '@vjcspy/r/build/util/createAction';

const PREFIX = 'MY_SIZE';

const setSoaMySizeAccount = generateAction<{}, { data: any }>(
  'SOA_MY_SIZE_ACCOUNT',
  PREFIX
);
export const setSoaMySizeAccountAction = setSoaMySizeAccount.ACTION;
export const setSoaMySizeAccountAfterAction = setSoaMySizeAccount.AFTER;

const getListQuestionSize = generateAction<{}, { data: any }>(
  'SOA_GET_LIST_QUESTION',
  PREFIX
);
export const getListQuestionSizeAction = getListQuestionSize.ACTION;
export const getListQuestionSizeAfterAction = getListQuestionSize.AFTER;
export const getListQuestionSizeErrorAction = getListQuestionSize.ERROR;

const SOA_CLEAR_MY_SIZE_ACCOUNT = 'SOA_CLEAR_MY_SIZE_ACCOUNT';
export const soaClearMySizeAccount = createAction(
  SOA_CLEAR_MY_SIZE_ACCOUNT,
  PREFIX
);

const SOA_MY_SET_SIZE_EDITING_ACCOUNT = 'SOA_MY_SIZE_EDITING_ACCOUNT';

export const soaSetMySizeEditingAccount = createAction<{
  data: any;
}>(SOA_MY_SET_SIZE_EDITING_ACCOUNT, PREFIX);

const SOA_MY_CLEAR_SIZE_EDITING_ACCOUNT = 'SOA_MY_CLEAR_SIZE_EDITING_ACCOUNT';

export const soaClearMySizeEditingAccount = createAction(
  SOA_MY_CLEAR_SIZE_EDITING_ACCOUNT,
  PREFIX
);
