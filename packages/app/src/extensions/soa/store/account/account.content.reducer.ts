import { createReducer } from '@reduxjs/toolkit';

import {
  getListQuestionSizeAfterAction,
  setSoaMySizeAccountAfterAction,
  soaClearMySizeAccount,
  soaClearMySizeEditingAccount,
  soaSetMySizeEditingAccount,
} from './account.content.actions';
import { soaAccountContentStateFactory } from './account.content.state';

export const soaAccountReducer = createReducer(
  soaAccountContentStateFactory(),
  (builder) => {
    builder
      .addCase(setSoaMySizeAccountAfterAction, (state, action) => {
        state.mySize = action.payload.data;
      })
      .addCase(soaClearMySizeAccount, (state) => {
        state.mySize = [];
      })
      .addCase(getListQuestionSizeAfterAction, (state, action) => {
        state.listQuestionSize = action?.payload?.data;
      })
      .addCase(soaSetMySizeEditingAccount, (state, action) => {
        state.mySizeEditing = action?.payload?.data;
      })
      .addCase(soaClearMySizeEditingAccount, (state, action) => {
        state.mySizeEditing = {};
      });
  }
);
