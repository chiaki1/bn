import {
  clearCustomerTokenAfter,
  generateCustomerTokenSuccessAction,
  gotCustomerDetail,
} from '@vjcspy/r/build/modules/account/store/account.actions';
import { requestOtpErrorAction } from '@vjcspy/r/build/modules/account/store/phone/phone.actions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import { toast } from 'react-toastify';
import { EMPTY } from 'rxjs';
import { map } from 'rxjs/operators';

import { soaClearListFlowBrand } from '../brand/brand.content.actions';
import {
  setSoaMySizeAccountAfterAction,
  soaClearMySizeAccount,
} from './account.content.actions';

const generateCustomerSuccess$ = createEffect((action$) =>
  action$.pipe(
    ofType(generateCustomerTokenSuccessAction),
    map(() => {
      RouterSingleton.checkReferUrl();

      return EMPTY;
    })
  )
);

const clearListFlowBrand$ = createEffect((action$) =>
  action$.pipe(
    ofType(clearCustomerTokenAfter),
    map(() => {
      return soaClearListFlowBrand();
    })
  )
);

const getMySizeAfterLogin$ = createEffect((action$) =>
  action$.pipe(
    ofType(gotCustomerDetail),
    map((action) => {
      if (action?.payload?.customer?.sizesV2?.items) {
        return setSoaMySizeAccountAfterAction({
          data: action?.payload?.customer?.sizesV2?.items,
        });
      }

      return setSoaMySizeAccountAfterAction({
        data: [],
      });
    })
  )
);

const clearMySize$ = createEffect((action$) =>
  action$.pipe(
    ofType(clearCustomerTokenAfter),
    map(() => {
      return soaClearMySizeAccount();
    })
  )
);

const requestLoginError$ = createEffect((action$) =>
  action$.pipe(
    ofType(requestOtpErrorAction),
    map(() => {
      toast.error('Không thể đăng nhập, vui lòng kiểm tra lại kết nối.');
      return EMPTY;
    })
  )
);

export const SOA_ACCOUNT_EFFECTS = [
  generateCustomerSuccess$,
  clearListFlowBrand$,
  getMySizeAfterLogin$,
  clearMySize$,
  requestLoginError$,
];
