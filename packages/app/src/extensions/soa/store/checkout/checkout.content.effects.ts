import {
  createNewCustomerAddressAfterAction,
  createNewCustomerAddressErrorAction,
  deleteCustomerAddressAfterAction,
  deleteCustomerAddressErrorAction,
  updateCustomerAddressAfterAction,
  updateCustomerAddressErrorAction,
} from '@vjcspy/r/build/modules/account/store/customer-address/actions';
import { addProductsToCartTypeConfigurableError } from '@vjcspy/r/build/modules/checkout/store/cart/actions/add-type/configurable';
import {
  checkoutCartDetailRemoveItemAfterAction,
  checkoutCartDetailRemoveItemErrorAction,
  updateCartItemError,
} from '@vjcspy/r/build/modules/checkout/store/cart/actions/detail.actions';
import { placeOrderError } from '@vjcspy/r/build/modules/checkout/store/cart/actions/order.actions';
import {
  reorderCartAfterAction,
  reorderCartErrorAction,
} from '@vjcspy/r/build/modules/checkout/store/cart/actions/reorder.actions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import { EMPTY } from 'rxjs';
import { map } from 'rxjs/operators';

import { toastErrorMessage } from '../../../../modules/ui/util/toast/toastErrorMessage';
import { toastInfoMessage } from '../../../../modules/ui/util/toast/toastInfoMessage';
import { AppRoute } from '../../../../router/Route';

const deleteAddressAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(deleteCustomerAddressAfterAction),
    map((action) => {
      toastInfoMessage('Đã xóa khỏi sổ địa chỉ');
      return EMPTY;
    })
  )
);

const deleteAddressError$ = createEffect((action$) =>
  action$.pipe(
    ofType(deleteCustomerAddressErrorAction),
    map((action) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại sau.');
      return EMPTY;
    })
  )
);
const reorderCartAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(reorderCartAfterAction),
    map((action) => {
      toastInfoMessage('Đã mua lại đơn hàng thành công');
      RouterSingleton.push('/' + AppRoute.CART_PAGE);
      return EMPTY;
    })
  )
);
const reorderCartError$ = createEffect((action$) =>
  action$.pipe(
    ofType(reorderCartErrorAction),
    map((action) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

const createAddressAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(createNewCustomerAddressAfterAction),
    map((action) => {
      toastInfoMessage('Tạo mới địa chỉ thành công.');
      return EMPTY;
    })
  )
);

const createAddressError$ = createEffect((action$) =>
  action$.pipe(
    ofType(createNewCustomerAddressErrorAction),
    map((action) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

const updateAddressAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(updateCustomerAddressAfterAction),
    map((action) => {
      toastInfoMessage('Update địa chỉ thành công.');
      return EMPTY;
    })
  )
);

const updateAddressError$ = createEffect((action$) =>
  action$.pipe(
    ofType(updateCustomerAddressErrorAction),
    map((error) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

const removeItemInCartAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(checkoutCartDetailRemoveItemAfterAction),
    map((action) => {
      toastInfoMessage('Đã xóa sản phẩm khỏi giỏ hàng.');
      return EMPTY;
    })
  )
);

const removeItemInCartError$ = createEffect((action$) =>
  action$.pipe(
    ofType(checkoutCartDetailRemoveItemErrorAction),
    map((error) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

// const placeOrderAfter$ = createEffect((action$) =>
//   action$.pipe(
//     ofType(placeOrderAfter),
//     map((action) => {
//       RouterSingleton.push('/' + AppRoute.CHECKOUT_COMPLETE);
//       return EMPTY;
//     })
//   )
// );

const placeOrderError$ = createEffect((action$) =>
  action$.pipe(
    ofType(placeOrderError),
    map((error) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

const addProductToCartError$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCartTypeConfigurableError),
    map((error) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

const updateCartItemError$ = createEffect((action$) =>
  action$.pipe(
    ofType(updateCartItemError),
    map((error) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);
const checkoutCartDetailRemoveItemError$ = createEffect((action$) =>
  action$.pipe(
    ofType(checkoutCartDetailRemoveItemErrorAction),
    map((error) => {
      toastErrorMessage('Có lỗi xảy ra, vui lòng thử lại.');
      return EMPTY;
    })
  )
);

export const SOA_CHECKOUT_EFFECTS = [
  deleteAddressAfter$,
  deleteAddressError$,
  reorderCartAfter$,
  reorderCartError$,
  createAddressAfter$,
  createAddressError$,
  updateAddressAfter$,
  updateAddressError$,
  removeItemInCartAfter$,
  removeItemInCartError$,
  // placeOrderAfter$,
  placeOrderError$,
  addProductToCartError$,
  updateCartItemError$,
  checkoutCartDetailRemoveItemError$,
];
