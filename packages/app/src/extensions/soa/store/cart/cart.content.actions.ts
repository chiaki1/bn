import { generateAction } from '@vjcspy/r/build/util/createAction';

const PREFIX = 'soa_cart';

const addListRemoveItemCart = generateAction<{ data: any }>(
  'ADD_LIST_REMOVE_ITEM_CART',
  PREFIX
);

export const addListRemoveItemCartAction = addListRemoveItemCart.ACTION;
// export const getCustomerAddressAfterAction = getCustomerAddress.AFTER;
// export const getCustomerAddressErrorAction = getCustomerAddress.ERROR;

const removeListRemoveItemCart = generateAction<{}>(
  'REMOVE_LIST_REMOVE_ITEM_CART',
  PREFIX
);
export const removeListRemoveItemCartAction = removeListRemoveItemCart.ACTION;
