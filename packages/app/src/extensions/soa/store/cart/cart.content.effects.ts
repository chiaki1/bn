import { updateMultiCartItemAfterAction } from '@vjcspy/r/build/modules/checkout/store/cart/actions/detail.actions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import { EMPTY } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppRoute } from '../../../../router/Route';
import { removeListRemoveItemCartAction } from './cart.content.actions';

const updateCartItemAfterActionAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(updateMultiCartItemAfterAction),
    map((action) => {
      RouterSingleton.push('/' + AppRoute.CHECKOUT);
      return removeListRemoveItemCartAction({});
    })
  )
);

export const SOA_CART_EFFECTS = [updateCartItemAfterActionAfter$];
