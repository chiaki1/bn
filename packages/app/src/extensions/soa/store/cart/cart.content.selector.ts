import { SoaCartContentState } from './cart.content.state';

export const selectListRemoveItemCartData = (state: {
  soa: { cart: SoaCartContentState };
}) => state.soa?.cart?.listRemoveItem;
