import { createReducer } from '@reduxjs/toolkit';

import {
  addListRemoveItemCartAction,
  removeListRemoveItemCartAction,
} from './cart.content.actions';
import { soaCartContentStateFactory } from './cart.content.state';

export const soaCartReducer = createReducer(
  soaCartContentStateFactory(),
  (builder) => {
    builder
      .addCase(addListRemoveItemCartAction, (state, action) => {
        state.listRemoveItem = action.payload.data;
      })
      .addCase(removeListRemoveItemCartAction, (state) => {
        state.listRemoveItem = [];
      });
  }
);
