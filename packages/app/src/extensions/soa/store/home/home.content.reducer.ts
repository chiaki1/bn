import { createReducer } from '@reduxjs/toolkit';

import {
  getListProductHotSaleDataAfter,
  soaAddDataHomePageAfterAction,
} from './home.content.actions';
import { soaHomeContentStateFactory } from './home.content.state';

export const soaHomeContentReducer = createReducer(
  soaHomeContentStateFactory(),
  (builder) => {
    builder.addCase(soaAddDataHomePageAfterAction, (state, action) => {
      state.dataHomePage = action.payload.data;
    });
    builder.addCase(getListProductHotSaleDataAfter, (state, action) => {
      state.dataListProductHotSale = action.payload.data;
    });
  }
);
