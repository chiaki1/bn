import {
  createAction,
  generateAction,
} from '@vjcspy/r/build/util/createAction';

const PREFIX = 'soa_homepage';

const SOA_ADD_DATA_HOME_PAGE = 'SOA_ADD_DATA_HOME_PAGE';
export const soaAddDataHomePage = createAction(SOA_ADD_DATA_HOME_PAGE, PREFIX);

const SOA_ADD_DATA_HOME_PAGE_AFTER = 'SOA_ADD_DATA_HOME_PAGE_AFTER';
export const soaAddDataHomePageAfterAction = createAction<{
  data: any;
}>(SOA_ADD_DATA_HOME_PAGE_AFTER, PREFIX);

const SOA_ADD_DATA_HOME_PAGE_ERROR = 'SOA_ADD_DATA_HOME_PAGE_ERROR';
export const soaAddDataHomePageErrorAction = createAction<{
  error: Error;
}>(SOA_ADD_DATA_HOME_PAGE_ERROR, PREFIX);

const GET_LIST_PRODUCT_HOT_SALE_DATA = 'GET_LIST_PRODUCT_HOT_SALE_DATA';
export const getListProductHotSaleData = createAction<{
  dataFilter: any;
}>(GET_LIST_PRODUCT_HOT_SALE_DATA, PREFIX);

const GET_LIST_PRODUCT_HOT_SALE_DATA_AFTER =
  'GET_LIST_PRODUCT_HOT_SALE_DATA_AFTER';
export const getListProductHotSaleDataAfter = createAction<{
  data: any;
}>(GET_LIST_PRODUCT_HOT_SALE_DATA_AFTER, PREFIX);

const GET_LIST_PRODUCT_HOT_SALE_DATA_ERROR =
  'GET_LIST_PRODUCT_HOT_SALE_DATA_ERROR';
export const getListProductHotSaleDataError = createAction<{
  error: Error;
}>(GET_LIST_PRODUCT_HOT_SALE_DATA_ERROR, PREFIX);
