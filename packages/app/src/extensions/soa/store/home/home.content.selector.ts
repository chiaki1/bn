import { SoaHomeContentState } from './home.content.state';

export const selectHomePageData = (state: {
  soa: { home: SoaHomeContentState };
}) => state.soa?.home?.dataHomePage;

export const selectHomeTopBannerData = (state: {
  soa: { home: SoaHomeContentState };
}) => state.soa?.home?.dataHomePage?.top_banner_block || {};

export const selectHomeFlashSaleData = (state: {
  soa: { home: SoaHomeContentState };
}) => state.soa?.home?.dataHomePage?.flash_sale_block_array || [];

export const selectHomeCategoryListData = (state: {
  soa: { home: SoaHomeContentState };
}) => state.soa?.home?.dataHomePage?.category_list_block || {};

export const selectHomeAdditionalData = (state: {
  soa: { home: SoaHomeContentState };
}) => state.soa?.home?.dataHomePage?.additional_blocks || [];

export const selectListProductHotSale = (state: {
  soa: { home: SoaHomeContentState };
}) => state.soa.home.dataListProductHotSale;
