import './assets/styles/main.scss';

import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';
import { UiManager } from '@vjcspy/ui-extension';
import { storeManager } from '@vjcspy/web-r';
import { combineReducers } from 'redux';

import { SOA_CPT_EXT_CFG } from './components';
import { SOA_ACCOUNT_PAGE_EXT_CFG } from './components/account';
import { SOA_ACCOUNT_INVITE_EXT_CFG } from './components/account/account-invite';
import { SOA_BRAND_DETAIL_EXT_CFG } from './components/brand/detail';
import { SOA_CART_EXT_CFG } from './components/cart';
import { SOA_CHECKOUT_PAGE_EXT_CFG } from './components/checkout';
import { SOA_PAGES_EXT_CFG } from './pages';
import { soaConfigRouter } from './router';
import { SOA_ACCOUNT_EFFECTS } from './store/account/account.content.effects';
import { soaAccountReducer } from './store/account/account.content.reducer';
import { SOA_BRAND_EFFECTS } from './store/brand/brand.content.effects';
import { soaBrandContentReducer } from './store/brand/brand.content.reducer';
import { SOA_CART_EFFECTS } from './store/cart/cart.content.effects';
import { soaCartReducer } from './store/cart/cart.content.reducer';
import { SOA_CHECKOUT_EFFECTS } from './store/checkout/checkout.content.effects';
import { soaHomeContentReducer } from './store/home/home.content.reducer';
import { soaProductReducer } from './store/product/product.reducer';
import { SOA_CATALOG_EFFECTS } from './store/product/products.effects';

UiManager.config({
  extensionConfigs: [
    ...SOA_PAGES_EXT_CFG,
    ...SOA_CPT_EXT_CFG,
    ...SOA_BRAND_DETAIL_EXT_CFG,
    ...SOA_ACCOUNT_PAGE_EXT_CFG,
    ...SOA_ACCOUNT_INVITE_EXT_CFG,
    ...SOA_CART_EXT_CFG,
    ...SOA_CHECKOUT_PAGE_EXT_CFG,
  ],
});

storeManager.mergeReducers({
  soa: combineReducers({
    home: soaHomeContentReducer,
    brand: soaBrandContentReducer,
    cart: soaCartReducer,
    product: soaProductReducer,
    account: soaAccountReducer,
  }),
});

storeManager.addEpics('soa-brand', [...SOA_BRAND_EFFECTS]);
storeManager.addEpics('soa-checkout', [...SOA_CHECKOUT_EFFECTS]);
storeManager.addEpics('soa-account', [...SOA_ACCOUNT_EFFECTS]);
storeManager.addEpics('soa-catalog', [...SOA_CATALOG_EFFECTS]);
storeManager.addEpics('soa-cart', [...SOA_CART_EFFECTS]);

// Routes
soaConfigRouter();

export function bootSOA() {
  DataValueExtension.add(
    'tokyo',
    'COMMON',
    {
      BRAND_NAME: 'SaleOff.Asia',
      CONFIG_PREFIX: 'SOA_',
    },
    10
  );
}
