export const BLOCK_CAMPAIGN_SLIDER = {
  type: 'banner_campaign',
  component_banner: 'campaign_slider',
};

export const CMS_CONTENT_CAMPAIGN = {
  type: 'banner_campaign',
  component_banner: 'cms_content',
};
export const BLOCK_CAMPAIGN_IMAGE_LISTING = {
  type: 'banner_campaign',
  component_banner: 'image_listing',
};

export const TYPES_CONTENT_BANNER = 'banner_campaign';
export const TYPES_CONTENT_FLASH_SALE = 'flash_sale';

export const TYPE_PRODUCT_OPTION_SIZE = 'size';
export const TYPE_PRODUCT_OPTION_COLOR = 'color';
export const INFORMATION_SECURITY_POLICY = 'information-security-policy';
export const POLICY_RULE = 'policy-rule';
export const ABOUT = 'about';
export const SIZE_GUIDE = 'size-guide';
export const SHOPPING = 'shopping';
export const PAY = 'pay';
export const TRANSPORT = 'transport';
export const EXCHANGE_AND_REFUND = 'exchange-and-refund';
export const OTHER = 'other';
