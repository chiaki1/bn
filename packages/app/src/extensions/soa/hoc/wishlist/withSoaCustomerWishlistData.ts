import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaCustomerWishlistData } from '../../hook/wishlist/useSoaCustomerWishlistData';

export const withSoaCustomerWishlistData = createUiHOC(
  () => useSoaCustomerWishlistData(),
  'withSoaCustomerWishlistData'
);
