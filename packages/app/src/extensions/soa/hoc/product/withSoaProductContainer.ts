import { useSoaProductDetailByUrlKeyQuery } from '@vjcspy/apollo-sale-off';
import { useProductData } from '@vjcspy/r/build/modules/catalog/hook/product/useProductData';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withSoaProductContainer = createUiHOC(() => {
  return useProductData(useSoaProductDetailByUrlKeyQuery as any);
}, 'withSoaProductContainer');
