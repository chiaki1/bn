import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaProductBundlePack } from '../../hook/product/useSoaProductBundlePack';

export const withSoaProductBundlePack = createUiHOC(
  () => useSoaProductBundlePack(),
  'withSoaProductBundlePack'
);
