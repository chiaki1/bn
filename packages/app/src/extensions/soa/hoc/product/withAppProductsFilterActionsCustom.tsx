import { useResolveProductsFilters } from '@vjcspy/r/build/modules/catalog/hook/products/useResolveProductsFilters';
import { productsClearFiltersCustom } from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

export const withAppProductsFilterActionsCustom = createUiHOC(() => {
  const dispatch = useDispatch();
  const { filters } = useResolveProductsFilters();

  const clearFiltersCustom = useCallback(() => {
    let filterx: any[] = [];
    if (filters) {
      filterx = filters.filter((it: any) => it?.type);
    }
    dispatch(productsClearFiltersCustom({ filters: filterx }));
  }, []);
  return {
    actions: {
      clearFiltersCustom,
    },
  };
}, 'withAppProductsFilterActionsCustom');
