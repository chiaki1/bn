import { useGetAppVersionLazyQuery } from '@vjcspy/apollo-sale-off';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useEffect, useState } from 'react';

import COMMON from '../../../../values/extendable/COMMON';

export const withAppVersion = createUiHOC(() => {
  const [hasNewVersion, setHasNewVersion] = useState(false);
  const [versionQuery, versionRes] = useGetAppVersionLazyQuery({
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    versionQuery();
  }, []);

  useEffect(() => {
    if (
      versionRes?.data?.getVersion?.version &&
      parseFloat(versionRes?.data?.getVersion?.version + '') >
        parseFloat(COMMON.r('APP_VERSION'))
    ) {
      setHasNewVersion(true);
    }
  }, [versionRes?.data]);

  return { hasNewVersion };
}, 'withAppVersion');
