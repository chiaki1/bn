import { createUiHOC } from '@vjcspy/ui-extension';

import { useClearCartActions } from '../../hook/cart/useClearCartActions';

export const withClearCartActions = createUiHOC(
  () => useClearCartActions(),
  'wittClearCartActions'
);
