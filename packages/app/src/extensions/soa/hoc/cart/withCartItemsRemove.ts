import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { removeListRemoveItemCartAction } from '../../store/cart/cart.content.actions';
import { selectListRemoveItemCartData } from '../../store/cart/cart.content.selector';

export const withCartItemsRemove = createUiHOC(() => {
  const dispatch = useDispatch();
  const listRemoveStore = useSelector(selectListRemoveItemCartData);
  const clearListItemRemoveStore = useCallback(() => {
    dispatch(removeListRemoveItemCartAction());
  }, []);

  return {
    state: { listRemoveStore },
    actions: { clearListItemRemoveStore },
  };
}, 'withCartItemsRemoveData');
