import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect, useMemo, useState } from 'react';

export const withCancelOrderListBrandData = createUiHOC((props) => {
  const [listSelectItem, setListSelectItem] = useState<any[]>([]);
  const [random, setRandom] = useState<any>('');
  const [listSelectItemWithQty, setListSelectItemWithQty] = useState<any>({});

  const listBrand = useMemo(() => {
    const listBrands: any[] = [];
    if (props?.orderDetail?.items) {
      props?.orderDetail?.items
        // .filter((it: any) => it?.product?.brand?.brand_id)
        .forEach((item: any, index: number) => {
          if (item?.brand?.brand_id) {
            listBrands[item?.brand?.brand_id] = {
              ...item?.brand,
              items: [],
              countItem: 0,
            };
          } else {
            listBrands[0] = {
              brand_id: null,
              name: 'Other',
              items: [],
              countItem: 0,
            };
          }
        });
    }
    return listBrands;
  }, [props?.orderDetail?.items]);

  const listItemShow = useMemo(() => {
    let listItems: any[] = [];
    const brand: any[] = listBrand;

    if (
      brand &&
      props?.orderDetail?.items &&
      props?.orderDetail?.items?.length > 0 &&
      Object.values(brand).length > 0
    ) {
      listItems = props?.orderDetail?.items.reduce((result: any, item: any) => {
        if (item?.brand?.brand_id && result[item?.brand?.brand_id]) {
          const itemProducts = result[item?.brand?.brand_id]?.items;
          itemProducts.push(item);
          result[item?.brand?.brand_id] = {
            ...result[item?.brand?.brand_id],
            items: itemProducts,
            countItem: result[item?.brand?.brand_id]?.countItem + 1,
          };

          return result;
        } else {
          const itemProducts = result[0]?.items;
          itemProducts.push(item);
          result[0] = {
            ...result[0],
            items: itemProducts,
            countItem: result[0]?.countItem + 1,
          };
          return result;
        }
        return result;
      }, brand);
    }
    return listItems?.reverse()?.filter((item: any) => item?.name);
  }, [props?.orderDetail?.items, listBrand]);

  const toggleItem = useCallback(
    (itemId: any, qty: any) => {
      if (listSelectItem.includes(itemId)) {
        setListSelectItem(
          listSelectItem.filter((item: any) => item !== itemId)
        );

        setListSelectItemWithQty((prevState: any) => {
          return (
            Object.keys(prevState)
              .filter((key) => key !== itemId)
              .reduce(
                (res: any, key) => ((res[key] = prevState[key]), res),
                {}
              ) || {}
          );
        });
      } else {
        setListSelectItem((prevState) => [...prevState, itemId]);
        setListSelectItemWithQty((prevState: any) => {
          prevState[itemId] = qty;
          return prevState;
        });
      }
    },
    [listSelectItem]
  );

  const checkedBrand = useCallback(
    (brandId: any) => {
      let check = true;
      if (listSelectItem && listItemShow) {
        const dataBrand = listItemShow?.filter(
          (item: any) => item?.brand_id === brandId
        );
        check = true;
        if (dataBrand && dataBrand[0]?.items) {
          dataBrand[0]?.items.forEach((it: any) => {
            if (listSelectItem.includes(it?.id)) {
              check = false;
            }
            return check;
          });
        }
      }
      return check;
    },
    [listSelectItem, listItemShow]
  );

  const unCheckBrandAction = useCallback(
    (brandId: any, status: boolean) => {
      if (listItemShow) {
        const dataBrand = listItemShow?.filter(
          (item: any) => item?.brand_id === brandId
        );

        if (dataBrand && dataBrand[0]?.items) {
          const listRemove: any[] = [];
          dataBrand[0]?.items.forEach((it: any) => {
            if (!status) {
              listRemove.push(it?.id);
            } else {
              listRemove.push(it?.id);
            }
          });

          if (!status) {
            setListSelectItem((prevState) =>
              prevState.filter((check: any) => !listRemove.includes(check))
            );
          } else {
            setListSelectItem((prevState) => prevState.concat(listRemove));
          }
        }
      }
    },
    [listItemShow]
  );

  const getQtyItem = useCallback(
    (itemId: any, qty: any) => {
      if (listSelectItemWithQty[itemId]) {
        return listSelectItemWithQty[itemId];
      } else {
        return qty;
      }
    },
    [listSelectItemWithQty]
  );

  const onchangeQtyItem = useCallback(
    (itemId: any, qty: any) => {
      const newObj = listSelectItemWithQty;
      if (newObj[itemId]) {
        newObj[itemId] = qty;
        setListSelectItemWithQty(newObj);
        setRandom(Math.floor(10 + Math.random() * 90));
      }
    },
    [listSelectItemWithQty]
  );

  return {
    state: {
      listBrand,
      listItemShow,
      listSelectItem,
      listSelectItemWithQty,
      random,
    },
    actions: {
      toggleItem,
      checkedBrand,
      unCheckBrandAction,
      onchangeQtyItem,
      getQtyItem,
    },
  };
}, 'withCancelOrderListBrandData');
