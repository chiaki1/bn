import { selectCart } from '@vjcspy/r/build/modules/checkout/store/cart/cart.selector';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { addListRemoveItemCartAction } from '../../store/cart/cart.content.actions';
import { selectListRemoveItemCartData } from '../../store/cart/cart.content.selector';

export const withCartListBrandData = createUiHOC(() => {
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);
  const listRemoveStore = useSelector(selectListRemoveItemCartData);
  const [listRemoveItem, setListRemoveItem] = useState<any[]>(
    listRemoveStore || []
  );
  const [unCheckAllItem, setUnCheckAllItem] = useState<boolean>(false);

  useEffect(() => {
    const checkItem: any[] = [];
    if (cart?.items && cart?.items.length > 0 && listRemoveItem.length > 0) {
      cart?.items.forEach((ca: any) => {
        if (listRemoveItem.includes(ca?.id)) {
          checkItem.push(ca?.id);
        }
      });
      setListRemoveItem(checkItem);
    }
  }, [cart?.items]);

  useEffect(() => {
    if (cart?.items && listRemoveItem.length > 0) {
      setUnCheckAllItem(true);
    } else {
      setUnCheckAllItem(false);
    }
  }, [listRemoveItem]);

  useEffect(() => {
    dispatch(addListRemoveItemCartAction({ data: listRemoveItem }));
  }, [listRemoveItem]);

  const listBrand = useMemo(() => {
    const listBrands: any[] = [];
    if (cart?.items) {
      cart?.items
        // .filter((it: any) => it?.product?.brand?.brand_id)
        .forEach((item: any, index: number) => {
          if (item?.product?.brand?.brand_id) {
            listBrands[item?.product?.brand?.brand_id] = {
              ...item?.product?.brand,
              items: [],
              countItem: 0,
              totalsBrand: 0,
            };
          } else {
            listBrands[0] = {
              brand_id: null,
              name: 'Other',
              items: [],
              countItem: 0,
              totalsBrand: 0,
            };
          }
        });
    }
    return listBrands;
  }, [cart?.items]);

  const listItemShow = useMemo(() => {
    let listItems: any[] = [];
    const brand: any[] = listBrand;

    if (
      brand &&
      cart?.items &&
      cart?.items?.length > 0 &&
      Object.values(brand).length > 0
    ) {
      listItems = cart?.items.reduce((result: any, item: any) => {
        if (
          item?.product?.brand?.brand_id &&
          result[item?.product?.brand?.brand_id]
        ) {
          const itemProducts = result[item?.product?.brand?.brand_id]?.items;
          itemProducts.push(item);
          result[item?.product?.brand?.brand_id] = {
            ...result[item?.product?.brand?.brand_id],
            items: itemProducts,
            countItem: result[item?.product?.brand?.brand_id]?.countItem + 1,
            totalsBrand:
              result[item?.product?.brand?.brand_id]?.totalsBrand +
              parseInt(item?.prices?.row_total_including_tax?.value),
          };

          return result;
        } else {
          const itemProducts = result[0]?.items;
          itemProducts.push(item);
          result[0] = {
            ...result[0],
            items: itemProducts,
            countItem: result[0]?.countItem + 1,
            totalsBrand:
              result[0]?.totalsBrand +
              parseInt(item?.prices?.row_total_including_tax?.value),
          };
          return result;
        }
        return result;
      }, brand);
    }
    return listItems?.reverse()?.filter((item: any) => item?.name);
  }, [cart?.items, listBrand]);

  const toggleItemCart = useCallback(
    (itemId: any) => {
      if (listRemoveItem.includes(itemId)) {
        setListRemoveItem(
          listRemoveItem.filter((item: any) => item !== itemId)
        );
      } else {
        setListRemoveItem((prevState) => [...prevState, itemId]);
      }
    },
    [listRemoveItem]
  );

  const unCheckAllBrandAction = useCallback(() => {
    if (cart?.items && !unCheckAllItem) {
      // setUnCheckAllItem(true);
      const allItems = cart?.items.reduce((result: any, item: any) => {
        result.push(item?.id);
        return result;
      }, []);
      setListRemoveItem(allItems);
    } else {
      setListRemoveItem([]);
    }
  }, [cart?.items, unCheckAllItem]);

  const unCheckBrandAction = useCallback(
    (brandId: any, status: boolean) => {
      if (cart?.items && listItemShow) {
        const dataBrand = listItemShow?.filter(
          (item: any) => item?.brand_id === brandId
        );

        if (dataBrand && dataBrand[0]?.items) {
          const listRemove: any[] = [];
          dataBrand[0]?.items.forEach((it: any) => {
            if (!status) {
              listRemove.push(it?.id);
            } else {
              listRemove.push(it?.id);
            }
          });

          if (!status) {
            setListRemoveItem((prevState) =>
              prevState.filter((check: any) => !listRemove.includes(check))
            );
          } else {
            setListRemoveItem((prevState) => prevState.concat(listRemove));
          }
        }
      }
    },
    [listItemShow]
  );

  const checkedBrand = useCallback(
    (brandId: any) => {
      let check = true;
      if (cart?.items && listRemoveItem && listItemShow) {
        const dataBrand = listItemShow?.filter(
          (item: any) => item?.brand_id === brandId
        );
        check = true;
        if (dataBrand && dataBrand[0]?.items) {
          dataBrand[0]?.items.forEach((it: any) => {
            if (listRemoveItem.includes(it?.id)) {
              check = false;
            }
            return check;
          });
        }
      }
      return check;
    },
    [listRemoveItem, listItemShow]
  );

  return {
    state: { listItemShow, listRemoveItem, unCheckAllItem },
    actions: {
      toggleItemCart,
      unCheckAllBrandAction,
      unCheckBrandAction,
      checkedBrand,
    },
  };
}, 'withCartListBrandData');
