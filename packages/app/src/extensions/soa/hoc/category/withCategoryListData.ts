import { createUiHOC } from '@vjcspy/ui-extension';

import { useCategoryListData } from '../../hook/category/useCatgoryListData';

export const withCategoryListData = createUiHOC(
  () => useCategoryListData(),
  'withCategoryListData'
);
