import { createUiHOC } from '@vjcspy/ui-extension';
import { useNavigator } from '@vjcspy/r/build/modules/content/hook/useNavigator';

export const withSoaAppNavigator = createUiHOC(
  () => useNavigator('SOA_APP_NAVIGATOR'),
  'withSoaAppNavigator'
);
