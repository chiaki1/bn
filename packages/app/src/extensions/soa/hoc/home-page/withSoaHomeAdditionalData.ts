import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectHomeAdditionalData } from '../../store/home/home.content.selector';

export const withSoaHomeAdditionalData = createUiHOC(() => {
  return { additionalBlockData: useSelector(selectHomeAdditionalData) };
}, 'withSoaHomeAdditionalData');
