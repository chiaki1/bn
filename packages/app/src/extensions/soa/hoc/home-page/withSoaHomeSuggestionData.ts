import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useGetSoaListSuggestProductHomepageLazyQuery } from '@vjcspy/apollo-sale-off';
import { getListProductViewed } from '@vjcspy/r/build/modules/catalog/util/getProductFromLocalStorage';
import { createUiHOC } from '@vjcspy/ui-extension';
import debounce from 'lodash/debounce';
import { useCallback, useEffect, useState } from 'react';

export const withSoaHomeSuggestionData = createUiHOC(() => {
  const [pageInfo, setPageInfo] = useState<any>({});
  const [dataSuggestionProduct, setDataSuggestionProduct] = useState<any[]>([]);
  const [
    getSoaListSuggestProductHomepageQuery,
    getSoaListSuggestProductHomepageRes,
  ] = useGetSoaListSuggestProductHomepageLazyQuery({
    fetchPolicy: FetchPolicyResolve.NO_CACHE,
  });

  const getProductSuggest = useCallback(async (pageCurrent: any) => {
    const skuViewed: any[] = [];
    const listProducts = await getListProductViewed();

    if (
      listProducts &&
      Array.isArray(listProducts) &&
      listProducts.length > 0
    ) {
      listProducts.forEach((item: any) => {
        skuViewed.push(item?.sku);
      });
    }

    getSoaListSuggestProductHomepageQuery({
      variables: {
        skus: skuViewed,
        pageSize: 20,
        currentPage: pageCurrent,
      },
    });
  }, []);

  useEffect(() => {
    if (getSoaListSuggestProductHomepageRes?.error) {
      console.warn(
        'Can"t get data suggestion home page : ',
        getSoaListSuggestProductHomepageRes?.error
      );
    }

    if (getSoaListSuggestProductHomepageRes.data) {
      if (
        getSoaListSuggestProductHomepageRes.data?.getListSuggestProductHomepage
          ?.items
      ) {
        if (
          Array.isArray(
            getSoaListSuggestProductHomepageRes.data
              ?.getListSuggestProductHomepage?.items
          ) &&
          getSoaListSuggestProductHomepageRes.data
            ?.getListSuggestProductHomepage?.items.length > 0
        ) {
          const data: any[] = dataSuggestionProduct;
          getSoaListSuggestProductHomepageRes.data?.getListSuggestProductHomepage?.items.map(
            (item: any) => {
              if (!data.find((it: any) => it?.id === item?.id)) {
                data.push(item);
              }
            }
          );
          setDataSuggestionProduct(data);
        }
      }

      if (
        getSoaListSuggestProductHomepageRes.data?.getListSuggestProductHomepage
          ?.page_info
      ) {
        setPageInfo(
          getSoaListSuggestProductHomepageRes.data
            ?.getListSuggestProductHomepage?.page_info
        );
      }
    }
  }, [
    getSoaListSuggestProductHomepageRes.data,
    getSoaListSuggestProductHomepageRes.error,
  ]);

  const debounceLoadMoreDataSuggest = useCallback(
    debounce((currentPage: any) => getProductSuggest(currentPage), 1000),
    []
  );

  return {
    state: {
      dataSuggestionProduct: dataSuggestionProduct || [],
      pageInfo: pageInfo,
      isLoading: getSoaListSuggestProductHomepageRes.loading,
    },
    actions: {
      getProductSuggest,
      debounceLoadMoreDataSuggest,
    },
  };
}, 'withSoaHomeSuggestionData');
