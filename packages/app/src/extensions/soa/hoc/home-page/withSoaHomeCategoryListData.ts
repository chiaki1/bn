import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectHomeCategoryListData } from '../../store/home/home.content.selector';

export const withSoaHomeCategoryListData = createUiHOC(() => {
  return { topSearchData: useSelector(selectHomeCategoryListData) };
}, 'withSoaHomeCategoryListData');
