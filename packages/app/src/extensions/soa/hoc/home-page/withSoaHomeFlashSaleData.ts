import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectHomeFlashSaleData } from '../../store/home/home.content.selector';

export const withSoaHomeFlashSaleData = createUiHOC(() => {
  return { flashSaleData: useSelector(selectHomeFlashSaleData) };
}, 'withSoaHomeFlashSaleData');
