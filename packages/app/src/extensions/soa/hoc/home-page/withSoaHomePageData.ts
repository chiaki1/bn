import { createUiHOC } from '@vjcspy/ui-extension';
import { useSoaHomePageData } from 'src/extensions/soa/hook/home-page/useSoaHomePageData';

export const withSoaHomePageData = createUiHOC(
  () => useSoaHomePageData(),
  'withSoaHomePageData'
);
