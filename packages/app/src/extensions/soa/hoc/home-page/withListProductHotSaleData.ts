import { createUiHOC } from '@vjcspy/ui-extension';
import { useListProductHotSaleData } from 'src/extensions/soa/hook/home-page/useListProductHotSaleData';

export const withListProductHotSaleData = createUiHOC(
  (props) => useListProductHotSaleData(props),
  'withListProductHotSaleData'
);
