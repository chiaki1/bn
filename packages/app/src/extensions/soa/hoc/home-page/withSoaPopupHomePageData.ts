import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { usePopupsHomPageLazyQuery } from '@vjcspy/apollo-sale-off';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect } from 'react';

export const withSoaPopupHomePageData = createUiHOC(() => {
  const [popupsHomPageQuery, popupsHomPageRes] = usePopupsHomPageLazyQuery({
    fetchPolicy: FetchPolicyResolve.NETWORK_ONLY,
  });

  const getPopupHomePage = useCallback(() => {
    popupsHomPageQuery();
  }, []);

  useEffect(() => {
    if (popupsHomPageRes?.error) {
      console.warn('Can"t get data home page : ', popupsHomPageRes?.error);
    }
    // if (popupsHomPageRes?.data?.soHomeCmsData) {
    // }
  }, [popupsHomPageRes.error, popupsHomPageRes.data]);

  return {
    state: {
      popupsHomePage: popupsHomPageRes.data?.popups || {},
    },
    actions: {
      getPopupHomePage,
    },
  };
}, 'withSoaPopupHomePageData');
