import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectHomeTopBannerData } from '../../store/home/home.content.selector';

export const withSoaHomeTopBannerData = createUiHOC(() => {
  return { topBannerData: useSelector(selectHomeTopBannerData) };
}, 'withSoaHomeTopBannerData');
