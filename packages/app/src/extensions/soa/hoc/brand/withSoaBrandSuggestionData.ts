import { useGetSoaListSuggestProductBrandPageLazyQuery } from '@vjcspy/apollo-sale-off';
import { getListProductViewed } from '@vjcspy/r/build/modules/catalog/util/getProductFromLocalStorage';
import { createUiHOC } from '@vjcspy/ui-extension';
import debounce from 'lodash/debounce';
import { useCallback, useEffect, useState } from 'react';

export const withSoaBrandSuggestionData = createUiHOC((props) => {
  const [pageInfo, setPageInfo] = useState<any>({});
  const [dataSuggestionProduct, setDataSuggestionProduct] = useState<any[]>([]);
  const [
    getSoaListSuggestProductBrandPageQuery,
    getSoaListSuggestProductBrandPageRes,
  ] = useGetSoaListSuggestProductBrandPageLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const getProductSuggest = useCallback(async (pageCurrent: any) => {
    if (props?.brandId) {
      const skuViewed: any[] = [];
      const listProducts = await getListProductViewed();

      if (
        listProducts &&
        Array.isArray(listProducts) &&
        listProducts.length > 0
      ) {
        listProducts.forEach((item: any) => {
          skuViewed.push(item?.sku);
        });
      }

      getSoaListSuggestProductBrandPageQuery({
        variables: {
          skus: skuViewed,
          brandId: props.brandId,
          pageSize: 20,
          currentPage: pageCurrent,
        },
      });
    }
  }, []);

  useEffect(() => {
    if (getSoaListSuggestProductBrandPageRes?.error) {
      console.warn(
        'Can"t get data suggestion brand page : ',
        getSoaListSuggestProductBrandPageRes?.error
      );
    }

    if (getSoaListSuggestProductBrandPageRes.data) {
      if (
        getSoaListSuggestProductBrandPageRes.data?.getListSuggestProductBrand
          ?.items
      ) {
        if (
          Array.isArray(
            getSoaListSuggestProductBrandPageRes.data
              ?.getListSuggestProductBrand?.items
          ) &&
          getSoaListSuggestProductBrandPageRes.data?.getListSuggestProductBrand
            ?.items.length > 0
        ) {
          const data: any[] = dataSuggestionProduct;
          getSoaListSuggestProductBrandPageRes.data?.getListSuggestProductBrand?.items.map(
            (item: any) => {
              if (!data.find((it: any) => it?.id === item?.id)) {
                data.push(item);
              }
            }
          );
          setDataSuggestionProduct(data);
        }
      }

      if (
        getSoaListSuggestProductBrandPageRes.data?.getListSuggestProductBrand
          ?.page_info
      ) {
        setPageInfo(
          getSoaListSuggestProductBrandPageRes.data?.getListSuggestProductBrand
            ?.page_info
        );
      }
    }
  }, [
    getSoaListSuggestProductBrandPageRes.error,
    getSoaListSuggestProductBrandPageRes.data,
  ]);

  const debounceLoadMoreDataSuggest = useCallback(
    debounce((currentPage: any) => getProductSuggest(currentPage), 1000),
    []
  );

  return {
    state: {
      dataSuggestionProduct: dataSuggestionProduct || [],
      pageInfo: pageInfo,
      isLoading: getSoaListSuggestProductBrandPageRes.loading,
    },
    actions: {
      getProductSuggest,
      debounceLoadMoreDataSuggest,
    },
  };
}, 'withSoaHomeSuggestionData');
