import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectBrandCategoryData } from '../../store/brand/brand.content.selector';

export const withSoaBrandDetailCategoryData = createUiHOC(() => {
  const brandCategory = useSelector(selectBrandCategoryData);
  return { dataBrandDetailCategory: brandCategory };
}, 'withSoaBrandDetailCategoryData');
