import { createUiHOC } from '@vjcspy/ui-extension';
import { useBrandListData } from 'src/extensions/soa/hook/brand/useBrandListData';

export const withBrandListData = createUiHOC(
  () => useBrandListData(),
  'withBrandListData'
);
