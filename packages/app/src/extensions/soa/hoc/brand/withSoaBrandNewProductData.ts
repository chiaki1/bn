import { useGetSoaCategoryListingDataLazyQuery } from '@vjcspy/apollo-sale-off';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useEffect } from 'react';

export const withSoaBrandNewProductData = createUiHOC((props) => {
  const [getSoaCategoryListingDataQuery, getSoaCategoryListingDataRes] =
    useGetSoaCategoryListingDataLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  useEffect(() => {
    if (props?.brandId) {
      getSoaCategoryListingDataQuery({
        variables: {
          search: '',
          filters: [{ code: 'brand', data: { eq: props.brandId } }],
          pageSize: 20,
          currentPage: 1,
          // @ts-ignore
          sort: { new: 'DESC' },
        },
      });
    }
  }, [props.brandId]);

  useEffect(() => {
    if (getSoaCategoryListingDataRes?.error) {
      console.warn(
        'Can"t get data suggestion brand page : ',
        getSoaCategoryListingDataRes?.error
      );
    }
  }, [getSoaCategoryListingDataRes.error]);

  return {
    state: {
      dataNewsProductBrand:
        getSoaCategoryListingDataRes.data?.catalogCategoryListingData?.items ||
        [],
    },
  };
}, 'withSoaBrandNewProductData');
