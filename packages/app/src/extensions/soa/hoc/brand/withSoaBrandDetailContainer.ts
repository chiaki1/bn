import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaBrandDetailContainer } from '../../hook/brand/useSoaBrandDetailContainer';

export const withSoaBrandDetailContainer = createUiHOC(
  () => useSoaBrandDetailContainer(),
  'withSoaBrandDetailContainer'
);
