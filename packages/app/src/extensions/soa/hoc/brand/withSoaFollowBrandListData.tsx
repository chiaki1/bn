import { createUiHOC } from '@vjcspy/ui-extension';

import { useFollowBrandListData } from '../../hook/brand-list/useFollowBrandListData';

export const withSoaFollowBrandListData = createUiHOC(
  () => useFollowBrandListData(),
  'withSoaFollowBrandListData'
);
