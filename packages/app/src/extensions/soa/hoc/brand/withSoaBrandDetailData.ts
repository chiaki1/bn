import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectBrandData } from '../../store/brand/brand.content.selector';

export const withSoaBrandDetailData = createUiHOC(() => {
  const dataBrand = useSelector(selectBrandData);
  return { dataBrandDetail: dataBrand };
}, 'withSoaBrandDetailData');
