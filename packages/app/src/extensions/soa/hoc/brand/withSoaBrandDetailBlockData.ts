import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';

import { selectBrandBlocksData } from '../../store/brand/brand.content.selector';

export const withSoaBrandDetailBlockData = createUiHOC(() => {
  const dataBrand = useSelector(selectBrandBlocksData);
  return { dataBrandDetailBlocks: dataBrand };
}, 'withSoaBrandDetailBlockData');
