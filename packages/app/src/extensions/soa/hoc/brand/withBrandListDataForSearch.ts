import { createUiHOC } from '@vjcspy/ui-extension';

import { useBrandListDataForSearch } from '../../hook/brand/useBrandListDataForSearch';

export const withBrandListDataForSearch = createUiHOC(
  () => useBrandListDataForSearch(),
  'withBrandListDataForSearch'
);
