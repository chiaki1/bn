import { createUiHOC } from '@vjcspy/ui-extension';

import { useFollowBrandListActions } from '../../hook/brand-list/useFollowBrandListActions';

export const withSoaFollowBrandListActions = createUiHOC(
  () => useFollowBrandListActions(),
  'withSoaFollowBrandListActions'
);
