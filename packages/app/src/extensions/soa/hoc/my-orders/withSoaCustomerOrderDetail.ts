import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaCustomerOrderDetail } from '../../hook/my-orders/useSoaCustomerOrderDetail';

export const withSoaCustomerOrderDetail = createUiHOC(() => {
  return useSoaCustomerOrderDetail();
}, 'withSoaCustomerOrderDetail');
