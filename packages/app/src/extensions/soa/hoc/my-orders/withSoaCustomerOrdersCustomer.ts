import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaCustomerOrdersCustomer } from '../../hook/my-orders/useSoaCustomerOrdersCustomer';

export const withSoaCustomerOrdersCustomer = createUiHOC(() => {
  return useSoaCustomerOrdersCustomer();
}, 'withCustomerOrdersCustomer');
