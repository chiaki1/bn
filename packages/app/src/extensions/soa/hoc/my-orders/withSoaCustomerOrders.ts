import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaCustomerOrders } from '../../hook/my-orders/useSoaCustomerOrders';

export const withSoaCustomerOrders = createUiHOC(() => {
  return useSoaCustomerOrders();
}, 'withSoaCustomerOrders');
