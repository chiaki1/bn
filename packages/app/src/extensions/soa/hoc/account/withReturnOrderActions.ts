import { useRequestReturnMutation } from '@vjcspy/apollo-sale-off';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { toastInfoMessage } from '../../../../modules/ui/util/toast/toastInfoMessage';

export const withReturnOrderActions = createUiHOC(() => {
  const [statusButton, setStatusButton] = useState(false);
  const [statusAction, setStatusAction] = useState(false);
  const [requestReturnMutation, requestReturnRes] = useRequestReturnMutation();
  const saveReturnOrder = useCallback((input: any) => {
    setStatusButton(true);
    requestReturnMutation({
      variables: {
        input,
      },
    }).catch((e) => {
      console.log(e);
      toastInfoMessage('Có lỗi xảy ra vui lòng thử lại sau');
    });
  }, []);

  useEffect(() => {
    if (requestReturnRes.error) {
      console.log('Save request return error');
      setStatusButton(false);
      toastInfoMessage('Trả hàng không thành công!');
    }

    if (requestReturnRes?.data?.requestReturn?.returns?.items) {
      setStatusButton(false);
      setStatusAction(true);
    } else {
      setStatusButton(false);
      toastInfoMessage('Có lỗi xảy ra vui lòng thử lại sau');
    }
  }, [requestReturnRes.data, requestReturnRes.error]);

  return {
    state: {
      statusAction,
      statusButton,
    },
    actions: {
      saveReturnOrder,
    },
  };
}, 'withReturnOrderActions');
