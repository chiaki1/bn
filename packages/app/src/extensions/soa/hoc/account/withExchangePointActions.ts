import { useExchangePointToVoucherMutation } from '@vjcspy/apollo-sale-off';
import { getCustomerDetail } from '@vjcspy/r/build/modules/account/store/account.actions';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { toastErrorMessage } from '../../../../modules/ui/util/toast/toastErrorMessage';
import { toastInfoMessage } from '../../../../modules/ui/util/toast/toastInfoMessage';

export const withExchangePointActions = createUiHOC(() => {
  const dispatch = useDispatch();

  const [exchangePointToVoucherMutation, exchangePointToVoucherRes] =
    useExchangePointToVoucherMutation();

  const exchangePointActions = useCallback((exchangeId: any) => {
    exchangePointToVoucherMutation({
      variables: {
        exchange_id: exchangeId,
      },
    }).catch((e: any) => {
      console.log('error exchange point');
    });
  }, []);

  useEffect(() => {
    if (exchangePointToVoucherRes.error) {
      toastErrorMessage('Đã xảy ra lỗi vui lòng thử lại.');
    }

    if (exchangePointToVoucherRes?.data?.exchangePointToVoucher?.code) {
      dispatch(getCustomerDetail());
      toastInfoMessage('Đã đổi point thành công.');
    }
  }, [exchangePointToVoucherRes.data, exchangePointToVoucherRes.error]);

  return {
    actions: {
      exchangePointActions,
    },
    state: {},
  };
}, 'withExchangePointActions');
