import { useUpdateCustomerAvatarMutation } from '@vjcspy/apollo-sale-off';
import { getCustomerDetail } from '@vjcspy/r/build/modules/account/store/account.actions';
import {
  updateCustomerInfoAction,
  updateCustomerInfoAfterAction,
  updateCustomerInfoErrorAction,
} from '@vjcspy/r/build/modules/account/store/customer-info/customer.info.actions';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

export const withAvatarAccountActions = createUiHOC(() => {
  const [updateCustomerAvatarMutation, updateCustomerAvatarRes] =
    useUpdateCustomerAvatarMutation();
  const dispatch = useDispatch();
  const saveCustomerAvatar = useCallback(async (file: any, content: any) => {
    dispatch(updateCustomerInfoAction());
    updateCustomerAvatarMutation({
      variables: {
        file,
        content,
      },
    });
  }, []);
  const getCustomerDetailActions = useCallback(() => {
    dispatch(getCustomerDetail({}));
  }, []);

  useEffect(() => {
    if (updateCustomerAvatarRes.error) {
      console.log('Save avatar error');
      dispatch(
        updateCustomerInfoErrorAction({
          error: updateCustomerAvatarRes.error,
        })
      );
    }

    if (updateCustomerAvatarRes.data) {
      console.log('Save avatar success');
      dispatch(
        updateCustomerInfoAfterAction({
          customer:
            updateCustomerAvatarRes?.data?.updateCustomerAvatar?.customer,
        })
      );
    }
  }, [updateCustomerAvatarRes.data, updateCustomerAvatarRes.error]);

  return {
    actions: {
      saveCustomerAvatar,
      getCustomerDetailActions,
    },
  };
}, 'withAvatarAccountActions');
