import {
  useEditSizeAnswerMutation,
  usePrepareAnswerOfQuestionsMutation,
  useSaveAnswerOfQuestionsMutation,
  useSetSizeAsDefaultMutation,
} from '@vjcspy/apollo-sale-off';
import { getCustomerDetail } from '@vjcspy/r/build/modules/account/store/account.actions';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';

import { AppRoute } from '../../../../router/Route';

export const withMySizesActions = createUiHOC(() => {
  const dispatch = useDispatch();
  const [tempSize, setTempSize] = useState<any>(null);

  const [prepareAnswerOfQuestionsMutation, prepareAnswerOfQuestionsRes] =
    usePrepareAnswerOfQuestionsMutation();

  const [saveAnswerOfQuestionsMutation, saveAnswerOfQuestionsRes] =
    useSaveAnswerOfQuestionsMutation();

  const [editAnswerOfQuestionsMutation, editAnswerOfQuestionsRes] =
    useEditSizeAnswerMutation();

  const [setSizeAsDefaultMutation, setSizeAsDefaultRes] =
    useSetSizeAsDefaultMutation();

  const prepareAnswer = useCallback((input: any) => {
    prepareAnswerOfQuestionsMutation({
      variables: {
        input,
      },
    });
  }, []);

  const saveAnswer = useCallback((input: any) => {
    saveAnswerOfQuestionsMutation({
      variables: {
        input,
      },
    });
  }, []);

  const editAnswer = useCallback((input: any) => {
    editAnswerOfQuestionsMutation({
      variables: {
        input,
      },
    });
  }, []);

  const setSizeAsDefault = useCallback((sizeId: any) => {
    setSizeAsDefaultMutation({
      variables: {
        sizeId,
      },
    });
  }, []);

  useEffect(() => {
    if (prepareAnswerOfQuestionsRes.error) {
      console.log('prepare Answer Of Questions Res error');
      // toast
    }

    if (prepareAnswerOfQuestionsRes?.data?.prepareAnswerOfQuestions?.size) {
      setTempSize(
        prepareAnswerOfQuestionsRes?.data?.prepareAnswerOfQuestions?.size
      );
    }
  }, [prepareAnswerOfQuestionsRes.data, prepareAnswerOfQuestionsRes.error]);

  useEffect(() => {
    if (saveAnswerOfQuestionsRes.error) {
      toast.error('Có lỗi xảy ra, vui lòng thử lại.');
    }

    if (saveAnswerOfQuestionsRes?.data?.saveAnswerOfQuestions) {
      dispatch(getCustomerDetail());
      toast.success('Đã thêm size thành công!');
      RouterSingleton.push('/' + AppRoute.MY_SIZE_PAGE);
    }
  }, [saveAnswerOfQuestionsRes.data, saveAnswerOfQuestionsRes.error]);

  useEffect(() => {
    if (setSizeAsDefaultRes.error) {
      toast.error('Có lỗi xảy ra, vui lòng thử lại.');
    }

    if (setSizeAsDefaultRes?.data?.setSizeAsDefault?.status) {
      dispatch(getCustomerDetail());
      toast.success('Đã thay đổi size mặc định thành công!');
    }

    // if (setSizeAsDefaultRes?.data?.setSizeAsDefault.status) {
    //   dispatch(getCustomerDetail());
    //   toast.success('Đã thay đổi size mặc định thành công!');
    // }
  }, [setSizeAsDefaultRes.data, setSizeAsDefaultRes.error]);

  useEffect(() => {
    if (editAnswerOfQuestionsRes.error) {
      toast.error('Có lỗi xảy ra, vui lòng thử lại.');
    }

    if (editAnswerOfQuestionsRes?.data?.editSizeAnswer) {
      dispatch(getCustomerDetail());
      toast.success('Đã thay đổi size thành công!');
      RouterSingleton.push('/' + AppRoute.MY_SIZE_PAGE);
    }
  }, [editAnswerOfQuestionsRes.data, editAnswerOfQuestionsRes.error]);

  return {
    state: {
      tempSize,
    },
    actions: { prepareAnswer, saveAnswer, setSizeAsDefault, editAnswer },
  };
}, 'withMySizesActions');
