import { createUiHOC } from '@vjcspy/ui-extension';

import { useSoaCustomerInfoActions } from '../../hook/account/useSoaCustomerInfoActions';

export const withSoaUpdateCustomerInfoActions = createUiHOC(
  () => useSoaCustomerInfoActions(),
  'withSoaUpdateCustomerInfoActions'
);
