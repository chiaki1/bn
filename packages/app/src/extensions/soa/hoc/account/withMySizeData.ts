import { useMySizeQuestionsLazyQuery } from '@vjcspy/apollo-sale-off';
import { createUiHOC } from '@vjcspy/ui-extension';
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  getListQuestionSizeAfterAction,
  soaClearMySizeEditingAccount,
  soaSetMySizeEditingAccount,
} from '../../store/account/account.content.actions';
import {
  selectListQuestionSize,
  selectMySize,
  selectMySizeEditing,
} from '../../store/account/account.content.selector';

export const withMySizeData = createUiHOC(() => {
  const dispatch = useDispatch();
  const mySizeData = useSelector(selectMySize);
  const mySizeEditingData = useSelector(selectMySizeEditing);
  const listQuestionSize = useSelector(selectListQuestionSize);
  const [mySizeQuestionsQuery, mySizeQuestionsRes] =
    useMySizeQuestionsLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  const setAddressEditing = useCallback((data: any) => {
    dispatch(soaSetMySizeEditingAccount({ data }));
  }, []);

  const clearAddressEditing = useCallback(() => {
    dispatch(soaClearMySizeEditingAccount());
  }, []);

  useEffect(() => {
    if (!listQuestionSize?.additional_question) {
      mySizeQuestionsQuery();
    }
  }, [listQuestionSize]);

  useEffect(() => {
    if (mySizeQuestionsRes.error) {
      console.log('get List Question error');
    }

    if (mySizeQuestionsRes?.data?.mySizeQuestions) {
      dispatch(
        getListQuestionSizeAfterAction({
          data: mySizeQuestionsRes?.data?.mySizeQuestions,
        })
      );
    }
  }, [mySizeQuestionsRes.data, mySizeQuestionsRes.error]);

  return {
    state: {
      mySizeData,
      listQuestionSize,
      mySizeEditingData,
    },
    actions: { setAddressEditing, clearAddressEditing },
  };
}, 'withMySizeData');
