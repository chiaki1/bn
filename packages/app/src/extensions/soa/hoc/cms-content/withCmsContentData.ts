import { createUiHOC } from '@vjcspy/ui-extension';

import { useCmsContentData } from '../../hook/account-policy/useCmsContentData';

export const withCmsContentData = createUiHOC(
  (props) => useCmsContentData(props),
  'withCmsContentData'
);
