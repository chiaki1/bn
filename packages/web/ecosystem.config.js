module.exports = {
  apps: [
    {
      name: 'bn',
      script: './server.js',
      instances: '2',
      instance_var: 'INSTANCE_ID',
      exec_mode: 'cluster',
      env: {
        NODE_ENV: 'production',
        PORT: 3005,
      },
    },
  ],
};
