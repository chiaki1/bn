import { useBedKingdomContactActions } from '@extensions/bed-kingdom/hook/common/useBedKingdomContactActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withBedKingdomContactActions = createUiHOC(() => {
  return useBedKingdomContactActions();
}, 'withBedKingdomContactActions');
