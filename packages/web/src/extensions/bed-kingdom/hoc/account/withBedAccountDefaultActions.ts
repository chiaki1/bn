import { useBedAccountDefaultActions } from '@extensions/bed-kingdom/hook/account/useBedAccountDefaultActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withBedAccountDefaultActions = createUiHOC(() => {
  return useBedAccountDefaultActions();
}, 'withBedAccountDefaultActions');
