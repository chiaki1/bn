import { useHomePageCategoryData } from '@extensions/bed-kingdom/hook/home-category/useHomePageCategoryData';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export default createUiHOC((props) => {
  return useHomePageCategoryData(props);
}, 'withHomePageCategoryData');
