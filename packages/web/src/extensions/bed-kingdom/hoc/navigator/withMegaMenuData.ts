import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useGetBedKingdomMegaMenuQuery } from '@vjcspy/apollo-bed-kingdom';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export default createUiHOC((props) => {
  const { data } = useGetBedKingdomMegaMenuQuery({
    fetchPolicy: FetchPolicyResolve.CACHE_AND_NETWORK,
    nextFetchPolicy: 'cache-first',
    variables: {
      menuId: props.megamenuId,
    },
  });

  return {
    state: {
      megamenu: data?.getMenuItems,
    },
  };
}, 'withMegaMenuData');
