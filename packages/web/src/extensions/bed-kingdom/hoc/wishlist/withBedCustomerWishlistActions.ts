import { useBedCustomerWishlistActions } from '@extensions/bed-kingdom/hook/wishlist/useBedCustomerWishlistActions';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withBedCustomerWishlistActions = createUiHOC(
  () => useBedCustomerWishlistActions(),
  'withBedCustomerWishlistActions'
);
