import { useBedCustomerWishlistData } from '@extensions/bed-kingdom/hook/wishlist/useBedCustomerWishlistData';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withBedCustomerWishlistData = createUiHOC(
  () => useBedCustomerWishlistData(),
  'withBedCustomerWishlistData'
);
