import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useGetBedKingdomCategoryDetailForListingQuery } from '@vjcspy/apollo-bed-kingdom';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { productsGotCategoryData } from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import { useUrlRewriteContext } from '@vjcspy/r/build/modules/router/context/url-rewrite';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';
// Just incase the data is unsorted, lets sort it.
const sortCrumbs = (a: any, b: any) =>
  a.category_level > b.category_level ? 1 : -1;

// Generates the path for the category.
const getPath = (path: any, suffix: any) => {
  if (path) {
    return `/${path}${suffix}`;
  }

  // If there is no path this is just a dead link.
  return '#';
};
export const withBedKingdomCategoryData = createUiHOC(() => {
  const urlRewriteContextValue = useUrlRewriteContext();

  const categoryQuery = useGetBedKingdomCategoryDetailForListingQuery({
    variables: {
      category_id: urlRewriteContextValue.urlRewriteData.id,
    },
    fetchPolicy: FetchPolicyResolve.CACHE_AND_NETWORK,
    nextFetchPolicy: 'cache-first',
  });

  useEffect(() => {
    if (!!urlRewriteContextValue.urlRewriteData.id && categoryQuery.error) {
      logger.error('Could not load bedkingdom `category` data');
    }
  }, [categoryQuery.error]);

  const dispatch = useDispatch();

  // When we have breadcrumb data sort and normalize it for easy rendering.
  const category: any = useMemo(() => {
    if (categoryQuery.data) {
      // Default to .html for when the query has not yet returned.
      const categoryUrlSuffix =
        (categoryQuery.data && categoryQuery.data.category?.url_suffix) ||
        '.html';

      const breadcrumbData = categoryQuery.data.category?.breadcrumbs;
      const breadcrumbs =
        breadcrumbData &&
        breadcrumbData
          .map((c: any) => ({
            ...c,
            category_level: c.category_level,
            text: c.category_name,
            path: getPath(c.category_url_path, categoryUrlSuffix),
          }))
          .sort(sortCrumbs);

      return {
        ...categoryQuery.data?.category!,
        breadcrumbs,
      };
    }
    return {};
  }, [categoryQuery.data, categoryQuery.loading]);

  // save to store
  useEffect(() => {
    if (categoryQuery.data) {
      dispatch(
        productsGotCategoryData({
          category,
        })
      );
    }
  }, [categoryQuery.data]);

  return { category };
}, 'withBedKingdomCategoryData');
