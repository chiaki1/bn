import { setIsOpenPopup } from '@extensions/bed-kingdom/store/content/content.content.actions';
import { selectIsPopupOpening } from '@extensions/bed-kingdom/store/content/content.content.selector';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

export const withBedStatusPopupData = createUiHOC(() => {
  const dispatch = useDispatch();
  const isOpenPopup = useSelector(selectIsPopupOpening);
  const setIsOpenPopupActions = useCallback((value: any) => {
    dispatch(setIsOpenPopup({ value }));
  }, []);
  return {
    state: {
      isOpenPopup,
    },
    actions: { setIsOpenPopupActions },
  };
}, 'withBedStatusPopupData');
