import { bedResolvedAmastyPage } from '@extensions/bed-kingdom/store/content/content.content.actions';
import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useGetAmastyPageQuery } from '@vjcspy/apollo-bed-kingdom';
import { Registry } from '@vjcspy/chitility';
import { useUrlRewriteContext } from '@vjcspy/r/build/modules/router/context/url-rewrite';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';

export const withAmastyPageContainer = createUiHOC((props) => {
  const dispath = useDispatch();
  const urlRewriteContextValue = useUrlRewriteContext();

  const { data, loading } = useGetAmastyPageQuery({
    fetchPolicy: FetchPolicyResolve.CACHE_AND_NETWORK,
    // nextFetchPolicy: 'cache-first',
    // fetchPolicy: FetchPolicyResolve.NO_CACHE,
    variables: {
      pageId: urlRewriteContextValue?.urlRewriteData?.id,
    },
  });

  useMemo(() => {
    Registry.getInstance().register('CATALOG_CATEGORY_ADDITION_FILTERS', [
      {
        code: 'landing_page_id',
        data: {
          eq: urlRewriteContextValue.urlRewriteData?.id,
        },
      },
    ]);
    return {};
  }, []);

  useEffect(() => {
    if (data?.amlanding?.page_id) {
      dispath(
        bedResolvedAmastyPage({
          amastyPage: data.amlanding,
        })
      );
    }
  }, [data?.amlanding]);

  return {
    state: {
      amastyPage: data?.amlanding,
      loading,
    },
  };
}, 'withAmastyPage');
