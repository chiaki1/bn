import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { useGetBedKingdomHomeBannerQuery } from '@vjcspy/apollo-bed-kingdom';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withHomeBannerData = createUiHOC((props: any) => {
  const { data } = useGetBedKingdomHomeBannerQuery({
    fetchPolicy: FetchPolicyResolve.NO_CACHE,
    variables: {
      sliderId: parseInt(props?.sliderId) ?? 1,
    },
  });

  return {
    state: {
      bannerConfig: data?.getBannerHomepage,
    },
  };
}, 'withHomeBannerData');
