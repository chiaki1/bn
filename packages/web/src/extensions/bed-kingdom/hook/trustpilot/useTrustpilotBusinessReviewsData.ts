import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import {
  useGetTrustpilotBusinessReviewsLazyQuery,
  useGetTrustpilotBusinessReviewsQuery,
} from '@vjcspy/apollo-bed-kingdom';
import { useEffect } from 'react';

export const useTrustpilotBusinessReviewsData = () => {
  const [getTrustpilotBusinessReviewsQuery, getTrustpilotBusinessReviewsRes] =
    useGetTrustpilotBusinessReviewsLazyQuery({
      fetchPolicy: FetchPolicyResolve.withLifetime(
        'useGetTrustpilotBusinessReviewsQuery'
      ),
      variables: {},
    });

  useEffect(() => {
    getTrustpilotBusinessReviewsQuery();
  }, []);

  useEffect(() => {
    if (getTrustpilotBusinessReviewsRes.error) {
      console.error('Could not load Trustpilot Business Reviews` data');
    }
  }, [getTrustpilotBusinessReviewsRes.error]);

  return {
    state: {
      businessReviewsData:
        getTrustpilotBusinessReviewsRes?.data?.getTrustpilotBusinessReviews,
    },
  };
};
