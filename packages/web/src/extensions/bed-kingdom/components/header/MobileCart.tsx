import withMegaMenuData from '@extensions/bed-kingdom/hoc/navigator/withMegaMenuData';
import { combineHOC } from '@vjcspy/web-ui-extension';
import React from 'react';

const MobileCart = combineHOC(withMegaMenuData)((props) => {
  return <></>;
});

export default MobileCart;
