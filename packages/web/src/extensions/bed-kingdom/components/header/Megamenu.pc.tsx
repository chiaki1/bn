import withMegaMenuData from '@extensions/bed-kingdom/hoc/navigator/withMegaMenuData';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { withRouter } from '@vjcspy/web-store/build/hoc/router/withRouterWithStoreActions';
import { combineHOC, UiExtension } from '@vjcspy/web-ui-extension';
import React, { useCallback } from 'react';

const MegamenuPc: React.FC = combineHOC(
  withMegaMenuData,
  withRouter
)((props) => {
  const goToCat = useCallback((cat: any) => {
    if (typeof cat?.url_path === 'string') {
      props?.actions?.go(`/${cat.url_path}.html`);
    }
  }, []);
  return (
    <>
      <div className="b-header-bottom">
        <div className="b-horizontal-menu">
          <ul className="b-main-nav">
            {props?.state?.megamenu?.map((item: any) => {
              return (
                <UiExtension
                  key={item!.id}
                  uiId="MEGA_MENU_ITEM"
                  goToCat={goToCat}
                  item={item}
                />
              );
            })}
          </ul>
        </div>
      </div>
    </>
  );
});

export default MegamenuPc;
