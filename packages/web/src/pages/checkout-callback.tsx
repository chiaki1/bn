import { bootstrap } from '@modules/bootstrap';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { UiExtension } from '@vjcspy/web-ui-extension';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

bootstrap();
const CheckoutCallback: NextPage = () => {
  const [resolved, setResolved] = useState(false);
  const router = useRouter();

  useEffect(() => {
    switch (router?.query?.return_type) {
      case 'checkout_success':
        const orderId = router.query?.order_id;
        if (orderId) {
          setResolved(true);
          RouterSingleton.push('/order-complete?order_number=' + orderId);
        }

        return;
    }
  }, [router.query]);

  return (
    <>
      {!resolved && (
        <UiExtension uiId="LOADING_INDICATOR" global={true} defaultMessage />
      )}
    </>
  );
};

export default CheckoutCallback;
