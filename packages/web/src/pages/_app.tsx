import '@assets/styles/main.scss';
import '@extensions/bed-kingdom/assets/styles/main.scss';
import 'animate.css/animate.min.css';
import 'react-toastify/dist/ReactToastify.min.css';

import { bootstrap } from '@modules/bootstrap';
import { isSSR } from '@vjcspy/chitility/build/util/isSSR';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { UiExtension } from '@vjcspy/web-ui-extension';
import type { AppProps } from 'next/app';
import Router, { useRouter } from 'next/router';
import NextNprogress from 'nextjs-progressbar';
import React, { useEffect, useMemo } from 'react';

console.log('build 230113');
bootstrap();

/*
 * Inject router global
 * */
function injectRouter(router: any) {
  RouterSingleton.registerRouter({
    push: async (url) => {
      Router.push(url, undefined, { shallow: true });
    },
    getPathname: () => {
      if (isSSR()) {
        {
          return router.pathname;
        }
      }
      return Router.pathname;
    },
    getQuery: () => {
      if (isSSR()) {
        {
          return router.query;
        }
      }
      return Router.query;
    },
    back: () => {
      Router.back();
    },
    forward: () => {},
    replace: async (url) => {
      Router.replace(url).then();
    },
    reload: () => {
      Router.reload();
    },
  });
}

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();

  useMemo(() => {
    injectRouter(router);
  }, [router]);

  useEffect(() => {}, []);

  return (
    <>
      <NextNprogress
        color="#29D"
        startPosition={0.3}
        stopDelayMs={200}
        height={3}
        showOnShallow={true}
      />
      {/*<UiExtension uiId="DEFAULT_SEO" />*/}
      <UiExtension uiId="DEFAULT_HEADER" />
      <Component {...pageProps} />
    </>
  );
}

// export function reportWebVitals(metric: NextWebVitalsMetric) {
//   console.log(metric);
// }

export default MyApp;
