import { bootstrap } from '@modules/bootstrap';
import { withWebAdapter } from '@modules/core/util/adapter';
import { UiExtension } from '@vjcspy/web-ui-extension';
import React from 'react';

bootstrap();
const TestSsr = () => {
  return <UiExtension uiId="TEST_PRODUCT_SSR" />;
};

export default withWebAdapter(TestSsr, {
  ssr: true,
});
