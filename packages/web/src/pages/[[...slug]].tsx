import { withWebRewriteAdapter } from '@modules/core/util/adapter';
import UrlRewrite from '@vjcspy/web-url-rewrite/build/components/url-rewrite';
import { NextPage } from 'next';

const RewriteRouterPage: NextPage = () => {
  return <UrlRewrite />;
};

export default withWebRewriteAdapter(RewriteRouterPage, {
  ssr: true,
});
