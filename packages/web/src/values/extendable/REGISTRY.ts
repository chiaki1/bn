import { DataValueExtension } from '@vjcspy/chitility/build/util/extension/data-value-extension';

export default DataValueExtension.resolve('REGISTRY', {
  PROXY_DEFAULT_URL_KEY: 'https://api.bluestone.systems',
  GRAPHQL_DEFAULT_URL_KEY: 'https://api.bluestone.systems/proxy/base/graphql',
  PCMS_DEFAULT_URL_KEY: 'https://base.bluestone.systems',
  CLIENT_ID_KEY: 'YXBwbGljYXRpb25faWQ6YXBwbGljYXRpb25fc2VjcmV0',
  PROXY_APP_NAME: 'base',

  DEFAULT_WEBSITE_IDS: [2],

  MGT_CE: true,
});
