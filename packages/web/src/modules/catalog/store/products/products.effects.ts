import { filtersToUrl } from '@modules/catalog/util/filtersToUrl';
import {
  createProductReviewAfterAction,
  createProductReviewErrorAction,
} from '@vjcspy/r/build/modules/catalog/store/product/product.actions';
import {
  productsAddFilter,
  productsClearFilters,
  productsRemoveFilter,
  productsToggleAggregationItem,
} from '@vjcspy/r/build/modules/catalog/store/products/products.actions';
import { ProductsState } from '@vjcspy/r/build/modules/catalog/store/products/products.state';
import { createEffect } from '@vjcspy/r/build/util/createEffect';
import { ofType } from '@vjcspy/r/build/util/ofType';
import { translate } from '@vjcspy/web-i18n';
import Router from 'next/router';
import { EMPTY } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { map, mapTo, switchMap, withLatestFrom } from 'rxjs/operators';

const whenRemoveFilter$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(
      productsRemoveFilter,
      productsAddFilter,
      productsClearFilters,
      productsToggleAggregationItem
    ),
    withLatestFrom(state$, (v1, v2: { products: ProductsState }) => [
      v1,
      v2.products.filters,
    ]),
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    switchMap(([_, filters]) => {
      // @ts-ignore
      const urlData = filtersToUrl(filters);

      return fromPromise(
        Router.push(
          {
            pathname: `/${urlData.pathname}`,
            query: urlData.query,
          },
          undefined,
          {
            shallow: true,
          }
        )
      ).pipe(mapTo(EMPTY));
    })
  )
);

const setReviewError$ = createEffect((action$) =>
  action$.pipe(
    ofType(createProductReviewErrorAction),
    map(() => {
      const errorMessage = translate('create_review_error', {
        ns: ['catalog'],
      });
      // _errorToast(errorMessage);
      return EMPTY;
    })
  )
);

const setReviewSuccess$ = createEffect((action$) =>
  action$.pipe(
    ofType(createProductReviewAfterAction),
    map(() => {
      const message = translate('create_review_success', {
        ns: ['catalog'],
      });
      // _successToast(message);
      return EMPTY;
    })
  )
);

export const WEB_CATALOG_PRODUCTS_EFFECTS = [
  whenRemoveFilter$,
  setReviewError$,
  setReviewSuccess$,
];
