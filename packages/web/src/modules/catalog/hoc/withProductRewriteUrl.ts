import { useProductRewriteUrl } from '@modules/catalog/hook/category/useProductRewriteUrl';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export const withProductRewriteUrl = createUiHOC((props) => {
  return useProductRewriteUrl(props);
}, 'withProductRewriteUrl');
