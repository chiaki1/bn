import { selectSearchString } from '@vjcspy/r/build/modules/catalog/store/products/products.selectors';
import { createUiHOC } from '@vjcspy/web-ui-extension';
import { useSelector } from 'react-redux';

export const withStringSearchData = createUiHOC(() => {
  const searchString = useSelector(selectSearchString);
  return {
    state: { searchString },
  };
}, 'withStringSearchData');
