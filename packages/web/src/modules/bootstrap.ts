import { bootBedKingdom } from '@extensions/bed-kingdom/bootstrap';
import { bootAccount } from '@modules/account/boot';
import { bootCatalog } from '@modules/catalog/boot';
import { bootCheckout } from '@modules/checkout/boot';
import { bootCore } from '@modules/core/boot';
import { bootUiModule } from '@modules/ui/boot';
import REGISTRY from '@values/extendable/REGISTRY';
import { Registry } from '@vjcspy/chitility';
import { BrowserPersistence } from '@vjcspy/chitility/build/util/browser-persistence';
import { isSSR } from '@vjcspy/chitility/build/util/isSSR';
import { boostrapR } from '@vjcspy/r';
import { R_DEFAULT_VALUE } from '@vjcspy/r/build/values/R_DEFAULT_VALUE';
import * as localForage from 'localforage';
import { forEach } from 'lodash';

let browserPersistence: any = new BrowserPersistence();

if (!isSSR() && typeof localForage !== 'undefined') {
  browserPersistence = localForage.createInstance({
    // ios lỗi indexDB nên sử dụng thêm WEBSQL
    driver: [localForage.INDEXEDDB, localForage.WEBSQL],
    name: 'ASYNC_STORAGE_INSTANCE',
    version: 1.0,
    storeName: 'ASYNC_STORAGE_INSTANCE', // Should be alphanumeric, with underscores.
    description: '',
  });
}

Registry.getInstance().register(
  R_DEFAULT_VALUE.ASYNC_STORAGE_INSTANCE_KEY,
  () => browserPersistence
);

export function bootstrap() {
  boostrapR();

  // boot modules
  bootCore();
  bootUiModule();

  bootAccount();
  bootCatalog();
  bootCheckout();

  // boot extensions
  bootBedKingdom();

  // INIT CONFIG
  forEach(REGISTRY.getData(), (v, k) => Registry.getInstance().register(k, v));
}
