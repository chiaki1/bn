import { TEST_CPT_CFG } from '@modules/core/components';
import { UiManager } from '@vjcspy/web-ui-extension';
import { URL_REWRITE_CPT } from '@vjcspy/web-url-rewrite/build/components';

UiManager.config({
  extensionConfigs: [...TEST_CPT_CFG, ...URL_REWRITE_CPT],
  uiHOCs: [],
});

export function bootCore() {}
