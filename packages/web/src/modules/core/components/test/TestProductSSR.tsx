import { useProductDetailBySkuQuery } from '@vjcspy/apollo/build/graphql/generated/_generated-hooks';
import { withPriceFormat } from '@vjcspy/r/build/modules/catalog/hoc/product/withPriceFormat';
import { combineHOC } from '@vjcspy/web-ui-extension';
import React from 'react';

export default combineHOC(withPriceFormat)(() => {
  console.log('TestProductSSRComponent');
  const { data } = useProductDetailBySkuQuery({
    ssr: true,
    variables: {
      sku: '2744LISU',
    },
  });

  return (
    <div>
      <pre>
        {JSON.stringify(
          data?.products?.items?.find(() => true),
          undefined,
          4
        )}
      </pre>
    </div>
  );
});
