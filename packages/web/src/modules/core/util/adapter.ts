import { withBedRichSnippet } from '@extensions/bed-kingdom/drivers';
import { withWebAccount } from '@modules/account/drivers';
import { withWebCatalog } from '@modules/catalog/drivers';
import { withWebCheckout } from '@modules/checkout/drivers';
import { withUi } from '@modules/ui/driver/ui';
import {
  WebUiAdapterOptions,
  WebUiPageDefaultProps,
} from '@vjcspy/chitility/build/types/drivers';
import { isSSR } from '@vjcspy/chitility/build/util/isSSR';
import { withAdapter } from '@vjcspy/chitility/build/util/withAdapters';
import { withApollo } from '@vjcspy/web-apollo';
import { withDomain } from '@vjcspy/web-domain/build/drivers/domain';
import { withI18n } from '@vjcspy/web-i18n';
import { withRedux } from '@vjcspy/web-r';
import { withStore } from '@vjcspy/web-store/build/drivers/store';
import { withUrlRewrite } from '@vjcspy/web-url-rewrite/build/drivers/url-rewrite';
import { NextPage } from 'next';

const ADAPTERS: any[] = [
  withDomain,
  withStore,
  withRedux,
  withI18n,
  withApollo,
  withUi,

  withWebAccount,
  withWebCatalog,
  withWebCheckout,
];

export const withWebAdapter = (
  PageComponent: NextPage<any>,
  webUiAdapterOptions?: WebUiAdapterOptions
): NextPage<WebUiPageDefaultProps> => {
  webUiAdapterOptions = { ssr: false, ...webUiAdapterOptions };
  return withAdapter(PageComponent, webUiAdapterOptions, ADAPTERS);
};

export const withWebRewriteAdapter = (
  PageComponent: NextPage<any>,
  webUiAdapterOptions?: WebUiAdapterOptions
): NextPage<WebUiPageDefaultProps> => {
  webUiAdapterOptions = { ssr: false, ...webUiAdapterOptions };
  if (isSSR()) {
    return withAdapter(PageComponent, webUiAdapterOptions, [
      ...ADAPTERS,
      withUrlRewrite,
      // withBedRichSnippet,
    ]);
  }
  return withAdapter(PageComponent, webUiAdapterOptions, [
    ...ADAPTERS,
    withUrlRewrite,
  ]);
};
