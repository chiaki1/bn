import { useGetCmsPageDetailByUserQuery } from '@vjcspy/apollo';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { useDomainContext } from '@vjcspy/r/build/modules/domain/context/domain';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

export const usePCMSPage = (urlKey: string) => {
  const domainContext = useDomainContext();
  const { t } = useTranslation('common');

  const [page, setPageData] = useState<any>({
    content: `
    <div style="margin-top: 15px;font-size: 13px;margin-bottom: 5px">
    ${t('not_yet_config_pcms_page')} 
    <strong style="color: darkred">${urlKey}</strong>
    </div>
    `,
  });

  const pcmsPageQuery = useGetCmsPageDetailByUserQuery({
    variables: {
      urlKey: urlKey,
      userId: domainContext.domainData.shopOwnerId,
    },
  });

  useEffect(() => {
    if (pcmsPageQuery.error) {
      logger.warn('Fetch PCMS Page error', pcmsPageQuery.error);
    }
    if (pcmsPageQuery.data?.cmsPageByUser) {
      setPageData(pcmsPageQuery.data?.cmsPageByUser);
    }
  }, [pcmsPageQuery.error, pcmsPageQuery.data?.cmsPageByUser]);

  return {
    page,
  };
};
