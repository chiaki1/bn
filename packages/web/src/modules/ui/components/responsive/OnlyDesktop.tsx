import { useResponsive } from '@modules/ui/hook/useResponsive';
import { isSSR } from '@vjcspy/chitility/build/util/isSSR';
import { combineHOC } from '@vjcspy/web-ui-extension';
import React from 'react';

const OnlyDesktop: React.FC = combineHOC()(
  React.memo((props) => {
    const { isDesktop: isDesktopOrLaptop } = useResponsive();
    return <>{(isSSR() || isDesktopOrLaptop) && props?.children}</>;
  })
);

export default OnlyDesktop;
