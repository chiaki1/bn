import { ExtensionConfig } from '@vjcspy/web-ui-extension';
import dynamic from 'next/dynamic';

export const IMAGE_EXT_CFG: ExtensionConfig[] = [
  {
    uiId: 'IMAGE',
    component: dynamic(() => import('./DefaultImage')),
  },
];
