import Image from 'next/image';
import React, { useCallback, useState } from 'react';

const DefaultImage: React.FC<{
  src: any;
  alt?: any;
  width?: number;
  height?: number;
}> = React.memo((props) => {
  const [width, setWidth] = useState(props?.width);
  const [height, setHeight] = useState(props?.height);

  const reCalculateSizeBaseOnOrigin = useCallback(
    (ratio: number) => {
      if (
        (typeof props?.width === 'undefined' ||
          typeof props?.height === 'undefined') &&
        ratio > 0
      ) {
        if (typeof props?.width === 'undefined' && props?.height) {
          setWidth(parseInt(props.height * ratio + ''));
        }
        if (typeof props?.height === 'undefined' && props?.width) {
          setHeight(parseInt(props.width / ratio + ''));
        }
      }
    },
    [props?.width, props.height]
  );

  return (
    <Image
      src={props.src}
      alt={props?.alt ?? ''}
      width={width}
      height={height}
      layout={
        typeof props?.width === 'undefined' ||
        typeof props?.height === 'undefined'
          ? 'fill'
          : undefined
      }
      // placeholder={
      //   typeof props?.src === 'string' && props.src.indexOf('base64') > -1
      //     ? undefined
      //     : 'blur'
      // }
      onLoadingComplete={(result) =>
        reCalculateSizeBaseOnOrigin(result.naturalWidth / result.naturalHeight)
      }
    />
  );
});

export default DefaultImage;
