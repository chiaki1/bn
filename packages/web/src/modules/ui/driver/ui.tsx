import { useWebUiConfig } from '@modules/ui/hook/config/useWebUiConfig';
import {
  UiContextProvider,
  UiContextValue,
} from '@vjcspy/chitility/build/context/ui';
import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { NextPage } from 'next';
import * as React from 'react';
import { useMemo, useState } from 'react';

export const withUi = (
  PageComponent: any,
  webUiAdapterOptions?: WebUiAdapterOptions
): NextPage<any> => {
  const WithUi: NextPage<any> = React.memo((props) => {
    logger.render('WithUi');
    const [uiContextValue, setUiContextValue] = useState<UiContextValue>({
      themeName: 'default',
      uiConfig: {},
      ...props?.webUi,
    });
    const {
      state: { webUiConfig },
    } = useWebUiConfig();
    const value: UiContextValue = useMemo(() => {
      return {
        ...uiContextValue,
        uiConfig: webUiConfig,
        setValue: setUiContextValue,
      };
    }, [webUiConfig]);

    return (
      <UiContextProvider value={value}>
        <PageComponent {...props} />
      </UiContextProvider>
    );
  });

  wrapSSRFn(
    PageComponent,
    WithUi,
    undefined,
    undefined,
    webUiAdapterOptions?.ssr
  );

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithUi.displayName = `withUi(${displayName})`;

  return WithUi;
};
