import { useCmsBlockData } from '@modules/ui/hook/cms-block/useCmsBlockData';
import { createUiHOC } from '@vjcspy/web-ui-extension';

export default createUiHOC((props) => {
  return useCmsBlockData(props);
}, 'withCmsBlockData');
