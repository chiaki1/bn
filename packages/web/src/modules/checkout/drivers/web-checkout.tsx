import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { CHECKOUT_EFFECTS } from '@vjcspy/r/build/modules/checkout/store/checkout.effects';
import { checkoutReducer } from '@vjcspy/r/build/modules/checkout/store/checkout.reducer';
import { storeManager } from '@vjcspy/web-r';
import React from 'react';

import { WEB_CHECKOUT_EFFECTS } from '../store/effects';

let _initCheckout = false;
storeManager.mergeReducers({
  checkout: checkoutReducer,
});
function initCheckout() {
  if (_initCheckout) return;

  storeManager.addEpics('web-checkout', [
    ...CHECKOUT_EFFECTS,
    ...WEB_CHECKOUT_EFFECTS,
  ]);

  _initCheckout = true;
}

initCheckout();

export const withWebCheckout = (
  PageComponent: any,
  webUiAdapterOptions?: WebUiAdapterOptions
): any => {
  const WithWebCheckout = React.memo((props: any) => {
    logger.render('WithWebCheckout');

    return (
      <>
        <PageComponent {...props} />
      </>
    );
  });
  wrapSSRFn(
    PageComponent,
    WithWebCheckout,
    undefined,
    undefined,
    webUiAdapterOptions?.ssr
  );

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithWebCheckout.displayName = `withWebAccount(${displayName})`;

  return WithWebCheckout;
};
