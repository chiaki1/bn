import { WEB_ACCOUNT_EFFECTS } from '@modules/account/store/toast.effects';
import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { ACCOUNT_EFFECTS } from '@vjcspy/r/build/modules/account/store/account.effects';
import { accountReducer } from '@vjcspy/r/build/modules/account/store/account.reducer';
import { storeManager } from '@vjcspy/web-r';
import { NextPage } from 'next';
import React from 'react';

let _initAccount = false;
storeManager.mergeReducers({
  account: accountReducer,
});
function initAccount() {
  if (_initAccount) {
    return;
  }
  storeManager.addEpics('web-account', [
    ...ACCOUNT_EFFECTS,
    ...WEB_ACCOUNT_EFFECTS,
  ]);

  _initAccount = true;
}

initAccount();

export const withWebAccount = (
  PageComponent: any,
  webUiAdapterOptions?: WebUiAdapterOptions
): NextPage<any> => {
  const WithWebAccount = React.memo((props: any) => {
    logger.render('WithWebAccount');

    return (
      <>
        <PageComponent {...props} />
      </>
    );
  });

  wrapSSRFn(
    PageComponent,
    WithWebAccount,
    undefined,
    undefined,
    webUiAdapterOptions?.ssr
  );

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithWebAccount.displayName = `withWebAccount(${displayName})`;

  return WithWebAccount;
};
