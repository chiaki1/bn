import { red } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';

// Create a theme instance.
export const blackTheme = createTheme({
  palette: {
    primary: {
      main: 'rgba(0, 0, 0, 1)',
    },
    secondary: {
      main: '#181E3B',
      dark: '#151a34',
      light: '#1f274c',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});
