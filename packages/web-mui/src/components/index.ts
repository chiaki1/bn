import { ExtensionConfig } from '@vjcspy/web-ui-extension';

import DefaultBadge from './badge/DefaultBadge';
import ButtonWithState from './button/ButtonWithState';
// import GridListToggle from './grid-list-toggle/GridListToggle';
import DefaultInput from './input/DefaultInput';
import DefaultPassword from './input/DefaultPassword';
import BottomNav from './nav-bar/BottomNav';

export const MUI_CPT_CFG: ExtensionConfig[] = [
  {
    uiId: 'MATERIAL_BUTTON_WITH_STATE',
    component: ButtonWithState,
  },
  {
    uiId: 'MATERIAL_BADGE',
    component: DefaultBadge,
  },
  // {
  //   uiId: 'MATERIAL_GRID_LIST_TOGGLE',
  //   component: GridListToggle,
  // },
  // {
  //   uiId: 'MATERIAL_GRID_LIST_TOGGLE',
  //   component: GridListToggle,
  // },
  {
    uiId: 'MATERIAL_INPUT',
    component: DefaultInput,
  },
  {
    uiId: 'MATERIAL_PASSWORD',
    component: DefaultPassword,
  },
  {
    uiId: 'MATERIAL_BOTTOM_NAV_BAR',
    component: BottomNav,
  },
];
