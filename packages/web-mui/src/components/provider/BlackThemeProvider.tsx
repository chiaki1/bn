import { ThemeProvider } from '@mui/material';
import React from 'react';

import { blackTheme } from '../../util/theme/blackTheme';

const BlackThemeProvider: React.FC = (props: any) => {
  return <ThemeProvider theme={blackTheme}>{props.children}</ThemeProvider>;
};

export default BlackThemeProvider;
