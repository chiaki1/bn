import { ThemeProvider } from '@mui/material';
import React from 'react';

import { coedoTheme } from '../../util/theme/coedoTheme';

const CoedoThemeProvider: React.FC = (props: any) => {
  return <ThemeProvider theme={coedoTheme}>{props.children}</ThemeProvider>;
};

export default CoedoThemeProvider;
