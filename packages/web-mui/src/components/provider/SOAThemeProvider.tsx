import { ThemeProvider } from '@mui/material';
import React from 'react';

import { soaTheme } from '../../util/theme/soaTheme';

const SOAThemeProvider: React.FC = (props: any) => {
  return <ThemeProvider theme={soaTheme}>{props.children}</ThemeProvider>;
};

export default SOAThemeProvider;
