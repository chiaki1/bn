import { Badge } from '@mui/material';
import React from 'react';

const DefaultBadge: React.FC<{
  color?: any;
  badgeContent?: any;
  invisible: boolean;
  icon?: any;
  isLoading?: boolean;
}> = (props) => {
  return (
    <Badge
      badgeContent={props.badgeContent ?? ''}
      color={props.color ?? 'error'}
      invisible={props.invisible ?? true}
    >
      <span className="cart-icon">
        {props.isLoading && (
          <div className="lds-spinner">
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
          </div>
        )}
        {props.icon}
      </span>
    </Badge>
  );
};

export default DefaultBadge;
