import { Checkbox, FormControlLabel } from '@mui/material';
import { generateRandomString } from '@vjcspy/r/build/util/randomId';
import React, { useCallback, useState } from 'react';
import { Controller } from 'react-hook-form';

const DefaultCheckbox: React.FC<{
  control: any;
  label?: string;
  name?: string;
  rows?: number;
  value?: any;
}> = React.memo((props) => {
  const [checked, setChecked] = useState(props.value);

  const InputRender = useCallback(
    // @ts-ignore
    ({ field }) => {
      const onChange = (event: any) => {
        setChecked(event.target.checked);
        field.onChange(event);
      };
      return (
        <FormControlLabel
          control={
            <Checkbox color="primary" checked={checked} onChange={onChange} />
          }
          label={props.label}
        />
      );
    },
    [checked]
  );

  return (
    <>
      {props.control ? (
        <Controller
          name={props.name ?? generateRandomString()}
          control={props.control}
          render={InputRender}
          defaultValue={props.value}
        />
      ) : (
        // @ts-ignore
        InputRender({})
      )}
    </>
  );
});

export default DefaultCheckbox;
