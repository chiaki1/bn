import GridViewIcon from '@mui/icons-material/GridView';
import ViewListIcon from '@mui/icons-material/ViewList';
import React from 'react';

const GridListToggle: React.FC<{
  isGrid: boolean;
}> = (props) => {
  return <>{props?.isGrid === true ? <GridViewIcon /> : <ViewListIcon />}</>;
};

export default GridListToggle;
