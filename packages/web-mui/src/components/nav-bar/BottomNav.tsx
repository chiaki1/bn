import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import React, { useCallback } from 'react';

import { BottomNavActionData } from '../../types/ui';

const BottomNav: React.FC<{
  nav: BottomNavActionData[];
  index: string;
  handleChange: (event: any, newValue: any) => void;
}> = (props) => {
  const isActive = useCallback(
    // @ts-ignore
    (value, activeValues?: any[]) => {
      if (Array.isArray(activeValues)) {
        return activeValues.includes(props.index);
      } else {
        return props.index === value;
      }
    },
    [props.index]
  );
  return (
    <BottomNavigation
      showLabels
      sx={{
        height: 55,
        '& .MuiBottomNavigationAction-label': {
          paddingTop: '5px',
          fontSize: 12,
        },
        '& .Mui-selected': {
          fontSize: '0.82rem !important',
          fontWeight: 400,
        },
      }}
      value={props.index}
      onChange={props.handleChange}
    >
      {props.nav.map((n) => (
        <BottomNavigationAction
          key={n.value}
          label={n.label}
          value={n.value}
          icon={isActive(n.value, n.activeValues) ? n.iconActive : n.icon}
        />
      ))}
    </BottomNavigation>
  );
};

export default BottomNav;
