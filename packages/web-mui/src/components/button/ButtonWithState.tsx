import DoneIcon from '@mui/icons-material/Done';
import TrendingFlatIcon from '@mui/icons-material/TrendingFlat';
import LoadingButton from '@mui/lab/LoadingButton';
import { styled } from '@mui/material';
import React, { useCallback, useMemo } from 'react';

const CustomButton = styled(LoadingButton)(({ theme }) => ({
  width: '100%',
  '&:disabled': {
    color: '#fff',
    backgroundColor: theme.palette.primary.main,
  },
  '& .MuiButton-endIcon': {
    display: 'flex',
    right: 14,
    position: 'absolute',
    visibility: 'visible',
  },
  '&.MuiButton-sizeLarge': {
    height: 45,
    fontSize: '1rem',
  },
  '&.MuiButton-sizeLarge svg': {
    fontSize: 25,
  },
}));

const ButtonWithState: React.FC<{
  variant?: 'text' | 'outlined' | 'contained';
  label: string;
  isLoading?: boolean;
  isDone?: boolean;
  isSuccess?: boolean;
  color?:
    | 'inherit'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'error'
    | 'info'
    | 'warning';
  onClick?: () => void;
  size?: 'small' | 'medium' | 'large';
  disabled?: boolean;
  type?: any;
  loadingText?: string;
}> = (props) => {
  const IconState = useMemo(() => {
    // if (props.isLoading) {
    //   // return <DataUsageIcon className={classes.iconLoading} />;
    //   return (
    //     <CircularProgress
    //       sx={{
    //         width: '20px !important',
    //         height: '20px !important',
    //         color: 'white',
    //       }}
    //     />
    //   );
    // }

    if (props.isDone || props.isSuccess) {
      return <DoneIcon className="animate__animated animate__heartBeat" />;
    }

    return <TrendingFlatIcon />;
  }, [props.isLoading]);

  const onSubmit = useCallback(() => {
    if (typeof props.onClick === 'function') {
      props.onClick();
    }
  }, [props.onClick]);

  return (
    <CustomButton
      type={props?.type ?? 'button'}
      variant={props.variant ?? 'contained'}
      color={props.color ?? 'primary'}
      loadingPosition="end"
      endIcon={IconState}
      size={props.size ?? 'medium'}
      onClick={() => onSubmit()}
      loading={props.isLoading}
      disabled={props.isLoading || props.disabled}
    >
      {props.isLoading
        ? props.loadingText
          ? `${props.loadingText}...`
          : props.label
        : props.label}
    </CustomButton>
  );
};

export default ButtonWithState;
