import { Autocomplete, TextField } from '@mui/material';
import { generateRandomString } from '@vjcspy/r/build/util/randomId';
import find from 'lodash/find';
import React from 'react';
import { Controller } from 'react-hook-form';

const DefaultAutoComplete: React.FC<{
  control?: any;
  label?: string;
  name?: string;
  options: any[];
  isReadonly?: boolean;
  rules?: any;
  error?: any;
  helperText?: any;
}> = React.memo((props) => {
  return (
    <Controller
      name={props.name ?? generateRandomString()}
      control={props.control}
      rules={props.rules}
      render={({ field: { onChange, value, ...p1 } }) => {
        return (
          <>
            {/*makesure always controlled component*/}
            {typeof value !== 'undefined' && (
              <Autocomplete
                sx={{
                  width: '100%',
                }}
                options={props.options ?? []}
                getOptionLabel={(option: any) => option}
                disabled={props.isReadonly === true}
                value={value}
                onChange={(e, data) => onChange(data)}
                isOptionEqualToValue={(option, value) => {
                  const isExisted = find(props.options, (o) => o == value);
                  if (isExisted) {
                    return option == value;
                  }
                  return true;
                }}
                renderInput={(params: any) => (
                  <TextField
                    {...params}
                    error={!!props.error}
                    helperText={props.error ? props.helperText : ''}
                    label={props.label}
                    variant="outlined"
                    name={props.name ?? generateRandomString()}
                  />
                )}
                // renderOption={(props, option, { inputValue }) => {
                //   const matches = match(option, inputValue);
                //   const parts = parse(option, matches);
                //   return (
                //     <div key={option}>
                //       {parts.map((part: any, index: any) => (
                //         <span
                //           key={index}
                //           style={{ fontWeight: part.highlight ? 700 : 400 }}
                //         >
                //           {part.text}
                //         </span>
                //       ))}
                //     </div>
                //   );
                // }}
                {...p1}
              />
            )}
          </>
        );
      }}
    />
  );
});
export default DefaultAutoComplete;
