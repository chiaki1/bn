import * as React from 'react';

export interface BottomNavActionData {
  label?: string;
  icon?: React.ReactNode;
  iconActive?: React.ReactNode;
  value: string;
  activeValues?: any[];
}
