import { Registry } from '@vjcspy/chitility';
import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { absoluteUrl } from '@vjcspy/chitility/build/util/absoluteUrl';
import { isSSR } from '@vjcspy/chitility/build/util/isSSR';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { wrapSSRFn } from '@vjcspy/chitility/build/util/wrapSSRFn';
import { DomainContextProvider } from '@vjcspy/r/build/modules/domain/context/domain';
import { DomainPersistent } from '@vjcspy/r/build/modules/domain/util/domain-persistent';
import { NextPage } from 'next';
import * as React from 'react';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { DomainManager } from '../util/domain-manager';
import { WEB_DOMAIN_KEY } from '../values/WEB_DOMAIN_KEY';

export const withDomain = (
  PageComponent: NextPage<any>,
  webUiAdapterOptions?: WebUiAdapterOptions
): NextPage<any> => {
  const WithDomain: NextPage<any> = React.memo((props) => {
    logger.render('WithDomain');
    const [domainData, setDomainData] = useState<any>(
      Registry.getInstance().registry(WEB_DOMAIN_KEY.DOMAIN_DATA) ??
        props.domainData ??
        false
    );

    const _resolveDomainDataInCsr = useCallback(async () => {
      if (!domainData) {
        // cache first
        const _cachedDomainData = await DomainPersistent.getItem(
          WEB_DOMAIN_KEY.DOMAIN_DATA
        );
        if (_cachedDomainData) {
          setDomainData(_cachedDomainData);
        } else {
          DomainManager.getInstance()
            .resolveDomainData(absoluteUrl().host)
            .then(async (d) => {
              await DomainPersistent.saveItem(WEB_DOMAIN_KEY.DOMAIN_DATA, d);
              setDomainData(d);
            });
        }
      }
    }, [domainData]);

    useEffect(() => {
      _resolveDomainDataInCsr().then().catch();
    }, []);

    const domainContextValue = useMemo(
      () => ({ domainData, setDomainData }),
      [domainData]
    );

    return (
      domainData && (
        <DomainContextProvider value={domainContextValue}>
          <PageComponent {...props} />
        </DomainContextProvider>
      )
    );
  });

  const displayName =
    PageComponent.displayName || PageComponent.name || 'PageComponent';
  WithDomain.displayName = `withDomain(${displayName})`;

  const getDomainDataSSrFn = async (ctx: any) => {
    if (!isSSR()) return {};

    console.info('ssr: withDomain');
    const { host } = absoluteUrl(ctx.req);
    const domainData = await DomainManager.getInstance().resolveDomainData(
      host
    );
    Registry.getInstance().register(WEB_DOMAIN_KEY.DOMAIN_DATA, domainData);

    return {
      domainData,
    };
  };

  wrapSSRFn(
    PageComponent,
    WithDomain,
    getDomainDataSSrFn,
    undefined,
    webUiAdapterOptions?.ssr
  );

  return WithDomain;
};
