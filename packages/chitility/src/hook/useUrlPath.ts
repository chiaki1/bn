import Router from 'next/router';

import { isSSR } from '../util/isSSR';

export const useUrlPath = () => {
  if (isSSR()) {
    return { pathname: null };
  } else {
    // pathname
    let pathname = '';
    const routerQuery = Router.query;
    if (routerQuery.hasOwnProperty('slug') && Array.isArray(routerQuery.slug)) {
      pathname = routerQuery.slug.join('/');
    } else {
      console.error('only support on `slug` name page and products page');
    }

    return { pathname };
  }
};
