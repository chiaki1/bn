/**
 * Adapter options
 */
export type WebUiAdapterOptions = {
  ssr?: boolean;
  apollo?: { apiBase: string };
};

/**
 * Props mặc định được pass từ server
 */
export type WebUiPageDefaultProps = {
  webUiContext?: {
    pathname: string;
    generatedTime: string;
  };

  apollo?: {
    initialData: any;
    ssrComplete: boolean;
  };
};
