export function objectGetPath(obj: any, path: string[]) {
  return path.reduce((previousValue, currentValue) => {
    return previousValue
      ? previousValue.hasOwnProperty(currentValue)
        ? previousValue[currentValue]
        : undefined
      : undefined;
  }, obj);
}
