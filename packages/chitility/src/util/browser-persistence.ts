/**
 * Persistence layer with expiration based on localStorage.
 */
import { isSSR } from './isSSR';

export class NamespacedLocalStorage {
  localStorage: any;
  key: string;

  constructor(localStorage: any, key: string) {
    this.localStorage = localStorage;
    this.key = key;
  }

  protected checkStorage() {
    if (!this.localStorage) {
      throw new Error("Browser doesn't support local storage");
    }
  }

  _makeKey(key: string) {
    return `${this.key}__${key}`;
  }

  getItem(name: string) {
    if (!this.localStorage) {
      return null;
    }
    return this.localStorage.getItem(this._makeKey(name));
  }

  setItem(name: string, value: any) {
    this.checkStorage();
    return this.localStorage.setItem(this._makeKey(name), value);
  }

  removeItem(name: string) {
    this.checkStorage();
    return this.localStorage.removeItem(this._makeKey(name));
  }
}

export class BrowserPersistence {
  private readonly storage: NamespacedLocalStorage;

  constructor(localStorage: any = undefined, key = 'BROWTILITY_STORAGE') {
    if (!isSSR()) {
      if (window?.localStorage) {
        localStorage = window.localStorage;
      } else {
        console.error("Browser doesn't support local storage");
      }
    }

    this.storage = new NamespacedLocalStorage(localStorage, key);
  }

  async getItem(name: string) {
    const now = Date.now();
    const item = this.storage.getItem(name);
    if (!item) {
      return undefined;
    }
    const { value, ttl, timeStored } = JSON.parse(item);
    if (ttl && now - timeStored > ttl * 1000) {
      this.storage.removeItem(name);
      return undefined;
    }
    return JSON.parse(value);
  }

  async setItem(name: string, value: any, ttl?: number) {
    const timeStored = Date.now();
    this.storage.setItem(
      name,
      JSON.stringify({
        value: JSON.stringify(value),
        timeStored,
        ttl,
      })
    );
  }

  async removeItem(name: string) {
    this.storage.removeItem(name);
  }
}
