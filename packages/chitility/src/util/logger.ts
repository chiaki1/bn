import { isSSR } from './isSSR';
import { loggerFn } from './log';

export const logger = loggerFn();
if (!isSSR()) {
  // @ts-ignore
  window['logger'] = logger;
}
