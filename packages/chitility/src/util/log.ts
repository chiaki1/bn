const _logger = console;
// @ts-ignore
export const loggerFn = (appName = 'default') => {
  const level = {
    debug: process.env.NEXT_PUBLIC_ENVIRONMENT === 'development',
    info: true,
    warn: true,
    error: true,
  };

  const group: any = {
    common: true,
    render: true,
    loaded: true,
  };

  const _log = {
    debug: (...agrs: any[]) => {
      if (!level.debug) {
        return;
      }
      // @ts-ignore
      _logger.log(...agrs);
    },
    info: (...agrs: any[]) => {
      if (!level.info) {
        return;
      }
      // @ts-ignore
      _logger.info(...agrs);
    },
    warn: (...agrs: any[]) => {
      // @ts-ignore
      _logger.warn(...agrs);
    },
    warning: (...agrs: any[]) => {
      // @ts-ignore
      _logger.warn(...agrs);
    },
    error: (...agrs: any[]) => {
      // @ts-ignore
      _logger.error(...agrs);
    },
    render: (...agrs: any[]) => {
      if (group.render) {
        _log.info(...agrs);
      }
    },
    loaded: (...agrs: any[]) => {
      if (group.loaded) {
        _log.info(...agrs);
      }
    },
    group: (
      groupName: string,
      level: 'info' | 'warn' | 'debug' | 'error',
      ...agrs: any[]
    ) => {
      if (group[groupName]) {
        switch (level) {
          case 'debug':
            _log.debug(...agrs);
            break;
          case 'info':
            _log.info(...agrs);
            break;
          case 'warn':
            _log.warn(...agrs);
            break;
          case 'error':
            _log.error(...agrs);
            break;
        }
      }
    },
    enableDebug: () => {
      level.debug = true;

      return _log;
    },
    enableInfo: () => {
      level.info = true;

      return _log;
    },
    enableCommonGroup: () => {
      group.common = true;

      return _log;
    },
    enableRenderGroup: () => {
      group.render = true;

      return _log;
    },
    enableLoadedGroup: () => {
      group.loaded = true;

      return _log;
    },
  };

  return _log;
};
