import reduce from 'lodash/reduce';
import reverse from 'lodash/reverse';
import { NextPage } from 'next';

import { WebUiAdapterOptions, WebUiPageDefaultProps } from '../types/drivers';
import { logger } from './logger';

export const withAdapter = (
  PageComponent: NextPage<any>,
  webUiAdapterOptions?: WebUiAdapterOptions,
  adapters: any[] = []
): NextPage<WebUiPageDefaultProps> => {
  if (!webUiAdapterOptions) {
    webUiAdapterOptions = {};
  }
  /*
   * Mặc định là dùng SPA/CSR
   * Tất cả các page phải chạy được trong 2 TH là SSR và Non-SSR
   * */
  if (typeof webUiAdapterOptions.ssr === 'undefined') {
    webUiAdapterOptions.ssr = false;
  }

  /*
   * Force disable SSR
   * */
  if (process.env.NEXT_PUBLIC_FORCE_DISABLE_SSR === 'true') {
    webUiAdapterOptions.ssr = false;
  }

  logger.info(webUiAdapterOptions.ssr ? 'SSR' : 'CSR');

  return reduce(
    reverse(adapters),
    (result, value) => {
      return value(result, webUiAdapterOptions);
    },
    PageComponent
  );
};
