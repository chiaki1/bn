import { useResolveChiakiPageQuery } from '@vjcspy/apollo';
import { FetchPolicyResolve } from '@vjcspy/apollo/build/util/fetch-policy-resolve';
import { DataObject } from '@vjcspy/chitility/build/util/extension/data-object';
import { ExtensionPoint } from '@vjcspy/chitility/build/util/extension/extension-point';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { useDomainContext } from '@vjcspy/r/build/modules/domain/context/domain';
import debounce from 'lodash/debounce';
import isEmpty from 'lodash/isEmpty';
import replace from 'lodash/replace';
import { useEffect, useRef, useState } from 'react';

import { resolveChiakiPageResolver } from '../util/resolveChiakiPageResolver';

export const resolveStaticLayout = (pathname?: string) => {
  const pathWithOutPrefix = replace(pathname ?? '', '.html', '');

  const objectLayout = new DataObject({
    pathname: pathWithOutPrefix,
    extConfig: undefined,
  });

  return ExtensionPoint.extend('resolveStaticLayout', objectLayout).getData(
    'extConfig'
  );
};

export const useResolveUnknownRouter = (fromServer: any, urlKey: string) => {
  const domainContextValue = useDomainContext();

  const { data, refetch } = useResolveChiakiPageQuery({
    fetchPolicy: FetchPolicyResolve.DEFAULT,
    nextFetchPolicy: FetchPolicyResolve.DEFAULT,
    variables: {
      // @ts-ignore
      urlKey,
      userId: domainContextValue.domainData.shopOwnerId,
    },
  });

  const [resolvedUrlData, setResolvedUrlData] = useState<any>(
    !!fromServer
      ? fromServer
      : !!resolveStaticLayout(urlKey)
      ? resolveStaticLayout(urlKey)
      : !!data
      ? resolveChiakiPageResolver({ data }, urlKey)
      : {
          isResolved: false,
        }
  );

  const doQueryDbRef = useRef(
    debounce((urlKey) => {
      // Khi thực hiện chuyển page trên client cũng cần phải resolve static trước
      const staticData = resolveStaticLayout(urlKey);
      if (staticData) {
        setResolvedUrlData(staticData);
      } else {
        refetch({
          urlKey: isEmpty(urlKey) ? 'index' : urlKey,
          userId: domainContextValue.domainData.shopOwnerId,
        });
      }
    }, 77)
  );

  useEffect(() => {
    if (data && urlKey !== resolvedUrlData?.requestedPathname) {
      logger.debug('>>> new uiConfig data');
      setResolvedUrlData(
        resolveChiakiPageResolver(
          {
            data,
          },
          urlKey
        )
      );
    }
  }, [data, urlKey, resolvedUrlData]);

  /**
   * query database
   * Nếu trong context value chưa có giá trị thì chứng tỏ là kiểu CSR
   * Lúc đó sẽ thức hiện lazy query
   * Ở đây đơn giản hoá điều kiện chỉ cần check xem pathname đã resolve trong context với urlKey hiện tại
   */
  useEffect(() => {
    if (urlKey !== resolvedUrlData?.requestedPathname) {
      setResolvedUrlData({
        isResolved: false,
      });
      doQueryDbRef.current(urlKey);
    }
  }, [urlKey]);

  return {
    urlRewriteData: resolvedUrlData,
    setUrlRewriteData: setResolvedUrlData,
  };
};
