import { logger } from '@vjcspy/chitility/build/util/logger';
import { useUrlRewriteContext } from '@vjcspy/r/build/modules/router/context/url-rewrite';
import { RouterSingleton } from '@vjcspy/r/build/modules/router/util/router-singleton';
import { UiExtension } from '@vjcspy/web-ui-extension';
import React, { useMemo } from 'react';

const UrlRewrite: React.FC = React.memo(() => {
  const urlRewriteContextValue = useUrlRewriteContext();

  const HTML = useMemo(() => {
    if (
      !urlRewriteContextValue.urlRewriteData ||
      !urlRewriteContextValue.urlRewriteData?.isResolved
    ) {
      return (
        <UiExtension
          uiId="LOADING_INDICATOR"
          global={true}
          defaultMessage={true}
        />
      );
    } else if (
      urlRewriteContextValue.urlRewriteData?.type &&
      urlRewriteContextValue.urlRewriteData?.config_data
    ) {
      return (
        <UiExtension
          extensionDataConfig={
            urlRewriteContextValue.urlRewriteData.config_data
          }
        />
      );
    } else {
      RouterSingleton.push('/notfound');
      return null;
      // return <div>Sorry. We could not resolve url</div>;
    }
  }, [urlRewriteContextValue.urlRewriteData?.pathname]);
  logger.debug('>>> render UrlRewrite');
  return <>{HTML}</>;
});

export default UrlRewrite;
