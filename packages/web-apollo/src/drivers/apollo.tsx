import { ApolloClient, ApolloProvider } from '@apollo/client';
import { Registry } from '@vjcspy/chitility';
import { WebUiAdapterOptions } from '@vjcspy/chitility/build/types/drivers';
import { isSSR } from '@vjcspy/chitility/build/util/isSSR';
import { logger } from '@vjcspy/chitility/build/util/logger';
import { R_DEFAULT_VALUE } from '@vjcspy/r/build/values/R_DEFAULT_VALUE';
import { UiExtension } from '@vjcspy/web-ui-extension';
import { NextPage } from 'next';
import React from 'react';

import { initApolloClient, useApolloClient } from '../hook/use-apollo-client';

/**
 *
 * @param PageComponent
 * @param adapterProps
 * @returns {React.ReactElement<any, any> | null}
 */
export const withApollo = (
  PageComponent: NextPage<any>,
  adapterProps?: WebUiAdapterOptions
) => {
  const {
    apollo: apollo = {
      apiBase: isSSR()
        ? Registry.getInstance().registry(
            R_DEFAULT_VALUE.GRAPHQL_BACKEND_URL_KEY
          ) ??
          Registry.getInstance().registry(
            R_DEFAULT_VALUE.GRAPHQL_DEFAULT_URL_KEY
          )
        : Registry.getInstance().registry(
            R_DEFAULT_VALUE.GRAPHQL_DEFAULT_URL_KEY
          ),
    },
    ssr = false,
  } = adapterProps ?? {};
  const apiBase = apollo.apiBase;

  const WithApollo: NextPage<any> = (props) => {
    logger.render('WithApollo');
    const { apollo, ...pageProps } = props;

    const { client, initPersistent } = useApolloClient({
      ...apollo,
      apiBase,
    });

    if (!initPersistent) {
      return <UiExtension uiId="LOADING_INDICATOR" global={true} />;
    }

    return (
      <>
        <ApolloProvider client={client}>
          {<PageComponent {...pageProps} />}
        </ApolloProvider>
      </>
    );
  };

  // Set the correct displayName in development
  if (process.env.NODE_ENV !== 'production') {
    const displayName =
      PageComponent.displayName || PageComponent.name || 'PageComponent';

    if (displayName === 'App') {
      console.warn('This withApollo HOC only works with PageComponents.');
    }

    WithApollo.displayName = `withApollo(${displayName})`;
  }

  if (ssr || PageComponent.getInitialProps) {
    WithApollo.getInitialProps = async (ctx) => {
      // @ts-ignore
      let client: ApolloClient<any> = ctx?.apolloClient;
      if (!client) {
        client = initApolloClient(apollo);
      }

      // Run wrapped getInitialProps methods
      let pageProps = {};
      if (PageComponent.getInitialProps) {
        pageProps = await PageComponent.getInitialProps(ctx);
      }

      let initialData: any;

      // Only on the server:
      if (isSSR()) {
        logger.info('ssr: Apollo');
        // When redirecting, the response is finished.
        // No point in continuing to render
        if (ctx.res && ctx.res.writableEnded) {
          console.warn('writableEnded');
          return pageProps;
        }

        // Only if ssr is enabled
        if (ssr) {
          try {
            // Run all GraphQL queries
            const { getDataFromTree } = await import('@apollo/react-ssr');
            logger.debug('>>> Apollo getDataFromTree');
            await getDataFromTree(
              <ctx.AppTree
                pageProps={{
                  ...pageProps,
                  apollo: { client },
                }}
              />
            );

            // Extract query data from the Apollo store
            initialData = client.cache.extract();
            logger.debug('>>> WithApollo: initialize data process done ! <<<');
          } catch (error) {
            // Prevent Apollo Client GraphQL errors from crashing SSR.
            // Handle them in components via the data.error prop:
            // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
            console.error(
              'WithApollo: Error while running `getDataFromTree`',
              error
            );
          }

          // getDataFromTree does not call componentWillUnmount
          // head side effect therefore need to be cleared manually
          // Check thì thấy tự render lại
          // Head.rewind();
        }
      }
      return {
        ...pageProps,
        apollo: { initialData, client },
        ssrComplete: true,
      };
    };
  }

  return WithApollo;
};
