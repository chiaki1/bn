/**
 * Apollo options for `Adapter options`
 */
export type WebUiApolloOptions = {
  apiBase: string;
  initialData?: any;
  client?: any;
};
