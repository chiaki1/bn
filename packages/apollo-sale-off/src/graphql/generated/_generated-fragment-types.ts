
      export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
      const result: IntrospectionResultData = {
  "__schema": {
    "types": [
      {
        "kind": "INTERFACE",
        "name": "AggregationOptionInterface",
        "possibleTypes": [
          {
            "name": "AggregationOption"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "CartAddressInterface",
        "possibleTypes": [
          {
            "name": "BillingCartAddress"
          },
          {
            "name": "ShippingCartAddress"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "CartItemInterface",
        "possibleTypes": [
          {
            "name": "BundleCartItem"
          },
          {
            "name": "ConfigurableCartItem"
          },
          {
            "name": "DownloadableCartItem"
          },
          {
            "name": "GiftCardCartItem"
          },
          {
            "name": "SimpleCartItem"
          },
          {
            "name": "VirtualCartItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "CategoryInterface",
        "possibleTypes": [
          {
            "name": "CategoryTree"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "CreditMemoItemInterface",
        "possibleTypes": [
          {
            "name": "BundleCreditMemoItem"
          },
          {
            "name": "CreditMemoItem"
          },
          {
            "name": "DownloadableCreditMemoItem"
          },
          {
            "name": "GiftCardCreditMemoItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "CustomizableOptionInterface",
        "possibleTypes": [
          {
            "name": "CustomizableAreaOption"
          },
          {
            "name": "CustomizableCheckboxOption"
          },
          {
            "name": "CustomizableDateOption"
          },
          {
            "name": "CustomizableDropDownOption"
          },
          {
            "name": "CustomizableFieldOption"
          },
          {
            "name": "CustomizableFileOption"
          },
          {
            "name": "CustomizableMultipleOption"
          },
          {
            "name": "CustomizableRadioOption"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "CustomizableProductInterface",
        "possibleTypes": [
          {
            "name": "BundleProduct"
          },
          {
            "name": "ConfigurableProduct"
          },
          {
            "name": "DownloadableProduct"
          },
          {
            "name": "GiftCardProduct"
          },
          {
            "name": "SimpleProduct"
          },
          {
            "name": "VirtualProduct"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "GiftRegistryDynamicAttributeInterface",
        "possibleTypes": [
          {
            "name": "GiftRegistryDynamicAttribute"
          },
          {
            "name": "GiftRegistryRegistrantDynamicAttribute"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "GiftRegistryDynamicAttributeMetadataInterface",
        "possibleTypes": [
          {
            "name": "GiftRegistryDynamicAttributeMetadata"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "GiftRegistryItemInterface",
        "possibleTypes": [
          {
            "name": "GiftRegistryItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "GiftRegistryItemUserErrorInterface",
        "possibleTypes": [
          {
            "name": "GiftRegistryItemUserErrors"
          },
          {
            "name": "MoveCartItemsToGiftRegistryOutput"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "GiftRegistryOutputInterface",
        "possibleTypes": [
          {
            "name": "GiftRegistryOutput"
          },
          {
            "name": "MoveCartItemsToGiftRegistryOutput"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "InvoiceItemInterface",
        "possibleTypes": [
          {
            "name": "BundleInvoiceItem"
          },
          {
            "name": "DownloadableInvoiceItem"
          },
          {
            "name": "GiftCardInvoiceItem"
          },
          {
            "name": "InvoiceItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "LayerFilterItemInterface",
        "possibleTypes": [
          {
            "name": "LayerFilterItem"
          },
          {
            "name": "SwatchLayerFilterItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "ListingImageInterface",
        "possibleTypes": [
          {
            "name": "ProductImageResize"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "MediaGalleryInterface",
        "possibleTypes": [
          {
            "name": "ProductImage"
          },
          {
            "name": "ProductVideo"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "OrderItemInterface",
        "possibleTypes": [
          {
            "name": "BundleOrderItem"
          },
          {
            "name": "DownloadableOrderItem"
          },
          {
            "name": "GiftCardOrderItem"
          },
          {
            "name": "OrderItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "PhysicalProductInterface",
        "possibleTypes": [
          {
            "name": "BundleProduct"
          },
          {
            "name": "ConfigurableProduct"
          },
          {
            "name": "GiftCardProduct"
          },
          {
            "name": "GroupedProduct"
          },
          {
            "name": "SimpleProduct"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "ProductInterface",
        "possibleTypes": [
          {
            "name": "BundleProduct"
          },
          {
            "name": "ConfigurableProduct"
          },
          {
            "name": "DownloadableProduct"
          },
          {
            "name": "GiftCardProduct"
          },
          {
            "name": "GroupedProduct"
          },
          {
            "name": "SimpleProduct"
          },
          {
            "name": "VirtualProduct"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "ProductLinksInterface",
        "possibleTypes": [
          {
            "name": "ProductLinks"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "RoutableInterface",
        "possibleTypes": [
          {
            "name": "BundleProduct"
          },
          {
            "name": "CategoryTree"
          },
          {
            "name": "CmsPage"
          },
          {
            "name": "ConfigurableProduct"
          },
          {
            "name": "DownloadableProduct"
          },
          {
            "name": "GiftCardProduct"
          },
          {
            "name": "GroupedProduct"
          },
          {
            "name": "SimpleProduct"
          },
          {
            "name": "VirtualProduct"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "ShipmentItemInterface",
        "possibleTypes": [
          {
            "name": "BundleShipmentItem"
          },
          {
            "name": "GiftCardShipmentItem"
          },
          {
            "name": "ShipmentItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "SwatchDataInterface",
        "possibleTypes": [
          {
            "name": "ColorSwatchData"
          },
          {
            "name": "ImageSwatchData"
          },
          {
            "name": "TextSwatchData"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "SwatchLayerFilterItemInterface",
        "possibleTypes": [
          {
            "name": "SwatchLayerFilterItem"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "WishlistItemInterface",
        "possibleTypes": [
          {
            "name": "BundleWishlistItem"
          },
          {
            "name": "ConfigurableWishlistItem"
          },
          {
            "name": "DownloadableWishlistItem"
          },
          {
            "name": "GiftCardWishlistItem"
          },
          {
            "name": "GroupedProductWishlistItem"
          },
          {
            "name": "SimpleWishlistItem"
          },
          {
            "name": "VirtualWishlistItem"
          }
        ]
      }
    ]
  }
};
      export default result;
    